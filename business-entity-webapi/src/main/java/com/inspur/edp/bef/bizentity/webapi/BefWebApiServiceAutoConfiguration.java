package com.inspur.edp.bef.bizentity.webapi;

import io.iec.edp.caf.databaseobject.api.service.IDatabaseObjectDtService;
import io.iec.edp.caf.rest.RESTEndpoint;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BefWebApiServiceAutoConfiguration {
    @Bean
    public RESTEndpoint getBefWebApiEndpoint(IDatabaseObjectDtService databaseService) {
        return new RESTEndpoint(
                "/dev/main/v1.0/bef",
                new BeCodeGenController(databaseService)
        );
    }
}

