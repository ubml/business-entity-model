package com.inspur.edp.bef.extendinfo.server.api;

import com.inspur.edp.bef.bizentity.gspbusinessentity.entity.GspBeExtendInfo;
import io.iec.edp.caf.rpc.api.annotation.GspServiceBundle;
import io.iec.edp.caf.rpc.api.annotation.RpcParam;

import java.util.List;

@GspServiceBundle(applicationName = "runtime", serviceUnitName = "Lcm", serviceName = "IGspBeExtendInfoRpcService")
public interface IGspBeExtendInfoRpcService {
    /**
     * 根据ID获取某条BE扩展信息
     *
     * @param id
     * @return
     */
    GspBeExtendInfo getBeExtendInfo(@RpcParam(paramName = "id") String id);

    /**
     * 根据configId获取某条BE扩展信息
     *
     * @param configId
     * @return
     */
    GspBeExtendInfo getBeExtendInfoByConfigId(@RpcParam(paramName = "configId") String configId);

    /**
     * 获取所有BE扩展信息
     *
     * @return
     */
    List<GspBeExtendInfo> getBeExtendInfos();

    /**
     * 保存
     *
     * @param infos
     */
    void saveGspBeExtendInfos(@RpcParam(paramName = "infos") List<GspBeExtendInfo> infos);

    /**
     * 删除
     *
     * @param id
     */
    void deleteBeExtendInfo(@RpcParam(paramName = "id") String id);
}
