package com.inspur.gsp.commonmodel.api.typeinfos;

public class CmAssemblyInfo {
    private String assemblyName = "";

    private String defaultNamespace = "";

    public CmAssemblyInfo(String assemblyName, String defaultNamespace) {
        this.assemblyName = assemblyName;
        this.defaultNamespace = defaultNamespace;
    }

    public String getAssemblyName() {
        return assemblyName;
    }

    public String getDefaultNamespace() {
        return defaultNamespace;
    }
}