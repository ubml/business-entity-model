package com.inspur.edp.bef.bemanager.gspbusinessentity.test;

import com.inspur.edp.bef.bemanager.gspbusinessentity.config.GspBeExtendInfoConfig;
import com.inspur.edp.bef.bizentity.gspbusinessentity.api.IGspBeExtendInfoService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@SpringBootTest(classes = {
        GspBeExtendInfoConfig.class
}, webEnvironment = RANDOM_PORT)
//@EnableAutoConfiguration
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class GspBeExtendInfoTest {

    @Autowired
    private IGspBeExtendInfoService beExtendInfoService;

    @Before
    public void before() throws Exception {
    }

    @After
    public void after() throws Exception {
    }

    @Test
    public void testsaveBizParameters() throws Exception {
//		List<GspBeExtendInfo> infos = beExtendInfoService.getBeExtendInfos();
//		Integer size = infos.size();
//		GspBeExtendInfo info = new GspBeExtendInfo();
//		info.setId(UUID.randomUUID().toString());
//		info.setCreatedOn(new Date());
//		info.setLastChangedOn(new Date());
//		info.setLastChangedBy("wjj");
//		info.setCreatedBy("wjj");
//		infos.add(info);
//		beExtendInfoService.saveGspBeExtendInfos(infos);
//		List<GspBeExtendInfo> infos2 = beExtendInfoService.getBeExtendInfos();
//		Integer size2 = infos2.size();
//		assert size + 1 == size2;
    }
}