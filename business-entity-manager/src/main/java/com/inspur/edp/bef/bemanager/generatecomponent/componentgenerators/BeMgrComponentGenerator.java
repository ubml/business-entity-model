package com.inspur.edp.bef.bemanager.generatecomponent.componentgenerators;


import com.inspur.edp.bef.bemanager.codegenerator.JavaCompCodeNames;
import com.inspur.edp.bef.bemanager.generatecomponent.ComponentClassNameGenerator;
import com.inspur.edp.bef.bemanager.util.CheckInfoUtil;
import com.inspur.edp.bef.bizentity.operation.BizMgrAction;
import com.inspur.edp.bef.bizentity.operation.BizOperation;
import com.inspur.edp.bef.bizentity.operation.componentbase.BizParameter;
import com.inspur.edp.bef.bizentity.operation.componentbase.BizVoidReturnType;
import com.inspur.edp.bef.bizentity.operation.componentinterface.IBizParameter;
import com.inspur.edp.bef.component.base.GspComponent;
import com.inspur.edp.bef.component.base.VoidReturnType;
import com.inspur.edp.bef.component.detailcmpentity.bemgr.BEMgrComponent;
import com.inspur.edp.bef.component.detailcmpentity.bemgr.BEMgrMethodParameter;
import com.inspur.edp.bef.component.enums.ParameterCollectionType;
import com.inspur.edp.bef.component.enums.ParameterMode;
import com.inspur.edp.bef.component.enums.ParameterType;

/**
 * 自定义动作构件生成器
 */
public class BeMgrComponentGenerator extends BaseComponentGenerator {
    public static BeMgrComponentGenerator getInstance() {
        return new BeMgrComponentGenerator();
    }

    private BeMgrComponentGenerator() {
    }

    @Override
    protected GspComponent buildComponent() {
        return new BEMgrComponent();
    }

    ///#region 将BizMgrAction中信息赋值给BEMgrComponent
    @Override
    protected void evaluateComponentInfo(GspComponent component, BizOperation bizMgrAction, GspComponent originalComponent) {
        //1、基本信息
        super.evaluateComponentInfo(component,bizMgrAction,originalComponent);
        //2、参数信息
        evaluateComponentParameterInfos((BEMgrComponent) component, (BizMgrAction) bizMgrAction);
        //3、返回值信息
        evaluateComponentReturnValueInfo((BEMgrComponent) component, (BizMgrAction) bizMgrAction);
    }

    @Override
    protected String getActionNameSpaceSuffix(BizOperation bizAction){
        return JavaCompCodeNames.MgrActionNameSpaceSuffix;
    }

    private void evaluateComponentParameterInfos(BEMgrComponent component, BizMgrAction bizMgrAction) {
        for (Object parameter : bizMgrAction.getParameters()) {
            evaluateComponentParameterInfo((IBizParameter) parameter, component);
        }
    }

    private void evaluateComponentParameterInfo(IBizParameter bizParameter, BEMgrComponent component) {
        BEMgrMethodParameter tempVar = new BEMgrMethodParameter();
        tempVar.setID(bizParameter.getID());
        tempVar.setParamCode(bizParameter.getParamCode());
        tempVar.setParamName(bizParameter.getParamName());
        tempVar.setParamDescription(bizParameter.getParamDescription());
        tempVar.setMode(ParameterMode.valueOf(bizParameter.getMode().name().toString()));
        tempVar.setParameterCollectionType(ParameterCollectionType.valueOf(bizParameter.getCollectionParameterType().name().toString()));
        tempVar.setParameterType(ParameterType.valueOf(bizParameter.getParameterType().name().toString()));
        tempVar.setAssembly(bizParameter.getAssembly());
        tempVar.setClassName(bizParameter.getClassName());
        tempVar.setDotnetClassName(((BizParameter) bizParameter).getNetClassName());
        BEMgrMethodParameter parameter = tempVar;

        component.getBeMgrMethod().getParameterCollection().add(parameter);
    }

    private void evaluateComponentReturnValueInfo(BEMgrComponent component, BizMgrAction bizMgrAction) {
        //类型
        if (bizMgrAction.getReturnValue() instanceof BizVoidReturnType) {
            component.getBeMgrMethod().setReturnValue(new VoidReturnType());
        } else {
            component.getBeMgrMethod().getReturnValue().setParameterCollectionType(ParameterCollectionType.valueOf(bizMgrAction.getReturnValue().getCollectionParameterType().name().toString()));
            component.getBeMgrMethod().getReturnValue().setParameterType(ParameterType.valueOf(bizMgrAction.getReturnValue().getParameterType().name().toString()));
            component.getBeMgrMethod().getReturnValue().setAssembly(bizMgrAction.getReturnValue().getAssembly());
            component.getBeMgrMethod().getReturnValue().setClassName(bizMgrAction.getReturnValue().getClassName());
            component.getBeMgrMethod().getReturnValue().setDotnetClassName(bizMgrAction.getReturnValue().getNetClassName());
        }
        //基本信息
        component.getBeMgrMethod().getReturnValue().setID(bizMgrAction.getReturnValue().getID());
        component.getBeMgrMethod().getReturnValue().setParamDescription(bizMgrAction.getReturnValue().getParamDescription());
    }

    ///#endregion
}