package com.inspur.edp.bef.bemanager.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.inspur.edp.bef.bemanager.ContentSerializer;
import com.inspur.edp.bef.bizentity.exceptions.BusinessEntityErrorCodeEnum;
import com.inspur.edp.bef.bizentity.exceptions.BusinessEntityException;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.common.Utils;
import com.inspur.edp.metadata.rtcustomization.spi.MetadataBeforeSaveToDBExtend;


public class SimplifyBeBeforeSavedToDB implements MetadataBeforeSaveToDBExtend {

    public String execute(GspMetadata gspMetadata) {
        ContentSerializer serializer = new ContentSerializer();
        JsonNode jsonNode = serializer.Serialize(gspMetadata.getContent());
        ObjectMapper mapper = Utils.getMapper();
        String mdValue;
        try {
            mdValue = mapper.writeValueAsString(jsonNode);
        } catch (JsonProcessingException e) {
            throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_JSON_0005, e, "Metadata", gspMetadata.getHeader().getId());
        }
        return mdValue;
    }

}
