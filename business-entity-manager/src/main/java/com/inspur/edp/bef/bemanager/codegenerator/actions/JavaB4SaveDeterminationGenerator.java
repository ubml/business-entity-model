package com.inspur.edp.bef.bemanager.codegenerator.actions;


import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.bef.bizentity.operation.BizOperation;

public class JavaB4SaveDeterminationGenerator extends JavaDeterminationGenerator {
    public JavaB4SaveDeterminationGenerator(GspBusinessEntity be, BizOperation operation, String nameSpace, String path) {
        super(be, operation, nameSpace, path);
    }

    @Override
    protected String getBaseClassName() {
        return "AbstractB4SaveDetermination";
    }
}