package com.inspur.edp.bef.bemanager.codegenerator.actions;


import com.inspur.edp.bef.bemanager.codegenerator.JavaCompCodeNames;
import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.bef.bizentity.operation.BizOperation;

public class JavaDeterminationGenerator extends JavaBaseCompCodeGenerator {
    @Override
    protected String getBaseClassName() {
        return "AbstractDetermination";
    }

    public JavaDeterminationGenerator(GspBusinessEntity be, BizOperation operation, String nameSpace, String path) {
        super(be, operation, nameSpace, path);

    }

    /**
     * C#->Java模板
     *
     * @return
     */
    @Override
    protected String GetNameSpaceSuffix() {
        return BizOperation.getOwner().getCode() + "." + JavaCompCodeNames.DeterminationNameSpaceSuffix; // 获取 表名.类名
    }

    @Override
    protected void JavaGenerateExtendUsing(StringBuilder result) {
        //jar包对应的字符串
        result.append(GetImportStr(JavaCompCodeNames.ActionApiNameSpace)).append(GetImportStr(JavaCompCodeNames.DaterminationNameSpace)).
                append(GetImportStr(JavaCompCodeNames.ChangesetNameSpace)).append(GetImportStr(JavaCompCodeNames.AbstractDeterminationNameSpace)).
                append(GetImportStr(JavaCompCodeNames.IDeterminationContextNameSpace)).append(GetImportStr(JavaCompCodeNames.IEntityDataNameSpace)).
                append(GetImportStr(JavaCompCodeNames.IBeforeRetrieveDtmContextContextNameSpace));
    }

    @Override
    protected void JavaGenerateConstructor(StringBuilder result) {
        result.append(GetIndentationStr()).append(JavaCompCodeNames.KeywordPublic).append(" ").append(GetCompName()).append("(IDeterminationContext context, IChangeDetail change)").append(" ").append("{").append(getNewline());
        result.append(GetIndentationStr()).append(GetIndentationStr()).append("super(context,change)").append(";").append(getNewline()); //添加基类构造函数
        result.append(GetIndentationStr()).append("}");
    }

    @Override
    protected void JavaGenerateExtendMethod(StringBuilder result) {
        if (this.isInterpretation) {
            JavaInterpretationGenerateDtmExtendMethod(result);
        } else {
            JavaGenerateDtmExtendMethod(result);
        }
    }

    protected void JavaGenerateDtmExtendMethod(StringBuilder result) {
        result.append(GetIndentationStr()).append(JavaCompCodeNames.KeywordPrivate).append(" ").append(EntityClassName).append(" ").append("getData()").append(" ").append("{").append(getNewline());

        result.append(GetIndentationStr()).append(GetIndentationStr()).append("return").append(" ").append("(").append(EntityClassName).append(")").append("super.getContext().getCurrentData()").append(";").append(getNewline());

        result.append(GetIndentationStr()).append("}").append(getNewline());
    }

    protected void JavaInterpretationGenerateDtmExtendMethod(StringBuilder result) {
        result.append(GetIndentationStr()).append(JavaCompCodeNames.KeywordPrivate).append(" ").append(JavaCompCodeNames.IEntityData).append(" ").append("getData()").append(" ").append("{").append(getNewline());

        result.append(GetIndentationStr()).append(GetIndentationStr()).append("return").append(" ").append("(").append(JavaCompCodeNames.IEntityData).append(")").append("super.getContext().getCurrentData()").append(";").append(getNewline());

        result.append(GetIndentationStr()).append("}");
    }


    @Override
    protected String GetInitializeCompName() {
        return String.format("%1$s%2$s%3$s", getChildCode(), BizOperation.getCode(), "Determination");
    }
}