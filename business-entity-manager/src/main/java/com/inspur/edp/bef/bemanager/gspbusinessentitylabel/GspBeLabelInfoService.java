package com.inspur.edp.bef.bemanager.gspbusinessentitylabel;

import com.inspur.edp.bef.bemanager.gspbusinessentitylabel.repository.GspBeLabelInfoRepository;
import com.inspur.edp.bef.bemanager.util.CheckInfoUtil;
import com.inspur.edp.bef.bizentity.gspbusinessentitylabel.api.IGspBeLabelInfoService;
import com.inspur.edp.bef.bizentity.gspbusinessentitylabel.entity.GspBeLabelInfo;

import java.util.List;

public class GspBeLabelInfoService implements IGspBeLabelInfoService {
    private final GspBeLabelInfoRepository labelInfoRepository;

    public GspBeLabelInfoService(GspBeLabelInfoRepository labelInfoRepository) {
        this.labelInfoRepository = labelInfoRepository;
    }

    @Override
    public GspBeLabelInfo getBeLabelInfo(String id) {
        return labelInfoRepository.getOne(id);
    }

    @Override
    public List<GspBeLabelInfo> getBeLabelInfos() {
        return labelInfoRepository.findAll();
    }

    @Override
    public void saveBeLabelInfos(List<GspBeLabelInfo> infos) {
        infos.forEach(item -> {
            CheckInfoUtil.checkNessaceryInfo("id", item.getId());
        });
        labelInfoRepository.saveAll(infos);
    }
}
