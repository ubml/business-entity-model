package com.inspur.edp.bef.bemanager.commonstructure;

import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.caf.cef.dt.spi.CommonStructureSchemaExtension;
import com.inspur.edp.caf.cef.schema.common.CommonStructureContext;
import com.inspur.edp.caf.cef.schema.common.CommonStructureInfo;
import com.inspur.edp.caf.cef.schema.structure.CommonStructure;
import com.inspur.edp.lcm.metadata.api.IMetadataContent;

public class BEComStructureSchemaExtension implements CommonStructureSchemaExtension {
    @Override
    public String getEntityType() {
        return "GSPBusinessEntity";
    }

    @Override
    public CommonStructure getCommonStructure(IMetadataContent iMetadataContent, CommonStructureContext commonStructureContext) {
        return (GspBusinessEntity) iMetadataContent;
    }

    @Override
    public CommonStructureInfo getCommonStructureSummary(IMetadataContent iMetadataContent) {
        return null;
    }
}
