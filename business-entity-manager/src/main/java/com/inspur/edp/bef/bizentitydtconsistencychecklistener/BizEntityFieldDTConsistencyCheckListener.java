package com.inspur.edp.bef.bizentitydtconsistencychecklistener;

import com.inspur.edp.bef.bizentity.GspBizEntityObject;
import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.bef.bizentity.bizentitydtevent.BizEntityFieldDTEventListener;
import com.inspur.edp.bef.bizentity.dtconsistencycheck.fieldeventargs.AbstractBeFieldEventArgs;
import com.inspur.edp.bef.bizentity.dtconsistencycheck.fieldeventargs.RemovingFieldEventArgs;
import com.inspur.edp.cef.designtime.api.collection.GspFieldCollection;
import com.inspur.edp.cef.designtime.api.dtconsistencycheck.ConsistencyCheckEventMessage;
import com.inspur.edp.cef.designtime.api.element.GspElementObjectType;
import com.inspur.edp.das.commonmodel.IGspCommonElement;
import com.inspur.edp.das.commonmodel.util.MessageI18nUtils;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.api.service.MetadataProjectService;
import com.inspur.edp.lcm.metadata.api.service.MetadataService;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

import java.util.ArrayList;
import java.util.List;

public
class BizEntityFieldDTConsistencyCheckListener extends BizEntityFieldDTEventListener {

    /**
     * 删除节点监听事件
     *
     * @param args
     * @return
     */
    @Override
    public RemovingFieldEventArgs removingField(
            RemovingFieldEventArgs args) {
        return (RemovingFieldEventArgs) bizEntityFieldConsistencyCheck(args);
    }

    /**
     * BE字段一致性校验
     *
     * @param args
     * @return
     */
    protected AbstractBeFieldEventArgs bizEntityFieldConsistencyCheck(AbstractBeFieldEventArgs args) {
        String returnMessage = getFieldRelatedInfo(args.getMetadataPath(), args.getBeId(),
                args.getFieldId());
        if (returnMessage == null || returnMessage.isEmpty()) {
            return args;
        }
        ConsistencyCheckEventMessage message = new ConsistencyCheckEventMessage(false, returnMessage);
        args.addEventMessage(message);
        return args;
    }

    /**
     * 获取的关联信息，格式：元数据包【XXX】中BE【XXX】上的字段【XXX】关联了当前BE节点。
     *
     * @param metadataPath 元数据路径
     * @param beId         元数据ID
     * @param beElementId  be字段ID
     * @return 关联当前BE的关联信息
     */
    protected String getFieldRelatedInfo(String metadataPath, String beId, String beElementId) {
        MetadataService metadataService = SpringBeanUtils
                .getBean(com.inspur.edp.lcm.metadata.api.service.MetadataService.class);
        List<GspMetadata> gspMetadataList = metadataService
                .getMetadataListByRefedMetadataId(metadataPath, beId);
        StringBuilder strBuilder = new StringBuilder();
        gspMetadataList.forEach(gspMetadata -> {
            if (!gspMetadata.getHeader().getType().equals("GSPBusinessEntity")) {
                return;
            }
            GspBusinessEntity businessEntity = (GspBusinessEntity) metadataService
                    .loadMetadata(gspMetadata.getHeader().getFileName(), gspMetadata.getRelativePath())
                    .getContent();
            java.util.ArrayList<GspBizEntityObject> bizEntityObjects = businessEntity.getAllNodes();
            for (GspBizEntityObject bizEntityObject : bizEntityObjects) {
                ArrayList<IGspCommonElement> commonElements = bizEntityObject.getAllElementList(true);
                commonElements.forEach(commonElement -> {
                    if (commonElement.getObjectType() == GspElementObjectType.Association) {
                        if (commonElement.getChildAssociations().isEmpty()) {
                            return;
                        }
                        GspFieldCollection fieldsCollection = commonElement.getChildAssociations().get(0)
                                .getRefElementCollection();
                        fieldsCollection.forEach(field -> {
                            if (!field.getRefElementId().equals(beElementId)) {
                                return;
                            }
                            strBuilder.append(returnMessage(projectName(gspMetadata.getRelativePath()),
                                    bizEntityObject.getCode(), commonElement.getCode()));
                        });
                    }
                });
            }

        });
        return strBuilder.toString();
    }

    /**
     * 获取工程名称
     */
    protected String projectName(String metadataPath) {
        MetadataProjectService projectService = SpringBeanUtils
                .getBean(com.inspur.edp.lcm.metadata.api.service.MetadataProjectService.class);
        return projectService.getMetadataProjInfo(metadataPath).getName();
    }

    /**
     * 拼接返回值信息
     */
    protected String returnMessage(String packageName, String ObjectCode,
                                   String elementCode) {
        return MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0113", packageName, ObjectCode, elementCode);
    }
}
