package com.inspur.edp.bef.bemanager.service;

import com.inspur.edp.bef.bemanager.commonstructure.BEEntitySchemaExtension;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration("com.inspur.edp.bef.bemanager.service.BeanConfigService")
public class BeanConfigService {
    @Bean
    public BEEntitySchemaExtension beEntitySchemaExtension() {
        return new BEEntitySchemaExtension();
    }
}
