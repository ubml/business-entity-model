package com.inspur.edp.bef.bemanager.metadatartevent;

import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.bef.bizentity.exceptions.BusinessEntityErrorCodeEnum;
import com.inspur.edp.bef.bizentity.exceptions.BusinessEntityException;
import com.inspur.edp.bef.bizentity.gspbusinessentity.api.IGspBeExtendInfoService;
import com.inspur.edp.bef.bizentity.gspbusinessentity.entity.BeConfigCollectionInfo;
import com.inspur.edp.bef.bizentity.gspbusinessentity.entity.GspBeExtendInfo;
import com.inspur.edp.bef.engine.core.BefEngineInfoCache;
import com.inspur.edp.bef.extendinfo.server.api.IGspBeExtendInfoRpcService;
import com.inspur.edp.cef.entity.config.CefConfig;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.api.entity.ProcessMode;
import com.inspur.edp.metadata.rtcustomization.spi.event.IMetadataRtEventListener;
import com.inspur.edp.metadata.rtcustomization.spi.event.MetadataRtEventArgs;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BeMetadataRtEventListener implements IMetadataRtEventListener {

    @Override
    public void fireGeneratedMetadataSavingEvent(MetadataRtEventArgs metadataRtEventArgs) {

    }

    @Override
    public void fireGeneratedMetadataSavedEvent(MetadataRtEventArgs metadataRtEventArgs) {

    }

    @Override
    public void fireMetadataSavingEvent(MetadataRtEventArgs metadataRtEventArgs) {

    }

    @Override
    public void fireMetadataSavedEvent(MetadataRtEventArgs metadataRtEventArgs) {

        GspMetadata metadata = metadataRtEventArgs.getMetadata();
        if (!"GSPBusinessEntity".equals(metadataRtEventArgs.getMetadata().getHeader().getType()))
            return;
        GspBusinessEntity be = (GspBusinessEntity) metadata.getContent();
        BefEngineInfoCache.removeBefEngineInfo(be.getID());
        IGspBeExtendInfoService beInfoService = SpringBeanUtils.getBean(IGspBeExtendInfoService.class);
        beInfoService.deleteBeExtendInfo(be.getID());

        try {
            List<GspBeExtendInfo> infos = new ArrayList<>();
            ProcessMode processMode = metadataRtEventArgs.getProcessMode();
            BefEngineInfoCache.removeBefEngineInfo(be.getID());
            GspBeExtendInfo info = buildBeExtendInfo(processMode, be);
            if (metadata.isExtended()) {
                info.setBeConfigCollectionInfo(null);
            }
            if (be.getGeneratedConfigID() != null) {
                infos.add(info);
                beInfoService.saveGspBeExtendInfos(infos);
            }
        } catch (Exception e) {
            throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_BUSINESS_ENTITY_0018, e, be.getID());
        }
    }

    private GspBeExtendInfo buildBeExtendInfo(ProcessMode processMode, GspBusinessEntity be) {
        GspBeExtendInfo info = new GspBeExtendInfo();
        BeConfigCollectionInfo beConfigCollectionInfo = getBefConfigCollectionInfo(be);
        beConfigCollectionInfo.setProjectType(processMode);
        info.setId(be.getID());
        info.setConfigId(be.getGeneratedConfigID());
        info.setLastChangedOn(new Date());
        info.setBeConfigCollectionInfo(beConfigCollectionInfo);
        return info;
    }

    private BeConfigCollectionInfo getBefConfigCollectionInfo(GspBusinessEntity be) {
        CefConfig cefConfig = new CefConfig();
        cefConfig.setID(be.getGeneratedConfigID());
        cefConfig.setBEID(be.getID());
        String nameSpace = be.getCoreAssemblyInfo().getDefaultNamespace();
        cefConfig.setDefaultNamespace(nameSpace.toLowerCase());
        BeConfigCollectionInfo beConfigCollectionInfo = new BeConfigCollectionInfo();
        beConfigCollectionInfo.setConfig(cefConfig);
        return beConfigCollectionInfo;
    }

    @Override
    public void fireMetadataDeletingEvent(MetadataRtEventArgs metadataRtEventArgs) {

    }

    @Override
    public void fireMetadataDeletedEvent(MetadataRtEventArgs metadataRtEventArgs) {

        if (!"GSPBusinessEntity".equals(metadataRtEventArgs.getMetadata().getHeader().getType()))
            return;
        BefEngineInfoCache.removeBefEngineInfo(metadataRtEventArgs.getMetadata().getHeader().getId());
        IGspBeExtendInfoRpcService beInfoService = SpringBeanUtils.getBean(IGspBeExtendInfoRpcService.class);
        GspBeExtendInfo existing = beInfoService.getBeExtendInfo(metadataRtEventArgs.getMetadata().getHeader().getId());
        if (existing != null) {
            beInfoService.deleteBeExtendInfo(metadataRtEventArgs.getMetadata().getHeader().getId());
        }
    }

}

