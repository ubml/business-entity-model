package com.inspur.edp.bef.bemanager.util;

import com.inspur.edp.bef.bizentity.exceptions.BusinessEntityErrorCodeEnum;
import com.inspur.edp.bef.bizentity.exceptions.BusinessEntityException;
import com.inspur.edp.jittojava.context.service.CommonService;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import lombok.extern.slf4j.Slf4j;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.file.Files;

@Slf4j
public class BefFileUtil {

    private static String pomFile = "pom.xml";

    public static String getPomFilePath(String projectPath) {
        return projectPath + File.separator + pomFile;
    }

    public static String getAbsolutePathString(String path) {
        return new File(path).getAbsolutePath();
    }

    public static String getTagValue(Element ele, String tagName) {
        Element tagElement = ele.element(tagName);
        if (tagElement == null) {
            return null;
        }
        return tagElement.getText();
    }

    public static Document readDocument(String filePath) {
        SAXReader sr = new SAXReader();
        try {
            return sr.read(filePath);
        } catch (DocumentException e) {
            throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_COMMON_0004, e, filePath);
        }
    }

    public static void saveDocument(Document document, File xmlFile) {
        // 创建输出流
        Writer osWrite;
        XMLWriter writer;
        try {
            // 创建输出流
            osWrite = new OutputStreamWriter(Files.newOutputStream(xmlFile.toPath()));

            OutputFormat format = OutputFormat.createPrettyPrint(); // 获取输出的指定格式
            format.setEncoding("UTF-8");// 设置编码 ，确保解析的xml为UTF-8格式
            writer = new XMLWriter(osWrite, format);// XMLWriter
            // 指定输出文件以及格式
            writer.write(document);// 把document写入xmlFile指定的文件(可以为被解析的文件或者新创建的文件)
            writer.flush();
            writer.close();
        } catch (IOException e) {
            log.error("保存文档失败，错误信息为：" + e.getMessage());
        }
    }

    public static void log(String message) {
        Boolean showLog = true;
//		if (showLog) {
//			System.out.println(message);
//		}
    }

    public static void excecuteCommand(Process process, String command, String funcMessage) {
        // 执行命令
        try {
            process = Runtime.getRuntime().exec(command);
            BefFileUtil.log(funcMessage + "开始...");
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    process.getInputStream()));
            String line;
            while ((line = reader.readLine()) != null) {
                BefFileUtil.log(line);
            }
            process.waitFor();
            if (process.exitValue() == 1) {
                BefFileUtil.log(funcMessage + "失败");
            }
            BefFileUtil.log(funcMessage + "完成");
            process.destroy();
        } catch (IOException | InterruptedException e) {
            log.error(e.getMessage());
        }
    }

    public static String getJavaProjPath(String metaProjectPath) {
        return String.format("%1$s/%2$s", metaProjectPath,
                BefFileUtil.getService(CommonService.class).getProjectPath(metaProjectPath));
    }

    //TODO CAF方法
    public static String getServerPath() {
        String runtimePath = System.getProperty("java.class.path");
        int endIndex = runtimePath.indexOf("/runtime");
        if (endIndex == -1) {
            endIndex = runtimePath.indexOf("\\runtime");
            if (endIndex == -1) {
                throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_COMMON_0005, runtimePath);
            }
        }
        return runtimePath.substring(0, endIndex);
    }

    public static <T> T getService(Class<T> type) {
        return SpringBeanUtils.getBean(type);
    }
}
