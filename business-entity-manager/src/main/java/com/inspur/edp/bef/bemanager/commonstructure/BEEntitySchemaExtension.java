package com.inspur.edp.bef.bemanager.commonstructure;

import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.caf.cef.rt.api.CommonStructureInfo;
import com.inspur.edp.caf.cef.rt.spi.EntitySchemaExtension;
import com.inspur.edp.caf.cef.schema.structure.CommonStructure;
import com.inspur.edp.lcm.metadata.api.service.MetadataRTService;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

public class BEEntitySchemaExtension implements EntitySchemaExtension {
    @Override
    public CommonStructure getEntity(String metaId) {
        MetadataRTService service = SpringBeanUtils.getBean(MetadataRTService.class);
        GspBusinessEntity be = (GspBusinessEntity) service.getMetadata(metaId).getContent();
        return (CommonStructure) be;
    }

    @Override
    public CommonStructureInfo getEntitySummary(String s) {
        return null;
    }

    @Override
    public String getEntityType() {
        return "GSPBusinessEntity";
    }
}
