package com.inspur.edp.bef.bemanager.service;

import com.inspur.edp.bef.bizentity.GspBizEntityElement;
import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.bef.bizentity.exceptions.BusinessEntityErrorCodeEnum;
import com.inspur.edp.bef.bizentity.exceptions.BusinessEntityException;
import com.inspur.edp.das.commonmodel.IGspCommonObject;
import com.inspur.edp.das.commonmodel.entity.GspCommonObject;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.spi.MetadataElementService;
import com.inspur.edp.lcm.metadata.spi.entity.MetadataDependencyDetail;
import com.inspur.edp.lcm.metadata.spi.entity.MetadataElement;
import com.inspur.edp.lcm.metadata.spi.entity.MetadataElementLocator;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class BeMetadataElementService implements MetadataElementService {
    private static final String METADATA_TYPE_NAME = "GSPBusinessEntity";

    @Override
    public String getMetadataType() {
        return METADATA_TYPE_NAME;
    }

    /**
     * 根据locator信息，返回locator指向的当前be元数据上的节点
     */
    @Override
    public Map<MetadataElementLocator, MetadataElement> getMetadataElement(GspMetadata metadata, Set<MetadataElementLocator> set) {
        if (!(metadata.getContent() instanceof GspBusinessEntity))
            return null;
        if (set.isEmpty())
            return null;
        GspBusinessEntity be = (GspBusinessEntity) metadata.getContent();
        Map<MetadataElementLocator, MetadataElement> result = new HashMap<>();
        for (MetadataElementLocator locator : set) {
            buildMetadataElement(be, locator, result);
        }
        return result;
    }

    @Override
    public List<MetadataDependencyDetail> getMetadataDependencyDetails(GspMetadata metadata) {
        return null;
    }

    private void buildMetadataElement(GspBusinessEntity be, MetadataElementLocator locator, Map<MetadataElementLocator, MetadataElement> result) {
        switch (locator.getPath()) {
            case BeMetadataElementConst.BE_ELEMENT:
                buildBeElement(be, locator, result);
                break;
            case BeMetadataElementConst.BE_ENTITY:
                buildBeEntity(be, locator, result);
                break;
            default:
                throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_BUSINESS_ENTITY_0017, locator.getPath());
        }
    }

    private void buildBeElement(GspBusinessEntity be, MetadataElementLocator locator, Map<MetadataElementLocator, MetadataElement> result) {
        String beElementID = locator.getId();
        if (beElementID == null || beElementID.isEmpty())
            return;
        // 字段传参形式为：字段所在实体ID/字段ID
        String[] beElementPath = beElementID.split("/", 0);
        if (beElementPath.length < 2)
            return;
        String beEleBelongObjId = beElementPath[0];
        String beEleId = beElementPath[1];
        GspCommonObject commonObject = (GspCommonObject) be.findObjectById(beEleBelongObjId);
        if (commonObject == null)
            return;
        GspBizEntityElement bizEntityElement = (GspBizEntityElement) commonObject.findElement(beEleId);
        MetadataElement element = new MetadataElement();
        element.setType(locator.getPath());
        element.setContent(bizEntityElement);
        result.put(locator, element);
    }

    private void buildBeEntity(GspBusinessEntity be, MetadataElementLocator locator, Map<MetadataElementLocator, MetadataElement> result) {
        String id = locator.getId();
        if (id == null || id.isEmpty())
            return;
        IGspCommonObject commonObject = be.findObjectById(id);
        MetadataElement element = new MetadataElement();
        element.setType(locator.getPath());
        element.setContent(commonObject);
        result.put(locator, element);
    }
}
