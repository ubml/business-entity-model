package com.inspur.edp.bef.bemanager.dependency;

public class MavenDependencyConst {
    public static String groupId = "groupId";
    public static String artifactId = "artifactId";
    public static String version = "version";
    public static String file = "file";
    public static String dependency = "dependency";
    public static String dependencies = "dependencies";

    public static String modules = "modules";

}
