package com.inspur.edp.bef.bemanager.increment;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.inspur.edp.bef.bizentity.exceptions.BusinessEntityErrorCodeEnum;
import com.inspur.edp.bef.bizentity.exceptions.BusinessEntityException;
import com.inspur.edp.bef.bizentity.increment.BizEntityIncrement;
import com.inspur.edp.metadata.rtcustomization.api.ICustomizedContent;
import com.inspur.edp.metadata.rtcustomization.spi.CustomizationSerializer;

public class BizEntityCustomizationSerializer implements CustomizationSerializer {
    private ObjectMapper mapper;

    private ObjectMapper getMapper() {
        if (mapper == null) {
            mapper = new ObjectMapper();
//            SimpleModule module = new SimpleModule();
//            module.addSerializer(CommonModelIncrement.class, new BizEntityIncrementSerializer());
//            module.addDeserializer(CommonModelIncrement.class, new BizEntityIncrementDeserializer());
//            mapper.registerModule(module);
        }
        return mapper;
    }

    @Override
    public String serialize(ICustomizedContent increament) {

        try {
            return getMapper().writeValueAsString(increament);
        } catch (JsonProcessingException e) {
            throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_JSON_0004, e, "increament");
        }
    }

    @Override
    public ICustomizedContent deSerialize(String increamentStr) {
        try {
            return getMapper().readValue(increamentStr, BizEntityIncrement.class);
        } catch (JsonProcessingException e) {
            throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_JSON_0001, e, "BizEntityIncrement");
        }
    }
}
