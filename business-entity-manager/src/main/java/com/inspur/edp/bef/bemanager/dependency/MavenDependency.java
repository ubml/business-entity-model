package com.inspur.edp.bef.bemanager.dependency;

public class MavenDependency {

    // region 构造函数
    public MavenDependency(String groupId, String artifactId, String version) {
        setGroupId(groupId);
        setArtifactId(artifactId);
        setVersion(version);
    }

    public MavenDependency(String groupId, String artifactId, String version, String file) {
        setGroupId(groupId);
        setArtifactId(artifactId);
        setVersion(version);
        setFile(file);
    }

    public MavenDependency() {

    }

    // endregion

    private String artifactId;
    private String version;
    private String file;
    private String groupId;

    // region 属性
    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getArtifactId() {
        return artifactId;
    }

    public void setArtifactId(String artifactId) {
        this.artifactId = artifactId;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getGroupId() {
        return groupId;
    }
    // endregion

    public boolean equals(MavenDependency source) {
        if (source == null) {
            return false;
        }
        return source.getGroupId().equals(getGroupId())
                && source.getArtifactId().equals(getArtifactId());
        // 不比较版本
//		if (result) {
//			if (source.getVersion() != null) {
//				return source.getVersion().equals(getVersion());
//			} else {
//				return getVersion() == null;
//			}
//		} else {
//			return result;
//		}
    }
}
