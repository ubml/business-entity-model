package com.inspur.edp.bef.bemanager.codegenerator.actions;

import com.inspur.edp.bef.bemanager.codegenerator.JavaCompCodeNames;
import com.inspur.edp.bef.bemanager.util.CefConfigBeanGenCode;
import com.inspur.edp.bef.bemanager.util.ComponentExtendProperty;
import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.bef.bizentity.exceptions.BusinessEntityErrorCodeEnum;
import com.inspur.edp.bef.bizentity.exceptions.BusinessEntityException;
import com.inspur.edp.cdf.component.api.schema.CommonComponent;
import com.inspur.edp.lcm.metadata.api.IMetadataContent;

public class JavaTccActionGenerator extends JavaBaseCommonCompCodeGen {

    public JavaTccActionGenerator(GspBusinessEntity be,
                                  com.inspur.edp.bef.bizentity.operation.BizOperation operation, String nameSpace,
                                  String path) {
        super(be, operation, nameSpace, path);
        // 对构件扩展属性进行初始化-如有使用
        this.extendProperty = new ComponentExtendProperty();
    }

    @Override
    protected String getBaseClassName() {
        return "AbstractBefTccHandler";
    }

    @Override
    protected String GetNameSpaceSuffix() {
        return BizOperation.getOwner().getCode() + "." + JavaCompCodeNames.TccActionsNameSpaceSuffix; // 获取 表名.类名
    }

    @Override
    protected String getClassNameFromComp(IMetadataContent content) {
        if (content instanceof CommonComponent) {
            CommonComponent commonComponent = (CommonComponent) content;
            // 通用构件的元数据信息中存放数据量太少，暂时只能通过Code进行处理,去掉结尾的Controller
            String className = commonComponent.getCode();
            if (className.endsWith("Controller")) {
                return className.substring(0, className.indexOf("Controller"));
            }
            throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_BUSINESS_ENTITY_0052, className);
        }
        throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_BUSINESS_ENTITY_0051);
    }

    @Override
    protected void JavaGenerateExecute(StringBuilder result) {
    }

    @Override
    protected void JavaGenerateExtendUsing(StringBuilder result) {
        result.append(GetImportStr(JavaCompCodeNames.AbstractBefTccHandler));
    }

    @Override
    protected void JavaGenerateConstructor(StringBuilder result) {
    }

    @Override
    protected String GetInitializeCompName() {
        return String.format("%1$s%2$s%3$s", getChildCode(), BizOperation.getCode(), JavaCompCodeNames.TccAction);
    }

    @Override
    protected void generateBeanAnnotation(StringBuilder result) {
        CefConfigBeanGenCode genCodeInfo = new CefConfigBeanGenCode();

        genCodeInfo.setBeanCode(BizOperation.getComponentId());
        genCodeInfo.setClassName(getCompName());
        genCodeInfo.setImportCode(NameSpace);

        this.extendProperty.setCefConfigBeanGenCode(genCodeInfo);
    }

    @Override
    protected void JavaGenerateExtendMethod(StringBuilder result) {
        generateClassAnnotation(result);
        generateGetNameMethod(result);
        generateCanExecuteAnno(result);
        generateTccExtendMethod(result, JavaCompCodeNames.CanExecuteInTcc, JavaCompCodeNames.KeywordBool, "false");
        generateMethodAnnotation(result, JavaCompCodeNames.ConfirmParamAnno);
        generateTccExtendMethod(result, JavaCompCodeNames.ConfirmInTcc, JavaCompCodeNames.KeywordVoid, null);
        generateMethodAnnotation(result, JavaCompCodeNames.CancelParamAnno);
        generateTccExtendMethod(result, JavaCompCodeNames.CancelInTcc, JavaCompCodeNames.KeywordVoid, null);
    }

    private void generateGetNameMethod(StringBuilder result) {
        result.append(GetIndentationStr()).append("@").append(JavaCompCodeNames.KeywordOverride)
                .append(getNewline()).append(GetIndentationStr())
                .append(JavaCompCodeNames.KeywordPublic).append(" ")
                .append(JavaCompCodeNames.KeywordString).append(" ")
                .append("getName()").append(" ").append("{").append(getNewline());

        result.append(GetIndentationStr()).append(GetIndentationStr())
                .append(JavaCompCodeNames.KeyWordReturn).append(" ").append("\"")
                .append(this.BizOperation.getCode()).append("\";")
                .append(getNewline());

        result.append(GetIndentationStr()).append("}").append(getNewline());
    }

    private void generateTccExtendMethod(StringBuilder result, String methodName, String returnType, String returnValue) {
        result.append(getNewline()).append(GetIndentationStr()).append("@").append(JavaCompCodeNames.KeywordOverride)
                .append(getNewline()).append(GetIndentationStr())
                .append(JavaCompCodeNames.KeywordPublic).append(" ")
                .append(returnType).append(" ")
                .append(methodName).append("(").append(JavaCompCodeNames.TccHandlerContextPackage).append(" ")
                .append(JavaCompCodeNames.TccHandlerContextName).append(") {").append(getNewline());
        if (returnType.equals(JavaCompCodeNames.KeywordVoid)) {
            result.append(GetIndentationStr()).append("}").append(getNewline());
        } else {
            result.append(GetDoubleIndentationStr()).append("return").append(" ").append(returnValue).append(";").append(getNewline())
                    .append(GetIndentationStr()).append("}").append(getNewline());
        }
    }

    private void generateClassAnnotation(StringBuilder result) {
        result.append(GetIndentationStr()).append(JavaCompCodeNames.BeginCommonAnnotation).append(JavaCompCodeNames.TccActionClassAnno).append(getNewline());
    }

    private void generateCanExecuteAnno(StringBuilder result) {
        result.append(GetIndentationStr()).append(JavaCompCodeNames.BeginAnnotation).append(getNewline())
                .append(GetIndentationStr()).append(" *").append(JavaCompCodeNames.TccActionCanExecuteAnno).append(getNewline())
                .append(GetIndentationStr()).append(" *").append(JavaCompCodeNames.KeyWordParam).append(" ctx ctx").append(JavaCompCodeNames.CanExecuteInputParamAnno).append(getNewline())
                .append(GetIndentationStr()).append(" *").append(JavaCompCodeNames.KeyWordParam).append(JavaCompCodeNames.CanExecuteReturnParamAnno).append(getNewline())
                .append(GetIndentationStr()).append(" ").append(JavaCompCodeNames.EndAnnotation);
    }

    private void generateMethodAnnotation(StringBuilder result, String methodParamAnno) {
        result.append(GetIndentationStr()).append(JavaCompCodeNames.BeginAnnotation).append(getNewline())
                .append(GetIndentationStr()).append(" *").append(methodParamAnno).append(getNewline())
                .append(GetIndentationStr()).append(" *").append(JavaCompCodeNames.KeyWordParam).append(" ctx ").append(JavaCompCodeNames.ConfirmParamAnno).append(getNewline())
                .append(GetIndentationStr()).append(" ").append(JavaCompCodeNames.EndAnnotation);
    }
}
