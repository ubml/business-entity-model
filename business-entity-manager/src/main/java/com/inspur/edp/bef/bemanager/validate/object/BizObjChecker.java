package com.inspur.edp.bef.bemanager.validate.object;

import com.inspur.edp.bef.bemanager.validate.element.BizFieldChecker;
import com.inspur.edp.bef.bemanager.validate.operation.BizActionChecker;
import com.inspur.edp.bef.bemanager.validate.operation.TccActionChecker;
import com.inspur.edp.bef.bizentity.GspBizEntityObject;
import com.inspur.edp.bef.bizentity.operation.BizActionBase;
import com.inspur.edp.bef.bizentity.operation.BizOperation;
import com.inspur.edp.bef.bizentity.operation.internalmgraction.TccSettingElement;
import com.inspur.edp.cef.designtime.api.validate.common.CheckUtil;
import com.inspur.edp.cef.designtime.api.validate.element.FieldChecker;
import com.inspur.edp.das.commonmodel.IGspCommonObject;
import com.inspur.edp.das.commonmodel.util.MessageI18nUtils;
import com.inspur.edp.das.commonmodel.validate.object.CMObjectChecker;

public class BizObjChecker extends CMObjectChecker {
    private static BizObjChecker bizObject;

    public static BizObjChecker getInstance() {
        if (bizObject == null) {
            bizObject = new BizObjChecker();
        }
        return bizObject;
    }

    @Override
    protected FieldChecker getFieldChecker() {
        return BizFieldChecker.getInstance();
    }

    @Override
    protected void checkAction(IGspCommonObject commonObject) {
        for (BizOperation bizAction : ((GspBizEntityObject) commonObject).getBizActions()) {
            BizActionChecker.getInstance().checkBizAction((BizActionBase) bizAction);
        }
    }

    @Override
    protected void checkExtension(IGspCommonObject commonObject) {
        for (TccSettingElement element : ((GspBizEntityObject) commonObject).getTccSettings()) {
            checkTccSettingElement(commonObject, element);
        }
    }

    private void checkTccSettingElement(IGspCommonObject commonObject, TccSettingElement ele) {
        checkBasicFieldInfo(commonObject, ele);
        checkTccAction(ele);

    }

    private void checkBasicFieldInfo(IGspCommonObject commonObject, TccSettingElement ele) {
        if (ele.getCode() == null || ele.getCode().isEmpty()) {
            if (ele.getName() == null || ele.getName().isEmpty()) {
                CheckUtil.exception(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0107",commonObject.getCode()));
            }
            CheckUtil.exception(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0108", commonObject.getCode(), ele.getName()));
        }
        if (!CheckUtil.isLegality(ele.getCode())) {
            CheckUtil.exception(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0109", commonObject.getCode(), ele.getCode()));
        }
    }

    private void checkTccAction(TccSettingElement ele) {
        TccActionChecker.getInstance().checkTccAction(ele.getTccAction());
    }

}
