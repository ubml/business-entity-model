package com.inspur.edp.bef.bemanager.generatecomponent;


import com.inspur.edp.bef.bemanager.util.CheckInfoUtil;
import com.inspur.edp.bef.bizentity.exceptions.BusinessEntityErrorCodeEnum;
import com.inspur.edp.bef.bizentity.exceptions.BusinessEntityException;
import com.inspur.edp.lcm.fs.api.IFsService;
import com.inspur.edp.lcm.metadata.api.entity.MetadataProject;
import com.inspur.edp.lcm.metadata.api.service.MetadataProjectService;
import com.inspur.edp.lcm.metadata.api.service.MetadataService;
import io.iec.edp.caf.commons.exception.ExceptionLevel;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import lombok.var;

public final class ComponentGenUtil {
    public static final String ComponentDir = "component";

    public static String getComponentAssemblyName(String path) {

        MetadataProjectService projService = SpringBeanUtils.getBean(MetadataProjectService.class);


        MetadataProject projInfo = projService.getMetadataProjInfo(path);
        if (projInfo == null) {
            throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_BUSINESS_ENTITY_0030, ExceptionLevel.Info);
        }
        return projInfo.getMetadataPackageInfo().getName();
    }

    public static MetadataProject getComponentProjInfo(String path) {


        MetadataProjectService projService = SpringBeanUtils.getBean(MetadataProjectService.class);

        var projInfo = projService.getMetadataProjInfo(path);
        if (projInfo == null) {
            throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_BUSINESS_ENTITY_0030, ExceptionLevel.Info);
        }
        return projInfo;
    }

    public static String GetComponentNamespace(String path) {

        MetadataService projService = SpringBeanUtils.getBean(MetadataService.class);

        var projInfo = projService.getGspProjectInfo(path);
        if (projInfo == null) {
            throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_BUSINESS_ENTITY_0030, ExceptionLevel.Info);
        }
        return projInfo.getProjectNameSpace();
    }

    public static String PrepareComponentDir(String bePath) {

        String path = CheckInfoUtil.getCombinePath(bePath, ComponentGenUtil.ComponentDir);

        IFsService fileService = SpringBeanUtils.getBean(IFsService.class);
        if (!fileService.existsAsDir(path)) {
            fileService.createDir(path);
        }
        return path;
    }
}