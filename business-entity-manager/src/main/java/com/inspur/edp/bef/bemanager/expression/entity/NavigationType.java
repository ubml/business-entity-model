package com.inspur.edp.bef.bemanager.expression.entity;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.inspur.edp.bef.bemanager.expression.json.NavigationTypeJsonSerializer;

@JsonSerialize(using = NavigationTypeJsonSerializer.class)
public enum NavigationType {
    EntitySet(0),
    EntityType(1);
    private int intValue;
    private static java.util.HashMap<Integer, NavigationType> mappings;

    private synchronized static java.util.HashMap<Integer, NavigationType> getMappings() {
        if (mappings == null) {
            mappings = new java.util.HashMap<Integer, NavigationType>();
        }
        return mappings;
    }

    private NavigationType(int value) {
        intValue = value;
        NavigationType.getMappings().put(value, this);
    }

    public int getValue() {
        return intValue;
    }

    public static NavigationType forValue(int value) {
        return getMappings().get(value);
    }
}
