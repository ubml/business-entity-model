package com.inspur.edp.bef.bemanager.pushchangesetevent;

import com.inspur.edp.bef.bizentity.pushchangesetargs.PushChangeSetArgs;
import io.iec.edp.caf.commons.event.EventBroker;
import io.iec.edp.caf.commons.event.config.EventListenerSettings;

public class PushChangeSetEventBroker extends EventBroker {
    private final PushChangeSetEventManager eventManager;

    public PushChangeSetEventBroker(EventListenerSettings settings, PushChangeSetEventManager eventManager) {
        super(settings);
        this.eventManager = eventManager;
        this.init();
    }

    @Override
    protected void onInit() {
        this.getEventManagerCollection().add(this.eventManager);
    }

    public void firePushChangeSet(PushChangeSetArgs args) {
        this.eventManager.firePushChangeSet(args);
    }
}
