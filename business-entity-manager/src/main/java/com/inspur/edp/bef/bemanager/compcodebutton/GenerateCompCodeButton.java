package com.inspur.edp.bef.bemanager.compcodebutton;

import com.inspur.edp.bef.bemanager.codegenerator.JavaCmpCodeGeneratorFactory;
import com.inspur.edp.bef.bemanager.codegenerator.actions.JavaBaseCommonCompCodeGen;
import com.inspur.edp.bef.bizentity.GspBizEntityObject;
import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.bef.bizentity.exceptions.BusinessEntityErrorCodeEnum;
import com.inspur.edp.bef.bizentity.exceptions.BusinessEntityException;
import com.inspur.edp.bef.bizentity.operation.BizOperation;
import com.inspur.edp.bef.bizentity.operation.TccAction;
import com.inspur.edp.bef.bizentity.operation.internalmgraction.TccSettingElement;
import com.inspur.edp.cef.designtime.api.operation.CommonDetermination;
import com.inspur.edp.cef.designtime.api.operation.CommonOperation;
import com.inspur.edp.cef.variable.dtgenerator.vardtmGenerate.javageneratecmpcode.JavaCommonDeterminationGenerator;

import java.util.ArrayList;

public class GenerateCompCodeButton {

    public String getCompCodeButton(GspBusinessEntity be, String type, String relativePath, ArrayList<String> actionList, String compAssemblyName) {

        BizOperation op = initAction(type, be, actionList);
        GspBizEntityObject belongNode = initBelongNode(type, be, actionList);
        String code;

        if (!"VarDeterminations".equals(type)) {
            JavaBaseCommonCompCodeGen codeGen = (belongNode == null || belongNode.getIsRootNode()) ?
                    JavaCmpCodeGeneratorFactory.JavaGetGenerator(be, op, compAssemblyName, relativePath) :
                    JavaCmpCodeGeneratorFactory.JavaGetChildNodeGenerator(be, belongNode.getCode(), op, compAssemblyName, relativePath);
            code = codeGen.Generate();

        } else {
            CommonOperation dtm = initDtm(be, actionList);
            JavaCommonDeterminationGenerator gen = new JavaCommonDeterminationGenerator(be, dtm, compAssemblyName, relativePath);
            code = gen.generateExecute();
        }
        return code;
    }

    private BizOperation initAction(String type, GspBusinessEntity be, ArrayList<String> actionList) {
        java.util.ArrayList<GspBizEntityObject> nodes = be.getAllNodes();
        switch (type) {
            case "BizActions":
                for (BizOperation action : be.getCustomMgrActions()) {
                    if (actionList.contains(action.getCode()))
                        return action;
                }
                break;
            case "EntityActions":
                for (GspBizEntityObject node : nodes) {
                    for (BizOperation action : node.getCustomBEActions()) {
                        if (actionList.contains(action.getCode())) {
                            return action;
                        }
                    }
                }
                break;
            case "TccActions":
                for (GspBizEntityObject node : nodes) {
                    for (TccSettingElement ele : node.getTccSettings()) {
                        TccAction action = ele.getTccAction();
                        if (actionList.contains(action.getCode())) {
                            return action;
                        }
                    }
                }
                break;
            case "Determinations":
                for (GspBizEntityObject node : be.getAllNodes()) {
                    for (BizOperation action : node.getDeterminations()) {
                        if (actionList.contains(action.getCode())) {
                            return action;
                        }
                    }
                }
                break;
            case "Validations":
                for (GspBizEntityObject node : be.getAllNodes()) {
                    for (BizOperation action : node.getValidations()) {
                        if (actionList.contains(action.getCode())) {
                            return action;
                        }
                    }
                }
                break;
            case "VarDeterminations":
                return null;
            default:
                throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_ENUM_NOTSUPPORT_0006, type);
        }
        return null;
    }

    private GspBizEntityObject initBelongNode(String type, GspBusinessEntity be, ArrayList<String> actionList) {
        java.util.ArrayList<GspBizEntityObject> nodes = be.getAllNodes();
        switch (type) {
            case "BizActions":
            case "VarDeterminations":
                return null;
            case "EntityActions":
                for (GspBizEntityObject node : nodes) {
                    for (BizOperation action : node.getCustomBEActions()) {
                        if (actionList.contains(action.getCode())) {
                            return node;
                        }
                    }
                }
            case "TccActions":
                for (GspBizEntityObject node : nodes) {
                    for (TccSettingElement ele : node.getTccSettings()) {
                        TccAction action = ele.getTccAction();
                        if (actionList.contains(action.getCode())) {
                            return node;
                        }
                    }
                }
                break;
            case "Determinations":
                for (GspBizEntityObject node : be.getAllNodes()) {
                    for (BizOperation action : node.getDeterminations()) {
                        if (actionList.contains(action.getCode())) {
                            return node;
                        }
                    }
                }
                break;
            case "Validations":
                for (GspBizEntityObject node : be.getAllNodes()) {
                    for (BizOperation action : node.getValidations()) {
                        if (actionList.contains(action.getCode())) {
                            return node;
                        }
                    }
                }
                break;
            default:
                throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_ENUM_NOTSUPPORT_0006, type);
        }
        return null;
    }

    public CommonOperation initDtm(GspBusinessEntity be, ArrayList<String> actionList) {
        CommonOperation dt = new CommonDetermination();
        for (CommonDetermination dtm : be.getVariables().getDtmAfterModify()) {
            if (dtm.getIsRef() || !dtm.getIsGenerateComponent() || !actionList.contains(dtm.getCode())) {
                continue;
            }
            dt = dtm;
        }
        return dt;
    }
}

