package com.inspur.edp.bef.bemanager.codegenerator.genutils;

import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.bef.bizentity.operation.BizMgrAction;
import com.inspur.edp.bef.bizentity.operation.bemgrcomponent.BizMgrActionParameter;
import com.inspur.edp.bef.bizentity.operation.componentenum.BizCollectionParameterType;
import com.inspur.edp.cef.designtime.core.utilsgenerator.JavaAccessModifier;
import com.inspur.edp.cef.designtime.core.utilsgenerator.JavaClassInfo;
import com.inspur.edp.cef.designtime.core.utilsgenerator.MethodInfo;
import com.inspur.edp.cef.designtime.core.utilsgenerator.TypeRefInfo;
import org.springframework.util.StringUtils;

public class BeMgrActionMethodInvocationGenerator {
    private final BizMgrAction mgrAction;
    private final JavaClassInfo classInfo;
    private final GspBusinessEntity be;
    private final MethodInfo methodInfo = new MethodInfo();

    BeMgrActionMethodInvocationGenerator(GspBusinessEntity be, BizMgrAction mgrAction, JavaClassInfo classInfo) {
        this.be = be;
        this.mgrAction = mgrAction;
        this.classInfo = classInfo;
    }

    private String getMethodName() {
        return mgrAction.getCode();
    }

    private TypeRefInfo getReturnType() {
        return getTypeInfo(mgrAction);
    }

    public static TypeRefInfo getTypeInfo(BizMgrAction action) {
        TypeRefInfo typeInfo = new TypeRefInfo(String.class);
        if (StringUtils.isEmpty(action.getReturnValue().getClassName()) || action.getReturnValue().getClassName().toLowerCase().equalsIgnoreCase("void")) {
            typeInfo.setTypeName("void");
        } else {
            typeInfo.setTypeName(action.getReturnValue().getClassName());
        }
        return typeInfo;
    }

    public void generate() {
        methodInfo.setMethodName(getMethodName());
        methodInfo.setReturnType(getReturnType());
        methodInfo.getAccessModifiers().add(JavaAccessModifier.Public);
        methodInfo.getAccessModifiers().add(JavaAccessModifier.Static);

        if (mgrAction.getParameters() != null && mgrAction.getParameters().getCount() > 0) {
            for (Object bizParameter : mgrAction.getParameters()) {
                BizMgrActionParameter actionParameter = (BizMgrActionParameter) bizParameter;
                TypeRefInfo typeInfo = new TypeRefInfo(String.class);
                if (actionParameter.getCollectionParameterType() == BizCollectionParameterType.List) {
                    typeInfo.setTypeName("java.util.ArrayList<" + actionParameter.getClassName() + ">");
                } else if (actionParameter.getCollectionParameterType() == BizCollectionParameterType.Array) {
                    typeInfo.setTypeName(actionParameter.getClassName() + "[]");
                } else {
                    typeInfo.setTypeName(actionParameter.getClassName());
                }

                methodInfo.getParameters().add(new com.inspur.edp.cef.designtime.core.utilsgenerator.ParameterInfo(actionParameter.getParamCode(), typeInfo));
            }
        }
        generateMethodBodies();
        classInfo.addMethodInfo(methodInfo);
    }

    private void generateMethodBodies() {
        classInfo.getImportInfos().addImportPackage("com.inspur.edp.bef.api.be.MgrActionUtils");
        classInfo.getImportInfos().addImportPackage("com.inspur.edp.bef.api.lcp.IStandardLcp;");
        classInfo.getImportInfos().addImportPackage("com.inspur.edp.bef.api.BefRtBeanUtil;");
        methodInfo.getMethodBodies().add("IStandardLcp lcp = BefRtBeanUtil.getLcpFactory().createLcp(\"" + this.be.getGeneratedConfigID() + "\");");
        StringBuilder sb = new StringBuilder();
        if (mgrAction.getParameters() != null && mgrAction.getParameters().getCount() > 0) {
            for (Object bizParameter : mgrAction.getParameters()) {
                BizMgrActionParameter actionParameter = (BizMgrActionParameter) bizParameter;
                sb.append(",").append(actionParameter.getParamCode());
            }
        }
        if (methodInfo.getReturnType().getTypeName().equalsIgnoreCase("void")) {
            methodInfo.getMethodBodies().add("MgrActionUtils.executeCustomAction(lcp,\"" + mgrAction.getCode() + "\"" + sb.toString() + ");");
        } else {
            methodInfo.getMethodBodies().add("return (" + methodInfo.getReturnType().getTypeName() + ")MgrActionUtils.executeCustomAction(lcp,\"" + mgrAction.getCode() + "\"" + sb.toString() + ");");
        }
    }
}
