package com.inspur.edp.bef.bemanager.validate.model;

import com.inspur.edp.bef.bemanager.service.CheckBeChildTableService;
import com.inspur.edp.bef.bemanager.validate.object.BizObjChecker;
import com.inspur.edp.bef.bemanager.validate.operation.BizMgrActionChecker;
import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.bef.bizentity.operation.BizMgrAction;
import com.inspur.edp.bef.bizentity.operation.BizOperation;
import com.inspur.edp.das.commonmodel.entity.GspCommonModel;
import com.inspur.edp.das.commonmodel.validate.model.CommonModelChecker;
import com.inspur.edp.das.commonmodel.validate.object.CMObjectChecker;

public class BizEntityChecker extends CommonModelChecker {
    private static BizEntityChecker bizEntityChecker;

    public static BizEntityChecker getInstance() {
        if (bizEntityChecker == null) {
            bizEntityChecker = new BizEntityChecker();
        }
        return bizEntityChecker;
    }

    public final void check(GspBusinessEntity businessEntity) {
        super.checkCM(businessEntity);
        CheckBeChildTableService.getInstance().checkChildTable(businessEntity.getMainObject().getContainChildObjects());
    }

    @Override
    protected void checkExtension(GspCommonModel gspCommonModel) {
        checkMgrAciton((GspBusinessEntity) gspCommonModel);
    }

    @Override
    protected CMObjectChecker getCMObjectChecker() {
        return BizObjChecker.getInstance();
    }

    private void checkMgrAciton(GspBusinessEntity businessEntity) {
        for (BizOperation operation : businessEntity.getBizMgrActions()) {
            BizMgrActionChecker.getInstance().checkMgrAciton((BizMgrAction) operation);
        }

    }

}
