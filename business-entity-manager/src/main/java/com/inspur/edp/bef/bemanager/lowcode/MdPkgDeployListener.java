package com.inspur.edp.bef.bemanager.lowcode;

import com.inspur.edp.lcm.metadata.spi.event.MdPkgChangedArgs;
import com.inspur.edp.lcm.metadata.spi.event.MdPkgChangedEventListener;

public class MdPkgDeployListener implements MdPkgChangedEventListener {
    @Override
    public void fireMdPkgAddedEvent(MdPkgChangedArgs mdPkgChangedArgs) {
//    MetadataPackage metadataPackage = mdPkgChangedArgs.getMetadataPackage();
//    handleMdPkg(metadataPackage);
    }

    @Override
    public void fireMdPkgChangedEvent(MdPkgChangedArgs mdPkgChangedArgs) {
//    MetadataPackage metadataPackage = mdPkgChangedArgs.getMetadataPackage();
//    handleMdPkg(metadataPackage);
    }

//  private void handleMdPkg(MetadataPackage metadataPackage) {
//    try {
////      if (metadataPackage.getHeader().getProcessMode() != ProcessMode.interpretation) {
////        //如果不是无需生成代码的元数据包，直接忽略
////        return;
////      }
//
//      List<GspMetadata> packageMetadataList = metadataPackage.getMetadataList();
//      if (packageMetadataList == null || packageMetadataList.isEmpty()) {
//        //如果元数据包下不存在元数据
//        return;
//      }
//      List<GspMetadata> metadatas = packageMetadataList.stream()
//          .filter(item -> item.getHeader().getType().equals("GSPBusinessEntity"))
//          .collect(Collectors.toList());
//      if (metadatas.isEmpty()) {
//        return;
//      }
//      ProcessMode processMode = Optional.ofNullable(metadataPackage.getHeader().getProcessMode()).orElse(ProcessMode.generation);
//      BSessionUtil.wrapFirstTenantBSession(tenant -> {
//        //获取当前已部署的eapi版本，并移除未变动的Eapi元数据
//        CustomizationService metadataService = SpringBeanUtils.getBean(CustomizationService.class);
//        IGspBeExtendInfoService beInfoService = SpringBeanUtils.getBean(IGspBeExtendInfoService.class);
//        List<GspBeExtendInfo> infos = new ArrayList(metadatas.size());
//        for (GspMetadata metadata : metadatas) {
//          GspBusinessEntity be = (GspBusinessEntity) metadataService.getMetadata(metadata.getHeader().getId()).getContent();
//          BefEngineInfoCache.removeBefEngineInfo(be.getID());
//          if (be.getGeneratedConfigID() == null) {
//            continue;
//          }
//          GspBeExtendInfo info = buildBeExtendInfo(processMode, be);
//          infos.add(info);
//        }
//        if (!infos.isEmpty()) {
//          beInfoService.saveGspBeExtendInfos(infos);
//        }
//      });
//    } catch (Exception e) {
//      throw new RuntimeException("部署解析BE元数据出错：", e);
//    }
//  }
//
//  private GspBeExtendInfo buildBeExtendInfo(ProcessMode processMode, GspBusinessEntity be) {
//    GspBeExtendInfo info = new GspBeExtendInfo();
//    BeConfigCollectionInfo beConfigCollectionInfo = getBefConfigCollectionInfo(be);
//    beConfigCollectionInfo.setProjectType(processMode);
//    info.setId(be.getID());
//    info.setConfigId(be.getGeneratedConfigID());
//    info.setLastChangedOn(new Date());
//    info.setBeConfigCollectionInfo(beConfigCollectionInfo);
//    return info;
//  }
//
//  private BeConfigCollectionInfo getBefConfigCollectionInfo(GspBusinessEntity be) {
//      CefConfig cefConfig = new CefConfig();
//      cefConfig.setID(be.getGeneratedConfigID());
//      cefConfig.setBEID(be.getID());
//      String nameSpace = be.getCoreAssemblyInfo().getDefaultNamespace();
//      cefConfig.setDefaultNamespace(nameSpace.toLowerCase());
////      MgrConfig mgrConfig = new MgrConfig();
////      cefConfig.setMgrConfig(mgrConfig);
//      BeConfigCollectionInfo beConfigCollectionInfo = new BeConfigCollectionInfo();
//      beConfigCollectionInfo.setConfig(cefConfig);
//      return beConfigCollectionInfo;
//    }
}
