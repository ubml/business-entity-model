package com.inspur.edp.bef.bemanager.gspbusinessentitylabel.config;

import com.inspur.edp.bef.bemanager.gspbusinessentitylabel.GspBeLabelInfoService;
import com.inspur.edp.bef.bemanager.gspbusinessentitylabel.repository.GspBeLabelInfoRepository;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EntityScan({"com.inspur.edp.bef.bizentity.gspbusinessentitylabel.entity"})
@EnableJpaRepositories({"com.inspur.edp.bef.bemanager.gspbusinessentitylabel.repository"})
public class GspBeLabelInfoConfig {

    @Bean
    public GspBeLabelInfoService getBeLabelInfoService(GspBeLabelInfoRepository beLabelInfoRepository) {
        return new GspBeLabelInfoService(beLabelInfoRepository);
    }
}
