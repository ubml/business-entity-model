package com.inspur.edp.bef.bemanager.validate.element;

import com.inspur.edp.das.commonmodel.validate.element.CMFieldChecker;

public class BizFieldChecker extends CMFieldChecker {
    private static BizFieldChecker bizField;

    public static BizFieldChecker getInstance() {
        if (bizField == null) {
            bizField = new BizFieldChecker();
        }
        return bizField;
    }
}
