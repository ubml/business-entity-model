package com.inspur.edp.bef.bemanager.expression.entity;

import java.util.ArrayList;
import java.util.List;

public class ExpressionSchema {

    private List<ExpressionEntitySet> entitySets;
    private List<ExpressionEntityType> entityTypes;
    private String description;

    public List<ExpressionEntitySet> getEntitySets() {
        if (entitySets == null) {
            entitySets = new ArrayList<>();
        }
        return entitySets;
    }

    public void setEntitySets(
            List<ExpressionEntitySet> entitySets) {
        this.entitySets = entitySets;
    }

    public List<ExpressionEntityType> getEntityTypes() {
        if (entityTypes == null) {
            entityTypes = new ArrayList<>();
        }
        return entityTypes;
    }

    public void setEntityTypes(
            List<ExpressionEntityType> entityTypes) {
        this.entityTypes = entityTypes;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
