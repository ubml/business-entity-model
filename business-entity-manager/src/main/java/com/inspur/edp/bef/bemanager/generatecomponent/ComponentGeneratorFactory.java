package com.inspur.edp.bef.bemanager.generatecomponent;

import com.inspur.edp.bef.bemanager.generatecomponent.componentgenerators.BaseComponentGenerator;
import com.inspur.edp.bef.bemanager.generatecomponent.componentgenerators.BeComponentGenerator;
import com.inspur.edp.bef.bemanager.generatecomponent.componentgenerators.BeMgrComponentGenerator;
import com.inspur.edp.bef.bemanager.generatecomponent.componentgenerators.DeterminationComponentGenerator;
import com.inspur.edp.bef.bemanager.generatecomponent.componentgenerators.ValidationComponentGenerator;
import com.inspur.edp.bef.bizentity.beenum.BEOperationType;
import com.inspur.edp.bef.bizentity.exceptions.BusinessEntityErrorCodeEnum;
import com.inspur.edp.bef.bizentity.exceptions.BusinessEntityException;

public class ComponentGeneratorFactory {

    private static ComponentGeneratorFactory instance = new ComponentGeneratorFactory();

    public static ComponentGeneratorFactory getInstance() {
        return instance;
    }

    public BaseComponentGenerator getGenerator(BEOperationType opType) {
        switch (opType) {
            case BizAction:
                return BeComponentGenerator.getInstance();
            case Validation:
                return ValidationComponentGenerator.getInstance();
            case BizMgrAction:
                return BeMgrComponentGenerator.getInstance();
            case Determination:
                return DeterminationComponentGenerator.getInstance();
            default:
                throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_BUSINESS_ENTITY_0029);
        }
    }
}
