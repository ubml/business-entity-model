package com.inspur.edp.bef.bemanager.metadatacopy;

import com.inspur.edp.bef.bizentity.GspBizEntityObject;
import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.bef.bizentity.exceptions.BusinessEntityErrorCodeEnum;
import com.inspur.edp.bef.bizentity.exceptions.BusinessEntityException;
import com.inspur.edp.das.commonmodel.IGspCommonElement;
import com.inspur.edp.das.commonmodel.collection.GspElementCollection;
import com.inspur.edp.das.commonmodel.util.HandleAssemblyNameUtil;
import com.inspur.edp.das.commonmodel.util.MetadataProjectUtil;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.metadata.rtcustomization.api.entity.MimicParamKeyEnum;
import com.inspur.edp.metadata.rtcustomization.api.entity.MimicTypeEnum;
import com.inspur.edp.metadata.rtcustomization.context.MimicServiceContextHolder;
import com.inspur.edp.metadata.rtcustomization.spi.MetadataCopySpi;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Map;

/**
 * 该类配置在server\config\platform\common\lcm_rtcustomization.json的MetadataCopy节点下，用于复制应用
 * 当前仅支持零代码场景在不同bo下的复制
 *
 * @author Kaixuan Shi
 * @since 2023/4/11
 */
public class BeMetadataCopyProvider implements MetadataCopySpi {

    private final static String CODE_RULE_CONFIG_ID_PREFIX = "GspCodeRuleDefine_id_";
    private final static String BUSINESS_FIELD_ID_PREFIX = "businessfield_id_";
    private final static String DBO_CONFIG_ID_PREFIX = "gspdatabaseobject_id_";

    //todo 低代码复制应使用此类，不再重复创建
    @Override
    public void metadataCopy(Map<MimicParamKeyEnum, String> map, GspMetadata gspMetadata) {
        String metadataAssembly = gspMetadata.getHeader().getNameSpace();
        GspBusinessEntity be = (GspBusinessEntity) gspMetadata.getContent();
        be.setComponentAssemblyName(metadataAssembly);
        be.setDotnetGeneratingAssembly(metadataAssembly);
        //获取包路径前缀
        String packagePrefix = MetadataProjectUtil.getPackagePrefix(gspMetadata.getRelativePath());
        be.setGeneratingAssembly(HandleAssemblyNameUtil.convertToJavaPackageName(packagePrefix, metadataAssembly));
        be.setI18nResourceInfoPrefix(null);
        List<GspBizEntityObject> bizEntityObjects = be.getAllNodes();
        if (bizEntityObjects.isEmpty()) {
            return;
        }
        for (GspBizEntityObject bizEntityObject : bizEntityObjects) {
            //擦除I18nResourceInfoPrefix字段
            bizEntityObject.setI18nResourceInfoPrefix(null);
            //将BE上的编码规则、业务字段替换为新ID
            this.replaceCodeRuleIdAndBusinessFieldId(bizEntityObject);
            //替换为新DBO的ID
            this.replaceDboId(bizEntityObject);
        }
    }

    @Override
    public MimicTypeEnum getMimicTypeEnum() {
        return MimicTypeEnum.NO_CODE_APP_COPY;
    }

    /**
     * 将GspBizEntityObject上的编码规则、业务字段替换为新ID
     */
    private void replaceCodeRuleIdAndBusinessFieldId(GspBizEntityObject bizEntityObject) {
        GspElementCollection elementCollection = bizEntityObject.getContainElements();
        if (elementCollection == null || elementCollection.isEmpty()) {
            return;
        }
        for (int i = 0; i < elementCollection.size(); i++) {
            IGspCommonElement element = elementCollection.getItem(i);
            if (element == null) {
                continue;
            }
            //替换引用的编码规则ID
            if (element.getBillCodeConfig() != null
                    && StringUtils.isNoneBlank(element.getBillCodeConfig().getBillCodeID())) {
                String oldCodeRuleId = element.getBillCodeConfig().getBillCodeID();
                String newCodeRuleId = MimicServiceContextHolder
                        .findConfigDataNewValue(CODE_RULE_CONFIG_ID_PREFIX + oldCodeRuleId);
                if (StringUtils.isBlank(newCodeRuleId)) {
                    throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_BUSINESS_ENTITY_0014, oldCodeRuleId);
                }
                element.getBillCodeConfig().setBillCodeID(newCodeRuleId);
            }
            //替换引用的业务字段ID
            if (StringUtils.isNoneBlank(element.getRefBusinessFieldId())) {
                String oldBusinessFieldId = element.getRefBusinessFieldId();
                String newBusinessFieldId = MimicServiceContextHolder
                        .findConfigDataNewValue(BUSINESS_FIELD_ID_PREFIX + oldBusinessFieldId);
                if (StringUtils.isBlank(newBusinessFieldId)) {
                    throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_BUSINESS_ENTITY_0015, oldBusinessFieldId);
                }
                element.setRefBusinessFieldId(newBusinessFieldId);
            }
        }
    }

    /**
     * 替换为新的DboId
     */
    private void replaceDboId(GspBizEntityObject bizEntityObject) {
        String oldRefObjectName = bizEntityObject.getRefObjectName();
        if (StringUtils.isNoneBlank(oldRefObjectName)) {
            String newRefObjectName = MimicServiceContextHolder
                    .findConfigDataNewValue(DBO_CONFIG_ID_PREFIX + oldRefObjectName);
            if (StringUtils.isBlank(newRefObjectName)) {
                throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_BUSINESS_ENTITY_0016, oldRefObjectName);
            }
            bizEntityObject.setRefObjectName(newRefObjectName);
        }
    }

}
