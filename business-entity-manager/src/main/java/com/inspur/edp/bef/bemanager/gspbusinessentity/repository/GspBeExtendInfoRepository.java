package com.inspur.edp.bef.bemanager.gspbusinessentity.repository;

import com.inspur.edp.bef.bizentity.gspbusinessentity.entity.GspBeExtendInfoEntity;
import io.iec.edp.caf.data.orm.DataRepository;

import java.util.List;

public interface GspBeExtendInfoRepository extends DataRepository<GspBeExtendInfoEntity, String> {

    /**
     * 根据configId获取某条BE扩展信息
     *
     * @param configId
     * @return
     */
    GspBeExtendInfoEntity getBeExtendInfoByConfigId(String configId);

    List<GspBeExtendInfoEntity> getBeExtendInfosByConfigId(String configId);

}
