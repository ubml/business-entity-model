package com.inspur.edp.bef.bemanager.validate.operation;

import com.inspur.edp.bef.bizentity.operation.BizOperation;
import com.inspur.edp.das.commonmodel.util.MessageI18nUtils;

import static com.inspur.edp.cef.designtime.api.validate.common.CheckUtil.exception;
import static com.inspur.edp.cef.designtime.api.validate.common.CheckUtil.isLegality;

public abstract class BizOperationChecker {
    protected final void opException(String code) {
        if (code == null || code.isEmpty())
            return;
        exception(code);
    }

    //be & deter动作编号
    private void checkActionCode(BizOperation bizOperation) {
        if (bizOperation == null)
            return;
        if (bizOperation.getCode() == null || bizOperation.getCode().isEmpty()) {
            opException(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0111"));
        }

        if (isLegality(bizOperation.getCode()))
            return;
        opException(MessageI18nUtils.getMessage("GSP_BEMODEL_INFO_0112", bizOperation.getCode()));
    }

    public final void checkBizOperation(BizOperation bizOperation) {
        checkActionCode(bizOperation);
        checkBizOperationExtension(bizOperation);
    }

    protected void checkBizOperationExtension(BizOperation bizOperation) {

    }
}
