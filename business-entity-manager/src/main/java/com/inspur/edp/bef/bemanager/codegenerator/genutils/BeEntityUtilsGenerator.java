package com.inspur.edp.bef.bemanager.codegenerator.genutils;

import com.inspur.edp.bef.bizentity.GspBizEntityObject;
import com.inspur.edp.cef.designtime.core.utilsgenerator.DataTypeUtilsGenerator;
import com.inspur.edp.das.commonmodel.IGspCommonObject;

public final class BeEntityUtilsGenerator extends DataTypeUtilsGenerator {
    private final GspBizEntityObject bizEntityObject;
    private final String packageName;

    public BeEntityUtilsGenerator(GspBizEntityObject bizEntityObject, String packageName, String basePath) {
        super(bizEntityObject, basePath);
        this.bizEntityObject = bizEntityObject;
        this.packageName = packageName;
    }

    @Override
    protected String getClassName() {
        return bizEntityObject.getCode() + "Utils";
    }

    @Override
    protected String getClassPackage() {
        return packageName;
    }

    @Override
    protected void generateExtendInfos() {
        super.generateExtendInfos();
        generateGetChildObjectDatas();
    }

    private void generateGetChildObjectDatas() {
        if (bizEntityObject.getContainChildObjects() == null || bizEntityObject.getContainChildObjects().isEmpty())
            return;
        for (IGspCommonObject childObject : bizEntityObject.getContainChildObjects()) {
            new EntityGetChildDatasMethodGenerator(childObject.getCode(), getClassInfo()).generate();
        }
    }
}
