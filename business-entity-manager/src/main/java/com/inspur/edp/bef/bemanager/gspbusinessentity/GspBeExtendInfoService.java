package com.inspur.edp.bef.bemanager.gspbusinessentity;

import com.inspur.edp.bef.bemanager.gspbusinessentity.cache.GspBeExtendInfoCacheManager;
import com.inspur.edp.bef.bizentity.exceptions.BusinessEntityErrorCodeEnum;
import com.inspur.edp.bef.bizentity.exceptions.BusinessEntityException;
import com.inspur.edp.bef.bizentity.gspbusinessentity.api.IGspBeExtendInfoService;
import com.inspur.edp.bef.bizentity.gspbusinessentity.entity.GspBeExtendInfo;
import io.iec.edp.caf.commons.exception.CAFRuntimeException;
import io.iec.edp.caf.rpc.api.service.RpcClient;
import io.iec.edp.caf.rpc.api.support.Type;
import org.springframework.dao.IncorrectResultSizeDataAccessException;

import java.util.LinkedHashMap;
import java.util.List;


public class GspBeExtendInfoService implements IGspBeExtendInfoService {

    private static final String Lcm_SU = "Lcm";
    private static final String BE_EXTEND_INFO_SERVICE = "com.inspur.edp.bef.extendinfo.server.api.IGspBeExtendInfoRpcService";
    // 缓存管理器，common层只负责读取缓存，缓存的更新由LCM下“com.inspur.edp.bef.extendinfo.server.core.GspBeExtendInfoRpcServiceImp”负责
    private final GspBeExtendInfoCacheManager gspBeExtendInfoCacheManager;
    private final RpcClient rpcClient;

    public GspBeExtendInfoService(GspBeExtendInfoCacheManager gspBeExtendInfoCacheManager, RpcClient rpcClient) {
        this.gspBeExtendInfoCacheManager = gspBeExtendInfoCacheManager;
        this.rpcClient = rpcClient;
    }

    @Override
    public GspBeExtendInfo getBeExtendInfo(String id) {
        GspBeExtendInfo gspBeExtendInfoFromCache = gspBeExtendInfoCacheManager.get(id);
        if (gspBeExtendInfoFromCache != null) {
            return gspBeExtendInfoFromCache;
        }
        LinkedHashMap<String, Object> params = new LinkedHashMap<>();
        params.put("id", id);
        try {
            return rpcClient.invoke(GspBeExtendInfo.class,
                    getMethodString("getBeExtendInfo"), Lcm_SU, params, null);
        } catch (CAFRuntimeException cafRuntimeException) {
            throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_COMMON_0007, cafRuntimeException, getMethodString("getBeExtendInfo"));
        }
    }

    @Override
    public GspBeExtendInfo getBeExtendInfoByConfigId(String id) {
        GspBeExtendInfo gspBeExtendInfoFromCache = gspBeExtendInfoCacheManager.get(id);
        if (gspBeExtendInfoFromCache != null) {
            return gspBeExtendInfoFromCache;
        }
        try {
            LinkedHashMap<String, Object> params = new LinkedHashMap<>();
            params.put("configId", id);
            return rpcClient.invoke(GspBeExtendInfo.class,
                    getMethodString("getBeExtendInfoByConfigId"), Lcm_SU, params, null);
        } catch (IncorrectResultSizeDataAccessException e) {
            throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_BUSINESS_ENTITY_0042, e, id);
        }
    }

    @Override
    public List<GspBeExtendInfo> getBeExtendInfos() {
        try {
            LinkedHashMap<String, Object> params = new LinkedHashMap<>();
            Type<List> type = getBeExtendInfoListType();
            return (List) rpcClient.invoke(type,
                    getMethodString("getBeExtendInfos"), Lcm_SU, params, null);
        } catch (CAFRuntimeException cafRuntimeException) {
            throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_COMMON_0007, cafRuntimeException, getMethodString("getBeExtendInfos"));
        }

    }

    @Override
    public void saveGspBeExtendInfos(List<GspBeExtendInfo> infos) {
        LinkedHashMap<String, Object> params = new LinkedHashMap<>();
        params.put("infos", infos);
        try {
            rpcClient.invoke(Void.class,
                    getMethodString("saveGspBeExtendInfos"), Lcm_SU, params, null);
        } catch (CAFRuntimeException cafRuntimeException) {
            throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_COMMON_0007, cafRuntimeException, getMethodString("saveGspBeExtendInfos"));
        }
    }

    @Override
    public void deleteBeExtendInfo(String id) {
        if (id == null || id.isEmpty())
            return;
        LinkedHashMap<String, Object> params = new LinkedHashMap<>();
        params.put("id", id);
        try {
            rpcClient.invoke(Void.class,
                    getMethodString("deleteBeExtendInfo"), Lcm_SU, params, null);
        } catch (CAFRuntimeException cafRuntimeException) {
            throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_COMMON_0007, cafRuntimeException, getMethodString("deleteBeExtendInfo"));
        }
    }

    private String getMethodString(String methodName) {
        return BE_EXTEND_INFO_SERVICE + "." + methodName;
    }

    private Type<List> getBeExtendInfoListType() {
        return new Type<List>(List.class, GspBeExtendInfo.class);
    }
}
