package com.inspur.edp.bef.bemanager.service;

import com.inspur.edp.bef.bemanager.util.CheckInfoUtil;
import com.inspur.edp.bef.bizentity.GspBizEntityElement;
import com.inspur.edp.bef.bizentity.GspBizEntityObject;
import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.bef.bizentity.common.InternalActionUtil;
import com.inspur.edp.bef.bizentity.exceptions.BusinessEntityErrorCodeEnum;
import com.inspur.edp.bef.bizentity.exceptions.BusinessEntityException;
import com.inspur.edp.bef.bizentity.operation.BizOperation;
import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.designtime.api.element.GspAssociation;
import com.inspur.edp.das.commonmodel.IGspCommonObject;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.api.entity.MetadataReference;
import com.inspur.edp.lcm.metadata.api.service.MetadataService;
import com.inspur.edp.lcm.metadata.api.service.RefCommonService;
import com.inspur.edp.lcm.metadata.spi.IMetadataReferenceManager;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

import java.util.ArrayList;
import java.util.List;


public class MetadataReferenceService implements IMetadataReferenceManager {

    private void buildMetadataReference(GspMetadata metadata) {
        if (metadata != null && metadata.getRefs() == null) {
            metadata.setRefs(new ArrayList<>());
        }

        MetadataService metadataService = SpringBeanUtils.getBean(MetadataService.class);

        GspBusinessEntity bizEntity = (GspBusinessEntity) metadata.getContent();

        if (!CheckInfoUtil.checkNull(bizEntity.getDependentEntityId())) {
            buildReference(bizEntity.getDependentEntityId(), metadata, metadataService);
        }

        if (bizEntity.getBizMgrActions() != null && bizEntity.getBizMgrActions().getCount() > 0) {

            for (BizOperation mgrAction : bizEntity.getBizMgrActions()) {

                if (!InternalActionUtil.InternalMgrActionIDs.contains(mgrAction.getID())) {
                    buildCompReference(mgrAction, metadata, metadataService);
                }
            }
        }
        dealObjectReference(bizEntity.getMainObject(), metadata, metadataService);
    }


    public void buildCompReference(BizOperation operation, GspMetadata metadata, MetadataService metadataService) {

        buildReference(operation.getComponentId(), metadata, metadataService);
    }


    private void buildReference(String refMetadataId, GspMetadata metadata, MetadataService metadataService) {

        if (CheckInfoUtil.checkNull(refMetadataId)) {
            return;
        }

        for (MetadataReference metadataReference : metadata.getRefs()) {
            if (refMetadataId.equals(metadataReference.getDependentMetadata().getId())) {
                return;
            }
        }
        RefCommonService refService = (RefCommonService) SpringBeanUtils.getBean(RefCommonService.class);
        GspMetadata refMetaData = refService.getRefMetadata(refMetadataId);
        if (refMetaData == null) {
            throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_BUSINESS_ENTITY_0054, refMetadataId);
        }
        MetadataReference metadataReference = new MetadataReference();
        metadataReference.setMetadata(metadata.getHeader());
        metadataReference.setDependentMetadata(refMetaData.getHeader());
        metadata.getRefs().add(metadataReference);
    }


    private void dealObjectReference(GspBizEntityObject obj, GspMetadata metadata, MetadataService metadataService) {
        if (obj.getContainElements() != null && obj.getContainElements().getCount() > 0) {
            for (IGspCommonField ele : obj.getContainElements()) {
                dealElementReference((GspBizEntityElement) ele, metadata, metadataService);
            }
        }
        if (obj.getBizActions() != null && obj.getBizActions().getCount() > 0) {

            for (BizOperation action : obj.getBizActions()) {

                if (!InternalActionUtil.InternalBeActionIDs.contains(action.getID())) {
                    buildCompReference(action, metadata, metadataService);
                }
            }
        }

        if (obj.getDeterminations() != null && obj.getDeterminations().getCount() > 0) {
            for (BizOperation determination : obj.getDeterminations()) {
                buildCompReference(determination, metadata, metadataService);
            }
        }
        if (obj.getValidations() != null && obj.getValidations().getCount() > 0) {
            for (BizOperation validation : obj.getValidations()) {
                buildCompReference(validation, metadata, metadataService);
            }
        }
        if (obj.getContainChildObjects() != null && obj.getContainChildObjects().getCount() > 0) {
            for (IGspCommonObject childObj : obj.getContainChildObjects()) {
                dealObjectReference((GspBizEntityObject) childObj, metadata, metadataService);
            }
        }
    }


    private void dealElementReference(GspBizEntityElement element, GspMetadata metadata, MetadataService metadataService) {
        if (element.getIsRef()) {
            return;
        }
        if (element.getIsUdt()) {

            buildReference(element.getUdtID(), metadata, metadataService);
        } else if (element.getHasAssociation()) {

            for (GspAssociation association : element.getChildAssociations()) {
                buildAssociationReference(association, metadata, metadataService);
            }
        }
    }


    private void buildAssociationReference(GspAssociation association, GspMetadata metadata,
                                           MetadataService metadataService) {
        buildReference(association.getRefModelID(), metadata, metadataService);
    }


    public List<MetadataReference> getConstraint(GspMetadata gspMetadata) {
        if (gspMetadata.getRefs() != null) {
            gspMetadata.getRefs().clear();
        }
        buildMetadataReference(gspMetadata);
        List<MetadataReference> list = new ArrayList<MetadataReference>();
        if (gspMetadata.getRefs() != null && !gspMetadata.getRefs().isEmpty()) {
            list.addAll(gspMetadata.getRefs());
        }
        return list;
    }
}
