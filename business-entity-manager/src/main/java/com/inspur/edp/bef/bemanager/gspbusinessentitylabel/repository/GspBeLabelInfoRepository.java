package com.inspur.edp.bef.bemanager.gspbusinessentitylabel.repository;

import com.inspur.edp.bef.bizentity.gspbusinessentitylabel.entity.GspBeLabelInfo;
import io.iec.edp.caf.data.orm.DataRepository;

public interface GspBeLabelInfoRepository extends DataRepository<GspBeLabelInfo, String> {

}
