package com.inspur.edp.bef.bemanager.befdtconsistencycheckevent;

import com.inspur.edp.bef.bizentity.dtconsistencycheck.actioneventargs.DeletingActionEventArgs;
import com.inspur.edp.bef.bizentity.dtconsistencycheck.entityeventargs.RemovingEntityEventArgs;
import com.inspur.edp.bef.bizentity.dtconsistencycheck.fieldeventargs.RemovingFieldEventArgs;
import io.iec.edp.caf.commons.event.EventBroker;
import io.iec.edp.caf.commons.event.config.EventListenerSettings;

public class BefDtConsistencyCheckEventBroker extends EventBroker {

    private final BefDtConsistencyCheckEventManager eventManager;

    public BefDtConsistencyCheckEventBroker(EventListenerSettings settings, BefDtConsistencyCheckEventManager eventManager) {
        super(settings);
        this.eventManager = eventManager;
        this.init();
    }

    @Override
    protected void onInit() {
        this.getEventManagerCollection().add(eventManager);
    }


    public void fireRemovingEntity(RemovingEntityEventArgs args) {
        eventManager.fireRemovingEntity(args);
    }

    public void fireRemovingField(RemovingFieldEventArgs args) {
        eventManager.fireRemovingField(args);
    }

    public void fireDeletingAction(DeletingActionEventArgs args) {
        eventManager.fireDeletingAction(args);
    }
}
