package com.inspur.edp.bef.bemanager.codegenerator.genutils;

import com.inspur.edp.cef.designtime.core.utilsgenerator.JavaAccessModifier;
import com.inspur.edp.cef.designtime.core.utilsgenerator.JavaClassInfo;
import com.inspur.edp.cef.designtime.core.utilsgenerator.MethodInfo;
import com.inspur.edp.cef.designtime.core.utilsgenerator.ParameterInfo;
import com.inspur.edp.cef.designtime.core.utilsgenerator.TypeRefInfo;
import com.inspur.edp.cef.entity.entity.IEntityData;
import com.inspur.edp.cef.entity.entity.IEntityDataCollection;

public class EntityGetChildDatasMethodGenerator {
    private final String childNodeCode;
    private final JavaClassInfo classInfo;
    private final MethodInfo methodInfo;

    public EntityGetChildDatasMethodGenerator(String childNodeCode, JavaClassInfo classInfo) {

        this.childNodeCode = childNodeCode;
        this.classInfo = classInfo;
        methodInfo = new MethodInfo();
    }

    public void generate() {
        methodInfo.setMethodName("get" + childNodeCode + "s");
        methodInfo.setReturnType(new TypeRefInfo(IEntityDataCollection.class));
        methodInfo.getAccessModifiers().add(JavaAccessModifier.Public);
        methodInfo.getAccessModifiers().add(JavaAccessModifier.Static);
        methodInfo.getParameters().add(new ParameterInfo("data", new TypeRefInfo(IEntityData.class)));
        generateMethodBodies();
        classInfo.addMethodInfo(methodInfo);
    }

    private void generateMethodBodies() {
        classInfo.getImportInfos().addImportPackage("com.inspur.edp.cef.entity.entity.EntityDataUtils");
        methodInfo.getMethodBodies().add("return EntityDataUtils.getChildDatas(data,\"" + childNodeCode + "\");");
    }
}
