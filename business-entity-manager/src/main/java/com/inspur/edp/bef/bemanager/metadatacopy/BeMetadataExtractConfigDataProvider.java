package com.inspur.edp.bef.bemanager.metadatacopy;

import com.inspur.edp.bef.bizentity.GspBizEntityObject;
import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.das.commonmodel.IGspCommonElement;
import com.inspur.edp.das.commonmodel.collection.GspElementCollection;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.metadata.rtcustomization.api.entity.ConfigDataDependenciesEnum;
import com.inspur.edp.metadata.rtcustomization.api.entity.MimicTypeEnum;
import com.inspur.edp.metadata.rtcustomization.spi.ExtractConfigDataSpi;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 该类配置在server\config\platform\common\lcm_rtcustomization.json的ExtractConfigData节点下，用于复制应用
 * 当前仅支持零代码场景在不同bo下的复制
 * 功能描述：提取依赖的配置数据标识信息（如：编码规则id）
 *
 * @author Kaixuan Shi
 * @since 2023/4/11
 */
public class BeMetadataExtractConfigDataProvider implements ExtractConfigDataSpi {

    @Override
    public Map<ConfigDataDependenciesEnum, Set<String>> extractConfigDataDependencies(GspMetadata gspMetadata) {
        GspBusinessEntity be = (GspBusinessEntity) gspMetadata.getContent();
        Map<ConfigDataDependenciesEnum, Set<String>> configDataIds = new HashMap<>();
        List<GspBizEntityObject> bizEntityObjects = be.getAllNodes();
        if (bizEntityObjects.isEmpty()) {
            return configDataIds;
        }
        for (GspBizEntityObject bizEntityObject : bizEntityObjects) {
            this.extractElementConfigData(configDataIds, bizEntityObject);
        }
        return configDataIds;
    }

    @Override
    public MimicTypeEnum getMimicTypeEnum() {
        return MimicTypeEnum.NO_CODE_APP_COPY;
    }

    private void extractElementConfigData(Map<ConfigDataDependenciesEnum, Set<String>> configDataIds,
                                          GspBizEntityObject bizEntityObject) {
        GspElementCollection elementCollection = bizEntityObject.getContainElements();
        if (elementCollection == null || elementCollection.isEmpty()) {
            return;
        }
        for (int i = 0; i < elementCollection.size(); i++) {
            IGspCommonElement element = elementCollection.getItem(i);
            if (element == null) {
                continue;
            }
            if (element.getBillCodeConfig() != null
                    && StringUtils.isNoneBlank(element.getBillCodeConfig().getBillCodeID())) {
                configDataIds.putIfAbsent(ConfigDataDependenciesEnum.CODE_RULE, new HashSet<>());
                configDataIds.get(ConfigDataDependenciesEnum.CODE_RULE)
                        .add(element.getBillCodeConfig().getBillCodeID());
            }
            if (StringUtils.isNoneBlank(element.getRefBusinessFieldId())) {
                configDataIds.putIfAbsent(ConfigDataDependenciesEnum.BUSINESS_FIELD, new HashSet<>());
                configDataIds.get(ConfigDataDependenciesEnum.BUSINESS_FIELD).add(element.getRefBusinessFieldId());
            }
        }
    }
}
