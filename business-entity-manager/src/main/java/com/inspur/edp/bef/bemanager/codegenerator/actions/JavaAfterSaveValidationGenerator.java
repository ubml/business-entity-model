package com.inspur.edp.bef.bemanager.codegenerator.actions;


import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.bef.bizentity.operation.BizOperation;

public class JavaAfterSaveValidationGenerator extends JavaValidationGenerator {
    @Override
    protected String getBaseClassName() {
        return "AbstractAfterSaveValidation";
    }

    public JavaAfterSaveValidationGenerator(GspBusinessEntity be, BizOperation operation, String nameSpace, String path) {
        super(be, operation, nameSpace, path);

    }
}