package com.inspur.edp.bef.bemanager.codegenerator;


import com.inspur.edp.bef.bemanager.codegenerator.actions.JavaAfterSaveValidationGenerator;
import com.inspur.edp.bef.bemanager.codegenerator.actions.JavaB4QueryDeterminationGenerator;
import com.inspur.edp.bef.bemanager.codegenerator.actions.JavaB4RetrieveDeterminationGenerator;
import com.inspur.edp.bef.bemanager.codegenerator.actions.JavaB4SaveDeterminationGenerator;
import com.inspur.edp.bef.bemanager.codegenerator.actions.JavaBaseCommonCompCodeGen;
import com.inspur.edp.bef.bemanager.codegenerator.actions.JavaBaseCompCodeGenerator;
import com.inspur.edp.bef.bemanager.codegenerator.actions.JavaBeActionGenerator;
import com.inspur.edp.bef.bemanager.codegenerator.actions.JavaBeMgrActionGenerator;
import com.inspur.edp.bef.bemanager.codegenerator.actions.JavaDeterminationGenerator;
import com.inspur.edp.bef.bemanager.codegenerator.actions.JavaTccActionGenerator;
import com.inspur.edp.bef.bemanager.codegenerator.actions.JavaValidationGenerator;
import com.inspur.edp.bef.bemanager.util.DataValidatorUtil;
import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.bef.bizentity.beenum.BETriggerTimePointType;
import com.inspur.edp.bef.bizentity.beenum.RequestNodeTriggerType;
import com.inspur.edp.bef.bizentity.operation.BizOperation;
import com.inspur.edp.bef.bizentity.operation.Determination;
import com.inspur.edp.bef.bizentity.operation.Validation;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

///#region C# module CmpCodeGeneratorFactory
public class JavaCmpCodeGeneratorFactory {
    public static JavaBaseCommonCompCodeGen JavaGetGenerator(GspBusinessEntity be, BizOperation operation, String nameSpace, String path) {
        switch (operation.getOpType()) {
            case Determination:
                return getDeterminationGenerator(be, operation, nameSpace, path);
            case Validation:
                return getValidationGenerator(be, operation, nameSpace, path);
            case BizAction:
                return new JavaBeActionGenerator(be, operation, nameSpace, path);
            case BizMgrAction:
                return new JavaBeMgrActionGenerator(be, operation, nameSpace, path);
            case TccAction:
                return new JavaTccActionGenerator(be, operation, nameSpace, path);
        }

        return null;
    }

    private static JavaBaseCompCodeGenerator getValidationGenerator(GspBusinessEntity be, BizOperation operation, String nameSpace, String path) {
        if (isAfterSave(operation)) {
            return new JavaAfterSaveValidationGenerator(be, operation, nameSpace, path);
        } else {
            return new JavaValidationGenerator(be, operation, nameSpace, path);
        }
    }

    private static boolean isAfterSave(BizOperation operation) {

        HashMap<BETriggerTimePointType, EnumSet<RequestNodeTriggerType>> trigger = ((Validation) operation).getValidationTriggerPoints();
        if (trigger == null) {
            return false;
        }
        boolean afterSave = false;

        for (Map.Entry<BETriggerTimePointType, EnumSet<RequestNodeTriggerType>> pair : trigger.entrySet()) {
            if (pair.getKey() == BETriggerTimePointType.AfterSave) {

                if (pair.getValue().equals(EnumSet.of(RequestNodeTriggerType.None))) {
                    return false;
                } else {
                    afterSave = true;
                }
            } else {
                if (pair.getValue().equals(EnumSet.noneOf(RequestNodeTriggerType.class)))
                    continue;
                if (!pair.getValue().equals(EnumSet.of(RequestNodeTriggerType.None))) {
                    return false;
                }
            }
        }
        return afterSave;
    }

    private static JavaBaseCompCodeGenerator getDeterminationGenerator(GspBusinessEntity be, BizOperation operation, String nameSpace, String path) {
        if (((Determination) operation).getTriggerTimePointType().equals(EnumSet.of(BETriggerTimePointType.BeforeQuery))) {
            return new JavaB4QueryDeterminationGenerator(be, operation, nameSpace, path);

        } else if (((Determination) operation).getTriggerTimePointType().equals(EnumSet.of(BETriggerTimePointType.BeforeCheck))) {
            return new JavaB4SaveDeterminationGenerator(be, operation, nameSpace, path);

        } else if (((Determination) operation).getTriggerTimePointType().equals(EnumSet.of(BETriggerTimePointType.BeforeRetrieve))) {
            return new JavaB4RetrieveDeterminationGenerator(be, operation, nameSpace, path);

        } else {
            return new JavaDeterminationGenerator(be, operation, nameSpace, path);

        }
    }

    public static JavaBaseCommonCompCodeGen JavaGetChildNodeGenerator(GspBusinessEntity be, String childNodeCode, BizOperation operation, String nameSpace, String path) {
        DataValidatorUtil.CheckForEmptyString(childNodeCode, "childNodeCode");

        JavaBaseCommonCompCodeGen rez = JavaGetGenerator(be, operation, nameSpace, path);
        rez.SetChild(childNodeCode);
        return rez;
    }
}

///#endregion
