package com.inspur.edp.cef.designtime.api.i18n.merger;

import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.designtime.api.i18n.context.ICefResourceMergeContext;

public abstract class AssoRefFieldResourceMerger extends CefFieldResourceMerger {
    private final IGspCommonField commonField;

    protected AssoRefFieldResourceMerger(IGspCommonField commonField,
                                         ICefResourceMergeContext context) {
        super(commonField, context);
        this.commonField = commonField;
    }
}
