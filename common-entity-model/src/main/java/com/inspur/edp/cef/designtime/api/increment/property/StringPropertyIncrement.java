package com.inspur.edp.cef.designtime.api.increment.property;

public class StringPropertyIncrement extends NativePropertyIncrement {
    public StringPropertyIncrement(boolean hasIncrement) {
        super(hasIncrement);
    }

    @Override
    public final IncrementPropType getPropertyType() {
        return IncrementPropType.String;
    }

}
