package com.inspur.edp.cef.designtime.api.element;

/**
 * The Definition Of FieldCollectionType
 *
 * @ClassName: FieldCollectionType
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public enum FieldCollectionType {
    /**
     * 未使用集合形式
     */
    None,
    /**
     * 列表
     */
    List,
    /**
     * 数组
     */
    Array;

    public int getValue() {
        return this.ordinal();
    }

    public static FieldCollectionType forValue(int value) {
        return values()[value];
    }
}