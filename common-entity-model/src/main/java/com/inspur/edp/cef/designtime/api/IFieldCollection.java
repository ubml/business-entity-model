package com.inspur.edp.cef.designtime.api;

/**
 * The Intereface Of Field Collecion
 *
 * @ClassName: DimensionInfo
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public interface IFieldCollection extends java.util.List<IGspCommonField> {

    /**
     * Get Field By LabelId
     *
     * @param labelId
     * @return Field  With The Input LabelId
     */
    IGspCommonField getByLabelId(String labelId);
}
