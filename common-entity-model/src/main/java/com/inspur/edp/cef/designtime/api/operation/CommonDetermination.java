package com.inspur.edp.cef.designtime.api.operation;

import com.inspur.edp.cef.designtime.api.collection.DtmElementCollection;

import java.util.EnumSet;
import java.util.HashMap;

/**
 * The Definition Of Common Determination
 *
 * @ClassName: CommonDetermination
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class CommonDetermination extends CommonOperation {
    private DtmElementCollection rqtElements;

    public HashMap<String, DtmElementCollection> getRequestChildElements() {
        return requestChildElements;
    }

    public void setRequestChildElements(HashMap<String, DtmElementCollection> requestChildElements) {
        this.requestChildElements = requestChildElements;
    }

    private java.util.HashMap<String, DtmElementCollection> requestChildElements;
    private EnumSet<ExecutingDataStatus> status = EnumSet.of(ExecutingDataStatus.forValue(0));

    public CommonTriggerPointType getTriggerPointType() {
        return triggerPointType;
    }

    public void setTriggerPointType(CommonTriggerPointType triggerPointType) {
        this.triggerPointType = triggerPointType;
    }

    private CommonTriggerPointType triggerPointType = CommonTriggerPointType.forValue(0);

    public final EnumSet<ExecutingDataStatus> getGetExecutingDataStatus() {
        return status;
    }

    public final void setGetExecutingDataStatus(EnumSet<ExecutingDataStatus> value) {
        status = value;
    }

    public final DtmElementCollection getRequestElements() {
        if (rqtElements == null) {
            rqtElements = new DtmElementCollection();
        }
        return rqtElements;
    }

    public final void setRequestElements(DtmElementCollection value) {
        rqtElements = value;
    }

    public HashMap<String, ChildDtmTriggerInfo> getChildTriggerInfo() {
        return childTriggerInfo;
    }

    public void setChildTriggerInfo(HashMap<String, ChildDtmTriggerInfo> childTriggerInfo) {
        this.childTriggerInfo = childTriggerInfo;
    }

    private HashMap<String, ChildDtmTriggerInfo> childTriggerInfo;
}