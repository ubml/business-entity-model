package com.inspur.edp.cef.designtime.api.element;

public enum AssoConditionType {
    //a.code = 'test'
    AssoValue,
    //a.code = b.code
    AssoField
}
