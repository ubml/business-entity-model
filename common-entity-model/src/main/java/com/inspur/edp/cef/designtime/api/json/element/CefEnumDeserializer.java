package com.inspur.edp.cef.designtime.api.json.element;


import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;

/**
 * The Json Parser Of Cef Enum
 *
 * @ClassName: CefEnumDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class CefEnumDeserializer<T extends Enum<T>> extends JsonDeserializer<T> {

    private final Class<T> enumClass;
    private final T[] enumValues;

    CefEnumDeserializer(Class<T> enumClass, T[] enumValues) {
        this.enumClass = enumClass;
        this.enumValues = enumValues;
    }

    @Override
    public T deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) {
        return SerializerUtils.readPropertyValue_Enum(jsonParser, enumClass, enumValues);
    }
}
