package com.inspur.edp.cef.designtime.api.element.increment;

import com.inspur.edp.cef.designtime.api.increment.AbstractIncrement;

/**
 * The Definition Of DimensionInfo
 *
 * @ClassName: DimensionInfo
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public abstract class GspCommonFieldIncrement extends AbstractIncrement {
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
