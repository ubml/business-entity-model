package com.inspur.edp.cef.designtime.api.collection;

/**
 * The Collection Of Validation Element
 *
 * @ClassName: ValElementCollection
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class ValElementCollection extends BaseList<String> implements Cloneable {

    private static final long serialVersionUID = 1L;
}