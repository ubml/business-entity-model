package com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.parser;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.entity.ControlRuleDefItem;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.entity.ControlRuleDefinition;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.serializer.ControlRuleDefNames;
import com.inspur.edp.cef.designtime.api.exceptions.CommonEntityErrorCodeEnum;
import com.inspur.edp.cef.designtime.api.exceptions.CommonEntityException;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;

import java.io.IOException;

import static com.fasterxml.jackson.core.JsonToken.END_ARRAY;
import static com.fasterxml.jackson.core.JsonToken.FIELD_NAME;

/**
 * The JsonParser Of ControlRuleDef
 *
 * @ClassName: ControlRuleDefParser
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class ControlRuleDefParser<T extends ControlRuleDefinition> extends JsonDeserializer<T> {
    @Override
    public T deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        T ruleDefinition = createRuleDefinition();
        SerializerUtils.readStartObject(jsonParser);
        readSelfInfos(ruleDefinition, jsonParser, deserializationContext);
        SerializerUtils.readEndObject(jsonParser);
        return ruleDefinition;
    }


    private void readSelfInfos(T ruleDefinition, JsonParser jsonParser, DeserializationContext deserializationContext) {
        while (jsonParser.getCurrentToken() == FIELD_NAME) {
            String propName = SerializerUtils.readPropertyName(jsonParser);
            switch (propName) {
                case ControlRuleDefNames.RuleObjectType:
                    ruleDefinition.setRuleObjectType(SerializerUtils.readPropertyValue_String(jsonParser));
                    break;
                case ControlRuleDefNames.SelfControlRules:
                    readSelfControlRules(ruleDefinition, jsonParser, deserializationContext);
                    break;
                case ControlRuleDefNames.ChildControlRules:
                    readChildControlRules(ruleDefinition, jsonParser, deserializationContext);
                    break;
                default:
                    if (!readExtendControlRuleProperty(ruleDefinition, propName, jsonParser, deserializationContext)) {
                        throw CommonEntityException.createException(CommonEntityErrorCodeEnum.GSP_BEMODEL_COMMON_ENTITY_0006, "ControlRuleDefParser", propName);
                    }
                    break;
            }
        }
    }

    private void readSelfControlRules(ControlRuleDefinition ruleDefinition, JsonParser jsonParser, DeserializationContext deserializationContext) {
        SerializerUtils.readStartArray(jsonParser);
        JsonToken tokentype = jsonParser.getCurrentToken();
        if (tokentype != END_ARRAY) {
            while (jsonParser.getCurrentToken() == tokentype || jsonParser.getLastClearedToken() == tokentype) {
                ControlDefItemParser deserializer = new ControlDefItemParser();
                try {
                    ControlRuleDefItem item = deserializer.deserialize(jsonParser, null);
                    ruleDefinition.getSelfControlRules().put(item.getRuleName(), item);
                } catch (IOException e) {
                    throw CommonEntityException.createException(CommonEntityErrorCodeEnum.GSP_BEMODEL_JSON_0001, e, "ControlRuleDefItem");
                }

            }
        }
        SerializerUtils.readEndArray(jsonParser);
    }

    private void readChildControlRules(ControlRuleDefinition ruleDefinition, JsonParser jsonParser, DeserializationContext deserializationContext) {
        SerializerUtils.readStartObject(jsonParser);
        while (jsonParser.getCurrentToken() == FIELD_NAME) {
            String childTypeName = SerializerUtils.readPropertyName(jsonParser);
            JsonDeserializer childDeserializer = getChildDeserializer(childTypeName);
            try {
                ControlRuleDefinition childRuleDefinition = (ControlRuleDefinition) childDeserializer.deserialize(jsonParser, null);
                ruleDefinition.getChildControlRules().put(childTypeName, childRuleDefinition);
            } catch (IOException e) {
                throw CommonEntityException.createException(CommonEntityErrorCodeEnum.GSP_BEMODEL_JSON_0002, e, "ChildControlRule", childTypeName);
            }
        }

        SerializerUtils.readEndObject(jsonParser);
    }

    protected Class getChildTypes(String childTypeName) {
        return ControlRuleDefinition.class;
    }

    protected JsonDeserializer getChildDeserializer(String childTypeName) {
        return new ControlRuleDefParser();
    }

    protected boolean readExtendControlRuleProperty(T ruleDefinition, String propName, JsonParser jsonParser, DeserializationContext deserializationContext) {
        return false;
    }

    protected T createRuleDefinition() {
        return (T) new ControlRuleDefinition();
    }
}
