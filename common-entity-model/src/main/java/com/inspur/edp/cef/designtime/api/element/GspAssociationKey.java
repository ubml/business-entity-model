package com.inspur.edp.cef.designtime.api.element;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.inspur.edp.cef.designtime.api.json.CefNames;

/**
 * The Definition Of AssociationKey
 *
 * @ClassName: AssociationKey
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class GspAssociationKey implements Cloneable {
    private String sourceElement;
    private String sourceElementDisplay;
    private String targetElement;
    private String targetElementDisplay;
    private String refdataModelName;

    /**
     * 创建关联外键
     */
    public GspAssociationKey() {

    }

    /**
     * 源元素ID，子对象关联字段的元素ID。
     */
    @JsonProperty(CefNames.SourceElement)
    public final String getSourceElement() {
        return sourceElement;
    }

    public final void setSourceElement(String value) {
        sourceElement = value;
    }

    /**
     * 来源字段显示名称
     */
    @JsonProperty(CefNames.SourceElementDisplay)
    public final String getSourceElementDisplay() {
        return sourceElementDisplay;
    }

    public final void setSourceElementDisplay(String value) {
        sourceElementDisplay = value;
    }

    /**
     * 目标元素ID，父对象关联字段的元素ID。
     */
    @JsonProperty(CefNames.TargetElement)
    public final String getTargetElement() {
        return targetElement;
    }

    public final void setTargetElement(String value) {
        targetElement = value;
    }

    /**
     * 目的字段显示名称
     */
    @JsonProperty(CefNames.TargetElementDisplay)
    public final String getTargetElementDisplay() {
        return targetElementDisplay;
    }

    public final void setTargetElementDisplay(String value) {
        targetElementDisplay = value;
    }

    /**
     * RefDataModelName,实现需要，要来在添加引用列时取得对应的DataModel
     */
    @JsonProperty(CefNames.RefDataModelName)
    public final String getRefDataModelName() {
        return refdataModelName;
    }

    public final void setRefDataModelName(String value) {
        refdataModelName = value;
    }

    /// region ICloneable Members

    /**
     * 克隆
     *
     * @return
     * @throws CloneNotSupportedException
     */
    public final GspAssociationKey clone() {
        Object tempVar;
        try {
            tempVar = super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
        GspAssociationKey newAssKey = (GspAssociationKey) tempVar;
        newAssKey.sourceElement = this.sourceElement;
        newAssKey.sourceElementDisplay = this.sourceElementDisplay;
        newAssKey.targetElement = this.targetElement;
        newAssKey.targetElementDisplay = this.targetElementDisplay;
        newAssKey.refdataModelName = this.refdataModelName;
        return newAssKey;
    }

    /// endregion
}