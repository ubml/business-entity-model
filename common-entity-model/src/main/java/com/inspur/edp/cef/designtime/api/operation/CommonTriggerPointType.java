package com.inspur.edp.cef.designtime.api.operation;

public enum CommonTriggerPointType {
    /**
     * 不执行,用于判断时机比较结果
     */
    None(0),

    /**
     * 数据更新后
     * 可以操作持久化或非持久化属性和Node
     */
    AfterModify(1),

    /**
     * 一致性检查前，保存中进行Validation之前进行，是修改数据的最后时机
     */
    BeforeSave(2);

    private final int intValue;
    private static java.util.HashMap<Integer, CommonTriggerPointType> mappings;

    private synchronized static java.util.HashMap<Integer, CommonTriggerPointType> getMappings() {
        if (mappings == null) {
            mappings = new java.util.HashMap<Integer, CommonTriggerPointType>();
        }
        return mappings;
    }

    private CommonTriggerPointType(int value) {
        intValue = value;
        CommonTriggerPointType.getMappings().put(value, this);
    }

    public int getValue() {
        return intValue;
    }

    public static CommonTriggerPointType forValue(int value) {
        return getMappings().get(value);
    }
}
