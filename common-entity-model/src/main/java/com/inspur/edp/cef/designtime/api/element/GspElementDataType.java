package com.inspur.edp.cef.designtime.api.element;

/**
 * The Definition Of ElementDataType
 *
 * @ClassName: ElementDataType
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public enum GspElementDataType {
    /**
     * 字符串
     */
    String(0),
    /**
     * 长文本
     */
    Text(1),
    /**
     * 整形
     */
    Integer(2),
    /**
     * 十进制
     */
    Decimal(3),
    /**
     * 布尔型
     */
    Boolean(4),
    /**
     * 日期型
     */
    Date(5),
    /**
     * 日期时间型
     */
    DateTime(6),
    /**
     * 二进制
     */
    Binary(7);

    private int intValue;
    private static java.util.HashMap<Integer, GspElementDataType> mappings;

    private synchronized static java.util.HashMap<Integer, GspElementDataType> getMappings() {
        if (mappings == null) {
            mappings = new java.util.HashMap<Integer, GspElementDataType>();
        }
        return mappings;
    }

    private GspElementDataType(int value) {
        intValue = value;
        GspElementDataType.getMappings().put(value, this);
    }

    public int getValue() {
        return intValue;
    }

    public static GspElementDataType forValue(int value) {
        return getMappings().get(value);
    }
}