package com.inspur.edp.cef.designtime.api.operation;

import com.inspur.edp.cef.designtime.api.collection.DtmElementCollection;

import java.util.EnumSet;

public class ChildDtmTriggerInfo {
    private DtmElementCollection requestChildElements;
    private EnumSet<ExecutingDataStatus> status = EnumSet.of(ExecutingDataStatus.forValue(0));

    public DtmElementCollection getRequestChildElements() {
        return requestChildElements;
    }

    public void setRequestChildElements(DtmElementCollection requestChildElements) {
        this.requestChildElements = requestChildElements;
    }

    public final EnumSet<ExecutingDataStatus> getGetExecutingDataStatus() {
        return status;
    }

    public final void setGetExecutingDataStatus(EnumSet<ExecutingDataStatus> value) {
        status = value;
    }
}

