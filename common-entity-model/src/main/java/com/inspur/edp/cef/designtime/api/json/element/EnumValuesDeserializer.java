package com.inspur.edp.cef.designtime.api.json.element;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.inspur.edp.cef.designtime.api.collection.GspEnumValueCollection;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;

import java.io.IOException;

public class EnumValuesDeserializer extends JsonDeserializer<GspEnumValueCollection> {
    @Override
    public GspEnumValueCollection deserialize(JsonParser jsonParser, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        GspEnumValueCollection collection = new GspEnumValueCollection();
        GspEnumValueDeserializer deserializer = new GspEnumValueDeserializer();
        SerializerUtils.readArray(jsonParser, deserializer, collection);
        return collection;
    }
}
