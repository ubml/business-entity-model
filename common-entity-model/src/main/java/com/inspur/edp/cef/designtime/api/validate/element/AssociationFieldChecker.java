package com.inspur.edp.cef.designtime.api.validate.element;

import com.inspur.edp.cef.designtime.api.IGspCommonDataType;
import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.designtime.api.element.GspAssociation;
import com.inspur.edp.cef.designtime.api.exceptions.MessageI18nUtils;
import com.inspur.edp.cef.designtime.api.validate.common.CheckUtil;

import static com.inspur.edp.cef.designtime.api.validate.common.CheckUtil.isLegality;

/**
 * The Definition Of DimensionInfo
 *
 * @ClassName: DimensionInfo
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class AssociationFieldChecker {
    private static AssociationFieldChecker associationFieldChecker;

    public static AssociationFieldChecker getInstance() {
        if (associationFieldChecker == null) {
            associationFieldChecker = new AssociationFieldChecker();
        }
        return associationFieldChecker;
    }

    public final void checkAssociationField(IGspCommonField field, IGspCommonDataType commonDataType) {
        for (GspAssociation association : field.getChildAssociations()) {
            for (IGspCommonField refField : association.getRefElementCollection()) {
                if (!isLegality(refField.getLabelID())) {
                    CheckUtil.exception(MessageI18nUtils.getMessage("GSP_VALIDATE_ERROR_0001", commonDataType.getCode(), refField.getLabelID()));
                }
            }
        }
    }
}
