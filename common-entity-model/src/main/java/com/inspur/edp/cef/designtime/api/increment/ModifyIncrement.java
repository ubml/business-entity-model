package com.inspur.edp.cef.designtime.api.increment;

import com.inspur.edp.cef.designtime.api.increment.property.PropertyIncrement;

import java.util.HashMap;

public interface ModifyIncrement extends Increment {

    HashMap<String, PropertyIncrement> getChangeProperties();

}
