package com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.entity.ControlRuleDefItem;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;

import java.io.IOException;

/**
 * The Json Serializer Of ControlRuleDefItem
 *
 * @ClassName: ControlRuleDefItemSerializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class ControlRuleDefItemSerializer extends JsonSerializer<ControlRuleDefItem> {
    @Override
    public void serialize(ControlRuleDefItem controlRuleDefItem, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        SerializerUtils.writeStartObject(jsonGenerator);
        SerializerUtils.writePropertyValue(jsonGenerator, ControlRuleDefNames.Description, controlRuleDefItem.getDescription());
        SerializerUtils.writePropertyValue(jsonGenerator, ControlRuleDefNames.DefaultRuleValue, controlRuleDefItem.getDefaultRuleValue());
        SerializerUtils.writePropertyValue(jsonGenerator, ControlRuleDefNames.RuleDisplayName, controlRuleDefItem.getRuleDisplayName());
        SerializerUtils.writePropertyValue(jsonGenerator, ControlRuleDefNames.RuleName, controlRuleDefItem.getRuleName());
        SerializerUtils.writeEndObject(jsonGenerator);
    }
}
