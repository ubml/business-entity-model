package com.inspur.edp.cef.designtime.api.element.increment;

import com.inspur.edp.cef.designtime.api.entity.GspCommonField;
import com.inspur.edp.cef.designtime.api.increment.AddedIncrement;
import com.inspur.edp.cef.designtime.api.increment.IncrementType;

/**
 * The Definition Of DimensionInfo
 *
 * @ClassName: DimensionInfo
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class AddedFieldIncrement extends GspCommonFieldIncrement implements AddedIncrement {

    private GspCommonField field;

    public GspCommonField getAddedField() {
        return field;
    }

    public void setField(GspCommonField field) {
        this.field = field;
        setId(field.getId());
    }

    @Override
    public final IncrementType getIncrementType() {
        return IncrementType.Added;
    }


}
