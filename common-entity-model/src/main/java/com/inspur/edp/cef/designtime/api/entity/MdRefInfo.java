package com.inspur.edp.cef.designtime.api.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.inspur.edp.cef.designtime.api.exceptions.CommonEntityErrorCodeEnum;
import com.inspur.edp.cef.designtime.api.exceptions.CommonEntityException;

/**
 * The Definition Of DimensionInfo
 *
 * @ClassName: DimensionInfo
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class MdRefInfo implements Cloneable {
    /**
     * 元数据包名
     */
    private String privatePkgName;

    @JsonProperty("PkgName")
    public final String getPkgName() {
        return privatePkgName;
    }

    public final void setPkgName(String value) {
        privatePkgName = value;
    }

    /**
     * 元数据Id
     */
    private String privateId;

    @JsonProperty("Id")
    public final String getId() {
        return privateId;
    }

    public final void setId(String value) {
        privateId = value;
    }

    /**
     * 元数据名称
     */
    private String privateName;

    @JsonProperty("Name")
    public final String getName() {
        return privateName;
    }

    public final void setName(String value) {
        privateName = value;
    }

    /**
     * 拷贝副本
     *
     * @throws CloneNotSupportedException
     */
    public MdRefInfo clone() {
        try {
            return (MdRefInfo) super.clone();
        } catch (CloneNotSupportedException e) {
            throw CommonEntityException.createException(CommonEntityErrorCodeEnum.GSP_BEMODEL_CLONE_0001, e, "MdRefInfo");
        }
    }
}
