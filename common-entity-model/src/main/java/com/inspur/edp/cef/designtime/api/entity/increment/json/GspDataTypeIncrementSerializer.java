package com.inspur.edp.cef.designtime.api.entity.increment.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.cef.designtime.api.element.increment.GspCommonFieldIncrement;
import com.inspur.edp.cef.designtime.api.element.increment.json.GspFieldIncrementSerializer;
import com.inspur.edp.cef.designtime.api.entity.increment.AddedEntityIncrement;
import com.inspur.edp.cef.designtime.api.entity.increment.CommonEntityIncrement;
import com.inspur.edp.cef.designtime.api.entity.increment.DeletedEntityIncrement;
import com.inspur.edp.cef.designtime.api.entity.increment.ModifyEntityIncrement;
import com.inspur.edp.cef.designtime.api.exceptions.CommonEntityErrorCodeEnum;
import com.inspur.edp.cef.designtime.api.exceptions.CommonEntityException;
import com.inspur.edp.cef.designtime.api.increment.property.PropertyIncrement;
import com.inspur.edp.cef.designtime.api.increment.property.json.PropertyIncrementSerializer;
import com.inspur.edp.cef.designtime.api.json.CefNames;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.cef.designtime.api.json.object.GspCommonDataTypeSerializer;
import lombok.var;

import java.io.IOException;
import java.util.HashMap;

public abstract class GspDataTypeIncrementSerializer extends JsonSerializer<CommonEntityIncrement> {
    @Override
    public void serialize(CommonEntityIncrement value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        SerializerUtils.writeStartObject(gen);
        writeBaseInfo(value, gen);
        writeExtendInfo(value, gen);
        SerializerUtils.writeEndObject(gen);
    }

    private void writeBaseInfo(CommonEntityIncrement value, JsonGenerator gen) {
        SerializerUtils.writePropertyValue(gen, CefNames.IncrementType, value.getIncrementType().toString());
        switch (value.getIncrementType()) {
            case Added:
                writeAddedIncrement((AddedEntityIncrement) value, gen);
                break;
            case Modify:
                writeModifyIncrement((ModifyEntityIncrement) value, gen);
                break;
            case Deleted:
                writeDeletedIncrement((DeletedEntityIncrement) value, gen);
                break;
            default:
                throw CommonEntityException.createException(CommonEntityErrorCodeEnum.GSP_BEMODEL_ENUM_NOTSUPPORT_0004, value.getIncrementType().toString());
        }
    }

    //region Added
    private void writeAddedIncrement(AddedEntityIncrement value, JsonGenerator gen) {
        writeBaseAddedInfo(value, gen);
        writeExtendAddedInfo(value, gen);
    }

    private void writeBaseAddedInfo(AddedEntityIncrement value, JsonGenerator gen) {
        SerializerUtils.writePropertyName(gen, CefNames.AddedDataType);
        GspCommonDataTypeSerializer cmObjectSerializer = getDataTypeSerializer();
        cmObjectSerializer.serialize(value.getAddedDataType(), gen, null);
    }

    protected void writeExtendAddedInfo(AddedEntityIncrement value, JsonGenerator gen) {

    }

    protected abstract GspCommonDataTypeSerializer getDataTypeSerializer();
    //endregion

    //region Modify
    private void writeModifyIncrement(ModifyEntityIncrement value, JsonGenerator gen) {
        writeBaseModifyInfo(value, gen);
        writeExtendModifyInfo(value, gen);
    }

    private void writeBaseModifyInfo(ModifyEntityIncrement value, JsonGenerator gen) {

        HashMap<String, PropertyIncrement> properties = value.getChangeProperties();
        if (properties != null && !properties.isEmpty())
            writeProperties(properties, gen);

        HashMap<String, GspCommonFieldIncrement> fields = value.getFields();
        if (fields != null && !fields.isEmpty())
            writeElements(fields, gen);

        HashMap<String, CommonEntityIncrement> childEntities = value.getChildEntitis();
        if (childEntities != null && !childEntities.isEmpty())
            writeChildEntities(childEntities, gen);
    }

    private void writeProperties(HashMap<String, PropertyIncrement> properties, JsonGenerator gen) {
        SerializerUtils.writePropertyName(gen, CefNames.PropertyIncrements);
        SerializerUtils.WriteStartArray(gen);
        for (var item : properties.entrySet()) {

            SerializerUtils.writeStartObject(gen);
            SerializerUtils.writePropertyValue(gen, CefNames.Id, item.getKey());

            SerializerUtils.writePropertyName(gen, CefNames.Value);
            PropertyIncrementSerializer serializer = getPropertyIncrementSerializer();
            serializer.serialize(item.getValue(), gen, null);
            SerializerUtils.writeEndObject(gen);

        }
        SerializerUtils.WriteEndArray(gen);
    }

    private void writeElements(HashMap<String, GspCommonFieldIncrement> fields, JsonGenerator gen) {
        SerializerUtils.writePropertyName(gen, CefNames.FieldIncrements);
        SerializerUtils.WriteStartArray(gen);
        for (var item : fields.entrySet()) {
            try {
                SerializerUtils.writeStartObject(gen);
                SerializerUtils.writePropertyValue(gen, CefNames.Id, item.getKey());

                SerializerUtils.writePropertyName(gen, CefNames.Element);
                GspFieldIncrementSerializer serializer = getFieldSerializer();
                serializer.serialize(item.getValue(), gen, null);
                SerializerUtils.writeEndObject(gen);
            } catch (IOException e) {
                throw CommonEntityException.createException(CommonEntityErrorCodeEnum.GSP_BEMODEL_JSON_0004, e, "GspCommonFieldIncrement");
            }
        }
        SerializerUtils.WriteEndArray(gen);
    }

    protected abstract GspFieldIncrementSerializer getFieldSerializer();

    private void writeChildEntities(HashMap<String, CommonEntityIncrement> childEntities, JsonGenerator gen) {
        SerializerUtils.writePropertyName(gen, CefNames.ChildIncrements);
        SerializerUtils.WriteStartArray(gen);
        for (var item : childEntities.entrySet()) {
            try {
                SerializerUtils.writeStartObject(gen);
                SerializerUtils.writePropertyValue(gen, CefNames.Id, item.getKey());

                SerializerUtils.writePropertyName(gen, CefNames.ChildIncrement);
                GspDataTypeIncrementSerializer serializer = getGspDataTypeIncrementSerializer();
                serializer.serialize(item.getValue(), gen, null);
                SerializerUtils.writeEndObject(gen);
            } catch (IOException e) {
                throw CommonEntityException.createException(CommonEntityErrorCodeEnum.GSP_BEMODEL_JSON_0004, e, "CommonEntityIncrement");
            }
        }
        SerializerUtils.WriteEndArray(gen);
    }

    protected abstract GspDataTypeIncrementSerializer getGspDataTypeIncrementSerializer();

    protected void writeExtendModifyInfo(ModifyEntityIncrement value, JsonGenerator gen) {

    }

    private PropertyIncrementSerializer getPropertyIncrementSerializer() {
        return new PropertyIncrementSerializer();
    }
    //endregion

    //region Deleted
    private void writeDeletedIncrement(DeletedEntityIncrement value, JsonGenerator gen) {
        writeBaseDeletedInfo(value, gen);
        writeExtendDeletedInfo(value, gen);
    }

    private void writeBaseDeletedInfo(DeletedEntityIncrement value, JsonGenerator gen) {
        SerializerUtils.writePropertyValue(gen, CefNames.DeletedId, value.getDeleteId());
    }

    protected void writeExtendDeletedInfo(DeletedEntityIncrement value, JsonGenerator gen) {

    }
    //endregion

    protected void writeExtendInfo(CommonEntityIncrement value, JsonGenerator gen) {

    }
}
