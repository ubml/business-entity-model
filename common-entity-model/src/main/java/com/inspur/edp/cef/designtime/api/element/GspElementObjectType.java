package com.inspur.edp.cef.designtime.api.element;

/**
 * The Definition Of ElementObjectType
 *
 * @ClassName: ElementObjectType
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public enum GspElementObjectType {
    /**
     * 未设置
     */
    None(0),
    /**
     * 关联
     */
    Association(1),
    /**
     * 枚举
     */
    Enum(2),
    /**
     * 动态属性
     */
    DynamicProp(3);

    private final int intValue;
    private static java.util.HashMap<Integer, GspElementObjectType> mappings;

    public synchronized static java.util.HashMap<Integer, GspElementObjectType> getMappings() {
        if (mappings == null) {
            mappings = new java.util.HashMap<Integer, GspElementObjectType>();
        }
        return mappings;
    }

    private GspElementObjectType(int value) {
        intValue = value;
        GspElementObjectType.getMappings().put(value, this);
    }

    public int getValue() {
        return intValue;
    }

    public static GspElementObjectType forValue(int value) {
        return getMappings().get(value);
    }
}