package com.inspur.edp.cef.designtime.api.i18n.context;

import com.inspur.edp.cef.designtime.api.util.DataValidator;
import com.inspur.edp.lcm.metadata.api.entity.I18nResourceItemCollection;

public class CefResourceMergeContext implements ICefResourceMergeContext {

    private String metaPrefix;
    private final I18nResourceItemCollection resourceItems;
    //#endregion

    public CefResourceMergeContext(String metaPrefix, I18nResourceItemCollection items) {
        DataValidator.checkForEmptyString(metaPrefix, "元数据前缀");
        DataValidator.checkForNullReference(items, "国际化资源项");
        this.metaPrefix = metaPrefix;
        this.resourceItems = items;
    }

    @Override
    public String getKeyPrefix() {
        return this.metaPrefix;
    }

    @Override
    public void setKeyPrefix(String keyPrefix) {
        this.metaPrefix = keyPrefix;
    }

    public I18nResourceItemCollection getResourceItems() {
        return this.resourceItems;
    }

}
