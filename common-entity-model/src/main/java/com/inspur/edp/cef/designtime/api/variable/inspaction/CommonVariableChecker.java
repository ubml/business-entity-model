package com.inspur.edp.cef.designtime.api.variable.inspaction;

import com.inspur.edp.cef.designtime.api.validate.object.CommonDataTypeChecker;
import com.inspur.edp.cef.designtime.api.variable.CommonVariableEntity;

/**
 * The Definition Of DimensionInfo
 *
 * @ClassName: DimensionInfo
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public abstract class CommonVariableChecker extends CommonDataTypeChecker {

    protected final void checkCVC(CommonVariableEntity entity) {
        checkCDT(entity);
        checkVarExtension(entity);
    }

    protected void checkVarExtension(CommonVariableEntity entity) {

    }

}
