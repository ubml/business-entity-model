package com.inspur.edp.cef.designtime.api.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.inspur.edp.cef.designtime.api.json.CefNames;

/**
 * The Definition Of DimensionInfo
 *
 * @ClassName: DimensionInfo
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class DimensionInfo {

    private String firstDimension;

    @JsonProperty(CefNames.FirstDimension)
    public String getFirstDimension() {
        return firstDimension;
    }

    public void setFirstDimension(String firstDimension) {
        this.firstDimension = firstDimension;
    }

    private String secondDimension;

    @JsonProperty(CefNames.SecondDimension)
    public String getSecondDimension() {
        return secondDimension;
    }

    public void setSecondDimension(String secondDimension) {
        this.secondDimension = secondDimension;
    }


    private String firstDimensionCode;

    @JsonProperty(CefNames.FirstDimensionCode)
    public String getFirstDimensionCode() {
        return firstDimensionCode;
    }

    public void setFirstDimensionCode(String firstDimensionCode) {
        this.firstDimensionCode = firstDimensionCode;
    }

    private String secondDimensionCode;

    @JsonProperty(CefNames.SecondDimensionCode)
    public String getSecondDimensionCode() {
        return secondDimensionCode;
    }

    public void setSecondDimensionCode(String secondDimensionCode) {
        this.secondDimensionCode = secondDimensionCode;
    }


    private String firstDimensionName;

    @JsonProperty(CefNames.FirstDimensionName)
    public String getFirstDimensionName() {
        return firstDimensionName;
    }

    public void setFirstDimensionName(String firstDimensionName) {
        this.firstDimensionName = firstDimensionName;
    }


    private String secondDimensionName;

    @JsonProperty(CefNames.SecondDimensionName)
    public String getSecondDimensionName() {
        return secondDimensionName;
    }

    public void setSecondDimensionName(String secondDimensionName) {
        this.secondDimensionName = secondDimensionName;
    }


    public boolean equals(DimensionInfo info) {
        if (this.firstDimension == null
                && info.firstDimension == null
                && this.secondDimension == null
                && info.secondDimension == null)
            return true;

        return this.firstDimension != null
                && this.firstDimension.equals(info.firstDimension)
                && this.secondDimension != null
                && this.secondDimension.equals(info.secondDimension);
    }
}
