package com.inspur.edp.cef.designtime.api.validate.element;

import com.inspur.edp.cef.designtime.api.IGspCommonDataType;
import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.designtime.api.element.EnumIndexType;
import com.inspur.edp.cef.designtime.api.element.GspElementDataType;
import com.inspur.edp.cef.designtime.api.element.GspEnumValue;
import com.inspur.edp.cef.designtime.api.exceptions.MessageI18nUtils;
import com.inspur.edp.cef.designtime.api.validate.common.CheckUtil;
import org.springframework.util.StringUtils;

import static com.inspur.edp.cef.designtime.api.validate.common.CheckUtil.isLegality;

/**
 * The Definition Of DimensionInfo
 *
 * @ClassName: DimensionInfo
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class EunmFieldChecker {
    private static EunmFieldChecker eunmFieldChecker;

    public static EunmFieldChecker getInstance() {
        if (eunmFieldChecker == null) {
            eunmFieldChecker = new EunmFieldChecker();
        }
        return eunmFieldChecker;
    }

    public final void checkEunm(IGspCommonField field, IGspCommonDataType commonDataType) {
        if (field.getContainEnumValues() == null || field.getContainEnumValues().isEmpty()) {
            CheckUtil.exception(MessageI18nUtils.getMessage("GSP_VALIDATE_ERROR_0002", commonDataType.getCode(), field.getName()));
        }
        for (GspEnumValue enumValue : field.getContainEnumValues()) {
            if (StringUtils.isEmpty(enumValue.getValue())) {
                CheckUtil.exception(MessageI18nUtils.getMessage("GSP_VALIDATE_ERROR_0003", commonDataType.getCode(), field.getName(), enumValue.getName()));
            }
            if (StringUtils.isEmpty(enumValue.getName())) {
                CheckUtil.exception(MessageI18nUtils.getMessage("GSP_VALIDATE_ERROR_0004", commonDataType.getCode(), field.getName(), enumValue.getValue()));
            }
            if (!isLegality(enumValue.getValue())) {
                CheckUtil.exception(MessageI18nUtils.getMessage("GSP_VALIDATE_ERROR_0005", commonDataType.getCode(), field.getName(), enumValue.getValue()));
            }
            if (field.getEnumIndexType() == EnumIndexType.String) {
                if (field.getMDataType() != GspElementDataType.String) {
                    CheckUtil.exception(MessageI18nUtils.getMessage("GSP_VALIDATE_ERROR_0006", commonDataType.getCode(), field.getName()));
                }
                if (StringUtils.isEmpty(enumValue.getStringIndex())) {
                    CheckUtil.exception(MessageI18nUtils.getMessage("GSP_VALIDATE_ERROR_0007", commonDataType.getCode(), field.getName(), enumValue.getValue()));
                }
            } else {
                if (StringUtils.isEmpty(enumValue.getIndex())) {
                    CheckUtil.exception(MessageI18nUtils.getMessage("GSP_VALIDATE_ERROR_0007", commonDataType.getCode(), field.getName(), enumValue.getValue()));
                }
            }
        }
    }
}
