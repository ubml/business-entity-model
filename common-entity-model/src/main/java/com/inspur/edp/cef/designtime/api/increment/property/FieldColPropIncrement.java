package com.inspur.edp.cef.designtime.api.increment.property;

import com.fasterxml.jackson.databind.JsonDeserializer;
import com.inspur.edp.cef.designtime.api.collection.GspFieldCollection;

public class FieldColPropIncrement extends ObjectPropertyIncrement {
    public FieldColPropIncrement(boolean hasIncrement) {
        super(hasIncrement);
    }

    public Class getPropertyIncrementClass() {
        return GspFieldCollection.class;
    }

    public JsonDeserializer getJsonDeserializer() {
        return getCefFieldDeserializer();
    }
}
