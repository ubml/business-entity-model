package com.inspur.edp.cef.designtime.api.variable;

import com.inspur.edp.cef.designtime.api.entity.ClassInfo;
import com.inspur.edp.cef.designtime.api.entity.DataTypeAssemblyInfo;
import com.inspur.edp.cef.designtime.api.entity.GspCommonDataType;
import com.inspur.edp.cef.designtime.api.util.DataValidator;

/**
 * The Definition Of Common Variable Entity
 *
 * @ClassName: CommonVariableEntity
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class CommonVariableEntity extends GspCommonDataType {
    public CommonVariableEntity() {
        setContainElements(new CommonVariableCollection(this));
    }

    /**
     * 字段集合
     */
    public CommonVariableCollection getContainElements() {
        if (super.getContainElements() == null) {
            super.setContainElements(new CommonVariableCollection(this));
        }
        return (CommonVariableCollection) ((super.getContainElements() instanceof CommonVariableCollection)
                ? super.getContainElements()
                : null);
    }

    protected void setContainElements(CommonVariableCollection value) {
        super.setContainElements(value);
    }

    @Override
    public CommonVariable findElement(String elementId) {
        DataValidator.checkForNullReference(elementId, "elementId");
        return getContainElements().getItem(elementId);
    }

    /**
     * 元数据Entity程序集
     */
    private DataTypeAssemblyInfo privateParentEntityAssemblyInfo;

    public final DataTypeAssemblyInfo getParentEntityAssemblyInfo() {
        return privateParentEntityAssemblyInfo;
    }

    public final void setParentEntityAssemblyInfo(DataTypeAssemblyInfo value) {
        privateParentEntityAssemblyInfo = value;
    }

    /**
     * 元数据Api程序集
     */
    private DataTypeAssemblyInfo privateParentApiAssemblyInfo;

    public final DataTypeAssemblyInfo getParentApiAssemblyInfo() {
        return privateParentApiAssemblyInfo;
    }

    public final void setParentApiAssemblyInfo(DataTypeAssemblyInfo value) {
        privateParentApiAssemblyInfo = value;
    }

    /**
     * 元数据Core程序集
     */
    private DataTypeAssemblyInfo privateParentCoreAssemblyInfo;

    public final DataTypeAssemblyInfo getParentCoreAssemblyInfo() {
        return privateParentCoreAssemblyInfo;
    }

    public final void setParentCoreAssemblyInfo(DataTypeAssemblyInfo value) {
        privateParentCoreAssemblyInfo = value;
    }

    private static final String variableSuffix = "Variable";

    /**
     * 变量实体对应的类名
     *
     * @return
     */
    @Override
    public ClassInfo getGeneratedEntityClassInfo() {
        String classNameSpace = String.format("%1$s.%2$s", getParentEntityAssemblyInfo().getDefaultNamespace(),
                variableSuffix);
        String variableClassName = String.format("I%1$s", getCode()); // "IVariableEntity";
        return new ClassInfo(getParentApiAssemblyInfo(), variableClassName, classNameSpace);
    }

    /**
     * 变量对应的Api程序集
     *
     * @return
     */
    public final DataTypeAssemblyInfo getGeneratedApiAssemblyInfo() {
        String defaultNamespace = String.format("%1$s.%2$s", getParentApiAssemblyInfo().getDefaultNamespace(),
                variableSuffix);
        return new DataTypeAssemblyInfo(getParentApiAssemblyInfo().getAssemblyName(), defaultNamespace);
    }

    /**
     * 变量对应的Entity程序集
     *
     * @return
     */
    public final DataTypeAssemblyInfo getGeneratedEntityAssemblyInfo() {
        String defaultNamespace = String.format("%1$s.%2$s", getParentEntityAssemblyInfo().getDefaultNamespace(),
                variableSuffix);
        return new DataTypeAssemblyInfo(getParentApiAssemblyInfo().getAssemblyName(), defaultNamespace);
    }

    /**
     * 变量对应的Core程序集类名
     *
     * @return
     */
    public final DataTypeAssemblyInfo getGeneratedCoreAssemblyInfo() {
        String defaultNamespace = String.format("%1$s.%2$s", getParentCoreAssemblyInfo().getDefaultNamespace(),
                variableSuffix);
        return new DataTypeAssemblyInfo(getParentCoreAssemblyInfo().getAssemblyName(), defaultNamespace);
    }
}