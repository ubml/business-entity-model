package com.inspur.edp.cef.designtime.api.i18n.extractor;


import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.designtime.api.exceptions.CommonEntityErrorCodeEnum;
import com.inspur.edp.cef.designtime.api.exceptions.CommonEntityException;
import com.inspur.edp.cef.designtime.api.i18n.context.CefResourcePrefixInfo;
import com.inspur.edp.cef.designtime.api.i18n.context.ICefResourceExtractContext;

public abstract class AssoRefFieldResourceExtractor extends CefFieldResourceExtractor {
    private final IGspCommonField commonField;

    public AssoRefFieldResourceExtractor(IGspCommonField field, ICefResourceExtractContext context, CefResourcePrefixInfo parentResourceInfo) {
        super(field, context, parentResourceInfo);
        commonField = field;
    }

    protected CefResourcePrefixInfo buildCurrentPrefix() {
        if (!commonField.getIsRefElement()) {
            throw CommonEntityException.createException(CommonEntityErrorCodeEnum.GSP_BEMODEL_COMMON_ENTITY_0012);
        }
        if (getParentResourcePrefixInfo() == null) {
            throw CommonEntityException.createException(CommonEntityErrorCodeEnum.GSP_BEMODEL_COMMON_ENTITY_0013, commonField.getParentAssociation().toString());
        }
        CefResourcePrefixInfo fieldResourceInfo = new CefResourcePrefixInfo();
        fieldResourceInfo.setResourceKeyPrefix(getParentResourcePrefixInfo().getResourceKeyPrefix() + "." + commonField.getLabelID());
        fieldResourceInfo.setDescriptionPrefix(getParentResourcePrefixInfo().getDescriptionPrefix() + "的关联带出属性'" + commonField.getName() + "'");
        return fieldResourceInfo;
    }
}