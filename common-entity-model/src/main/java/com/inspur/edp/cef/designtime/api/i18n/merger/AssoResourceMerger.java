package com.inspur.edp.cef.designtime.api.i18n.merger;

import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.designtime.api.element.GspAssociation;
import com.inspur.edp.cef.designtime.api.i18n.context.ICefResourceMergeContext;

public abstract class AssoResourceMerger extends AbstractResourceMerger {
    private final GspAssociation asso;

    protected AssoResourceMerger(GspAssociation asso,
                                 ICefResourceMergeContext context) {
        super(context);
        this.asso = asso;
    }

    @Override
    protected void mergeItems() {
        if (asso.getRefElementCollection() != null && !asso.getRefElementCollection().isEmpty()) {
            for (IGspCommonField item : asso.getRefElementCollection()) {
                getAssoRefFieldResourceMerger(getContext(), item).merge();
            }
        }
    }


    protected abstract AssoRefFieldResourceMerger getAssoRefFieldResourceMerger(
            ICefResourceMergeContext context, IGspCommonField field);

}
