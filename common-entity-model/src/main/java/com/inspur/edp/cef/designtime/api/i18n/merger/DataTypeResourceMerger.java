package com.inspur.edp.cef.designtime.api.i18n.merger;

import com.inspur.edp.cef.designtime.api.IFieldCollection;
import com.inspur.edp.cef.designtime.api.IGspCommonDataType;
import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.designtime.api.i18n.context.ICefResourceMergeContext;
import com.inspur.edp.cef.designtime.api.i18n.names.CefResourceKeyNames;
import com.inspur.edp.lcm.metadata.api.entity.I18nResourceItemCollection;

import static com.inspur.edp.cef.designtime.api.increment.merger.MergeUtils.getKeyPrefix;

public abstract class DataTypeResourceMerger extends AbstractResourceMerger {

    private final IGspCommonDataType dataType;

    protected DataTypeResourceMerger(IGspCommonDataType commonDataType, ICefResourceMergeContext context) {
        super(context);
        this.dataType = commonDataType;
    }

    @Override
    protected void mergeItems() {
        I18nResourceItemCollection resourceItems = getContext().getResourceItems();
        String keyPrefix = getKeyPrefix(dataType.getI18nResourceInfoPrefix(), CefResourceKeyNames.Name);
        dataType.setName(resourceItems.getResourceItemByKey(keyPrefix).getValue());
        extractDataTypeFields(dataType.getContainElements());
        //扩展
        extractExtendProperties(dataType);
    }

    private void extractDataTypeFields(IFieldCollection fields) {
        if (fields == null || fields.isEmpty()) {
            return;
        }
        for (IGspCommonField field : fields) {
            CefFieldResourceMerger merger = getCefFieldResourceMerger(getContext(), field);
            merger.merge();
        }
    }

    protected abstract CefFieldResourceMerger getCefFieldResourceMerger(
            ICefResourceMergeContext context, IGspCommonField field);


    protected void extractExtendProperties(IGspCommonDataType dataType) {
    }

}
