package com.inspur.edp.cef.designtime.api.json.operation;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.cef.designtime.api.json.CefNames;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.cef.designtime.api.operation.CommonOperation;
import com.inspur.edp.cef.designtime.api.operation.ExecutingDataStatus;

import java.util.EnumSet;

/**
 * The Json Serializer Of Common Operation
 *
 * @ClassName: CommonOpSerializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public abstract class CommonOpSerializer<T extends CommonOperation> extends JsonSerializer<T> {

    protected boolean isFull = true;

    public CommonOpSerializer() {
    }

    public CommonOpSerializer(boolean full) {
        isFull = full;
    }

    @Override
    public void serialize(T value, JsonGenerator gen, SerializerProvider serializers) {
        SerializerUtils.writeStartObject(gen);
        writeBaseProperty(value, gen);
        writeSelfProperty(value, gen);
        SerializerUtils.writeEndObject(gen);
    }

    private void writeBaseProperty(CommonOperation info, JsonGenerator gen) {
        SerializerUtils.writePropertyValue(gen, "ID", info.getID());
        this.writeExtendCommonOpBaseProperty(gen, info);
    }


    private void writeSelfProperty(CommonOperation info, JsonGenerator writer) {
        SerializerUtils.writePropertyValue(writer, CefNames.Code, info.getCode());
        SerializerUtils.writePropertyValue(writer, CefNames.Name, info.getName());
        if (isFull || (info.getDescription() != null && !info.getDescription().isEmpty())) {
            SerializerUtils.writePropertyValue(writer, CefNames.Description, info.getDescription());
        }
        if (isFull || (info.getComponentId() != null && !info.getComponentId().isEmpty())) {
            SerializerUtils.writePropertyValue(writer, CefNames.ComponentId, info.getComponentId());
        }
        if (isFull || (info.getComponentName() != null && !info.getComponentName().isEmpty())) {
            SerializerUtils.writePropertyValue(writer, CefNames.ComponentName, info.getComponentName());
        }
        if (isFull || (info.getComponentPkgName() != null && !info.getComponentPkgName().isEmpty())) {
            SerializerUtils.writePropertyValue(writer, CefNames.ComponentPkgName, info.getComponentPkgName());
        }
        if (isFull || info.getIsRef()) {
            SerializerUtils.writePropertyValue(writer, CefNames.IsRef, info.getIsRef());
        }
        if (isFull || !info.getIsGenerateComponent()) {
            SerializerUtils.writePropertyValue(writer, CefNames.IsGenerateComponent, info.getIsGenerateComponent());
        }
        this.writeExtendCommonOpSelfProperty(writer, info);

    }

    protected abstract void writeExtendCommonOpBaseProperty(JsonGenerator writer, CommonOperation info);

    protected abstract void writeExtendCommonOpSelfProperty(JsonGenerator writer, CommonOperation info);

    protected void writeGetExecutingDataStatus(JsonGenerator writer, EnumSet<ExecutingDataStatus> value) {
        int intValue = 0;
        for (ExecutingDataStatus timePointType : value) {
            intValue += timePointType.getValue();
        }
        SerializerUtils.writePropertyValue_Integer(writer, intValue);
    }
}
