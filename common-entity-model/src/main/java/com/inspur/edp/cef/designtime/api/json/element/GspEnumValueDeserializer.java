package com.inspur.edp.cef.designtime.api.json.element;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.inspur.edp.cef.designtime.api.element.GspEnumValue;
import com.inspur.edp.cef.designtime.api.exceptions.CommonEntityErrorCodeEnum;
import com.inspur.edp.cef.designtime.api.exceptions.CommonEntityException;
import com.inspur.edp.cef.designtime.api.json.CefNames;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;

import static com.fasterxml.jackson.core.JsonToken.FIELD_NAME;

/**
 * The Json Parser Of EnumValue
 *
 * @ClassName: GspEnumValueDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class GspEnumValueDeserializer extends JsonDeserializer<GspEnumValue> {
    @Override
    public GspEnumValue deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) {
        return deserializeGspEnumValue(jsonParser);
    }

    public GspEnumValue deserializeGspEnumValue(JsonParser jsonParser) {
        GspEnumValue enumValue = new GspEnumValue();
        SerializerUtils.readStartObject(jsonParser);
        while (jsonParser.getCurrentToken() == FIELD_NAME) {
            String propName = SerializerUtils.readPropertyName(jsonParser);
            readPropertyValue(enumValue, propName, jsonParser);
        }
        SerializerUtils.readEndObject(jsonParser);
        return enumValue;
    }

    private void readPropertyValue(GspEnumValue enumValue, String propName, JsonParser jsonParser) {

        switch (propName) {
            case CefNames.Name:
                enumValue.setName(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case CefNames.Value:
                enumValue.setValue(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case CefNames.I18nResourceInfoPrefix:
                enumValue.setI18nResourceInfoPrefix(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case CefNames.IsDefaultEnum:
                enumValue.setIsDefaultEnum(SerializerUtils.readPropertyValue_boolean(jsonParser));
                break;
            case CefNames.EnumItemDisabled:
                enumValue.setEnumItemDisabled(SerializerUtils.readPropertyValue_boolean(jsonParser));
                break;
            case CefNames.Index:
                enumValue.setIndex(SerializerUtils.readPropertyValue_Integer(jsonParser));
                break;
            case CefNames.StringIndex:
                enumValue.setStringIndex(SerializerUtils.readPropertyValue_String(jsonParser));//String类型的索引
                break;
            default:
                throw CommonEntityException.createException(CommonEntityErrorCodeEnum.GSP_BEMODEL_COMMON_ENTITY_0006, "CefFieldDeserializer", propName);
        }
    }
}
