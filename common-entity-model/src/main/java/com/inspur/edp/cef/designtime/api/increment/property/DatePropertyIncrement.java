package com.inspur.edp.cef.designtime.api.increment.property;

public class DatePropertyIncrement extends NativePropertyIncrement {

    public DatePropertyIncrement(boolean hasIncrement) {
        super(hasIncrement);
    }

    @Override
    public IncrementPropType getPropertyType() {
        return IncrementPropType.Date;
    }
}
