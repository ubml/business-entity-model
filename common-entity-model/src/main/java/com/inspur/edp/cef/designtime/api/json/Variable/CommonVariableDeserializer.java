package com.inspur.edp.cef.designtime.api.json.Variable;

import com.fasterxml.jackson.core.JsonParser;
import com.inspur.edp.cef.designtime.api.collection.GspFieldCollection;
import com.inspur.edp.cef.designtime.api.entity.GspCommonField;
import com.inspur.edp.cef.designtime.api.json.CefNames;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.cef.designtime.api.json.element.CefFieldDeserializer;
import com.inspur.edp.cef.designtime.api.variable.CommonVariable;
import com.inspur.edp.cef.designtime.api.variable.CommonVariableCollection;
import com.inspur.edp.cef.designtime.api.variable.VariableSourceType;

/**
 * The Json Parser Of Common Variable
 *
 * @ClassName: CommonVariableDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class CommonVariableDeserializer extends CefFieldDeserializer {
    @Override
    protected boolean readExtendFieldProperty(GspCommonField item, String propName, JsonParser jsonParser) {
        boolean result = true;
        CommonVariable field = (CommonVariable) item;
        if (propName.equals(CefNames.VariableSourceType)) {
            field.setVariableSourceType(SerializerUtils.readPropertyValue_Enum(jsonParser, VariableSourceType.class, VariableSourceType.values(), VariableSourceType.BE));
        } else {
            result = false;
        }
        return result;
    }


    @Override
    protected void beforeCefElementDeserializer(GspCommonField item) {

    }

    @Override
    protected GspFieldCollection CreateFieldCollection() {
        return new CommonVariableCollection(null);
    }

    @Override
    protected GspCommonField CreateField() {
        return new CommonVariable();
    }
}
