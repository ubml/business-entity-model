package com.inspur.edp.cef.designtime.api.json.element;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.inspur.edp.cef.designtime.api.element.GspAssociationKey;
import com.inspur.edp.cef.designtime.api.json.CefNames;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;

import static com.fasterxml.jackson.core.JsonToken.FIELD_NAME;

/**
 * The Json Parser Of GspAssoKey
 *
 * @ClassName: GspAssoKeyDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class GspAssoKeyDeserializer extends JsonDeserializer<GspAssociationKey> {

    public GspAssoKeyDeserializer() {
    }

    @Override
    public GspAssociationKey deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) {
        return deserializeGspAssociation(jsonParser);
    }

    public GspAssociationKey deserializeGspAssociation(JsonParser jsonParser) {
        GspAssociationKey associationKey = new GspAssociationKey();
        SerializerUtils.readStartObject(jsonParser);
        while (jsonParser.getCurrentToken() == FIELD_NAME) {
            String propName = SerializerUtils.readPropertyName(jsonParser);
            readPropertyValue(associationKey, propName, jsonParser);
        }
        SerializerUtils.readEndObject(jsonParser);
        return associationKey;
    }

    private void readPropertyValue(GspAssociationKey associationKey, String propName, JsonParser jsonParser) {
        switch (propName) {
            case CefNames.SourceElement:
                associationKey.setSourceElement(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case CefNames.TargetElement:
                associationKey.setTargetElement(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case CefNames.SourceElementDisplay:
                associationKey.setSourceElementDisplay(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case CefNames.TargetElementDisplay:
                associationKey.setTargetElementDisplay(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case CefNames.RefDataModelName:
            case CefNames.RefdataModelName:
                associationKey.setRefDataModelName(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
        }
    }
}
