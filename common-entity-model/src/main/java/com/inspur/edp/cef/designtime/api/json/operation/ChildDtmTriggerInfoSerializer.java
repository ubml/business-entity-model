package com.inspur.edp.cef.designtime.api.json.operation;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.cef.designtime.api.collection.DtmElementCollection;
import com.inspur.edp.cef.designtime.api.json.CefNames;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.cef.designtime.api.operation.ChildDtmTriggerInfo;
import com.inspur.edp.cef.designtime.api.operation.ExecutingDataStatus;

import java.io.IOException;
import java.util.EnumSet;


public class ChildDtmTriggerInfoSerializer extends JsonSerializer<ChildDtmTriggerInfo> {
    protected boolean isFull;


    public ChildDtmTriggerInfoSerializer(boolean full) {
        isFull = full;
    }

    @Override
    public void serialize(ChildDtmTriggerInfo childDtmTriggerInfo, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        SerializerUtils.writeStartObject(jsonGenerator);
        if (isFull || (!childDtmTriggerInfo.getGetExecutingDataStatus().isEmpty() && !childDtmTriggerInfo.getGetExecutingDataStatus().iterator().next().equals(ExecutingDataStatus.None))) {
            SerializerUtils.writePropertyName(jsonGenerator, CefNames.GetExecutingDataStatus);
            writeGetExecutingDataStatus(jsonGenerator, childDtmTriggerInfo.getGetExecutingDataStatus());
        }
        if (isFull || (childDtmTriggerInfo.getRequestChildElements() != null && !childDtmTriggerInfo.getRequestChildElements().isEmpty()))
            writeRequestChildElements(jsonGenerator, childDtmTriggerInfo.getRequestChildElements());
        SerializerUtils.writeEndObject(jsonGenerator);
    }

    protected void writeGetExecutingDataStatus(JsonGenerator writer, EnumSet<ExecutingDataStatus> value) {
        int intValue = 0;
        for (ExecutingDataStatus timePointType : value) {
            intValue += timePointType.getValue();
        }
        SerializerUtils.writePropertyValue_Integer(writer, intValue);
    }

    protected void writeRequestChildElements(JsonGenerator writer, DtmElementCollection dic) {
        if (dic != null && !dic.isEmpty()) {
            SerializerUtils.writePropertyName(writer, CefNames.RequestChildElements);
            writeDtmElementCollection(writer, dic);
        }
    }

    private void writeDtmElementCollection(JsonGenerator writer, DtmElementCollection childElementsIds) {
        SerializerUtils.WriteStartArray(writer);
        if (!childElementsIds.isEmpty()) {
            for (String item : childElementsIds) {
                SerializerUtils.writePropertyValue_String(writer, item);
            }
        }
        SerializerUtils.WriteEndArray(writer);
    }
}
