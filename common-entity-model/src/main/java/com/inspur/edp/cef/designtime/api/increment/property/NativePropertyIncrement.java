package com.inspur.edp.cef.designtime.api.increment.property;

public abstract class NativePropertyIncrement extends PropertyIncrement {

    private Object propertyValue;

    public NativePropertyIncrement(boolean hasIncrement) {
        super(hasIncrement);
    }

    public abstract IncrementPropType getPropertyType();


    public Object getPropertyValue() {
        return propertyValue;
    }

    public void setPropertyValue(Object propertyValue) {
        this.propertyValue = propertyValue;
    }


}
