package com.inspur.edp.cef.designtime.api.operation;

import com.inspur.edp.cef.designtime.api.collection.DtmElementCollection;

import java.util.EnumSet;
import java.util.HashMap;

public class ChildTriggerInfo {
    private java.util.HashMap<String, DtmElementCollection> requestChildElements;
    private EnumSet<ExecutingDataStatus> status = EnumSet.of(ExecutingDataStatus.forValue(0));

    public HashMap<String, DtmElementCollection> getRequestChildElements() {
        return requestChildElements;
    }

    public void setRequestChildElements(HashMap<String, DtmElementCollection> requestChildElements) {
        this.requestChildElements = requestChildElements;
    }

    public final EnumSet<ExecutingDataStatus> getGetExecutingDataStatus() {
        return status;
    }

    public final void setGetExecutingDataStatus(EnumSet<ExecutingDataStatus> value) {
        status = value;
    }
}

