package com.inspur.edp.cef.designtime.api.i18n.context;

public interface ICefResourceExtractContext {
    /**
     * 获取前缀
     *
     * @return
     */
    String getKeyPrefix();

    /**
     * 新增/修改
     *
     * @param info
     */
    void setResourceItem(CefResourceInfo info);
}