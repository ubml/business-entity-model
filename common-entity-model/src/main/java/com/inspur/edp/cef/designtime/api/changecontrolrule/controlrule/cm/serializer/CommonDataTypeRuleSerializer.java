package com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.cm.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.basic.serializer.RangeRuleSerializer;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.cm.CommonDataTypeControlRule;

/**
 * The Json Serializer Of CommonDataTypeRule
 *
 * @ClassName: CommonDataTypeRuleSerializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class CommonDataTypeRuleSerializer<T extends CommonDataTypeControlRule> extends RangeRuleSerializer<T> {
    @Override
    protected final void writeRangeExtendInfos(T controlRule, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) {
        super.writeRangeExtendInfos(controlRule, jsonGenerator, serializerProvider);
        writeCommonDataTypeRuleExtendInfos(controlRule, jsonGenerator, serializerProvider);
    }

    protected void writeCommonDataTypeRuleExtendInfos(T controlRule, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) {

    }
}