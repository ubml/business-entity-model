package com.inspur.edp.cef.designtime.api.increment.property;

public class IntPropertyIncrement extends NativePropertyIncrement {

    public IntPropertyIncrement(boolean hasIncrement) {
        super(hasIncrement);
    }

    @Override
    public IncrementPropType getPropertyType() {
        return IncrementPropType.Int;
    }
}
