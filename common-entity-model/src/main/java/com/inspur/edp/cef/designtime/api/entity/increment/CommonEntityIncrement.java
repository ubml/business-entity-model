package com.inspur.edp.cef.designtime.api.entity.increment;

import com.inspur.edp.cef.designtime.api.increment.AbstractIncrement;

public abstract class CommonEntityIncrement extends AbstractIncrement {

    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
