package com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.cm;

/**
 * The Serializer Names Of CommonDataTypeRule
 *
 * @ClassName: CommonDataTypeRuleNames
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class CommonDataTypeRuleNames {
    public static String Name = "Name";
    public static String AddChildEntity = "AddChildEntity";
    public static String ModifyChildEntities = "ModifyChildEntities";
    public static String AddField = "AddField";
    public static String ModifyFields = "ModifyFields";
    public static String RuleObjectType = "ModifyFields";
    public static String FieldsRules = "FieldsRules";
    public static String MainObjectRule = "MainObjectRule";
}
