package com.inspur.edp.cef.designtime.api.validate.common;

import com.inspur.edp.cef.designtime.api.exceptions.CommonEntityErrorCodeEnum;
import com.inspur.edp.cef.designtime.api.exceptions.CommonEntityException;

import java.util.Arrays;

/**
 * The Definition Of DimensionInfo
 *
 * @ClassName: DimensionInfo
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class CheckUtil {
    public static boolean isLegality(String code) {
        if (code == null || code.isEmpty())
            return false;
        return Arrays.binarySearch(Keywords.javaKeyworks, code) < 0;
    }

    /**
     * 判断字符串只包含字母、数字、下划线
     *
     * @param value
     * @return
     */
    public static boolean isValidInput(String value) {
        boolean result = true;
        return result;
    }

    /**
     * 是否字母开头
     *
     * @param value
     * @return
     */
    public static boolean isStartWithChar(String value) {
        return Character.isLetter(value.charAt(0));
    }

    public static void exception(String message) {
        if (message == null || message.isEmpty())
            return;
        throw CommonEntityException.createException(CommonEntityErrorCodeEnum.GSP_BEMODEL_TEMPLATE_ERROR, message);
    }
}
