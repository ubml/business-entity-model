package com.inspur.edp.cef.designtime.api.validate.element;

import com.alibaba.druid.util.StringUtils;
import com.inspur.edp.cef.designtime.api.IGspCommonDataType;
import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.designtime.api.exceptions.MessageI18nUtils;
import com.inspur.edp.cef.designtime.api.validate.common.CheckUtil;
import com.inspur.edp.cef.designtime.api.variable.CommonVariable;

/**
 * The Definition Of DimensionInfo
 *
 * @ClassName: DimensionInfo
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public abstract class FieldChecker {
    protected boolean isLegality(String code) {
        return CheckUtil.isLegality(code);
    }

    public final void checkField(IGspCommonField field, IGspCommonDataType commonDataType) {
        checkBaseFieldInfo(field, commonDataType);
        checkFieldWithObjectType(field, commonDataType);
        checkFieldExtension(field);
    }

    protected void checkFieldExtension(IGspCommonField field) {

    }

    private void checkFieldWithObjectType(IGspCommonField field, IGspCommonDataType commonDataType) {
        if (field == null) return;
        switch (field.getObjectType()) {
            case Association:
                AssociationFieldChecker.getInstance().checkAssociationField(field, commonDataType);
                break;
            case Enum:
                EunmFieldChecker.getInstance().checkEunm(field, commonDataType);
                break;
            case None:
                break;
            case DynamicProp:
                break;
            default:
                break;
        }
    }

    private void checkBaseFieldInfo(IGspCommonField field, IGspCommonDataType gspCommonDataType) {
        if (field instanceof CommonVariable) {
            if (StringUtils.isEmpty(field.getLabelID())) {
                if (field.getName() == null || field.getName().isEmpty()) {
                    CheckUtil.exception(MessageI18nUtils.getMessage("GSP_VALIDATE_ERROR_0008"));
                }
                CheckUtil.exception(MessageI18nUtils.getMessage("GSP_VALIDATE_ERROR_0009", field.getName()));
            }
            if (!isLegality(field.getLabelID()))
                CheckUtil.exception(MessageI18nUtils.getMessage("GSP_VALIDATE_ERROR_0010", field.getLabelID()));
        } else {
            if (StringUtils.isEmpty(field.getLabelID())) {
                if (field.getName() == null || field.getName().isEmpty()) {
                    CheckUtil.exception(MessageI18nUtils.getMessage("GSP_VALIDATE_ERROR_0011", gspCommonDataType.getCode()));
                }
                CheckUtil.exception(MessageI18nUtils.getMessage("GSP_VALIDATE_ERROR_0012", gspCommonDataType.getCode(), field.getName()));
            }
            if (!isLegality(field.getLabelID()))
                CheckUtil.exception(MessageI18nUtils.getMessage("GSP_VALIDATE_ERROR_0013", gspCommonDataType.getCode(), field.getLabelID()));
        }

        checkBaseExtension();
    }

    protected void checkBaseExtension() {

    }
}
