package com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.basic;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.basic.parser.ControlRuleItemParser;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.basic.serializer.ControlRuleItemSerializer;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.entity.ControlRuleValue;

/**
 * The Definition Of ControlRuleItem
 *
 * @ClassName: ControlRuleItem
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
@JsonSerialize(using = ControlRuleItemSerializer.class)
@JsonDeserialize(using = ControlRuleItemParser.class)
public class ControlRuleItem {
    private String ruleName;
    private ControlRuleValue controlRuleValue = ControlRuleValue.Default;

    //region 规则名称
    public String getRuleName() {
        return ruleName;
    }

    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }
    //endregion

    //region 规则
    public ControlRuleValue getControlRuleValue() {
        return controlRuleValue;
    }

    public void setControlRuleValue(ControlRuleValue controlRuleValue) {
        this.controlRuleValue = controlRuleValue;
    }
    //endregion
}
