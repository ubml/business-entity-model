package com.inspur.edp.cef.designtime.api.validate.operation;

import com.inspur.edp.cef.designtime.api.validate.common.CheckUtil;

/**
 * The Definition Of DimensionInfo
 *
 * @ClassName: DimensionInfo
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public abstract class OperationChecker {

    protected void opException(String message) {
        CheckUtil.exception(message);
    }

    protected boolean isLegality(String code) {
        return CheckUtil.isLegality(code);
    }

}
