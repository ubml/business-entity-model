package com.inspur.edp.cef.designtime.api.increment;

public interface DeletedIncrement extends Increment {

    String getDeleteId();

}
