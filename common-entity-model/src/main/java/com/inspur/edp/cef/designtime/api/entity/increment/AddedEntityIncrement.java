package com.inspur.edp.cef.designtime.api.entity.increment;

import com.inspur.edp.cef.designtime.api.entity.GspCommonDataType;
import com.inspur.edp.cef.designtime.api.increment.AddedIncrement;
import com.inspur.edp.cef.designtime.api.increment.IncrementType;

public class AddedEntityIncrement extends CommonEntityIncrement implements AddedIncrement {

    private GspCommonDataType addedDataType;


    public GspCommonDataType getAddedDataType() {
        return addedDataType;
    }

    public void setAddedDataType(GspCommonDataType addedDataType) {
        this.addedDataType = addedDataType;
        setId(addedDataType.getId());
    }

    @Override
    public final IncrementType getIncrementType() {
        return IncrementType.Added;
    }
}
