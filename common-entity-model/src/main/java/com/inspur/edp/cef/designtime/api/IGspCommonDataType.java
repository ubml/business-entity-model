package com.inspur.edp.cef.designtime.api;

import com.inspur.edp.cef.designtime.api.collection.CommonDtmCollection;
import com.inspur.edp.cef.designtime.api.collection.CommonValCollection;
import com.inspur.edp.cef.designtime.api.entity.ClassInfo;
import com.inspur.edp.cef.designtime.api.entity.CustomizationInfo;

import java.util.List;
import java.util.Map;

/**
 * The Interface Of CommonDataType
 *
 * @ClassName: IGspCommonDataType
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public interface IGspCommonDataType {

    /**
     * Get The Id Of Common Data Type
     *
     * @return The Id Of Common Data Type
     */
    String getID();

    /**
     * Set The Id Of Common Data Type
     *
     * @param value The Id To Be Setting
     */
    void setID(String value);


    /**
     * Get The Code Of Common Data Type
     *
     * @return The Code Of Common Data Type
     */
    String getCode();

    /**
     * Set The Code Of Common Data Type
     *
     * @param value The Code To Be Setting
     */
    void setCode(String value);

    /**
     * Get The Name Of Common Data Type
     *
     * @return The Name Of Common Data Type
     */
    String getName();

    /**
     * Set The Name Of Common Data Type
     *
     * @param value The Name To Be Setting
     */
    void setName(String value);

    /**
     * Get The Labels  Of Common Data Type
     *
     * @return The Labels Of Common Data Type
     */
    List<String> getBeLabel();

    /**
     * @param beLabel The Labels To Set Of Common Data Type
     */
    void setBeLabel(List<String> beLabel);

    /**
     * Get The Biz Tag Id List Of Common Data Type
     *
     * @return The Name Of Common Data Type
     */
    List<String> getBizTagIds();

    /**
     * @param bizTagIds Biz Tag Id List To Set
     */
    void setBizTagIds(List<String> bizTagIds);

    /**
     * Get Whether To Enable Big Numbers Of Common Data Type
     *
     * @return Enable Big Numbers Of Common Data Type
     */
    boolean isBigNumber();

    /**
     * @param bigNumber The Value Enable Big Numbers Of Common Data Type
     */
    void setIsBigNumber(boolean bigNumber);

    /**
     * Get The Elements Of Common Data Type
     *
     * @return The Elements Of Common Data Type
     */
    IFieldCollection getContainElements();

    /**
     * Get The Ref Value Of Common Data Type
     *
     * @return The Ref Value Of Common Data Type
     */
    boolean getIsRef();

    /**
     * @param value The Ref Value Of Common Data Type
     */
    void setIsRef(boolean value);

    /**
     * Get The Before Save Determinations Of Common Data Type
     *
     * @return The Before Save Determinations Of Common Data Type
     */
    CommonDtmCollection getDtmBeforeSave();

    CommonDtmCollection getDtmAfterSave();

    CommonDtmCollection getAllDtmBeforeSave();

    /**
     * @param value The Before Save Determinations Of Common Data Type
     */
    void setDtmBeforeSave(CommonDtmCollection value);
    void setDtmAfterSave(CommonDtmCollection value);

    /**
     * Get The After Modify Determinations Of Common Data Type
     *
     * @return The After Modify Determinations Of Common Data Type
     */
    CommonDtmCollection getDtmAfterModify();

    CommonDtmCollection getAllDtmAfterModify();

    /**
     * @param value The After Modify Determinations Of Common Data Type
     */
    void setDtmAfterModify(CommonDtmCollection value);

    /**
     * Get The After Create Determinations Of Common Data Type
     *
     * @return The After Create Determinations Of Common Data Type
     */
    CommonDtmCollection getDtmAfterCreate();

    /**
     * @param value The After Create Determinations Of Common Data Type
     */
    void setDtmAfterCreate(CommonDtmCollection value);

    /**
     * Get The Before Save Validations Of Common Data Type
     *
     * @return The Before Save Validations Of Common Data Type
     */
    CommonValCollection getValBeforeSave();

    CommonValCollection getallValBeforeSave();

    /**
     * @param value The Before Save Validations Of Common Data Type
     */
    void setValBeforeSave(CommonValCollection value);

    /**
     * Get The After Modify Validations Of Common Data Type
     *
     * @return The After Modify Validations Of Common Data Type
     */
    CommonValCollection getValAfterModify();

    CommonValCollection getAllValAfterModify();

    /**
     * @param value The After Modify Validations Of Common Data Type
     */
    void setValAfterModify(CommonValCollection value);

    /**
     * Get The I18nResourceInfoPrefix Of Common Data Type
     *
     * @return The I18nResourceInfoPrefix Of Common Data Type
     */
    String getI18nResourceInfoPrefix();

    /**
     * @param value The I18nResourceInfoPrefix Common Data Type
     */
    void setI18nResourceInfoPrefix(String value);

    /**
     * Get The Customization Info Of Common Data Type
     *
     * @return The Customization Info Of Common Data Type
     */
    CustomizationInfo getCustomizationInfo();


    /**
     * @param customizationInfo The Customization Info To Set
     */
    void setCustomizationInfo(CustomizationInfo customizationInfo);

    /**
     * Get The Generated Entity Class Info Of Common Data Type
     *
     * @return The Name Of Common Data Type
     */
    ClassInfo getGeneratedEntityClassInfo();


    /**
     * Find An Element By Element Id
     *
     * @param elementId The Primary Key Of The Element To Find
     * @return
     */
    IGspCommonField findElement(String elementId);
    // endregion

    Map<String, String> getExtendInfo();

    void setExtendInfo(Map<String, String> extendInfo);
}
