package com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.cm.parser;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.basic.parser.RangeRuleParser;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.cm.CommonDataTypeControlRule;

/**
 * The Json Parser Of CommonDataTypeRule
 *
 * @ClassName: CommonDataTypeRuleParser
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class CommonDataTypeRuleParser<T extends CommonDataTypeControlRule> extends RangeRuleParser<T> {

    protected boolean readRangeExtendControlRuleProperty(T ruleDefinition, String propName, JsonParser jsonParser, DeserializationContext deserializationContext) {
        if (super.readRangeExtendControlRuleProperty(ruleDefinition, propName, jsonParser, deserializationContext))
            return true;
        return readCommonDataTypeRuleExtendProperty(ruleDefinition, propName, jsonParser, deserializationContext);

    }

    protected boolean readCommonDataTypeRuleExtendProperty(T ruleDefinition, String propName, JsonParser jsonParser, DeserializationContext deserializationContext) {
        return false;
    }

    @Override
    protected final T createControlRule() {
        return (T) createCommonDataTypeRule();
    }

    protected CommonDataTypeControlRule createCommonDataTypeRule() {
        return new CommonDataTypeControlRule();
    }
}