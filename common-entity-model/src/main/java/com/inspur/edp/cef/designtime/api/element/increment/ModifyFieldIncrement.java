package com.inspur.edp.cef.designtime.api.element.increment;

import com.inspur.edp.cef.designtime.api.increment.IncrementType;
import com.inspur.edp.cef.designtime.api.increment.ModifyIncrement;
import com.inspur.edp.cef.designtime.api.increment.property.PropertyIncrement;

import java.util.HashMap;

/**
 * The Definition Of DimensionInfo
 *
 * @ClassName: DimensionInfo
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class ModifyFieldIncrement extends GspCommonFieldIncrement implements ModifyIncrement {

    private HashMap<String, PropertyIncrement> changeProperties;

    public HashMap<String, PropertyIncrement> getChangeProperties() {
        if (changeProperties == null)
            changeProperties = new HashMap<>();
        return changeProperties;
    }

    @Override
    public final IncrementType getIncrementType() {
        return IncrementType.Modify;
    }
}
