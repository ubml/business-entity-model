package com.inspur.edp.cef.designtime.api.collection;

import com.inspur.edp.cef.designtime.api.operation.CommonDetermination;
import com.inspur.edp.cef.designtime.api.util.Guid;

/**
 * The Collection Of Determination
 *
 * @ClassName: CommonDtmCollection
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class CommonDtmCollection extends BaseList<CommonDetermination> implements Cloneable {
    private static final long serialVersionUID = 1L;

    public CommonDtmCollection clone(boolean isGenerateId) {
        CommonDtmCollection col = new CommonDtmCollection();
        for (CommonDetermination var : this) {
            CommonDetermination dtm = (CommonDetermination) var.clone();
            if (isGenerateId) {
                dtm.setID(Guid.newGuid().toString());
            }
            col.add(dtm);
        }
        return col;
    }

    public CommonDtmCollection clone() {
        return clone(false);
    }
}