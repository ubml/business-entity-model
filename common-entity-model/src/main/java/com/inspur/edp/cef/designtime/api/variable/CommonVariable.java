package com.inspur.edp.cef.designtime.api.variable;

import com.inspur.edp.cef.designtime.api.entity.GspCommonField;

/**
 * The Definition Of Common Variable
 *
 * @ClassName: CommonVariable
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class CommonVariable extends GspCommonField {
    private VariableSourceType privateVariableSourceType = VariableSourceType.forValue(0);

    public final VariableSourceType getVariableSourceType() {
        return privateVariableSourceType;
    }

    public final void setVariableSourceType(VariableSourceType value) {
        privateVariableSourceType = value;
    }
}