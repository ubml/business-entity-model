package com.inspur.edp.cef.designtime.api.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The Definition Of DimensionInfo
 *
 * @ClassName: DimensionInfo
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class DynamicPropSetInfo {

    /**
     * 动态属性集合持久化构件
     */
    private MdRefInfo privateDynamicPropRepositoryComp;

    @JsonProperty("DynamicPropRepositoryComp")
    public final MdRefInfo getDynamicPropRepositoryComp() {
        return privateDynamicPropRepositoryComp;
    }

    public final void setDynamicPropRepositoryComp(MdRefInfo value) {
        privateDynamicPropRepositoryComp = value;
    }

    /**
     * 动态属性集合序列化构件
     */
    private MdRefInfo privateDynamicPropSerializerComp;

    @JsonProperty("DynamicPropSerializerComp")
    public final MdRefInfo getDynamicPropSerializerComp() {
        return privateDynamicPropSerializerComp;
    }

    public final void setDynamicPropSerializerComp(MdRefInfo value) {
        privateDynamicPropSerializerComp = value;
    }
}