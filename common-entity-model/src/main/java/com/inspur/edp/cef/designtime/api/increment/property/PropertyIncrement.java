package com.inspur.edp.cef.designtime.api.increment.property;

public abstract class PropertyIncrement {

    private boolean hasIncrement;

    public PropertyIncrement(boolean hasIncrement) {
        this.hasIncrement = hasIncrement;
    }

    public boolean hasIncrement() {
        return hasIncrement;
    }

    public void setHasIncrement(boolean hasIncrement) {
        this.hasIncrement = hasIncrement;
    }
}
