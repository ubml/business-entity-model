package com.inspur.edp.cef.designtime.api.entity.increment;

import com.inspur.edp.cef.designtime.api.element.increment.GspCommonFieldIncrement;
import com.inspur.edp.cef.designtime.api.increment.IncrementType;
import com.inspur.edp.cef.designtime.api.increment.ModifyIncrement;
import com.inspur.edp.cef.designtime.api.increment.property.PropertyIncrement;

import java.util.HashMap;

public class ModifyEntityIncrement extends CommonEntityIncrement implements ModifyIncrement {
    private HashMap<String, PropertyIncrement> changeProperties;
    private final HashMap<String, GspCommonFieldIncrement> fields = new HashMap<>();
    private final HashMap<String, CommonEntityIncrement> childEntitis = new HashMap<>();

    public HashMap<String, PropertyIncrement> getChangeProperties() {
        if (changeProperties == null)
            changeProperties = new HashMap<>();
        return changeProperties;
    }

    public HashMap<String, GspCommonFieldIncrement> getFields() {
        return fields;
    }

    public HashMap<String, CommonEntityIncrement> getChildEntitis() {
        return childEntitis;
    }

    @Override
    public final IncrementType getIncrementType() {
        return IncrementType.Modify;
    }
}
