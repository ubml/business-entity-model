package com.inspur.edp.cef.designtime.api.entity.increment;

import com.inspur.edp.cef.designtime.api.increment.DeletedIncrement;
import com.inspur.edp.cef.designtime.api.increment.IncrementType;

public class DeletedEntityIncrement extends CommonEntityIncrement implements DeletedIncrement {

    private final String deleteId;

    public DeletedEntityIncrement(String deleteId) {
        this.deleteId = deleteId;
        setId(deleteId);
    }

    public String getDeleteId() {
        return deleteId;
    }

    @Override
    public final IncrementType getIncrementType() {
        return IncrementType.Deleted;
    }
}
