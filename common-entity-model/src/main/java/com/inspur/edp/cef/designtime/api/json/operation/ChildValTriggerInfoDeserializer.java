package com.inspur.edp.cef.designtime.api.json.operation;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.deser.std.StringDeserializer;
import com.inspur.edp.cef.designtime.api.collection.ValElementCollection;
import com.inspur.edp.cef.designtime.api.exceptions.CommonEntityErrorCodeEnum;
import com.inspur.edp.cef.designtime.api.exceptions.CommonEntityException;
import com.inspur.edp.cef.designtime.api.json.CefNames;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.cef.designtime.api.operation.ChildValTriggerInfo;
import com.inspur.edp.cef.designtime.api.operation.ExecutingDataStatus;

import java.util.EnumSet;

public class ChildValTriggerInfoDeserializer extends JsonDeserializer<ChildValTriggerInfo> {
    @Override
    public ChildValTriggerInfo deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) {
        SerializerUtils.readStartObject(jsonParser);
        ChildValTriggerInfo childTriggerInfo = new ChildValTriggerInfo();
        while (jsonParser.getCurrentToken() == JsonToken.FIELD_NAME) {
            String propertyName = SerializerUtils.readPropertyName(jsonParser);
            switch (propertyName) {
                case CefNames.GetExecutingDataStatus:
                    childTriggerInfo.setGetExecutingDataStatus(readGetExecutingDataStatus(jsonParser));
                    break;
                case CefNames.RequestChildElements:
                    childTriggerInfo.setRequestChildElements(readRequestChildElements(jsonParser));
                    break;
                default:
                    throw CommonEntityException.createException(CommonEntityErrorCodeEnum.GSP_BEMODEL_COMMON_ENTITY_0006, "ChildValTriggerInfoDeserializer", propertyName);
            }
        }
        SerializerUtils.readEndObject(jsonParser);
        return childTriggerInfo;
    }

    protected EnumSet<ExecutingDataStatus> readGetExecutingDataStatus(JsonParser jsonParser) {
        EnumSet<ExecutingDataStatus> result = EnumSet.noneOf(ExecutingDataStatus.class);
        int intValueSum = SerializerUtils.readPropertyValue_Integer(jsonParser);
        ExecutingDataStatus[] values = ExecutingDataStatus.values();
        for (int i = values.length - 1; i >= 0; i--) {
            ExecutingDataStatus value = values[i];
            if (intValueSum > 0 && intValueSum >= value.getValue()) {
                result.add(value);
                intValueSum -= value.getValue();
            }
        }
        return result;
    }

    private ValElementCollection readRequestChildElements(JsonParser jsonParser) {
        ValElementCollection collection = new ValElementCollection();
        SerializerUtils.readArray(jsonParser, new StringDeserializer(), collection, true);
        return collection;
    }
}