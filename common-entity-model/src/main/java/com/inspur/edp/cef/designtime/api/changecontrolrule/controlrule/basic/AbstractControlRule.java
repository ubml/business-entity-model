package com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.basic;


import java.util.HashMap;
import java.util.Map;

/**
 * The Abstract Definition Of ControlRule
 *
 * @ClassName: AbstractControlRule
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public abstract class AbstractControlRule {
    private String ruleDefId;
    private String ruleId;
    private final Map<String, ControlRuleItem> selfControlRules = new HashMap<>();
    private final Map<String, Map<String, AbstractControlRule>> childRules = new HashMap<>();

    //region 规则定义Id
    public String getRuleDefId() {
        return ruleDefId;
    }

    public void setRuleDefId(String ruleDefId) {
        this.ruleDefId = ruleDefId;
    }
    //endregion

    //region 规则Id
    public String getRuleId() {
        return ruleId;
    }

    public void setRuleId(String ruleId) {
        this.ruleId = ruleId;
    }
    //endregion

    //region 规则项集合
    public Map<String, ControlRuleItem> getSelfControlRules() {
        return selfControlRules;
    }

    public ControlRuleItem getControlRule(String ruleName) {
        if (getSelfControlRules().containsKey(ruleName))
            return getSelfControlRules().get(ruleName);
        return null;
    }

    public void setControlRule(String ruleName, ControlRuleItem ruleItem) {
        getSelfControlRules().put(ruleName, ruleItem);
    }
    //endregion

    //region 子控制规则
    public Map<String, Map<String, AbstractControlRule>> getChildRules() {
        return childRules;
    }
    //endregion
}
