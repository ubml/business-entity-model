package com.inspur.edp.cef.designtime.api.dtconsistencycheck;

/**
 * The Definition Of DimensionInfo
 *
 * @ClassName: DimensionInfo
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class ConsistencyCheckEventMessage {

    public Boolean isCanceled;
    public String message;

    public Boolean getCanceled() {
        return isCanceled;
    }

    public void setCanceled(Boolean canceled) {
        isCanceled = canceled;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ConsistencyCheckEventMessage(Boolean isCanceled, String message) {
        this.isCanceled = isCanceled;
        this.message = message;
    }
}


