package com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.cm.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.basic.serializer.AbstractControlRuleSerializer;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.cm.CommonFieldControlRule;

/**
 * The Json Serializer Of CommonFieldRule
 *
 * @ClassName: CommonFieldRuleSerializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class CommonFieldRuleSerializer<T extends CommonFieldControlRule> extends AbstractControlRuleSerializer<T> {
    @Override
    protected final void writeExtendInfos(T controlRule, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) {
        super.writeExtendInfos(controlRule, jsonGenerator, serializerProvider);
        writeCommonFieldRuleExtendInfos(controlRule, jsonGenerator, serializerProvider);
    }

    protected void writeCommonFieldRuleExtendInfos(T controlRule, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) {

    }
}
