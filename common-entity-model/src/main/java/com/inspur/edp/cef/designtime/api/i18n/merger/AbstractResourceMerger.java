package com.inspur.edp.cef.designtime.api.i18n.merger;


import com.inspur.edp.cef.designtime.api.i18n.context.CefResourcePrefixInfo;
import com.inspur.edp.cef.designtime.api.i18n.context.ICefResourceMergeContext;

public abstract class AbstractResourceMerger {
    protected AbstractResourceMerger(
            ICefResourceMergeContext context, CefResourcePrefixInfo parentResourceInfo) {
        this.context = context;
        this.parentResourcePrefixInfo = parentResourceInfo;
    }

    protected AbstractResourceMerger(ICefResourceMergeContext context) {
        this.context = context;
    }

    //#region 属性


    private final ICefResourceMergeContext context;

    protected final ICefResourceMergeContext getContext() {
        return context;
    }


    private CefResourcePrefixInfo parentResourcePrefixInfo;

    protected final CefResourcePrefixInfo getParentResourcePrefixInfo() {
        return parentResourcePrefixInfo;
    }


    public void merge() {
        mergeItems();
    }


    protected abstract void mergeItems();


}
