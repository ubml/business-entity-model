package com.inspur.edp.cef.designtime.api.variable.inspaction;

import com.inspur.edp.cef.designtime.api.validate.element.FieldChecker;
import com.inspur.edp.cef.designtime.api.variable.CommonVariableEntity;

/**
 * The Definition Of DimensionInfo
 *
 * @ClassName: DimensionInfo
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class VariableChecker extends CommonVariableChecker {
    private static VariableChecker varObj;

    public static VariableChecker getInstance() {
        if (varObj == null) {
            varObj = new VariableChecker();
        }
        return varObj;
    }

    public final void checkVariable(CommonVariableEntity entity) {
        checkCVC(entity);
    }

    @Override
    protected FieldChecker getFieldChecker() {
        return VarFieldChecker.getInstance();
    }
}
