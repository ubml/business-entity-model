package com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.basic.parser;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.basic.ControlRuleItem;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.basic.ControlRuleNames;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.entity.ControlRuleValue;
import com.inspur.edp.cef.designtime.api.exceptions.CommonEntityErrorCodeEnum;
import com.inspur.edp.cef.designtime.api.exceptions.CommonEntityException;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;

/**
 * The Json Parser Of ControlRuleItem
 *
 * @ClassName: ControlRuleItemParser
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class ControlRuleItemParser extends JsonDeserializer<ControlRuleItem> {
    @Override
    public ControlRuleItem deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) {
        SerializerUtils.readStartObject(jsonParser);
        ControlRuleItem item = new ControlRuleItem();
        while (jsonParser.getCurrentToken() == JsonToken.FIELD_NAME) {
            String propertyName = SerializerUtils.readPropertyName(jsonParser);
            switch (propertyName) {
                case ControlRuleNames.RuleName:
                    item.setRuleName(SerializerUtils.readPropertyValue_String(jsonParser));
                    break;
                case ControlRuleNames.RuleValue:
                    item.setControlRuleValue(SerializerUtils.readPropertyValue_Enum(jsonParser, ControlRuleValue.class, ControlRuleValue.values()));
                    break;
                default:
                    throw CommonEntityException.createException(CommonEntityErrorCodeEnum.GSP_BEMODEL_COMMON_ENTITY_0006, "ControlRuleItemParser", propertyName);
            }
        }
        SerializerUtils.readEndObject(jsonParser);
        return item;
    }
}