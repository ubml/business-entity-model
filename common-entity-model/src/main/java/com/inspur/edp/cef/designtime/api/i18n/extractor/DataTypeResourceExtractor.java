package com.inspur.edp.cef.designtime.api.i18n.extractor;

import com.inspur.edp.cef.designtime.api.IFieldCollection;
import com.inspur.edp.cef.designtime.api.IGspCommonDataType;
import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.designtime.api.exceptions.CommonEntityErrorCodeEnum;
import com.inspur.edp.cef.designtime.api.exceptions.CommonEntityException;
import com.inspur.edp.cef.designtime.api.i18n.context.CefResourcePrefixInfo;
import com.inspur.edp.cef.designtime.api.i18n.context.ICefResourceExtractContext;
import com.inspur.edp.cef.designtime.api.i18n.names.CefResourceDescriptionNames;
import com.inspur.edp.cef.designtime.api.i18n.names.CefResourceKeyNames;

public abstract class DataTypeResourceExtractor extends AbstractResourceExtractor {

    private final IGspCommonDataType dataType;

    protected DataTypeResourceExtractor(IGspCommonDataType commonDataType, ICefResourceExtractContext context, CefResourcePrefixInfo parentResourceInfo) {
        super(context, parentResourceInfo);
        dataType = commonDataType;
    }

    protected final void extractItems() {
        //Name
        addResourceInfo(CefResourceKeyNames.Name, dataType.getName(), CefResourceDescriptionNames.Name);
        extractDataTypeFields(dataType.getContainElements());
        //扩展
        extractExtendProperties(dataType);
    }

    protected final CefResourcePrefixInfo buildCurrentPrefix() {
        CefResourcePrefixInfo prefixInfo = new CefResourcePrefixInfo();
        if (getParentResourcePrefixInfo() == null) {
            throw CommonEntityException.createException(CommonEntityErrorCodeEnum.GSP_BEMODEL_COMMON_ENTITY_0018, dataType.getName());

        }
        prefixInfo.setResourceKeyPrefix(getParentResourcePrefixInfo().getResourceKeyPrefix() + "." + dataType.getCode());
        prefixInfo.setDescriptionPrefix(getParentResourcePrefixInfo().getDescriptionPrefix() + "中'" + dataType.getName() + "'实体");
        return prefixInfo;
    }

    /**
     * 赋值节点的国际化项前缀
     */
    protected final void setPrefixInfo() {
        dataType.setI18nResourceInfoPrefix(getCurrentResourcePrefixInfo().getResourceKeyPrefix());
    }

    //#region 私有方法

    private void extractDataTypeFields(IFieldCollection fields) {
        if (fields == null || fields.isEmpty()) {
            return;
        }
        for (IGspCommonField field : fields) {
            CefFieldResourceExtractor extractor = getCefFieldResourceExtractor(getContext(), getCurrentResourcePrefixInfo(), field);
            extractor.extract();
        }


    }

    //#endregion

    /**
     * 获取字段抽取器
     *
     * @param context
     * @return
     */
    protected abstract CefFieldResourceExtractor getCefFieldResourceExtractor(ICefResourceExtractContext context, CefResourcePrefixInfo objPrefixInfo, IGspCommonField field);

    /**
     * 抽取扩展项
     *
     * @param dataType
     */
    protected void extractExtendProperties(IGspCommonDataType dataType) {
    }
}