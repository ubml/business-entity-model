package com.inspur.edp.cef.designtime.api.i18n.context;

import com.inspur.edp.cef.designtime.api.util.DataValidator;
import com.inspur.edp.lcm.metadata.api.entity.I18nResourceItem;
import com.inspur.edp.lcm.metadata.api.entity.I18nResourceItemCollection;

/**
 * 多语资源项抽取上下文
 */
public class CefResourceExtractContext implements ICefResourceExtractContext {
    //#region 字段
    private final String metaPrefix;
    private final I18nResourceItemCollection resourceItems;
    //#endregion

    public CefResourceExtractContext(String metaPrefix, I18nResourceItemCollection items) {
        DataValidator.checkForEmptyString(metaPrefix, "元数据前缀");
        DataValidator.checkForNullReference(items, "国际化资源项");
        this.metaPrefix = metaPrefix;
        this.resourceItems = items;
    }

    /**
     * 获取前缀
     *
     * @return
     */
    public final String getKeyPrefix() {
        return metaPrefix;
    }

    /**
     * 新增/修改
     *
     * @param info
     */
    public final void setResourceItem(CefResourceInfo info) {
        String key = info.getResourceKey();
        String value = info.getResourceValue();
        String description = info.getDescription();

        I18nResourceItem resource = GetNewResource(key, value, description);

        resourceItems.add(resource);
    }

    //#region 私有方法
    private I18nResourceItem GetNewResource(String key, String value, String description) {
        I18nResourceItem tempVar = new I18nResourceItem();
        tempVar.setKey(key);
        tempVar.setValue(value);
        tempVar.setComment(description);
        return tempVar;
    }
    //#endregion
}