package com.inspur.edp.cef.designtime.api.json.element;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.cef.designtime.api.element.ForeignKeyConstraintType;
import com.inspur.edp.cef.designtime.api.element.GspAssociation;
import com.inspur.edp.cef.designtime.api.element.GspAssociationKey;
import com.inspur.edp.cef.designtime.api.element.GspDeleteRuleType;
import com.inspur.edp.cef.designtime.api.json.CefNames;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.lcm.metadata.api.entity.ProcessMode;
import lombok.var;

/**
 * The Json Serializer Of GspAssociation
 *
 * @ClassName: GspAssociationSerializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class GspAssociationSerializer extends JsonSerializer<GspAssociation> {

    private final CefFieldSerializer eleConvertor;
    protected boolean isFull = true;

    public GspAssociationSerializer(boolean full) {
        isFull = full;
        this.eleConvertor = new CefFieldSerializer(isFull);
    }

    public GspAssociationSerializer(boolean full, CefFieldSerializer eleConvertor) {
        isFull = full;
        this.eleConvertor = eleConvertor;
    }

    public GspAssociationSerializer() {
        this.eleConvertor = new CefFieldSerializer();
    }

    public GspAssociationSerializer(CefFieldSerializer eleConvertor) {
        this.eleConvertor = eleConvertor;
    }

    @Override
    public void serialize(GspAssociation value, JsonGenerator writer, SerializerProvider serializers) {
        SerializerUtils.writeStartObject(writer);
        writeBaseProperty(value, writer);
        writeSelfProperty(value, writer);
        SerializerUtils.writeEndObject(writer);
    }


    //region BaseProp
    private void writeBaseProperty(GspAssociation value, JsonGenerator writer) {
        SerializerUtils.writePropertyValue(writer, CefNames.ID, value.getId());
        if (isFull) {
            SerializerUtils.writePropertyValue(writer, CefNames.I18nResourceInfoPrefix, value.getI18nResourceInfoPrefix());
        } else if (value.getBelongElement() != null && value.getBelongElement().getI18nResourceInfoPrefix() != null && !"".equals(value.getBelongElement().getI18nResourceInfoPrefix()) && !(value.getBelongElement().getI18nResourceInfoPrefix().equals(value.getI18nResourceInfoPrefix()))) {
            SerializerUtils.writePropertyValue(writer, CefNames.I18nResourceInfoPrefix, value.getI18nResourceInfoPrefix());
        } else if (value.getBelongElement() == null || value.getBelongElement().getI18nResourceInfoPrefix() == null || "".equals(value.getBelongElement().getI18nResourceInfoPrefix())) {
            SerializerUtils.writePropertyValue(writer, CefNames.I18nResourceInfoPrefix, value.getI18nResourceInfoPrefix());
        }
        this.writeRefElementCollection(value, writer);
        this.writeExtendAssoBaseProperty(value, writer);
    }

    private void writeRefElementCollection(GspAssociation ass, JsonGenerator writer) {
        if (isFull || !ass.getRefElementCollection().isEmpty()) {
            SerializerUtils.writePropertyName(writer, CefNames.RefElementCollection);
            SerializerUtils.WriteStartArray(writer);
            if (!ass.getRefElementCollection().isEmpty()) {
                for (var refElement : ass.getRefElementCollection()) {
                    this.eleConvertor.serialize(refElement, writer, null);
                }
            }
            SerializerUtils.WriteEndArray(writer);
        }
    }

    protected void writeExtendAssoBaseProperty(GspAssociation association, JsonGenerator writer) {
    }
    //endregion

    //region SelfProp
    private void writeSelfProperty(GspAssociation association, JsonGenerator writer) {
        if (isFull || (association.getRefModelID() != null && !association.getRefModelID().isEmpty())) {
            SerializerUtils.writePropertyValue(writer, CefNames.RefModelID, association.getRefModelID());
        }
        if (isFull || (association.getRefModelName() != null && !association.getRefModelName().isEmpty())) {
            SerializerUtils.writePropertyValue(writer, CefNames.RefModelName, association.getRefModelName());
        }
        if (isFull || (association.getRefModelCode() != null && !"".equals(association.getRefObjectCode()))) {
            SerializerUtils.writePropertyValue(writer, CefNames.RefModelCode, association.getRefModelCode());
        }
        //只有生成型的才会写入文件
        if ((association.getRefModelMode() == ProcessMode.interpretation)) {
            //当前是生成型
            SerializerUtils.writePropertyValue(writer, CefNames.RefModelMode, association.getRefModelMode());
        }

        if (isFull || (association.getRefModelPkgName() != null && !association.getRefModelPkgName().isEmpty())) {
            SerializerUtils.writePropertyValue(writer, CefNames.RefModelPkgName, association.getRefModelPkgName());
        }
        if (isFull || (association.getRefTableAlias() != null && !"".equals(association.getRefTableAlias()))) {
            SerializerUtils.writePropertyValue(writer, CefNames.RefTableAlias, association.getRefTableAlias());
        }
        if (isFull || (association.getRefModelID() != null && !association.getRefModelID().isEmpty())) {
            SerializerUtils.writePropertyValue(writer, CefNames.Asso_RefObjectID, association.getRefObjectID());
        }
        if (isFull || (association.getRefObjectCode() != null && !association.getRefObjectCode().isEmpty())) {
            SerializerUtils.writePropertyValue(writer, CefNames.Asso_RefObjectCode, association.getRefObjectCode());
        }
        if (isFull || (association.getRefObjectName() != null && !association.getRefObjectName().isEmpty())) {
            SerializerUtils.writePropertyValue(writer, CefNames.Asso_RefObjectName, association.getRefObjectName());
        }
        if (isFull || (association.isKeepAssoPropertyForExpression())) {
            SerializerUtils.writePropertyValue(writer, CefNames.keepAssoPropertyForExpression, association.isKeepAssoPropertyForExpression());
        }
        this.writeKeyCollection(writer, association);
        if (isFull || (association.getWhere() != null && !association.getWhere().isEmpty())) {
            SerializerUtils.writePropertyValue(writer, CefNames.Where, association.getWhere());
        }
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            if (isFull || (association.getAssoConditions() != null && !association.getAssoConditions().isEmpty())) {
                SerializerUtils.writePropertyValue(writer, CefNames.AssoConditions, objectMapper.writeValueAsString(association.getAssoConditions()));
            }
        } catch (JsonProcessingException e) {

        }
        try {
            if (isFull || (association.getAssoVariables() != null && !association.getAssoVariables().isEmpty())) {
                SerializerUtils.writePropertyValue(writer, CefNames.AssoVariables, objectMapper.writeValueAsString(association.getAssoVariables()));
            }
        } catch (JsonProcessingException e) {

        }
        if (isFull || (association.getAssSendMessage() != null && !association.getAssSendMessage().isEmpty())) {
            SerializerUtils.writePropertyValue(writer, CefNames.AssSendMessage, association.getAssSendMessage());
        }
        if (isFull || (association.getForeignKeyConstraintType() != ForeignKeyConstraintType.Permit)) {
            SerializerUtils.writePropertyValue(writer, CefNames.ForeignKeyConstraintType, association.getForeignKeyConstraintType().getValue());
        }
        if (isFull || (association.getDeleteRuleType() != GspDeleteRuleType.Refuse)) {
            SerializerUtils.writePropertyValue(writer, CefNames.DeleteRuleType, association.getDeleteRuleType().getValue());
        }
        if (isFull || association.getAssoModelInfo() != null) {
            if (isFull || ((association.getAssoModelInfo().getModelConfigId() != null && !association.getAssoModelInfo().getModelConfigId().isEmpty())
                    || (association.getAssoModelInfo().getMainObjCode() != null && !association.getAssoModelInfo().getMainObjCode().isEmpty()))) {
                SerializerUtils.writePropertyValue(writer, CefNames.AssoModelInfo, association.getAssoModelInfo());
            }
        }

        this.writeExtendAssoSelfProperty(writer, association);
    }

    private void writeKeyCollection(JsonGenerator writer, GspAssociation ass) {
        if (isFull || !ass.getKeyCollection().isEmpty()) {
            SerializerUtils.writePropertyName(writer, CefNames.KeyCollection);
            SerializerUtils.WriteStartArray(writer);
            if (!ass.getKeyCollection().isEmpty()) {
                for (GspAssociationKey assoKey : ass.getKeyCollection())
                    if (assoKey != null) {
                        SerializerUtils.writePropertyValue_Object(writer, assoKey);
                    }
            }
            SerializerUtils.WriteEndArray(writer);
        }

    }

    protected void writeExtendAssoSelfProperty(JsonGenerator writer, GspAssociation bizElement) {
    }
    //endregion
}
