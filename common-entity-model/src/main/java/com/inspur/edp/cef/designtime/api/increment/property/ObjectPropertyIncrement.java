package com.inspur.edp.cef.designtime.api.increment.property;

import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.inspur.edp.cef.designtime.api.json.element.CefFieldDeserializer;

public class ObjectPropertyIncrement extends NativePropertyIncrement {

    public ObjectPropertyIncrement(boolean hasIncrement) {
        super(hasIncrement);
    }

    public Class getPropertyIncrementClass() {
        return Object.class;
    }

    @Override
    public IncrementPropType getPropertyType() {
        return IncrementPropType.Object;
    }

    public JsonSerializer getJsonSerializer() {
        return null;
    }

    public JsonDeserializer getJsonDeserializer() {
        return null;
    }

    public CefFieldDeserializer getCefFieldDeserializer() {
        return cefFieldDeserializer;
    }

    public void setCefFieldDeserializer(CefFieldDeserializer cefFieldDeserializer) {
        this.cefFieldDeserializer = cefFieldDeserializer;
    }

    private CefFieldDeserializer cefFieldDeserializer;

}
