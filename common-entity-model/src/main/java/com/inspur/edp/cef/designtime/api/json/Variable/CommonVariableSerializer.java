package com.inspur.edp.cef.designtime.api.json.Variable;

import com.fasterxml.jackson.core.JsonGenerator;
import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.cef.designtime.api.json.element.CefFieldSerializer;
import com.inspur.edp.cef.designtime.api.variable.CommonVariable;
import com.inspur.edp.cef.designtime.api.variable.VariableSourceType;

/**
 * The Json Serializer Of Common Variable
 *
 * @ClassName: CommonVariableSerializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class CommonVariableSerializer extends CefFieldSerializer {
    @Override
    protected void writeExtendFieldBaseProperty(JsonGenerator writer, IGspCommonField field) {

    }

    public CommonVariableSerializer() {
    }

    public CommonVariableSerializer(boolean full) {
        super(full);
        isFull = full;
    }


    @Override
    protected void writeExtendFieldSelfProperty(JsonGenerator writer, IGspCommonField field) {
        CommonVariable commonVariable = (CommonVariable) field;
        if (isFull || (commonVariable.getVariableSourceType() != null && commonVariable.getVariableSourceType() != VariableSourceType.BE))
            SerializerUtils.writePropertyValue(writer, "VariableSourceType", commonVariable.getVariableSourceType().toString());
    }
}
