package com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.basic.parser;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.basic.AbstractControlRule;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.basic.ControlRuleItem;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.basic.ControlRuleNames;
import com.inspur.edp.cef.designtime.api.exceptions.CommonEntityErrorCodeEnum;
import com.inspur.edp.cef.designtime.api.exceptions.CommonEntityException;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static com.fasterxml.jackson.core.JsonToken.END_ARRAY;
import static com.fasterxml.jackson.core.JsonToken.FIELD_NAME;

/**
 * The Abstract Json Parser Of ControlRule
 *
 * @ClassName: AbstractControlRuleParser
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public abstract class AbstractControlRuleParser<T extends AbstractControlRule> extends JsonDeserializer<T> {
    @Override
    public T deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        T ruleDefinition = createControlRule();
        SerializerUtils.readStartObject(jsonParser);
        readSelfInfos(ruleDefinition, jsonParser, deserializationContext);
        SerializerUtils.readEndObject(jsonParser);
        return ruleDefinition;
    }


    private void readSelfInfos(T controlRule, JsonParser jsonParser, DeserializationContext deserializationContext) {
        while (jsonParser.getCurrentToken() == FIELD_NAME) {
            String propName = SerializerUtils.readPropertyName(jsonParser);
            switch (propName) {
                case ControlRuleNames.RuleId:
                    controlRule.setRuleId(SerializerUtils.readPropertyValue_String(jsonParser));
                    break;
                case ControlRuleNames.RuleDefId:
                    controlRule.setRuleDefId(SerializerUtils.readPropertyValue_String(jsonParser));
                    break;
                case ControlRuleNames.SelfControlRules:
                    readSelfControlRules(controlRule, jsonParser, deserializationContext);
                    break;
                case ControlRuleNames.ChildControlRules:
                    readChildControlRules(controlRule, jsonParser, deserializationContext);
                    break;
                default:
                    if (!readExtendControlRuleProperty(controlRule, propName, jsonParser, deserializationContext)) {
                        throw CommonEntityException.createException(CommonEntityErrorCodeEnum.GSP_BEMODEL_COMMON_ENTITY_0006, "AbstractControlRuleParser", propName);
                    }
                    break;
            }
        }
    }

    private void readSelfControlRules(AbstractControlRule ruleDefinition, JsonParser jsonParser, DeserializationContext deserializationContext) {
        SerializerUtils.readStartArray(jsonParser);
        JsonToken tokenType = jsonParser.getCurrentToken();
        if (tokenType != END_ARRAY) {
            while (jsonParser.getCurrentToken() == tokenType) {
                try {
                    jsonParser.nextToken();
                    jsonParser.nextToken();
                    jsonParser.nextToken();
                    jsonParser.nextToken();
                    ControlRuleItem item = SerializerUtils.readPropertyValue_Object(ControlRuleItem.class, jsonParser);
                    ruleDefinition.getSelfControlRules().put(item.getRuleName(), item);
                    jsonParser.nextToken();
                } catch (IOException e) {
                    throw CommonEntityException.createException(CommonEntityErrorCodeEnum.GSP_BEMODEL_JSON_0001, e, "ControlRuleItem");
                }
            }
        }
        SerializerUtils.readEndArray(jsonParser);
    }

    private void readChildControlRules(AbstractControlRule ruleDefinition, JsonParser jsonParser, DeserializationContext deserializationContext) {
        SerializerUtils.readStartArray(jsonParser);

        JsonToken tokenType = jsonParser.getCurrentToken();
        if (tokenType != END_ARRAY) {
            while (jsonParser.getCurrentToken() == tokenType) {
                try {
                    jsonParser.nextToken();
                    jsonParser.nextToken();
                    String childType = jsonParser.getValueAsString();
                    jsonParser.nextToken();
                    jsonParser.nextToken();
                    Map<String, AbstractControlRule> item = readChildControlRule(childType, jsonParser, deserializationContext);
                    ruleDefinition.getChildRules().put(childType, item);
                    jsonParser.nextToken();
                } catch (IOException e) {
                    throw CommonEntityException.createException(CommonEntityErrorCodeEnum.GSP_BEMODEL_JSON_0001, e, "ChildControlRules");
                }
            }
        }
        SerializerUtils.readEndArray(jsonParser);
    }

    private Map<String, AbstractControlRule> readChildControlRule(String childType, JsonParser jsonParser, DeserializationContext deserializationContext) {
        Map<String, AbstractControlRule> map = new HashMap<>();
        SerializerUtils.readStartArray(jsonParser);
        JsonToken tokenType = jsonParser.getCurrentToken();
        if (tokenType != END_ARRAY) {
            while (jsonParser.getCurrentToken() == tokenType) {
                try {
                    jsonParser.nextToken();
                    String key = jsonParser.getValueAsString();
                    jsonParser.nextToken();
                    JsonDeserializer deserializer = getChildDeserializer(childType);
                    AbstractControlRule item = (AbstractControlRule) deserializer.deserialize(jsonParser, deserializationContext);
                    map.put(key, item);
                    jsonParser.nextToken();
                } catch (IOException e) {
                    throw CommonEntityException.createException(CommonEntityErrorCodeEnum.GSP_BEMODEL_JSON_0001, e, "AbstractControlRule");
                }
            }
        }
//        JsonToken tokentype = jsonParser.getCurrentToken();
//        if (tokentype != END_ARRAY) {
//            while (jsonParser.getCurrentToken() == FIELD_NAME) {
//                try {
//                    String childName = SerializerUtils.readPropertyName(jsonParser);
//                    AbstractControlRule item = SerializerUtils.readPropertyValue_Object(AbstractControlRule.class, jsonParser);
//                    map.put(childName, item);
//                    jsonParser.nextToken();
//                } catch (IOException e) {
//                    throw new RuntimeException(e);
//                }
//            }
//        }
        SerializerUtils.readEndArray(jsonParser);
        return map;
    }

    protected Class getChildTypes(String childTypeName) {
        return AbstractControlRule.class;
    }

    protected boolean readExtendControlRuleProperty(T ruleDefinition, String propName, JsonParser jsonParser, DeserializationContext deserializationContext) {
        return false;
    }

    protected abstract T createControlRule();

    protected JsonDeserializer getChildDeserializer(String childTypeName) {
        return null;
    }
}
