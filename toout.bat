@echo off

set LCM_SU_PATH="server\platform\runtime\lcm\libs\"


set PFCOMMON_SU_PATH="server\platform\common\libs\"
set DEV_SU_PATH="server\platform\dev\main\libs\"
set METADATA_TOOL_PATH="tools\deploy\metadata\runtime\libs"

for /f "tokens=*" %%i in ('mvn help:evaluate "-Dexpression=project.version" "-DforceStdout=true" -q') do set version=%%i
ECHO version=%version%

rmdir /q /s .\out

MKDIR .\out\server\platform\runtime\lcm\libs
MKDIR .\out\server\platform\common\libs
MKDIR .\out\server\platform\dev\main\libs
MKDIR .\out\server\platform\common\resources

copy .\business-entity-extendinfo-server-api\target\business-entity-extendinfo-server-api-%version%.jar .\out\server\platform\runtime\lcm\libs\bef-extendinfo-server-api.jar
copy .\business-entity-extendinfo-server-core\target\business-entity-extendinfo-server-core-%version%.jar .\out\server\platform\runtime\lcm\libs\bef-extendinfo-server-core.jar
copy .\business-entity-manager\target\business-entity-manager-%version%.jar .\out\server\platform\common\libs\com.inspur.edp.bef.bemanager.jar
copy .\business-entity-model\target\business-entity-model-%version%.jar .\out\server\platform\common\libs\com.inspur.edp.bef.bizentity.jar
copy .\common-entity-manager\target\common-entity-manager-%version%.jar .\out\server\platform\common\libs\com.inspur.edp.cef.designtime.core.jar
copy .\common-entity-model\target\common-entity-model-%version%.jar .\out\server\platform\common\libs\com.inspur.edp.cef.designtime.api.jar
copy .\common-model\target\common-model-%version%.jar .\out\server\platform\common\libs\com.inspur.edp.das.commonmodel.jar
copy .\unified-datatype-model\target\unified-datatype-model-%version%.jar .\out\server\platform\common\libs\com.inspur.edp.udt.designtime.api.jar
copy .\unified-datatype-manager\target\unified-datatype-manager-%version%.jar .\out\server\platform\common\libs\com.inspur.edp.udt.designtime.manager.jar
copy .\business-entity-webapi\target\business-entity-webapi-%version%.jar .\out\server\platform\dev\main\libs\com.inspur.edp.bef.bizentity.webapi.jar
copy .\unified-datatype-webapi\target\unified-datatype-webapi-%version%.jar .\out\server\platform\dev\main\libs\com.inspur.edp.udt.designtime.webapi.jar


MKDIR .\out\tools\deploy\metadata\runtime\libs
copy .\business-entity-extendinfo-server-api\target\business-entity-extendinfo-server-api-%version%.jar .\out\tools\deploy\metadata\runtime\libs\bef-extendinfo-server-api.jar
copy .\business-entity-extendinfo-server-core\target\business-entity-extendinfo-server-core-%version%.jar .\out\tools\deploy\metadata\runtime\libs\bef-extendinfo-server-core.jar
copy .\business-entity-manager\target\business-entity-manager-%version%.jar .\out\tools\deploy\metadata\runtime\libs\com.inspur.edp.bef.bemanager.jar
copy .\business-entity-model\target\business-entity-model-%version%.jar .\out\tools\deploy\metadata\runtime\libs\com.inspur.edp.bef.bizentity.jar
copy .\common-entity-model\target\common-entity-model-%version%.jar .\out\tools\deploy\metadata\runtime\libs\com.inspur.edp.cef.designtime.api.jar
copy .\common-model\target\common-model-%version%.jar .\out\tools\deploy\metadata\runtime\libs\com.inspur.edp.das.commonmodel.jar
copy .\unified-datatype-model\target\unified-datatype-model-%version%.jar .\out\tools\deploy\metadata\runtime\libs\com.inspur.edp.udt.designtime.api.jar
copy .\unified-datatype-manager\target\unified-datatype-manager-%version%.jar .\out\tools\deploy\metadata\runtime\libs\com.inspur.edp.udt.designtime.manager.jar


XCOPY .\i18nresources .\out\server\platform\common\resources /S

::pause