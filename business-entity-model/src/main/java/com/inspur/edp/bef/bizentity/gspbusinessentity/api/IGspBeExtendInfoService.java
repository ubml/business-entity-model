package com.inspur.edp.bef.bizentity.gspbusinessentity.api;

import com.inspur.edp.bef.bizentity.gspbusinessentity.entity.GspBeExtendInfo;

import java.util.List;

public interface IGspBeExtendInfoService {
    /**
     * 根据ID获取某条BE扩展信息
     *
     * @param id
     * @return
     */
    GspBeExtendInfo getBeExtendInfo(String id);

    /**
     * 根据configId获取某条BE扩展信息
     *
     * @param configId
     * @return
     */
    GspBeExtendInfo getBeExtendInfoByConfigId(String configId);

    /**
     * 获取所有BE扩展信息
     *
     * @return
     */
    List<GspBeExtendInfo> getBeExtendInfos();

    /**
     * 保存
     *
     * @param infos
     */
    void saveGspBeExtendInfos(List<GspBeExtendInfo> infos);

    void deleteBeExtendInfo(String id);
}
