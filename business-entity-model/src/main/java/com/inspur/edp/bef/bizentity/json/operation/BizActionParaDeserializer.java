package com.inspur.edp.bef.bizentity.json.operation;

import com.inspur.edp.bef.bizentity.operation.beomponent.BizActionParameter;
import com.inspur.edp.bef.bizentity.operation.componentbase.BizParameter;

public class BizActionParaDeserializer extends BizParaDeserializer {
    @Override
    protected BizParameter createBizPara() {
        return new BizActionParameter();
    }
}
