package com.inspur.edp.bef.bizentity.json.operation;

import com.fasterxml.jackson.core.JsonGenerator;
import com.inspur.edp.bef.bizentity.operation.BizOperation;
import com.inspur.edp.bef.bizentity.operation.TccAction;

public class TccActionSerializer extends BizOperationSerializer<TccAction> {

    public TccActionSerializer() {
    }

    public TccActionSerializer(boolean full) {
        super(full);
        isFull = full;
    }

    @Override
    protected void writeExtendOperationBaseProperty(JsonGenerator writer, BizOperation op) {

    }

    @Override
    protected void writeExtendOperationSelfProperty(JsonGenerator writer, BizOperation op) {

    }
}
