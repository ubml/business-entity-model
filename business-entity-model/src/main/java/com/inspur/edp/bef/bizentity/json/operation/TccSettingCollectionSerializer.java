package com.inspur.edp.bef.bizentity.json.operation;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.bef.bizentity.operation.collection.TccSettingCollection;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;

import java.io.IOException;

public class TccSettingCollectionSerializer extends JsonSerializer<TccSettingCollection> {

    protected boolean isFull = true;

    public TccSettingCollectionSerializer() {
    }

    public TccSettingCollectionSerializer(boolean full) {
        isFull = full;
    }

    @Override
    public void serialize(TccSettingCollection value, JsonGenerator gen,
                          SerializerProvider serializers) throws IOException {
        writeBaseProperty(value, gen);
    }

    private void writeBaseProperty(TccSettingCollection settingCollection, JsonGenerator writer) {
        if (settingCollection.size() == 0) {
            SerializerUtils.WriteStartArray(writer);
            SerializerUtils.WriteEndArray(writer);
            return;
        }
        SerializerUtils.WriteStartArray(writer);
        for (int i = 0; i < settingCollection.size(); i++) {
            getConvertor().serialize(settingCollection.get(i), writer, null);
        }
        SerializerUtils.WriteEndArray(writer);
    }

    protected TccSettingElementSerializer getConvertor() {
        return new TccSettingElementSerializer(isFull);
    }
}
