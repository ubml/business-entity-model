package com.inspur.edp.bef.bizentity.beenum;

/**
 * 实体加锁类型
 */
public enum GspDataLockType {
    /**
     * 悲观锁：需要显式加锁成功后才能编辑数据
     */
    PessimisticLocking(0),
    /**
     * 乐观锁：提交数据时比较数据版本，如果不一致，数据作废
     */
    OptimisticLocking(1),
    /**
     * 不加锁：不保证实体数据并发一致性
     */
    None(2);

    private final int intValue;
    private static java.util.HashMap<Integer, GspDataLockType> mappings;

    private synchronized static java.util.HashMap<Integer, GspDataLockType> getMappings() {
        if (mappings == null) {
            mappings = new java.util.HashMap<Integer, GspDataLockType>();
        }
        return mappings;
    }

    private GspDataLockType(int value) {
        intValue = value;
        GspDataLockType.getMappings().put(value, this);
    }

    public int getValue() {
        return intValue;
    }

    public static GspDataLockType forValue(int value) {
        return getMappings().get(value);
    }
}