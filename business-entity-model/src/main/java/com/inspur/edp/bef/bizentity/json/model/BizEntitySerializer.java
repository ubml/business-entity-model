package com.inspur.edp.bef.bizentity.json.model;

import com.fasterxml.jackson.core.JsonGenerator;
import com.inspur.edp.bef.bizentity.BeThreadLocal;
import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.bef.bizentity.beenum.BECategory;
import com.inspur.edp.bef.bizentity.beenum.GspDataLockType;
import com.inspur.edp.bef.bizentity.common.BizEntityJsonConst;
import com.inspur.edp.bef.bizentity.common.InternalActionUtil;
import com.inspur.edp.bef.bizentity.exceptions.BusinessEntityErrorCodeEnum;
import com.inspur.edp.bef.bizentity.exceptions.BusinessEntityException;
import com.inspur.edp.bef.bizentity.json.object.BizObjectSerializer;
import com.inspur.edp.bef.bizentity.json.operation.BizMgrActionCollectionSerializer;
import com.inspur.edp.bef.bizentity.operation.BizOperation;
import com.inspur.edp.bef.bizentity.operation.collection.BizMgrActionCollection;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.das.commonmodel.IGspCommonModel;
import com.inspur.edp.das.commonmodel.json.model.CommonModelSerializer;
import com.inspur.edp.das.commonmodel.json.object.CmObjectSerializer;

import java.io.IOException;

public class BizEntitySerializer extends CommonModelSerializer {

    public BizEntitySerializer() {
        if (BeThreadLocal.get() != null)
            isFull = BeThreadLocal.get().getfull();
    }

    ;

    public BizEntitySerializer(boolean full) {
        super(full);
        isFull = full;
    }

    //region BaseProp
    @Override
    protected void writeExtendModelProperty(IGspCommonModel commonModel, JsonGenerator writer) {
        GspBusinessEntity be = (GspBusinessEntity) commonModel;
        writeBizActions(writer, be);
        if (isFull || (be.getDependentEntityId() != null && !"".equals(be.getDependentEntityId()))) {
            SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.DependentEntityId, be.getDependentEntityId());
        }
        if (isFull || (be.getDependentEntityName() != null && !"".equals(be.getDependentEntityName()))) {
            SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.DependentEntityName, be.getDependentEntityName());
        }
        if (isFull || (be.getDependentEntityPackageName() != null && !"".equals(be.getDependentEntityPackageName()))) {
            SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.DependentEntityPackageName, be.getDependentEntityPackageName());
        }
        if (isFull || (be.getCacheConfiguration() != null && !"".equals(be.getCacheConfiguration()))) {
            SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.CacheConfiguration, be.getCacheConfiguration());
        }
        if (isFull || be.getEnableCaching()) {
            SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.EnableCaching, be.getEnableCaching());
        }
        if (isFull || !be.getEnableTreeDtm()) {
            SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.EnableTreeDtm, be.getEnableTreeDtm());
        }
        SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.ComponentAssemblyName, be.getComponentAssemblyName());
        if (isFull || (be.getCategory() != null && be.getCategory() != BECategory.Standard)) {
            SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.Category, be.getCategory().getValue());
        }
        if (isFull || (be.getDataLockType() != null && be.getDataLockType() != GspDataLockType.PessimisticLocking)) {
            SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.DataLockType, be.getDataLockType().getValue());
        }
        if (isFull) {
            SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.ExtendType, be.getExtendType());
            SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.IsUsingTimeStamp, be.getIsUsingTimeStamp());
        }
        if (isFull || be.getEnableApproval() != false)
            SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.EnableApproval, be.getEnableApproval());
        if (isFull || be.isTccSupported() != false)
            SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.TccSupported, be.isTccSupported());
        if (isFull || be.isAutoTccLock() != true)
            SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.AutoTccLock, be.isAutoTccLock());
        if (isFull || be.isAutoComplete() != true)
            SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.AutoComplete, be.isAutoComplete());
        if (isFull || be.isAutoCancel() != true)
            SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.AutoCancel, be.isAutoCancel());
//        SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.Authorizations, be.getAuthorizations());
    }

    private void writeBizActions(JsonGenerator writer, GspBusinessEntity be) {
        if (!isFull && (be.getBizMgrActions() == null || be.getBizMgrActions().size() < 0))
            return;
        boolean isHaveSelfAction = false;
        BizMgrActionCollection actionList = be.getBizMgrActions();
        for (BizOperation a : actionList) {
            if (!InternalActionUtil.InternalMgrActionIDs.contains(a.getID()) && !InternalActionUtil.InternalBeActionIDs.contains(a.getID())) {
                isHaveSelfAction = true;
                break;
            }
        }
        if (isFull || isHaveSelfAction) {
            SerializerUtils.writePropertyName(writer, BizEntityJsonConst.BizMgrActions);
            BizMgrActionCollectionSerializer operationsConvertor = getMgrActionsConverter();
            try {
                operationsConvertor.serialize(actionList, writer, null);
            } catch (IOException e) {
                throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_JSON_0004, e, "BizMgrActionCollection");
            }
        }
    }

    private BizMgrActionCollectionSerializer getMgrActionsConverter() {
        return new BizMgrActionCollectionSerializer(isFull);
    }
    //endregion

    @Override
    protected void writeExtendModelSelfProperty(IGspCommonModel iGspCommonModel, JsonGenerator jsonGenerator) {

    }

    @Override
    protected final CmObjectSerializer getCmObjectSerializer() {
        return new BizObjectSerializer(isFull);
    }
}
