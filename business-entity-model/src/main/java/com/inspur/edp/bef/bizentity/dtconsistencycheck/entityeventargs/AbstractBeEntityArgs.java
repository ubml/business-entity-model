package com.inspur.edp.bef.bizentity.dtconsistencycheck.entityeventargs;


import com.inspur.edp.cef.designtime.api.dtconsistencycheck.AbstractDtEventArgs;

public abstract class AbstractBeEntityArgs extends AbstractDtEventArgs {

    public AbstractBeEntityArgs() {
    }

    protected String beEntityId;
    protected String beId;
    protected String metadataPath;

    public String getMetadataPath() {
        return metadataPath;
    }

    public void setBeEntityId(String beEntityId) {
        this.beEntityId = beEntityId;
    }

    public void setBeId(String beId) {
        this.beId = beId;
    }

    public String getBeEntityId() {
        return beEntityId;
    }

    public String getBeId() {
        return beId;
    }
}
