package com.inspur.edp.bef.bizentity.json.operation;

import com.inspur.edp.bef.bizentity.operation.collection.BizMgrActionCollection;

public class BizMgrActionCollectionSerializer extends BizOperationCollectionSerializer<BizMgrActionCollection> {
    public BizMgrActionCollectionSerializer() {
    }

    public BizMgrActionCollectionSerializer(boolean full) {
        super(full);
        isFull = full;
    }

    @Override
    protected BizOperationSerializer getConvertor() {
        return new BizMgrActionSerializer(isFull);
    }

}
