package com.inspur.edp.bef.bizentity.operation.beomponent;

import com.inspur.edp.bef.bizentity.operation.componentbase.BizParameterCollection;

public class BizActionParamCollection extends BizParameterCollection<BizActionParameter> {

    public BizActionParamCollection() {
    }
}