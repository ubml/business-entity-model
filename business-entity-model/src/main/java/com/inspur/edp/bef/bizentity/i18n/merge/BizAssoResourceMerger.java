package com.inspur.edp.bef.bizentity.i18n.merge;

import com.inspur.edp.bef.bizentity.GspBizEntityElement;
import com.inspur.edp.cef.designtime.api.element.GspAssociation;
import com.inspur.edp.cef.designtime.api.i18n.context.ICefResourceMergeContext;
import com.inspur.edp.das.commonmodel.IGspCommonElement;
import com.inspur.edp.das.commonmodel.i18n.merge.GspAssoRefEleResourceMerger;
import com.inspur.edp.das.commonmodel.i18n.merge.GspAssoResourceMerger;

public class BizAssoResourceMerger extends GspAssoResourceMerger {
    public BizAssoResourceMerger(GspAssociation asso, ICefResourceMergeContext context) {
        super(asso, context);
    }

    @Override
    protected GspAssoRefEleResourceMerger getAssoRefElementResourcemerger(ICefResourceMergeContext context, IGspCommonElement field) {
        return new BizAssoRefEleResourceMerger((GspBizEntityElement) ((field instanceof GspBizEntityElement) ? field : null), context);

    }
}
