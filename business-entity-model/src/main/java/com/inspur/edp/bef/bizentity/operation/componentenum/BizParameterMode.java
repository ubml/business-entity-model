package com.inspur.edp.bef.bizentity.operation.componentenum;

/**
 * 参数模式
 */
public enum BizParameterMode {
    /**
     * 传入参数
     */
    IN,
    /**
     * 传出参数
     */
    OUT,
    /**
     * 传入传出参数
     */
    INOUT;

    public int getValue() {
        return this.ordinal();
    }

    public static BizParameterMode forValue(int value) {
        return values()[value];
    }
}