package com.inspur.edp.bef.bizentity.json.operation;

import com.fasterxml.jackson.core.JsonGenerator;
import com.inspur.edp.bef.bizentity.common.BizEntityJsonConst;
import com.inspur.edp.bef.bizentity.operation.BizCommonDetermination;
import com.inspur.edp.bef.bizentity.operation.componentbase.BizParameter;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.cef.designtime.api.json.operation.CommonDtmSerializer;
import com.inspur.edp.cef.designtime.api.operation.CommonDetermination;
import com.inspur.edp.cef.designtime.api.operation.CommonOperation;

public class BizCommonDeterminationSerializer extends CommonDtmSerializer {
    public BizCommonDeterminationSerializer(boolean full) {
        super(full);
    }

    @Override
    protected void writeExtendCommonOpSelfProperty(JsonGenerator writer, CommonOperation info) {
        super.writeExtendCommonOpSelfProperty(writer, info);
        writeParameters(writer, (CommonDetermination) info);
        if (info instanceof BizCommonDetermination) {
            BizCommonDetermination bizCommonDetermination = (BizCommonDetermination) info;
            if (this.isFull || bizCommonDetermination.getRunOnce()) {
                SerializerUtils.writePropertyValue(writer, "RunOnce", bizCommonDetermination.getRunOnce());
            }
        }
    }

    private void writeParameters(JsonGenerator writer, CommonDetermination determination) {
        BizCommonDetermination bizCommonDetermination = (BizCommonDetermination) determination;
        if (isFull || (bizCommonDetermination.getParameterCollection() != null && bizCommonDetermination.getParameterCollection().getCount() > 0)) {
            SerializerUtils.writePropertyName(writer, BizEntityJsonConst.Parameters);
            //[
            SerializerUtils.WriteStartArray(writer);
            if (bizCommonDetermination.getParameterCollection() != null && bizCommonDetermination.getParameterCollection().getCount() > 0) {
                for (Object item : bizCommonDetermination.getParameterCollection()) {
                    new BizParameterSerializer(isFull).serialize((BizParameter) item, writer, null);
                }
            }
            //]
            SerializerUtils.WriteEndArray(writer);
        }
    }
}
