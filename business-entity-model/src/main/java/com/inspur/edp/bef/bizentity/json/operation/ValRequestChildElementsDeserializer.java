package com.inspur.edp.bef.bizentity.json.operation;

import com.inspur.edp.cef.designtime.api.collection.ValElementCollection;

import java.util.HashMap;
import java.util.LinkedHashMap;

public class ValRequestChildElementsDeserializer extends RequestChildElementsDeserializer<ValElementCollection> {

    @Override
    HashMap<String, ValElementCollection> createHashMap() {
        return new LinkedHashMap<>();
    }

    @Override
    ValElementCollection createValueType() {
        return new ValElementCollection();
    }
}
