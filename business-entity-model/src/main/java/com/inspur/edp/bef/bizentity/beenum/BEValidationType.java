package com.inspur.edp.bef.bizentity.beenum;

/**
 * 校验规则类型
 */
//C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//[Flags]
public enum BEValidationType {
    /**
     * 数据一致性校验
     */
    Consistency(0),

    /**
     * 操作许可校验
     */
    Operation(1);

    private int intValue;
    private static java.util.HashMap<Integer, BEValidationType> mappings;

    private synchronized static java.util.HashMap<Integer, BEValidationType> getMappings() {
        if (mappings == null) {
            mappings = new java.util.HashMap<Integer, BEValidationType>();
        }
        return mappings;
    }

    private BEValidationType(int value) {
        intValue = value;
        BEValidationType.getMappings().put(value, this);
    }

    public int getValue() {
        return intValue;
    }

    public static BEValidationType forValue(int value) {
        return getMappings().get(value);
    }
}