package com.inspur.edp.bef.bizentity.dtconsistencycheck.fieldeventargs;

public class RemovingFieldEventArgs extends AbstractBeFieldEventArgs {
    public RemovingFieldEventArgs() {

    }

    public RemovingFieldEventArgs(String beId, String entityId, String fieldId) {
        super(beId, entityId, fieldId);
    }
}
