package com.inspur.edp.bef.bizentity.json.increment;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.inspur.edp.bef.bizentity.common.BizEntityJsonConst;
import com.inspur.edp.bef.bizentity.exceptions.BusinessEntityErrorCodeEnum;
import com.inspur.edp.bef.bizentity.exceptions.BusinessEntityException;
import com.inspur.edp.bef.bizentity.increment.entity.validation.AddedValIncrement;
import com.inspur.edp.bef.bizentity.increment.entity.validation.ValIncrement;
import com.inspur.edp.bef.bizentity.json.operation.BizValidationDeserializer;
import com.inspur.edp.bef.bizentity.operation.Validation;
import com.inspur.edp.cef.designtime.api.increment.IncrementType;
import com.inspur.edp.cef.designtime.api.json.CefNames;

import java.io.IOException;

public class ValIncrementDeserializer extends JsonDeserializer<ValIncrement> {

    @Override
    public ValIncrement deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode node = mapper.readTree(jsonParser);
        String incrementTypeStr = node.get(CefNames.IncrementType).textValue();
        if (incrementTypeStr == null || "".equals(incrementTypeStr))
            return null;
        IncrementType incrementType = IncrementType.valueOf(incrementTypeStr);
        switch (incrementType) {
            case Added:
                return readAddIncrementInfo(node);
            case Modify:
//                return readModifyIncrementInfo(node);
            case Deleted:
//                return readDeletedIncrementInfo(node);

        }
        return null;
    }

    private AddedValIncrement readAddIncrementInfo(JsonNode node) {
        AddedValIncrement addIncrement = new AddedValIncrement();
        JsonNode addVauleNode = node.get(BizEntityJsonConst.AddedAction);
        if (addVauleNode == null)
            return addIncrement;
        Validation action = readValidation(addVauleNode);
        addIncrement.setAction(action);
        return addIncrement;
    }

    private Validation readValidation(JsonNode node) {
        ObjectMapper mapper = new ObjectMapper();
        BizValidationDeserializer serializer = new BizValidationDeserializer();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(Validation.class, serializer);
        mapper.registerModule(module);
        try {
            return mapper.readValue(node.toString(), Validation.class);
        } catch (IOException e) {
            throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_JSON_0001, e, "Validation");
        }
    }
}
