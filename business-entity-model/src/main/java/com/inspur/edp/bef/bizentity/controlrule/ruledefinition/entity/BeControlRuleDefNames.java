package com.inspur.edp.bef.bizentity.controlrule.ruledefinition.entity;

public class BeControlRuleDefNames {
    public static String beControlRuleObjectType = "GspBusinessObject";
    public static String AddCustomAction = "AddCustomAction";
    public static String AddVariableDtm = "AddVariableDtm";
    public static String ModifyCustomActions = "ModifyCustomActions";
    public static String ModifyVariableDtm = "ModifyVariableDtm";
    public static String EnableTreeDtm = "EnableTreeDtm";
    public static String UsingTimeStamp = "UsingTimeStamp";
}
