package com.inspur.edp.bef.bizentity.beenum;

/**
 * BE 操作类型
 */
public enum BEOperationType {
    /**
     * 自定义操作
     */
    BizMgrAction,

    /**
     * 实体操作
     */
    BizAction,

    /**
     * 校验规则
     */
    Validation,

    /**
     * 联动计算
     */
    Determination,

    /**
     * 查询
     */
    Query,

    /**
     * 事件订阅
     */
    Subscription,

    /**
     * 初始化
     */
    Initiation,

    /**
     * 系统服务
     */
    BasicService,
    /**
     * Tcc动作
     */
    TccAction;

    public int getValue() {
        return this.ordinal();
    }

    public static BEOperationType forValue(int value) {
        return values()[value];
    }
}
