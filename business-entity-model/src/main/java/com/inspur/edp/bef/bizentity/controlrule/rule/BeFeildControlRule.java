package com.inspur.edp.bef.bizentity.controlrule.rule;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.inspur.edp.bef.bizentity.controlrule.rule.parser.BeFieldRuleParser;
import com.inspur.edp.bef.bizentity.controlrule.rule.serializer.BeFieldControlRuleSerializer;
import com.inspur.edp.das.commonmodel.controlrule.CmFieldControlRule;

@JsonSerialize(using = BeFieldControlRuleSerializer.class)
@JsonDeserialize(using = BeFieldRuleParser.class)
public class BeFeildControlRule extends CmFieldControlRule {

}
