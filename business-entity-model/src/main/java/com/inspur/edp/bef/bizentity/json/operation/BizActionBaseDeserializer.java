package com.inspur.edp.bef.bizentity.json.operation;

import com.fasterxml.jackson.core.JsonParser;
import com.inspur.edp.bef.bizentity.beenum.AuthType;
import com.inspur.edp.bef.bizentity.common.BizEntityJsonConst;
import com.inspur.edp.bef.bizentity.operation.BizActionBase;
import com.inspur.edp.bef.bizentity.operation.BizOperation;
import com.inspur.edp.bef.bizentity.operation.componentbase.BizParameter;
import com.inspur.edp.bef.bizentity.operation.componentbase.BizParameterCollection;
import com.inspur.edp.bef.bizentity.operation.componentbase.BizReturnValue;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;

public abstract class BizActionBaseDeserializer extends BizOperationDeserializer<BizActionBase> {

    @Override
    protected final BizActionBase createBizOp() {
        return createBizActionBase();
    }

    @Override
    protected void beforeDeserializeBizoperation(BizOperation op) {
        BizActionBase action = (BizActionBase) op;
        action.setParameters(new BizParameterCollection());
        action.setReturnValue(new BizReturnValue());
    }

    @Override
    protected final boolean readExtendOpProperty(BizOperation op, String propName, JsonParser jsonParser) {
        BizActionBase action = (BizActionBase) op;
        boolean hasProperty = true;
        switch (propName) {
            case BizEntityJsonConst.CurentAuthType:
                SerializerUtils.readPropertyValue_Enum(jsonParser, AuthType.class, AuthType.values());
                break;
            case BizEntityJsonConst.OpIdList:
                action.setOpIdList(SerializerUtils.readStringArray(jsonParser));
                break;
            case BizEntityJsonConst.Parameters:
                readParameters(jsonParser, action);
                break;
            case BizEntityJsonConst.ReturnValue:
                readReturnValue(jsonParser, action);
                break;
            default:
                if (!readExtendActionProperty(jsonParser, op, propName)) {
                    hasProperty = false;
                }
                break;
        }
        return hasProperty;
    }

    private void readParameters(JsonParser jsonParser, BizActionBase action) {
        BizParaDeserializer paraDeserializer = createPrapConvertor();
        BizParameterCollection<BizParameter> collection = createPrapCollection();
        SerializerUtils.readArray(jsonParser, paraDeserializer, collection);
        action.setParameters(collection);
    }

    private void readReturnValue(JsonParser jsonParser, BizActionBase action) {
        BizReturnValueDeserializer returnValueDeserializer = new BizReturnValueDeserializer();
        BizReturnValue value = (BizReturnValue) returnValueDeserializer.deserialize(jsonParser, null);
        action.setReturnValue(value);
    }

    protected abstract BizActionBase createBizActionBase();

    protected abstract BizParaDeserializer createPrapConvertor();

    protected abstract BizParameterCollection createPrapCollection();

    protected boolean readExtendActionProperty(JsonParser jsonParser, BizOperation op, String propName) {
        return false;
    }
}
