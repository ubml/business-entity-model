package com.inspur.edp.bef.bizentity.dtservice;

import com.inspur.edp.bef.bizentity.GspBizEntityElement;
import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;

public interface IBizEntityDtService {

    /**
     * 根据BE元数据生成DBO
     *
     * @param metadata     BE元数据
     * @param metadataPath 元数据路径
     */
    void generateDbo(GspMetadata metadata, String metadataPath);

    GspBizEntityElement createUdtElement(String id, String code, String name, String labelId,
                                         String udtId);
}