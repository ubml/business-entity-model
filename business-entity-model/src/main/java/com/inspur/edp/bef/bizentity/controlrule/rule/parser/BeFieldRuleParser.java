package com.inspur.edp.bef.bizentity.controlrule.rule.parser;

import com.inspur.edp.bef.bizentity.controlrule.rule.BeFeildControlRule;
import com.inspur.edp.das.commonmodel.controlrule.CmFieldControlRule;
import com.inspur.edp.das.commonmodel.controlrule.parser.CmFieldRuleParser;

public class BeFieldRuleParser extends CmFieldRuleParser {
    protected CmFieldControlRule createCmFieldRuleDefinition() {
        return new BeFeildControlRule();
    }
}
