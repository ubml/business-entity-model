package com.inspur.edp.bef.bizentity.pushchangesetargs;

public class ObjectChangeSet {
    public ObjectChangeSet() {
    }

    public ObjectChangeSet(ObjectChangeDetail changeDetail, ChangeType changeType) {
        this.changeDetail = changeDetail;
        this.changeType = changeType;
    }

    public ObjectChangeDetail getChangeDetail() {
        return changeDetail;
    }

    public void setChangeDetail(ObjectChangeDetail changeDetail) {
        this.changeDetail = changeDetail;
    }

    public ChangeType getChangeType() {
        return changeType;
    }

    public void setChangeType(ChangeType changeType) {
        this.changeType = changeType;
    }

    private ObjectChangeDetail changeDetail;
    private ChangeType changeType;
}
