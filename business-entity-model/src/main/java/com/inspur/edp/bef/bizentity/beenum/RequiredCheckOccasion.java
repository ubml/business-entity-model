package com.inspur.edp.bef.bizentity.beenum;

/**
 * 必填的检查时机
 */
//C# TO JAVA CONVERTER TODO TASK: Java annotations will not correspond to .NET attributes:
//[Flags]
public enum RequiredCheckOccasion {
    /**
     * 仅保存时
     */
    Save(1),

    /**
     * 此选项在设计器上不暴露, 不应该存在修改时验证保存时不执行的情况
     */
    Modify(2),

    /**
     * 所有时机
     * 二进制为全1
     */
    All(-1);

    private final int intValue;
    private static java.util.HashMap<Integer, RequiredCheckOccasion> mappings;

    private synchronized static java.util.HashMap<Integer, RequiredCheckOccasion> getMappings() {
        if (mappings == null) {
            mappings = new java.util.HashMap<Integer, RequiredCheckOccasion>();
        }
        return mappings;
    }

    private RequiredCheckOccasion(int value) {
        intValue = value;
        RequiredCheckOccasion.getMappings().put(value, this);
    }

    public int getValue() {
        return intValue;
    }

    public static RequiredCheckOccasion forValue(int value) {
        return getMappings().get(value);
    }
}