package com.inspur.edp.bef.bizentity.increment.extract.action;

import com.inspur.edp.bef.bizentity.increment.entity.action.AddedMgrActionIncrement;
import com.inspur.edp.bef.bizentity.operation.BizMgrAction;

public class AddedMgrActionExtractor {

    public AddedMgrActionIncrement extract(BizMgrAction voAction) {
        AddedMgrActionIncrement addedEntityIncrement = this.createIncrement();
        addedEntityIncrement.setAction(voAction);
        return addedEntityIncrement;
    }

    protected AddedMgrActionIncrement createIncrement() {
        return new AddedMgrActionIncrement();
    }
}
