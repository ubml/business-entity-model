package com.inspur.edp.bef.bizentity.dtconsistencycheck.fieldeventargs;

public class ChangingFieldObjectTypeEventArgs extends AbstractBeFieldEventArgs {
    protected String newValue;
    protected String originalValue;

    public ChangingFieldObjectTypeEventArgs() {
    }

    public ChangingFieldObjectTypeEventArgs(String beId, String entityId, String fieldId,
                                            String newValue, String originalValue) {
        super(beId, entityId, fieldId);
        this.newValue = newValue;
        this.originalValue = originalValue;
    }

    public String getNewValue() {
        return newValue;
    }

    public void setNewValue(String newValue) {
        this.newValue = newValue;
    }

    public String getOriginalValue() {
        return originalValue;
    }

    public void setOriginalValue(String originalValue) {
        this.originalValue = originalValue;
    }
}
