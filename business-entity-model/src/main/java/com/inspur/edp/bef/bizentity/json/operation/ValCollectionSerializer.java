package com.inspur.edp.bef.bizentity.json.operation;

import com.inspur.edp.bef.bizentity.operation.collection.ValidationCollection;

public class ValCollectionSerializer extends BizOperationCollectionSerializer<ValidationCollection> {
    public ValCollectionSerializer() {
    }

    public ValCollectionSerializer(boolean full) {
        super(full);
        isFull = full;
    }

    @Override
    protected BizOperationSerializer getConvertor() {
        return new BizValidationSerializer(isFull);
    }

}
