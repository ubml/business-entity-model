package com.inspur.edp.bef.bizentity.json.operation;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.inspur.edp.bef.bizentity.operation.componentbase.BizParameter;
import com.inspur.edp.bef.bizentity.operation.componentbase.BizReturnValue;
import com.inspur.edp.bef.bizentity.operation.componentbase.BizVoidReturnType;

public class BizReturnValueDeserializer extends BizParaDeserializer {
    @Override
    public final BizReturnValue deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) {
        BizReturnValue rez = (BizReturnValue) super.deserialize(jsonParser, null);
        if (!rez.isVoidReturnType()) {
            if (BizVoidReturnType.assembly.equals(rez.getAssembly())
                    && BizVoidReturnType.className.equals(rez.getClassName())) {
                BizReturnValue newRez = new BizVoidReturnType();
                newRez.setID(rez.getID());
                newRez.setParamCode(rez.getParamCode());
                newRez.setParamDescription(rez.getParamDescription());
                newRez.setParameterType(rez.getParameterType());
                return newRez;
            }
            return rez;
        }
        BizVoidReturnType voidReturnType = getVoidReturnType(rez);
        return voidReturnType;
    }

    private BizVoidReturnType getVoidReturnType(BizReturnValue bizReturnValue) {
        BizVoidReturnType newRez = new BizVoidReturnType();
        newRez.setID(bizReturnValue.getID());
        newRez.setParamCode(bizReturnValue.getParamCode());
        newRez.setParamDescription(bizReturnValue.getParamDescription());
        newRez.setParameterType(bizReturnValue.getParameterType());
        newRez.setMode(bizReturnValue.getMode());
        return newRez;
    }

    @Override
    protected BizParameter createBizPara() {
        return new BizReturnValue();
    }
}
