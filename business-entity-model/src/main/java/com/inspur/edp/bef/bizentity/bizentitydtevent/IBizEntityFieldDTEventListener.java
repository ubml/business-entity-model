package com.inspur.edp.bef.bizentity.bizentitydtevent;

import com.inspur.edp.bef.bizentity.dtconsistencycheck.fieldeventargs.RemovingFieldEventArgs;
import io.iec.edp.caf.commons.event.IEventListener;

public interface IBizEntityFieldDTEventListener extends IEventListener {
    public RemovingFieldEventArgs removingField(RemovingFieldEventArgs args);

}
