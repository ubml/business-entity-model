package com.inspur.edp.bef.bizentity.controlrule.ruledefinition.entity;


import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.inspur.edp.bef.bizentity.controlrule.ruledefinition.parser.BeControlRuleDefParser;
import com.inspur.edp.bef.bizentity.controlrule.ruledefinition.serializer.BeControlRuleDefSerializer;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.entity.ControlRuleDefItem;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.entity.ControlRuleValue;
import com.inspur.edp.das.commonmodel.controlruledef.entity.CmControlRuleDef;
import com.inspur.edp.das.commonmodel.controlruledef.entity.CmRuleNames;
import com.inspur.edp.das.commonmodel.json.CommonModelNames;
import com.inspur.edp.das.commonmodel.util.MessageI18nUtils;

@JsonSerialize(using = BeControlRuleDefSerializer.class)
@JsonDeserialize(using = BeControlRuleDefParser.class)
public class BeControlRuleDef extends CmControlRuleDef {
    public BeControlRuleDef() {
        super(null, BeControlRuleDefNames.beControlRuleObjectType);
        init();
    }


    private void init() {
        ControlRuleDefItem codeRule = new ControlRuleDefItem() {
            {
                this.setRuleName(CmRuleNames.Code);
                this.setRuleDisplayName(MessageI18nUtils.getMessage("code"));
                this.setDescription(MessageI18nUtils.getMessage("entityCode"));
                this.setDefaultRuleValue(ControlRuleValue.Forbiddon);
            }
        };
        setCodeControlRule(codeRule);

        ControlRuleDefItem nameRule = new ControlRuleDefItem() {
            {
                this.setRuleName(CmRuleNames.Name);
                this.setRuleDisplayName(MessageI18nUtils.getMessage("name"));
                this.setDescription(MessageI18nUtils.getMessage("entityName"));
                this.setDefaultRuleValue(ControlRuleValue.Allow);
            }
        };
        setNameControlRule(nameRule);

//        setUsingTimeStampControlRule();

        ControlRuleDefItem addActionRule = new ControlRuleDefItem() {
            {
                this.setRuleName(BeControlRuleDefNames.AddCustomAction);
                this.setRuleDisplayName(MessageI18nUtils.getMessage("addCustomAction"));
                this.setDescription(MessageI18nUtils.getMessage("addCustomAction"));
                this.setDefaultRuleValue(ControlRuleValue.Forbiddon);
            }
        };
        setAddCustomActonControlRule(addActionRule);

        ControlRuleDefItem modifyActionRule = new ControlRuleDefItem() {
            {
                this.setRuleName(BeControlRuleDefNames.ModifyCustomActions);
                this.setRuleDisplayName(MessageI18nUtils.getMessage("modifyCustomAction"));
                this.setDescription(MessageI18nUtils.getMessage("modifyCustomAction"));
                this.setDefaultRuleValue(ControlRuleValue.Forbiddon);
            }
        };
        setModifyCustomActonControlRule(modifyActionRule);

        ControlRuleDefItem addVarRule = new ControlRuleDefItem() {
            {
                this.setRuleName(BeControlRuleDefNames.AddVariableDtm);
                this.setRuleDisplayName(MessageI18nUtils.getMessage("addVariableDtm"));
                this.setDescription(MessageI18nUtils.getMessage("addVariableDtm"));
                this.setDefaultRuleValue(ControlRuleValue.Forbiddon);
            }
        };
        setAddVariableDtmControlRule(addVarRule);

        ControlRuleDefItem modifyVarRule = new ControlRuleDefItem() {
            {
                this.setRuleName(BeControlRuleDefNames.ModifyVariableDtm);
                this.setRuleDisplayName(MessageI18nUtils.getMessage("modifyVariableDtm"));
                this.setDescription(MessageI18nUtils.getMessage("modifyVariableDtm"));
                this.setDefaultRuleValue(ControlRuleValue.Forbiddon);
            }
        };
        setModifyVariableDtmControlRule(modifyVarRule);

        BeEntityControlRuleDef objectRuleDef = new BeEntityControlRuleDef(this);
        getChildControlRules().put(CommonModelNames.MainObject, objectRuleDef);

    }

    public ControlRuleDefItem getAddCustomActonControlRule() {
        return super.getControlRuleItem(BeControlRuleDefNames.AddCustomAction);
    }

    public void setAddCustomActonControlRule(ControlRuleDefItem ruleItem) {
        super.setControlRuleItem(BeControlRuleDefNames.AddCustomAction, ruleItem);
    }

    public ControlRuleDefItem getModifyCustomActonControlRule() {
        return super.getControlRuleItem(BeControlRuleDefNames.ModifyCustomActions);
    }

    public void setModifyCustomActonControlRule(ControlRuleDefItem ruleItem) {
        super.setControlRuleItem(BeControlRuleDefNames.ModifyCustomActions, ruleItem);
    }

    public ControlRuleDefItem getAddVariableDtmControlRule() {
        return super.getControlRuleItem(BeControlRuleDefNames.AddVariableDtm);
    }

    public void setAddVariableDtmControlRule(ControlRuleDefItem ruleItem) {
        super.setControlRuleItem(BeControlRuleDefNames.AddVariableDtm, ruleItem);
    }

    public ControlRuleDefItem getModifyVariableDtmControlRule() {
        return super.getControlRuleItem(BeControlRuleDefNames.ModifyVariableDtm);
    }

    public void setModifyVariableDtmControlRule(ControlRuleDefItem ruleItem) {
        super.setControlRuleItem(BeControlRuleDefNames.ModifyVariableDtm, ruleItem);
    }

    public ControlRuleDefItem getEnableTreeDtmControlRule() {
        return super.getControlRuleItem(BeControlRuleDefNames.EnableTreeDtm);
    }

    public void setEnableTreeDtmControlRule(ControlRuleDefItem ruleItem) {
        super.setControlRuleItem(BeControlRuleDefNames.EnableTreeDtm, ruleItem);
    }

    public ControlRuleDefItem getUsingTimeStampControlRule() {
        return super.getControlRuleItem(BeControlRuleDefNames.UsingTimeStamp);
    }

    public void setUsingTimeStampControlRule(ControlRuleDefItem ruleItem) {
        super.setControlRuleItem(BeControlRuleDefNames.UsingTimeStamp, ruleItem);
    }

}
