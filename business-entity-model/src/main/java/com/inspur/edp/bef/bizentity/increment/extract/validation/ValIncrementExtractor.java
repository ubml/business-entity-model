package com.inspur.edp.bef.bizentity.increment.extract.validation;

import com.inspur.edp.bef.bizentity.increment.entity.validation.ValIncrement;
import com.inspur.edp.bef.bizentity.operation.Validation;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.cm.CommonDataTypeControlRule;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.cm.CommonDataTypeControlRuleDef;
import com.inspur.edp.cef.designtime.api.increment.extractor.AbstractIncrementExtractor;

public class ValIncrementExtractor extends AbstractIncrementExtractor {

    public ValIncrement extractorIncrement(Validation oldAction, Validation newAction, CommonDataTypeControlRule rule, CommonDataTypeControlRuleDef def) {

        if (oldAction == null && newAction == null) {
            return null;
        } else if (oldAction == null) {
            return new AddedValExtractor().extract(newAction);
        } else if (newAction == null) {
            //TODO 删除
            return null;
        } else {
            //TODO 修改
            return null;
        }
    }
}
