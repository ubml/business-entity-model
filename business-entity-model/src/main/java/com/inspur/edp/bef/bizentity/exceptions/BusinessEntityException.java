/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.bizentity.exceptions;

import io.iec.edp.caf.commons.exception.CAFRuntimeException;
import io.iec.edp.caf.commons.exception.ExceptionLevel;

public class BusinessEntityException extends CAFRuntimeException {
    private static final String SERVICE_UNIT_CODE = "pfcommon";
    private static final String RESOURCE_FILE = "business_entity_model_exception.properties";

    public BusinessEntityException(BusinessEntityErrorCodeEnum exceptionCode, String[] messageParams, Throwable innerException, ExceptionLevel level) {
        super(SERVICE_UNIT_CODE, RESOURCE_FILE, exceptionCode.name(), messageParams, innerException, level, exceptionCode.isBizException());
    }

    public static BusinessEntityException createException(BusinessEntityErrorCodeEnum exceptionCode) {
        return new BusinessEntityException(exceptionCode, null, null, ExceptionLevel.Error);
    }

    public static BusinessEntityException createException(BusinessEntityErrorCodeEnum exceptionCode, String... messageParams) {
        return new BusinessEntityException(exceptionCode, messageParams, null, ExceptionLevel.Error);
    }

    public static BusinessEntityException createException(BusinessEntityErrorCodeEnum exceptionCode, Throwable innerException) {
        return new BusinessEntityException(exceptionCode, null, innerException, ExceptionLevel.Error);
    }

    public static BusinessEntityException createException(BusinessEntityErrorCodeEnum exceptionCode, ExceptionLevel level) {
        return new BusinessEntityException(exceptionCode, null, null, level);
    }

    public static BusinessEntityException createException(BusinessEntityErrorCodeEnum exceptionCode, Throwable innerException, String... messageParams) {
        return new BusinessEntityException(exceptionCode, messageParams, innerException, ExceptionLevel.Error);
    }

    public static BusinessEntityException createException(BusinessEntityErrorCodeEnum exceptionCode, ExceptionLevel level, String... messageParams) {
        return new BusinessEntityException(exceptionCode, messageParams, null, level);
    }

    public static BusinessEntityException createException(BusinessEntityErrorCodeEnum exceptionCode, Throwable innerException, ExceptionLevel level, String... messageParams) {
        return new BusinessEntityException(exceptionCode, messageParams, innerException, level);
    }

}
