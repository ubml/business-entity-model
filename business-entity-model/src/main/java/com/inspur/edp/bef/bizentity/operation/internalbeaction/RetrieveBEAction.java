package com.inspur.edp.bef.bizentity.operation.internalbeaction;

import com.inspur.edp.bef.bizentity.operation.BizAction;
import com.inspur.edp.das.commonmodel.util.MessageI18nUtils;

/**
 * 内置Retrieve操作
 */
public class RetrieveBEAction extends BizAction implements IInternalBEAction {
    public static final String id = "cJwzDlFfa0uz4rChlTEd4g";
    public static final String code = "Retrieve";
    public static final String name = MessageI18nUtils.getMessage("Retrieve");

    public RetrieveBEAction() {
        setID(id);
        setCode(code);
        setName(name);
    }
}