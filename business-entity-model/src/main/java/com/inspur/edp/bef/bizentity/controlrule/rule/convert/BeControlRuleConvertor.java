package com.inspur.edp.bef.bizentity.controlrule.rule.convert;

import com.inspur.edp.bef.bizentity.GspBizEntityObject;
import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.bef.bizentity.controlrule.rule.BeControlRule;
import com.inspur.edp.bef.bizentity.controlrule.rule.BeFeildControlRule;
import com.inspur.edp.bef.bizentity.controlrule.rule.BeObjControlRule;
import com.inspur.edp.bef.bizentity.controlrule.ruledefinition.entity.BeControlRuleDef;
import com.inspur.edp.bef.bizentity.controlrule.ruledefinition.entity.BeEntityControlRuleDef;
import com.inspur.edp.bef.bizentity.controlrule.ruledefinition.entity.BeFieldControlRuleDef;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.basic.convert.ControlRuleConvertor;
import com.inspur.edp.das.commonmodel.json.CommonModelNames;
import lombok.var;

import java.util.HashMap;

public class BeControlRuleConvertor {

    public static void convert2ControlRule(BeControlRuleDef ruleDef, BeControlRule rule, GspBusinessEntity be) {

        ControlRuleConvertor.convert2ControlRule(ruleDef, rule);
        rule.setRuleId(be.getId());

        BeObjControlRule mainObjRule = convert2BeObjControlRule((BeEntityControlRuleDef) ruleDef.getChildControlRules().get(CommonModelNames.MainObject), be.getMainObject());
        rule.setMainEntityControlRule(mainObjRule);

    }

    private static BeObjControlRule convert2BeObjControlRule(BeEntityControlRuleDef ruleDef, GspBizEntityObject beObject) {

        BeObjControlRule objRule = new BeObjControlRule();
        ControlRuleConvertor.convert2ControlRule(ruleDef, objRule, beObject);

        dealChildObjs(ruleDef, objRule, beObject);

        BeFieldControlRuleDef elementRuleDef = (BeFieldControlRuleDef) ruleDef.getChildControlRules().get(CommonModelNames.Element);
        dealElement(elementRuleDef, objRule, beObject);

        return objRule;
    }

    private static void dealChildObjs(BeEntityControlRuleDef ruleDef, BeObjControlRule objRule, GspBizEntityObject beObject) {
        var childObjs = beObject.getContainChildObjects();
        if (childObjs == null || childObjs.isEmpty())
            return;
        objRule.getChildRules().put(CommonModelNames.ChildObject, new HashMap<>());

        for (var childObj : childObjs) {
            BeObjControlRule childRule = convert2BeObjControlRule(ruleDef, (GspBizEntityObject) childObj);
            objRule.getChildRules().get(CommonModelNames.ChildObject).put(childObj.getID(), childRule);
        }
    }

    private static void dealElement(BeFieldControlRuleDef ruleDef, BeObjControlRule objRule, GspBizEntityObject beObject) {
        var elements = beObject.getContainElements();
        if (elements == null || elements.isEmpty())
            return;
        objRule.getChildRules().put(CommonModelNames.Element, new HashMap<>());

        for (var element : elements) {
            BeFeildControlRule fieldRule = new BeFeildControlRule();
            ControlRuleConvertor.convert2ControlRule(ruleDef, fieldRule, element);
            objRule.getChildRules().get(CommonModelNames.Element).put(element.getID(), fieldRule);
        }
    }
}
