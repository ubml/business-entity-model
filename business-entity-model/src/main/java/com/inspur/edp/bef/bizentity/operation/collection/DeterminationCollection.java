package com.inspur.edp.bef.bizentity.operation.collection;

import com.inspur.edp.bef.bizentity.exceptions.BusinessEntityErrorCodeEnum;
import com.inspur.edp.bef.bizentity.exceptions.BusinessEntityException;
import com.inspur.edp.bef.bizentity.operation.BizOperation;
import com.inspur.edp.bef.bizentity.operation.BizOperationCollection;
import com.inspur.edp.bef.bizentity.operation.Determination;

import java.util.ArrayList;
import java.util.function.Predicate;

public class DeterminationCollection extends BizOperationCollection {
    private static final long serialVersionUID = 1L;

    // C# TO JAVA CONVERTER WARNING: There is no Java equivalent to C#'s shadowing
    // via the 'new' keyword:
    // ORIGINAL LINE: public new void add(BizOperation operation)
    public final boolean add(BizOperation operation) {
        if (operation instanceof Determination) {
            operation.setOwner(getOwner());
            return super.add(operation);
        }
        throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_BUSINESS_ENTITY_0082, "Determination");
    }

    // /**
    // 克隆BE Determinations动作集合

    // @param isGenerateId 是否生成Id
    // @return 返回动作集合
    // */
    // //C# TO JAVA CONVERTER WARNING: There is no Java equivalent to C#'s shadowing
    // via the 'new' keyword:
    // //ORIGINAL LINE: public new object clone(bool isGenerateId)
    // public final DeterminationCollection clone(boolean isGenerateId)
    // {
    // DeterminationCollection col = new DeterminationCollection();
    // //C# TO JAVA CONVERTER TODO TASK: Lambda expressions and anonymous methods
    // are not converted by C# to Java Converter:
    // col.addRange(this.select(item { return
    // (Determination)item.clone(isGenerateId)));
    // return col;
    // }

    // /**
    // 克隆BE Determinations动作集合

    // @return 返回动作集合
    // */
    // //C# TO JAVA CONVERTER WARNING: There is no Java equivalent to C#'s shadowing
    // via the 'new' keyword:
    // //ORIGINAL LINE: public new object clone()
    // public final Object clone()
    // {
    // return clone(false);
    // }

    @Override
    public DeterminationCollection clone() {
        return (DeterminationCollection) super.clone();
    }

    @Override
    protected Determination convertOperation(BizOperation op) {
        return (Determination) super.convertOperation(op);
    }

    @Override
    protected DeterminationCollection createOperationCollection() {
        return new DeterminationCollection();
    }

    /**
     * 查询符合条件的Node集合
     *
     * @param predicate 查询条件
     * @return 返回结果按照层次遍历顺序
     */
    public final ArrayList<Determination> getFiltedDtms(Predicate<Determination> predicate) {

        ArrayList<Determination> result = new ArrayList<Determination>();
        for (BizOperation op : this) {
            Determination dtm = (Determination) op;
            if (predicate.test(dtm)) {
                result.add(dtm);
            }
        }
        return result;
    }
}