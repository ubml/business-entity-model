package com.inspur.edp.bef.bizentity.dtconsistencycheck.actioneventargs;

import java.util.List;

public class ChangingActionParamsEventArgs extends AbstractMgrActionEventArgs {

    protected List<String> newParameters;
    protected List<String> originalParameters;

    public ChangingActionParamsEventArgs() {

    }

    public ChangingActionParamsEventArgs(String beId, String actionId, String metadataPath,
                                         List<String> newParameters, List<String> originalParameters) {
        super(beId, actionId, metadataPath);
        this.newParameters = newParameters;
        this.originalParameters = originalParameters;
    }

    public List<String> getNewParameters() {
        return newParameters;
    }

    public List<String> getOriginalParameters() {
        return originalParameters;
    }

    public void setNewParameters(List<String> newParameters) {
        this.newParameters = newParameters;
    }

    public void setOriginalParameters(List<String> originalParameters) {
        this.originalParameters = originalParameters;
    }
}
