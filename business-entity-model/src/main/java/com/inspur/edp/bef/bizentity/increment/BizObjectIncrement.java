package com.inspur.edp.bef.bizentity.increment;

import com.inspur.edp.bef.bizentity.increment.entity.determination.DtmIncrement;
import com.inspur.edp.bef.bizentity.increment.entity.validation.ValIncrement;
import com.inspur.edp.cef.designtime.api.entity.increment.ModifyEntityIncrement;

import java.util.HashMap;

public class BizObjectIncrement extends ModifyEntityIncrement {

    private final HashMap<String, DtmIncrement> determinations = new HashMap<>();

    public HashMap<String, DtmIncrement> getDeterminations() {
        return determinations;
    }

    private final HashMap<String, ValIncrement> validations = new HashMap<>();

    public HashMap<String, ValIncrement> getValidations() {
        return validations;
    }

}
