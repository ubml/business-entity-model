package com.inspur.edp.bef.bizentity.operation;

import com.inspur.edp.bef.bizentity.beenum.BEOperationType;
import com.inspur.edp.bef.bizentity.beenum.BETriggerTimePointType;
import com.inspur.edp.bef.bizentity.beenum.BEValidationType;
import com.inspur.edp.bef.bizentity.beenum.RequestNodeTriggerType;
import com.inspur.edp.bef.bizentity.operation.componentbase.BizParameterCollection;
import com.inspur.edp.cef.designtime.api.collection.ValElementCollection;

import java.util.EnumSet;

/**
 * 校验规则
 */
public class Validation extends BizOperation implements Cloneable {

    // region ���캯��

    /**
     * 默认构造方法
     */
    public Validation() {
    }

    // endregion

    public BizParameterCollection getParameterCollection() {
        return parameterCollection;
    }

    public void setParameterCollection(BizParameterCollection parameterCollection) {
        this.parameterCollection = parameterCollection;
    }

    // region ����
    private BizParameterCollection parameterCollection;
    /**
     * 类型：操作许可校验、数据一致性校验。
     */
    private BEValidationType privateValidationType = BEValidationType.forValue(0);

    public BEValidationType getValidationType() {
        return privateValidationType;
    }

    public void setValidationType(BEValidationType value) {
        privateValidationType = value;
    }

    /**
     * 执行时机
     */
    private EnumSet<BETriggerTimePointType> privateTriggerTimePointType = EnumSet.of(BETriggerTimePointType.forValue(0));

    public final EnumSet<BETriggerTimePointType> getTriggerTimePointType() {
        return privateTriggerTimePointType;
    }

    public final void setTriggerTimePointType(EnumSet<BETriggerTimePointType> value) {
        privateTriggerTimePointType = value;
    }

    // /**
    // * ִ��˳��
    // *
    // */
    // private int privateOrder;

    // public int getOrder() {
    // return privateOrder;
    // }

    // public void setOrder(int value) {
    // privateOrder = value;
    // }

    // private java.util.List<Validation> precedings;
    // /**
    // ǰ��У���б��б����Ⱥ����

    // */
    // public java.util.List<Validation> getPrecedings()
    // {
    // return InitPrecedings().toList();
    // }
    // /**
    // ����У���б��б����Ⱥ����

    // */
    // public java.util.List<Validation> getSucceedings()
    // {
    // return InitSucceedings().toList();
    // }

    // private java.util.List<String> precedingIds;
    // /**
    // ǰ��У���б��б����Ⱥ����

    // */
    // public java.util.List<String> getPrecedingIds()
    // {
    // return (precedingIds != null) ? precedingIds : (precedingIds = new
    // java.util.ArrayList<String>());
    // }

    // public void setPrecedingIds(java.util.List<String> value)
    // {
    // precedingIds = value;
    // }

    // private java.util.List<Validation> succeedings;

    // private java.util.List<String> succeedingIds;

    // /**
    // * ����У���б��б����Ⱥ����
    // *
    // */
    // public java.util.List<String> getSucceedingIds() {
    // return (succeedingIds != null) ? succeedingIds : (succeedingIds = new
    // java.util.ArrayList<String>());
    // }

    // public void setSucceedingIds(java.util.List<String> value) {
    // succeedingIds = value;
    // }

    /**
     * ִ��ʱ��
     */
    private EnumSet<RequestNodeTriggerType> privateRequestNodeTriggerType = EnumSet
            .of(RequestNodeTriggerType.forValue(0));

    public EnumSet<RequestNodeTriggerType> getRequestNodeTriggerType() {
        return privateRequestNodeTriggerType;
    }

    public void setRequestNodeTriggerType(EnumSet<RequestNodeTriggerType> value) {
        privateRequestNodeTriggerType = value;
    }

    private ValElementCollection rqtElements;

    /**
     * ����ʱ���������Ҫ�������ֶ�LabelID����
     */
    public final void setRequestElements(ValElementCollection value) {
        rqtElements = value;
    }

    public final ValElementCollection getRequestElements() {
        if (rqtElements == null) {
            rqtElements = new ValElementCollection();
        }
        return rqtElements;
    }

    private java.util.HashMap<String, ValElementCollection> requestChildElements;

    public final void setRequestChildElements(java.util.HashMap<String, ValElementCollection> value) {
        requestChildElements = value;
    }

    public final java.util.HashMap<String, ValElementCollection> getRequestChildElements() {
        if (requestChildElements == null) {
            requestChildElements = new java.util.LinkedHashMap<>();
        }
        return requestChildElements;
    }

    private java.util.HashMap<BETriggerTimePointType, EnumSet<RequestNodeTriggerType>> validationTriggerPoints;

    public final void setValidationTriggerPoints(java.util.HashMap<BETriggerTimePointType, EnumSet<RequestNodeTriggerType>> value) {
        validationTriggerPoints = value;
    }

    public final java.util.HashMap<BETriggerTimePointType, EnumSet<RequestNodeTriggerType>> getValidationTriggerPoints() {
        if (validationTriggerPoints == null) {
            validationTriggerPoints = new java.util.LinkedHashMap<>();
        }
        return validationTriggerPoints;
    }

    // endregion

    // //#region Private Methods

    // private java.util.List<Validation> InitSucceedings()
    // {

    // succeedings = InitValidationsById(getSucceedingIds());
    // return succeedings;
    // }

    // private java.util.List<Validation> InitPrecedings()
    // {
    // precedings = InitValidationsById(getPrecedingIds());
    // return precedings;
    // }

    // private java.util.List<Validation> InitValidationsById(java.util.List<String>
    // idList)
    // {
    // //�������
    // Object tempVar = getOwner().getValidations().firstOrDefault(item { return
    // item.getID(id.equals()));
    // //C# TO JAVA CONVERTER TODO TASK: Lambda expressions and anonymous methods
    // are not converted by C# to Java Converter:
    // return idList.select(id { return (Validation)((tempVar instanceof Validation)
    // ? tempVar : null)).toList();
    // }

    // //#endregion

    // region ����

    /**
     * �ж����
     *
     * @param obj �����뵱ǰУ�����ȽϵĶ���
     * @return ��ȷ���true
     */
    @Override
    public boolean equals(Object obj) {
        Validation other = (Validation) ((obj instanceof Validation) ? obj : null);
        return other != null && ((this == other) || equals(other));
    }

    /**
     * ��дEquals
     *
     * @param other �����뵱ǰУ�����Ƚϵ�У�������
     * @return ��ȷ���true
     */
    protected final boolean equals(Validation other) {
        // return super.equals(other) && getOrder() == other.getOrder() &&
        // other.getValidationType() == getValidationType()
        return super.equals(other) && other.getValidationType() == getValidationType()
                && other.getRequestNodeTriggerType() == getRequestNodeTriggerType();
    }

    /**
     * ��д GetHashCode
     *
     * @return
     */
    @Override
    public int hashCode() {
        {
            // int hashCode = (super.hashCode() * 397) ^ (new
            // Integer(getOrder())).hashCode();
            int hashCode = (super.hashCode() * 397);
            hashCode = (hashCode * 397) ^ getValidationType().hashCode();
            hashCode = (hashCode * 397) ^ getRequestNodeTriggerType().hashCode();
            return hashCode;
        }
    }

    /**
     * @return
     */
    @Override
    public Validation clone() {
        return clone(false);
    }

    /**
     * Clone����
     *
     * @param isGenerateId
     * @return
     */
    @Override
    public Validation clone(boolean isGenerateId) {
        Object tempVar = super.clone(isGenerateId);
        Validation result = (Validation) ((tempVar instanceof Validation) ? tempVar : null);

        // result.PrecedingIds = new java.util.ArrayList<String>(getPrecedingIds());
        // result.SucceedingIds = new java.util.ArrayList<String>(getSucceedingIds());

        return result;
    }

    public final void mergeWithDependentValidation(Validation dependentValidation) {
        if (getIsRef() || dependentValidation == null) {
            return;
        }
        mergeOperationBaseInfo(dependentValidation);
        mergeValidationInfo(dependentValidation);
    }

    private void mergeValidationInfo(Validation dependentValidation) {
        setValidationType(dependentValidation.getValidationType());
        setRequestNodeTriggerType(dependentValidation.getRequestNodeTriggerType());
        // setOrder(dependentValidation.getOrder());
        // setSucceedingIds(dependentValidation.getSucceedingIds());
        // setPrecedingIds(dependentValidation.getPrecedingIds());
    }

    // endregion

    /**
     * ��������
     */
    @Override
    public BEOperationType getOpType() {
        return BEOperationType.Validation;
    }
}