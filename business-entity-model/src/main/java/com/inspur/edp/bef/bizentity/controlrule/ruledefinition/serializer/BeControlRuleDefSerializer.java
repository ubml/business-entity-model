package com.inspur.edp.bef.bizentity.controlrule.ruledefinition.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.bef.bizentity.controlrule.ruledefinition.entity.BeControlRuleDef;
import com.inspur.edp.das.commonmodel.controlruledef.serializer.CmRuleDefSerializer;

public class BeControlRuleDefSerializer<T extends BeControlRuleDef> extends CmRuleDefSerializer<T> {
    @Override
    protected final void writeCmRuleExtendInfos(T controlRuleDefinition, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) {
        super.writeCmRuleExtendInfos(controlRuleDefinition, jsonGenerator, serializerProvider);

    }
}
