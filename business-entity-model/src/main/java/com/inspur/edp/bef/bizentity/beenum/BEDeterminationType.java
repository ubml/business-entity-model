package com.inspur.edp.bef.bizentity.beenum;

/**
 * BE Determination的类型
 */
public enum BEDeterminationType {
    /**
     * 非持久化: 所对应的计算结果字段是虚拟列，不需要持久化。 此时只需要读取和修改后进行计算
     */
    Transient(0),

    /**
     * 持久化: 所对应的计算结果字段是持久化的。数据保存前需要计算
     */
    Persistent(1);

    private int intValue;
    private static java.util.HashMap<Integer, BEDeterminationType> mappings;

    private synchronized static java.util.HashMap<Integer, BEDeterminationType> getMappings() {
        if (mappings == null) {
            mappings = new java.util.HashMap<Integer, BEDeterminationType>();
        }
        return mappings;
    }

    private BEDeterminationType(int value) {
        intValue = value;
        BEDeterminationType.getMappings().put(value, this);
    }

    public int getValue() {
        return intValue;
    }

    public static BEDeterminationType forValue(int value) {
        return getMappings().get(value);
    }
}