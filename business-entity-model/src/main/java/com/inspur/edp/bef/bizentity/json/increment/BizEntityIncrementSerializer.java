package com.inspur.edp.bef.bizentity.json.increment;

import com.fasterxml.jackson.core.JsonGenerator;
import com.inspur.edp.bef.bizentity.common.BizEntityJsonConst;
import com.inspur.edp.bef.bizentity.exceptions.BusinessEntityErrorCodeEnum;
import com.inspur.edp.bef.bizentity.exceptions.BusinessEntityException;
import com.inspur.edp.bef.bizentity.increment.BizEntityIncrement;
import com.inspur.edp.bef.bizentity.increment.entity.action.MgrActionIncrement;
import com.inspur.edp.cef.designtime.api.json.CefNames;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.das.commonmodel.entity.increment.CommonModelIncrement;
import com.inspur.edp.das.commonmodel.json.increment.CommonModelIncrementSerializer;
import com.inspur.edp.das.commonmodel.json.increment.CommonObjectIncrementSerializer;
import lombok.var;

import java.io.IOException;
import java.util.HashMap;

public class BizEntityIncrementSerializer extends CommonModelIncrementSerializer {
    @Override
    protected CommonObjectIncrementSerializer getObjectIncrementSerializer() {
        return new BizObjectIncrementSerializer();
    }

    @Override
    protected void writeExtendInfo(CommonModelIncrement value, JsonGenerator gen) {
        BizEntityIncrement increment = (BizEntityIncrement) value;
        writeMgrActions(increment.getActions(), gen);
    }


    private void writeMgrActions(HashMap<String, MgrActionIncrement> actions, JsonGenerator gen) {
        if (actions == null)
            return;
        SerializerUtils.writePropertyName(gen, BizEntityJsonConst.BizMgrActions);
        SerializerUtils.WriteStartArray(gen);
        for (var item : actions.entrySet()) {
            try {
                SerializerUtils.writeStartObject(gen);
                SerializerUtils.writePropertyValue(gen, CefNames.Id, item.getKey());

                SerializerUtils.writePropertyName(gen, BizEntityJsonConst.BizMgrAction);
                MgrActionIncrementSerializer serializer = new MgrActionIncrementSerializer();
                serializer.serialize(item.getValue(), gen, null);
                SerializerUtils.writeEndObject(gen);
            } catch (IOException e) {
                throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_JSON_0004, e, "MgrActionIncrement");
            }
        }
        SerializerUtils.WriteEndArray(gen);
    }
}
