package com.inspur.edp.bef.bizentity.util.changeset;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.inspur.edp.bef.bizentity.GspBizEntityElement;
import com.inspur.edp.bef.bizentity.GspBizEntityObject;
import com.inspur.edp.bef.bizentity.operation.BizMgrAction;
import com.inspur.edp.bef.bizentity.pushchangesetargs.ActionChangeDetail;
import com.inspur.edp.bef.bizentity.pushchangesetargs.ActionChangeSet;
import com.inspur.edp.bef.bizentity.pushchangesetargs.ChangeType;
import com.inspur.edp.bef.bizentity.pushchangesetargs.ElementChangeDetail;
import com.inspur.edp.bef.bizentity.pushchangesetargs.ElementChangeSet;
import com.inspur.edp.bef.bizentity.pushchangesetargs.MetadataInfo;
import com.inspur.edp.bef.bizentity.pushchangesetargs.ObjectChangeDetail;
import com.inspur.edp.bef.bizentity.pushchangesetargs.ObjectChangeSet;
import com.inspur.edp.bef.bizentity.pushchangesetargs.PushChangeSet;
import com.inspur.edp.bef.bizentity.pushchangesetargs.RelatedMetadata;
import com.inspur.edp.bef.bizentity.util.StringUtil;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Slf4j
public class ChangeSetInfoUtil {
    public static PushChangeSet getChangeSetInfo(JsonNode info) {
        if (StringUtil.checkNull(info)) {
            return null;
        }
        PushChangeSet changeSet = new PushChangeSet();
        try {
            if (!StringUtil.checkNull(info.get(ChangeSetConst.metadataInfo))) {
                changeSet.setMetadataInfo(new ObjectMapper().treeToValue(info.get(ChangeSetConst.metadataInfo), MetadataInfo.class));
            }
            changeSet.setRelatedMetadatas(getRelateMetadatas(info));
            changeSet.setActionChangeSets(getActionChangeSet(info));
            changeSet.setObjectChangeSets(getObjectChangeSets(info));
            changeSet.setElementChangeSets(getElementChangeSets(info));
        } catch (JsonProcessingException e) {
            log.error(e.getMessage());
        }
        return changeSet;
    }


    static List<RelatedMetadata> getRelateMetadatas(JsonNode info) throws JsonProcessingException {
        if (StringUtil.checkNull(info.get(ChangeSetConst.relatedMetadatas))) {
            return null;
        }
        List<RelatedMetadata> metadataList = new ArrayList<>();
        if (StringUtil.checkNull(info.get(ChangeSetConst.relatedMetadatas))) {
            return metadataList;
        }
        for (JsonNode jsonNode : info.get(ChangeSetConst.relatedMetadatas)) {
            metadataList.add(new ObjectMapper().treeToValue(jsonNode, RelatedMetadata.class));
        }
        return metadataList;
    }

    static List<ActionChangeSet> getActionChangeSet(JsonNode info) throws JsonProcessingException {
        if (StringUtil.checkNull(info.get(ChangeSetConst.actionChangeSet))) {
            return null;
        }
        List<ActionChangeSet> changeSetList = new ArrayList<>();
        for (JsonNode jsonNode : info.get(ChangeSetConst.actionChangeSet)) {
            ObjectNode node = (ObjectNode) jsonNode;

            ObjectNode detailNode = (ObjectNode) node.get(ChangeSetConst.changeDetail);
            String type = node.get(ChangeSetConst.changeType).textValue();

            if (StringUtil.checkNull(detailNode) | StringUtil.checkNull(type)) {
                continue;
            }
            ActionChangeDetail detail = new ActionChangeDetail();
            detail.setActionId(detailNode.get(ChangeSetConst.actionId).textValue());
            detail.setActionCode(detailNode.get(ChangeSetConst.actionCode).textValue());
            if (!StringUtil.checkNull(detailNode.get(ChangeSetConst.parameterId))) {
                detail.setParameterId(detailNode.get(ChangeSetConst.parameterId).textValue());
            }
            if (!StringUtil.checkNull(detailNode.get(ChangeSetConst.parameterCode))) {
                detail.setParameterCode(detailNode.get(ChangeSetConst.parameterCode).textValue());
            }
            detail.setNecessary(detailNode.get(ChangeSetConst.isNecessary).asBoolean());
            detail.setChangeInfo(new ObjectMapper().treeToValue(detailNode.get(ChangeSetConst.changeInfo), Map.class));
            if (!StringUtil.checkNull(detailNode.get(ChangeSetConst.mgrAction))) {
                detail.setMgrAction(new ObjectMapper().treeToValue(detailNode.get(ChangeSetConst.mgrAction), BizMgrAction.class));
            }
            changeSetList.add(new ActionChangeSet(detail, ChangeType.valueOf(type)));
        }
        return changeSetList;
    }

    public static List<ObjectChangeSet> getObjectChangeSets(JsonNode info) throws JsonProcessingException {
        if (StringUtil.checkNull(info.get(ChangeSetConst.objectChangeSets))) {
            return null;
        }
        List<ObjectChangeSet> changeSetList = new ArrayList<>();
        for (JsonNode jsonNode : info.get(ChangeSetConst.objectChangeSets)) {
            ObjectNode node = (ObjectNode) jsonNode;
            ObjectNode detailNode = (ObjectNode) node.get(ChangeSetConst.changeDetail);
            String type = node.get(ChangeSetConst.changeType).textValue();

            if (StringUtil.checkNull(detailNode) | StringUtil.checkNull(type)) {
                continue;
            }
            ObjectChangeDetail detail = new ObjectChangeDetail();
            detail.setBizObjectId(detailNode.get(ChangeSetConst.bizObjectId).textValue());
            detail.setBizObjectCode(detailNode.get(ChangeSetConst.bizObjectCode).textValue());
            detail.setNecessary(detailNode.get(ChangeSetConst.isNecessary).asBoolean());
            if (!StringUtil.checkNull(detailNode.get(ChangeSetConst.parentObjIDElementId))) {
                detail.setParentObjIDElementId(detailNode.get(ChangeSetConst.parentObjIDElementId).textValue());
            }
            detail.setChangeInfo(new ObjectMapper().treeToValue(detailNode.get(ChangeSetConst.changeInfo), Map.class));
            if (detailNode.get(ChangeSetConst.bizObject) != null) {
                detail.setBizObject(new ObjectMapper().treeToValue(detailNode.get(ChangeSetConst.bizObject), GspBizEntityObject.class));
            }
            changeSetList.add(new ObjectChangeSet(detail, ChangeType.valueOf(type)));
        }
        return changeSetList;
    }

    public static List<ElementChangeSet> getElementChangeSets(JsonNode info) throws JsonProcessingException {
        if (StringUtil.checkNull(info.get(ChangeSetConst.elementChangeSets))) {
            return null;
        }
        List<ElementChangeSet> changeSetList = new ArrayList<>();
        for (JsonNode jsonNode : info.get(ChangeSetConst.elementChangeSets)) {
            ObjectNode node = (ObjectNode) jsonNode;
            ObjectNode detailNode = (ObjectNode) node.get(ChangeSetConst.changeDetail);
            String type = node.get(ChangeSetConst.changeType).textValue();

            if (StringUtil.checkNull(detailNode) | StringUtil.checkNull(type)) {
                continue;
            }
            ElementChangeDetail detail = new ElementChangeDetail();
            detail.setBizObjectId(detailNode.get(ChangeSetConst.bizObjectId).textValue());
            detail.setBizObjectCode(detailNode.get(ChangeSetConst.bizObjectCode).textValue());
            detail.setBizElementId(detailNode.get(ChangeSetConst.bizElementId).textValue());
            //TODO: 前端偶发性没有elementCode, 项目演示临时兼容
            JsonNode elementCodeJson = detailNode.get(ChangeSetConst.bizElementCode);
            if (elementCodeJson != null) {
                detail.setBizElementCode(elementCodeJson.textValue());
            }
            detail.setChangeInfo(new ObjectMapper().treeToValue(detailNode.get(ChangeSetConst.changeInfo), Map.class));
            if (!StringUtil.checkNull(detailNode.get(ChangeSetConst.bizElement))) {
                detail.setBizElement(new ObjectMapper().treeToValue(detailNode.get(ChangeSetConst.bizElement), GspBizEntityElement.class));
            }
            changeSetList.add(new ElementChangeSet(detail, ChangeType.valueOf(type)));
        }
        return changeSetList;
    }

}
