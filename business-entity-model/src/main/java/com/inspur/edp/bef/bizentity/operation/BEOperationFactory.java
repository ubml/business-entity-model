package com.inspur.edp.bef.bizentity.operation;

import com.inspur.edp.bef.bizentity.beenum.BEOperationType;

/**
 * BE操作生成工厂
 */
public class BEOperationFactory {
    /**
     * 根据操作类型创建BOBizOperation
     *
     * @param operationType 操作类型
     * @return
     */
    public static BizOperation createElement(BEOperationType operationType) {
        BizOperation result;
        switch (operationType) {
            case BizAction:
            case Subscription:
            case Initiation:
                result = new BizAction();
                break;
            case BizMgrAction:
                result = new BizMgrAction();
                break;
            case Validation:
                result = new Validation();
                break;
            case Determination:
                result = new Determination();
                break;
            default:
                result = null;
                break;
        }
        return result;
    }
}
