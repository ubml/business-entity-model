package com.inspur.edp.bef.bizentity.operation.componentbase;

public enum BizParActualValueType {
    Constant,//常量
    Expression, //表达式
    Variable;//vo变量

    public int getValue() {
        return this.ordinal();
    }

    public static BizParActualValueType forValue(int value) {
        return values()[value];
    }
}
