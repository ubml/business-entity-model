package com.inspur.edp.bef.bizentity.operation.internalmgraction;

import com.inspur.edp.bef.bizentity.operation.BizMgrAction;
import com.inspur.edp.das.commonmodel.util.MessageI18nUtils;

/**
 * 内置Delete操作
 */
public class DeleteMgrAction extends BizMgrAction implements IInternalMgrAction {
    public static final String id = "9sEsnADPFkCRK6ZBC0OwiA";
    public static final String code = "Delete";
    public static final String name = MessageI18nUtils.getMessage("Delete");

    public DeleteMgrAction() {
        setID(id);
        setCode(code);
        setName(name);
    }
}