package com.inspur.edp.bef.bizentity.gspbusinessentity.entity;

import com.inspur.edp.cdp.common.utils.json.JsonUtil;
import io.swagger.util.Json;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.AttributeConverter;

public class BeConfigCollectionInfoConverter implements AttributeConverter<BeConfigCollectionInfo, String> {
    private static Logger logger = LoggerFactory.getLogger(BeConfigCollectionInfo.class);

    @Override
    public String convertToDatabaseColumn(BeConfigCollectionInfo attribute) {
        return Json.pretty(attribute);
        // return JsonSerializerUtils.writeValueAsString(attribute);
    }

    @Override
    public BeConfigCollectionInfo convertToEntityAttribute(String dbData) {
        if (dbData == null || dbData.isEmpty()) {
            return new BeConfigCollectionInfo();
        }
        BeConfigCollectionInfo configCollectionInfo = null;
        try {
            configCollectionInfo = JsonUtil.toObject(dbData, BeConfigCollectionInfo.class);
        } catch (JsonUtil.JsonParseException e) {
            logger.error("反序列化出错：" + dbData, e);
        }
        return configCollectionInfo;
    }
}
