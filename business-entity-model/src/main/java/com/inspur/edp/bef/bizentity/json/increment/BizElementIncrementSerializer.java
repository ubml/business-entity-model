package com.inspur.edp.bef.bizentity.json.increment;

import com.inspur.edp.bef.bizentity.json.element.BizElementSerializer;
import com.inspur.edp.das.commonmodel.json.element.CmElementSerializer;
import com.inspur.edp.das.commonmodel.json.increment.CommonElementIncrementSerializer;

public class BizElementIncrementSerializer extends CommonElementIncrementSerializer {
    @Override
    protected CmElementSerializer getCmElementSerializer() {
        return new BizElementSerializer();
    }
}
