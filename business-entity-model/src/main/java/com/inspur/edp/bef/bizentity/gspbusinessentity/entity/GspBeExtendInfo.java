package com.inspur.edp.bef.bizentity.gspbusinessentity.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.NoArgsConstructor;

import javax.persistence.Id;
import java.util.Date;

@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class GspBeExtendInfo {

    @Id
    private String id;
    private String extendInfo;
    private String configId;

    private String createdBy;
    private Date createdOn;
    private String lastChangedBy;
    private Date lastChangedOn;

    private BeConfigCollectionInfo beConfigCollectionInfo;

    public GspBeExtendInfo(String id, String extendInfo, String configId, String createdBy, Date createdOn,
                           String lastChangedBy, Date lastChangedOn) {
        this.id = id;
        this.extendInfo = extendInfo;
        this.configId = configId;
        this.createdBy = createdBy;
        this.createdOn = createdOn;
        this.lastChangedBy = lastChangedBy;
        this.lastChangedOn = lastChangedOn;
    }

    public GspBeExtendInfo(String id, String extendInfo, String configId, String createdBy, Date createdOn,
                           String lastChangedBy, Date lastChangedOn, BeConfigCollectionInfo beConfigCollectionInfo) {
        this.id = id;
        this.extendInfo = extendInfo;
        this.configId = configId;
        this.createdBy = createdBy;
        this.createdOn = createdOn;
        this.lastChangedBy = lastChangedBy;
        this.lastChangedOn = lastChangedOn;
        this.beConfigCollectionInfo = beConfigCollectionInfo;
    }

    // region 属性
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getExtendInfo() {
        return extendInfo;
    }

    public void setExtendInfo(String extendInfo) {
        this.extendInfo = extendInfo;
    }

    public String getConfigId() {
        return configId;
    }

    public void setConfigId(String configId) {
        this.configId = configId;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getLastChangedBy() {
        return lastChangedBy;
    }

    public void setLastChangedBy(String lastChangedBy) {
        this.lastChangedBy = lastChangedBy;
    }

    public Date getLastChangedOn() {
        return lastChangedOn;
    }

    public void setLastChangedOn(Date lastChangedOn) {
        this.lastChangedOn = lastChangedOn;
    }

    public BeConfigCollectionInfo getBeConfigCollectionInfo() {
        return beConfigCollectionInfo;
    }

    public void setBeConfigCollectionInfo(
            BeConfigCollectionInfo beConfigCollectionInfo) {
        this.beConfigCollectionInfo = beConfigCollectionInfo;
    }

    // endregion
}
