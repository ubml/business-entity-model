package com.inspur.edp.bef.bizentity.controlrule.ruledefinition.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.bef.bizentity.controlrule.ruledefinition.entity.BeEntityControlRuleDef;
import com.inspur.edp.das.commonmodel.controlruledef.serializer.CmEntityControlRuleDefSerializer;


public class BeEntityControlRuleDefSerializer<T extends BeEntityControlRuleDef> extends CmEntityControlRuleDefSerializer<T> {
    @Override
    protected final void writeCmEntityRuleExtendInfos(T controlRuleDefinition, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) {
        super.writeCmEntityRuleExtendInfos(controlRuleDefinition, jsonGenerator, serializerProvider);
    }
}
