package com.inspur.edp.bef.bizentity.common;

public class InternalActionUtil {
    //Internal MgrActionIDs
    public static final String DeleteMgrActionId = "9sEsnADPFkCRK6ZBC0OwiA";
    public static final String ModifyMgrActionId = "8Wna6KaBQEOHpRJfmGMBYQ";
    public static final String QueryMgrActionId = "m_6xIwJwoUy7qzsuZ6_S5A";
    public static final String RetrieveDefaultMgrActionId = "Tji_dbrLtEKQlOLRpOJj9Q";
    public static final String RetrieveMgrActionId = "fQbpcO7bIEGoRdTZZIyrag";

    //Internal BizActionIDs
    public static final String DeleteBeActionId = "aD73GrGaKEiA7N9jsPxIOQ";
    public static final String ModifyBeActionId = "tf9MVINdJ0yhUrsJgfcvBA";
    public static final String RetrieveBeActionId = "cJwzDlFfa0uz4rChlTEd4g";

    public static java.util.ArrayList<String> InternalMgrActionIDs = new java.util.ArrayList<String>(java.util.Arrays.asList(new String[]{DeleteMgrActionId, ModifyMgrActionId, QueryMgrActionId, RetrieveDefaultMgrActionId, RetrieveMgrActionId}));

    public static java.util.ArrayList<String> InternalBeActionIDs = new java.util.ArrayList<String>(java.util.Arrays.asList(new String[]{DeleteBeActionId, ModifyBeActionId, RetrieveBeActionId}));

}