package com.inspur.edp.bef.bizentity.controlrule.rule.parser;

import com.fasterxml.jackson.databind.JsonDeserializer;
import com.inspur.edp.bef.bizentity.controlrule.rule.BeObjControlRule;
import com.inspur.edp.das.commonmodel.controlrule.CmEntityControlRule;
import com.inspur.edp.das.commonmodel.controlrule.parser.CmEntityControlRuleParser;
import com.inspur.edp.das.commonmodel.json.CommonModelNames;

public class BeObjectControlRuleParser extends CmEntityControlRuleParser {

    protected JsonDeserializer getChildDeserializer(String childTypeName) {
        switch (childTypeName) {
            case CommonModelNames.ChildObject:
                return new BeObjectControlRuleParser();
            case CommonModelNames.Element:
                return new BeFieldRuleParser();
        }
        return null;
    }

    @Override
    protected CmEntityControlRule createCmEntityRule() {
        return new BeObjControlRule();
    }
}
