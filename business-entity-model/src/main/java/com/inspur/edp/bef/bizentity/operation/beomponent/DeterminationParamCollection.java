package com.inspur.edp.bef.bizentity.operation.beomponent;

import com.inspur.edp.bef.bizentity.operation.componentbase.BizParameterCollection;

public class DeterminationParamCollection extends BizParameterCollection<DeterminationParameter> {

    public DeterminationParamCollection() {
    }
}