package com.inspur.edp.bef.bizentity.increment.entity.validation;

import com.inspur.edp.bef.bizentity.operation.Validation;
import com.inspur.edp.cef.designtime.api.increment.IncrementType;

public class AddedValIncrement extends ValIncrement {

    private Validation action;

    @Override
    public IncrementType getIncrementType() {
        return IncrementType.Added;
    }

    public Validation getAction() {
        return action;
    }

    public void setAction(Validation action) {
        this.action = action;
    }
}
