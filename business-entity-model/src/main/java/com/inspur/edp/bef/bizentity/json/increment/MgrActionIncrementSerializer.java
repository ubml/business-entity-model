package com.inspur.edp.bef.bizentity.json.increment;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.bef.bizentity.common.BizEntityJsonConst;
import com.inspur.edp.bef.bizentity.exceptions.BusinessEntityErrorCodeEnum;
import com.inspur.edp.bef.bizentity.exceptions.BusinessEntityException;
import com.inspur.edp.bef.bizentity.increment.entity.action.AddedMgrActionIncrement;
import com.inspur.edp.bef.bizentity.increment.entity.action.MgrActionIncrement;
import com.inspur.edp.bef.bizentity.increment.entity.action.ModifyMgrActionIncrement;
import com.inspur.edp.bef.bizentity.json.operation.BizMgrActionSerializer;
import com.inspur.edp.bef.bizentity.operation.BizMgrAction;
import com.inspur.edp.cef.designtime.api.json.CefNames;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;

import java.io.IOException;

public class MgrActionIncrementSerializer extends JsonSerializer<MgrActionIncrement> {

    @Override
    public void serialize(MgrActionIncrement value, JsonGenerator gen, SerializerProvider serializerProvider) throws IOException {
        SerializerUtils.writeStartObject(gen);
        writeBaseInfo(value, gen);
        SerializerUtils.writeEndObject(gen);
    }

    private void writeBaseInfo(MgrActionIncrement value, JsonGenerator gen) {
        SerializerUtils.writePropertyValue(gen, CefNames.IncrementType, value.getIncrementType().toString());
        switch (value.getIncrementType()) {
            case Added:
                writeAddedIncrement((AddedMgrActionIncrement) value, gen);
                break;
            case Modify:
                writeModifyIncrement((ModifyMgrActionIncrement) value, gen);
                break;
            case Deleted:
//                writeDeletedIncrement((DeletedEntityIncrement) value, gen);
                break;
            default:
                throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_ENUM_NOTSUPPORT_0004, value.getIncrementType().toString());
        }
    }

    private void writeAddedIncrement(AddedMgrActionIncrement increment, JsonGenerator gen) {
        SerializerUtils.writePropertyName(gen, BizEntityJsonConst.AddedAction);
        writeBaseActionInfo(increment.getAction(), gen);
    }

    private void writeModifyIncrement(ModifyMgrActionIncrement increment, JsonGenerator gen) {
        SerializerUtils.writePropertyName(gen, BizEntityJsonConst.ModifyAction);
        writeBaseActionInfo(increment.getAction(), gen);
    }

    private void writeBaseActionInfo(BizMgrAction action, JsonGenerator gen) {
        BizMgrActionSerializer convertor = new BizMgrActionSerializer();
        try {
            convertor.serialize(action, gen, null);
        } catch (Exception e) {
            throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_JSON_0004, e, "BizMgrAction", action.getCode());
        }
    }
}
