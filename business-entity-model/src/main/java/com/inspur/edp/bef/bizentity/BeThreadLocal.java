package com.inspur.edp.bef.bizentity;

public class BeThreadLocal {
    public static final ThreadLocal beThreadLocal = new ThreadLocal();

    public static void set(Context user) {
        beThreadLocal.set(user);
    }

    public static void unset() {
        beThreadLocal.remove();
    }

    public static Context get() {
        return (Context) beThreadLocal.get();
    }
}
