package com.inspur.edp.bef.bizentity.json.operation;

import com.fasterxml.jackson.core.JsonGenerator;
import com.inspur.edp.bef.bizentity.common.BizEntityJsonConst;
import com.inspur.edp.bef.bizentity.operation.BizActionBase;
import com.inspur.edp.bef.bizentity.operation.BizMgrAction;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;

public class BizMgrActionSerializer extends BizActionBaseSerializer {

    public BizMgrActionSerializer() {
    }

    public BizMgrActionSerializer(boolean full) {
        super(full);
        isFull = full;
    }

    @Override
    protected BizParameterSerializer getParaConvertor() {
        return new BizParameterSerializer(isFull);
    }

    @Override
    protected void writeExtendActionBaseProperty(JsonGenerator writer, BizActionBase action) {
        if (isFull) {
            SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.FuncOperationID, ((BizMgrAction) action).getFuncOperationID());
            SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.FuncOperationName, ((BizMgrAction) action).getFuncOperationName());
        }
    }

    @Override
    protected void WriteExtendActionSelfProperty(JsonGenerator writer, BizActionBase action) {

    }
}
