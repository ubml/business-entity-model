package com.inspur.edp.bef.bizentity.controlrule.ruledefinition.parser;

import com.fasterxml.jackson.databind.JsonDeserializer;
import com.inspur.edp.bef.bizentity.controlrule.ruledefinition.entity.BeEntityControlRuleDef;
import com.inspur.edp.das.commonmodel.controlruledef.entity.CmEntityControlRuleDef;
import com.inspur.edp.das.commonmodel.controlruledef.parser.CmEntityControlRuleDefParser;

public class BeEntityControlRuleDefParser extends CmEntityControlRuleDefParser<BeEntityControlRuleDef> {
    @Override
    protected final CmEntityControlRuleDef createCmEntityRuleDef() {
        return new BeEntityControlRuleDef(null);
    }

    protected JsonDeserializer getChildDeserializer(String childTypeName) {
        return new BeFieldControlRuleDefParser();
    }

}
