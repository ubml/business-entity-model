package com.inspur.edp.bef.bizentity.increment.extract;

import com.inspur.edp.das.commonmodel.entity.object.increment.extract.CommonObjectIncrementExtractor;
import com.inspur.edp.das.commonmodel.entity.object.increment.extract.ModifyCommonObjectExtractor;

public class BizObjectIncrementExtractor extends CommonObjectIncrementExtractor {

    public BizObjectIncrementExtractor() {
        super();
    }

    public BizObjectIncrementExtractor(boolean includeAll) {
        super(includeAll);
    }


    protected ModifyCommonObjectExtractor getModifyCommonObjectExtractor() {
        return new ModifyBizObjectExtractor(this.includeAll);
    }

}
