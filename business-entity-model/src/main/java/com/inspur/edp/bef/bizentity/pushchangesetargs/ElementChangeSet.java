package com.inspur.edp.bef.bizentity.pushchangesetargs;

public class ElementChangeSet {
    private ElementChangeDetail changeDetail;
    private ChangeType type;

    public ElementChangeSet() {
    }

    public ElementChangeSet(ElementChangeDetail changeDetail, ChangeType type) {
        this.changeDetail = changeDetail;
        this.type = type;
    }

    public ElementChangeDetail getChangeDetail() {
        return changeDetail;
    }

    public void setChangeDetail(ElementChangeDetail changeDetail) {
        this.changeDetail = changeDetail;
    }

    public ChangeType getType() {
        return type;
    }

    public void setType(ChangeType type) {
        this.type = type;
    }
}
