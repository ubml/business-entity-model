package com.inspur.edp.bef.bizentity.pushchangesetargs;


public class ActionChangeSet {
    private ActionChangeDetail changeDetail;
    private ChangeType changeType;

    public ActionChangeSet(ActionChangeDetail changeDetail, ChangeType changeType) {
        this.changeDetail = changeDetail;
        this.changeType = changeType;
    }

    public ActionChangeSet() {
    }

    public ActionChangeDetail getChangeDetail() {
        return changeDetail;
    }

    public void setChangeDetail(ActionChangeDetail changeDetail) {
        this.changeDetail = changeDetail;
    }

    public ChangeType getChangeType() {
        return changeType;
    }

    public void setChangeType(ChangeType changeType) {
        this.changeType = changeType;
    }


}
