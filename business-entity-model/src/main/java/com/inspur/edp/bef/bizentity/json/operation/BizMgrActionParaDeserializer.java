package com.inspur.edp.bef.bizentity.json.operation;

import com.inspur.edp.bef.bizentity.operation.bemgrcomponent.BizMgrActionParameter;
import com.inspur.edp.bef.bizentity.operation.componentbase.BizParameter;

public class BizMgrActionParaDeserializer extends BizParaDeserializer {
    @Override
    protected BizParameter createBizPara() {
        return new BizMgrActionParameter();
    }
}
