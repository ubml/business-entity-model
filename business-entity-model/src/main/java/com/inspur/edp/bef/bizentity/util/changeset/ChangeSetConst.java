package com.inspur.edp.bef.bizentity.util.changeset;

public class ChangeSetConst {
    public static final String metadataInfo = "metadataInfo";
    public static final String relatedMetadatas = "relatedMetadatas";
    public static final String actionChangeSet = "actionChangeSets";
    public static final String changeDetail = "changeDetail";
    public static final String changeType = "changeType";
    public static final String isNecessary = "isNecessary";
    public static final String changeInfo = "changeInfo";

    //action
    public static final String actionId = "actionId";
    public static final String actionCode = "actionCode";
    public static final String mgrAction = "mgrAction";
    public static final String parameterId = "parameterId";
    public static final String parameterCode = "parameterCode";

    // object
    public static final String objectChangeSets = "objectChangeSets";
    public static final String bizObjectId = "bizObjectId";
    public static final String bizObjectCode = "bizObjectCode";
    public static final String bizObject = "bizObject";
    public static final String parentObjIDElementId = "parentObjIDElementId";

    // element
    public static final String elementChangeSets = "elementChangeSets";
    public static final String bizElementId = "bizElementId";
    public static final String bizElementCode = "bizElementCode";
    public static final String bizElement = "bizElement";


}
