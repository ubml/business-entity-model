package com.inspur.edp.bef.bizentity.json.increment;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.bef.bizentity.common.BizEntityJsonConst;
import com.inspur.edp.bef.bizentity.exceptions.BusinessEntityErrorCodeEnum;
import com.inspur.edp.bef.bizentity.exceptions.BusinessEntityException;
import com.inspur.edp.bef.bizentity.increment.entity.validation.AddedValIncrement;
import com.inspur.edp.bef.bizentity.increment.entity.validation.ValIncrement;
import com.inspur.edp.bef.bizentity.json.operation.BizValidationSerializer;
import com.inspur.edp.bef.bizentity.operation.Validation;
import com.inspur.edp.cef.designtime.api.json.CefNames;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;

import java.io.IOException;

public class ValIncrementSerializer extends JsonSerializer<ValIncrement> {

    @Override
    public void serialize(ValIncrement value, JsonGenerator gen, SerializerProvider serializerProvider) throws IOException {
        SerializerUtils.writeStartObject(gen);
        writeBaseInfo(value, gen);
        SerializerUtils.writeEndObject(gen);
    }

    private void writeBaseInfo(ValIncrement value, JsonGenerator gen) {
        SerializerUtils.writePropertyValue(gen, CefNames.IncrementType, value.getIncrementType().toString());
        switch (value.getIncrementType()) {
            case Added:
                writeAddedIncrement((AddedValIncrement) value, gen);
                break;
            case Modify:
//                writeModifyIncrement((ModifyMgrActionIncrement) value, gen);
                break;
            case Deleted:
//                writeDeletedIncrement((DeletedEntityIncrement) value, gen);
                break;
            default:
                throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_ENUM_NOTSUPPORT_0004, value.getIncrementType().toString());
        }
    }

    private void writeAddedIncrement(AddedValIncrement increment, JsonGenerator gen) {
        SerializerUtils.writePropertyName(gen, BizEntityJsonConst.AddedAction);
        writeBaseActionInfo(increment.getAction(), gen);
    }

    private void writeBaseActionInfo(Validation action, JsonGenerator gen) {
        BizValidationSerializer convertor = new BizValidationSerializer();
        try {
            convertor.serialize(action, gen, null);
        } catch (Exception e) {
            throw BusinessEntityException.createException(BusinessEntityErrorCodeEnum.GSP_BEMODEL_JSON_0004, e, "Validation", action.getCode());
        }
    }
}
