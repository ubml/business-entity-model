package com.inspur.edp.bef.bizentity.operation;

import com.inspur.edp.bef.bizentity.operation.componentbase.BizReturnValue;
import com.inspur.edp.bef.bizentity.operation.componentinterface.IBizParameterCollection;

/**
 * BE基本操作
 */
public abstract class BizActionBase extends BizOperation implements Cloneable {

    // region 属性
    private java.util.ArrayList<String> opIdList = new java.util.ArrayList<String>();
    // private AuthType authType = AuthType.TransAuth;

    // /**
    // 权限类型

    // */
    // public AuthType getCurentAuthType(){}
    // void setCurentAuthType(AuthType value)

    /**
     * 参数列表
     */
    private IBizParameterCollection parameterCollection;

    public final void setParameters(IBizParameterCollection value) {
        parameterCollection = value;
    }

    public final IBizParameterCollection getParameters() {
        if (parameterCollection == null) {
            parameterCollection = this.getActionParameters();
        }
        return parameterCollection;
    }

    /**
     * 返回值
     */
    private BizReturnValue privateReturnValue;

    public final BizReturnValue getReturnValue() {
        return privateReturnValue;
    }

    public final void setReturnValue(BizReturnValue value) {
        privateReturnValue = value;
    }

    // endregion

    // region 方法

    /**
     * 业务操作ID列表（用于控制动作权限）
     */
    public void setOpIdList(java.util.ArrayList<String> value) {
        opIdList = value;
    }

    public java.util.ArrayList<String> getOpIdList() {
        return (opIdList != null) ? opIdList : (opIdList = new java.util.ArrayList<String>());
    }

    protected abstract IBizParameterCollection getActionParameters();

    public final void mergeWithDependentAction(BizActionBase dependentAction) {
        if (getIsRef() || dependentAction == null) {
            return;
        }
        mergeOperationBaseInfo(dependentAction);

        // setCurentAuthType(dependentAction.getCurentAuthType());
        getOpIdList().addAll(dependentAction.getOpIdList());
        for (int i = 0; i < dependentAction.getActionParameters().getCount(); i++) {
            getActionParameters().add(dependentAction.getActionParameters().getItem(i));
        }
    }

    // endregion
}