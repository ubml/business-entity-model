package com.inspur.edp.bef.bizentity.dtconsistencycheck.fieldeventargs;

public class ChangingFieldDataTypeEventArgs extends AbstractBeFieldEventArgs {

    protected String newValue;
    protected String originalValue;

    public ChangingFieldDataTypeEventArgs() {

    }


    public String getNewValue() {
        return newValue;
    }

    public void setNewValue(String newValue) {
        this.newValue = newValue;
    }

    public String getOriginalValue() {
        return originalValue;
    }

    public void setOriginalValue(String originalValue) {
        this.originalValue = originalValue;
    }
}
