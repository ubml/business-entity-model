package com.inspur.edp.bef.bizentity.operation.internalmgraction;

import com.inspur.edp.bef.bizentity.operation.BizMgrAction;
import com.inspur.edp.das.commonmodel.util.MessageI18nUtils;

/**
 * 内置RetrieveDefault操作
 */
public class RetrieveDefaultMgrAction extends BizMgrAction implements IInternalMgrAction {
    public static final String id = "Tji_dbrLtEKQlOLRpOJj9Q";
    public static final String code = "RetrieveDefault";
    public static final String name = MessageI18nUtils.getMessage("RetrieveDefault");

    public RetrieveDefaultMgrAction() {
        setID(id);
        setCode(code);
        setName(name);
    }
}