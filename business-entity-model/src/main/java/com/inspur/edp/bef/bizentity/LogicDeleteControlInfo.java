package com.inspur.edp.bef.bizentity;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LogicDeleteControlInfo {
    @JsonProperty("EnableLogicDelete")
    private boolean enableLogicDelete = false;
    @JsonProperty("LogicDeleteControlElementId")
    private String logicDeleteControlElementId;

    public boolean getEnableLogicDelete() {
        return enableLogicDelete;
    }

    public void setEnableLogicDelete(boolean enableLogicDelete) {
        this.enableLogicDelete = enableLogicDelete;
    }

    public String getLogicDeleteControlElementId() {
        return logicDeleteControlElementId;
    }

    public void setLogicDeleteControlElementId(String logicDeleteControlElementId) {
        this.logicDeleteControlElementId = logicDeleteControlElementId;
    }

}
