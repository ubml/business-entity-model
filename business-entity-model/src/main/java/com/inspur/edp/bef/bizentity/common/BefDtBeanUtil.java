package com.inspur.edp.bef.bizentity.common;

import com.inspur.edp.bef.bizentity.gspbusinessentity.api.IGspBeExtendInfoService;
import com.inspur.edp.cef.designtime.api.util.CefDtBeanUtil;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

public class BefDtBeanUtil extends CefDtBeanUtil {
    private static volatile IGspBeExtendInfoService beExtendInfoService;

    public static IGspBeExtendInfoService getBEExtendInfoService() {
        if (beExtendInfoService == null) {
            beExtendInfoService = SpringBeanUtils.getBean(IGspBeExtendInfoService.class);
        }
        return beExtendInfoService;
    }
}
