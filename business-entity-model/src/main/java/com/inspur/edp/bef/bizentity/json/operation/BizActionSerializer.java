package com.inspur.edp.bef.bizentity.json.operation;

import com.fasterxml.jackson.core.JsonGenerator;
import com.inspur.edp.bef.bizentity.operation.BizActionBase;

public class BizActionSerializer extends BizActionBaseSerializer {

    public BizActionSerializer() {
    }

    public BizActionSerializer(boolean full) {
        super(full);
        isFull = full;
    }

    @Override
    protected BizParameterSerializer getParaConvertor() {
        return new BizParameterSerializer(isFull);
    }

    @Override
    protected void writeExtendActionBaseProperty(JsonGenerator writer, BizActionBase action) {

    }

    @Override
    protected void WriteExtendActionSelfProperty(JsonGenerator writer, BizActionBase action) {

    }
}
