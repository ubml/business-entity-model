package com.inspur.edp.bef.bizentity.operation.componentbase;

import com.inspur.edp.bef.bizentity.operation.componentenum.BizCollectionParameterType;
import com.inspur.edp.bef.bizentity.operation.componentenum.BizParameterMode;
import com.inspur.edp.bef.bizentity.operation.componentenum.BizParameterType;
import com.inspur.edp.bef.bizentity.operation.componentinterface.IBizParameter;

/**
 * 构件参数基类
 */
public abstract class BizParameter implements IBizParameter {
    private String privateID;

    public String getID() {
        return privateID;
    }

    public void setID(String value) {
        privateID = value;
    }

    /**
     * 参数名
     */
    private String privateParamCode;

    public String getParamCode() {
        return privateParamCode;
    }

    public void setParamCode(String value) {
        privateParamCode = value;
    }

    /**
     * 参数名
     */
    private String privateParamName;

    public String getParamName() {
        return privateParamName;
    }

    public void setParamName(String value) {
        privateParamName = value;
    }

    /**
     * 参数类型，与Assembly和ClassName关联
     */
    private BizParameterType privateParameterType = BizParameterType.forValue(0);

    public BizParameterType getParameterType() {
        return privateParameterType;
    }

    public void setParameterType(BizParameterType value) {
        privateParameterType = value;
    }

    /**
     * 参数类对应程序集类
     */
    private String privateAssembly;

    public String getAssembly() {
        return privateAssembly;
    }

    public void setAssembly(String value) {
        privateAssembly = value;
    }

    /**
     * 参数类名
     */
    private String privateClassName;

    public String getClassName() {
        return privateClassName;
    }

    public void setClassName(String value) {
        privateClassName = value;
    }

    private String netClassName;

    public String getNetClassName() {
        return netClassName;
    }

    public void setNetClassName(String value) {
        netClassName = value;
    }

    /**
     * 参数模式
     */
    private BizParameterMode privateMode = BizParameterMode.forValue(0);

    public BizParameterMode getMode() {
        return privateMode;
    }

    public void setMode(BizParameterMode value) {
        privateMode = value;
    }

    /**
     * 描述
     */
    private String privateParamDescription;

    public String getParamDescription() {
        return privateParamDescription;
    }

    public void setParamDescription(String value) {
        privateParamDescription = value;
    }

    private BizCollectionParameterType privateCollectionParameterType = BizCollectionParameterType.forValue(0);

    public final BizCollectionParameterType getCollectionParameterType() {
        return privateCollectionParameterType;
    }

    public final void setCollectionParameterType(BizCollectionParameterType value) {
        privateCollectionParameterType = value;
    }

    private BizParActualValue bizParActualValue;

    @Override
    public BizParActualValue getActualValue() {
        return bizParActualValue;
    }

    @Override
    public void setActualValue(BizParActualValue value) {
        this.bizParActualValue = value;
    }

    /**
     * 新版动作返回值是为空的判断
     */
    private boolean isVoidReturnType;

    public boolean isVoidReturnType() {
        return isVoidReturnType;
    }

    public void setVoidReturnType(boolean voidReturnType) {
        isVoidReturnType = voidReturnType;
    }


}