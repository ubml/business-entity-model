package com.inspur.edp.bef.bizentity.json.operation;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.deser.std.StringDeserializer;
import com.inspur.edp.bef.bizentity.beenum.BEDeterminationType;
import com.inspur.edp.bef.bizentity.collection.DtmElementCollection;
import com.inspur.edp.bef.bizentity.common.BizEntityJsonConst;
import com.inspur.edp.bef.bizentity.operation.BizOperation;
import com.inspur.edp.bef.bizentity.operation.Determination;
import com.inspur.edp.bef.bizentity.operation.componentbase.BizParameter;
import com.inspur.edp.bef.bizentity.operation.componentbase.BizParameterCollection;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;

import java.util.HashMap;
import java.util.LinkedHashMap;

public class BizDeterminationDeserializer extends BizOperationDeserializer<Determination> {
    @Override
    protected Determination createBizOp() {
        return new Determination();
    }

    @Override
    protected void beforeDeserializeBizoperation(BizOperation op) {
        Determination dtm = (Determination) op;
        dtm.setDeterminationType(BEDeterminationType.Transient);
        dtm.setRequestElements(new DtmElementCollection());
        dtm.setRequestChildElements(new LinkedHashMap<>());
    }

    @Override
    protected boolean readExtendOpProperty(BizOperation op, String propName, JsonParser jsonParser) {
        Determination dtm = (Determination) op;
        boolean hasProperty = true;
        switch (propName) {
            case BizEntityJsonConst.DeterminationType:
                dtm.setDeterminationType(SerializerUtils.readPropertyValue_Enum(jsonParser, BEDeterminationType.class, BEDeterminationType.values()));
                break;
            case BizEntityJsonConst.TriggerTimePointType:
                dtm.setTriggerTimePointType(readBETriggerTimePointType(jsonParser));
                break;
            case BizEntityJsonConst.RequestNodeTriggerType:
                dtm.setRequestNodeTriggerType(readRequestNodeTriggerType(jsonParser));
                break;
            case BizEntityJsonConst.RequestElements:
                dtm.setRequestElements(readElementArray(jsonParser));
                break;
            case BizEntityJsonConst.RequestChildElements:
                dtm.setRequestChildElements(readRequestChildElements(jsonParser));
                break;
            case BizEntityJsonConst.PreDtmId:
                break;
            case BizEntityJsonConst.Parameters:
                readParameters(jsonParser, (Determination) op);
                break;
            case BizEntityJsonConst.RunOnce:
                dtm.setRunOnce(SerializerUtils.readPropertyValue_boolean(jsonParser));
                break;
            default:
                hasProperty = false;
        }
        return hasProperty;
    }

    private void readParameters(JsonParser jsonParser, Determination action) {
        BizParaDeserializer paraDeserializer = new BizActionParaDeserializer();
        BizParameterCollection<BizParameter> collection = new BizParameterCollection<>();
        SerializerUtils.readArray(jsonParser, paraDeserializer, collection);
        action.setParameterCollection(collection);
    }

    private HashMap<String, DtmElementCollection> readRequestChildElements(JsonParser jsonParser) {
        RequestChildElementsDeserializer deserializer = new DtmRequestChildElementsDeserializer();
        return deserializer.deserialize(jsonParser, null);
    }

    private DtmElementCollection readElementArray(JsonParser jsonParser) {
        DtmElementCollection collection = new DtmElementCollection();
        SerializerUtils.readArray(jsonParser, new StringDeserializer(), collection, true);
        return collection;
    }
}
