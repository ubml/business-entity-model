package com.inspur.edp.bef.bizentity.controlrule.rule.parser;

import com.fasterxml.jackson.databind.JsonDeserializer;
import com.inspur.edp.bef.bizentity.controlrule.rule.BeControlRule;
import com.inspur.edp.das.commonmodel.controlrule.CmControlRule;
import com.inspur.edp.das.commonmodel.controlrule.parser.CmControlRuleParser;

public class BeControlRuleParser extends CmControlRuleParser {

    protected JsonDeserializer getChildDeserializer(String childTypeName) {
        return new BeObjectControlRuleParser();
    }

    @Override
    protected CmControlRule createCmRule() {
        return new BeControlRule();
    }
}
