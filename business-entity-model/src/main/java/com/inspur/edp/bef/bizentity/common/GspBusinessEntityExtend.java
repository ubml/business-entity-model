package com.inspur.edp.bef.bizentity.common;

import com.inspur.edp.bef.bizentity.GspBizEntityObject;
import com.inspur.edp.bef.bizentity.GspBusinessEntity;
import com.inspur.edp.bef.bizentity.operation.BizAction;
import com.inspur.edp.bef.bizentity.operation.BizMgrAction;
import com.inspur.edp.bef.bizentity.operation.BizOperation;
import com.inspur.edp.bef.bizentity.operation.internalbeaction.IInternalBEAction;
import com.inspur.edp.bef.bizentity.operation.internalmgraction.IInternalMgrAction;
import com.inspur.edp.cef.designtime.api.util.DataValidator;

import java.util.ArrayList;
// import Inspur.Ecp.Caf.Common.*;

// import Inspur.Gsp.Das.CommonModel.*;

/**
 * be扩展方法
 */
public final class GspBusinessEntityExtend {
    /**
     * 获取不包含内置动作的Manager动作集合
     */
    public static ArrayList<BizMgrAction> getCustomMgrActions(GspBusinessEntity be) {
        DataValidator.checkForNullReference(be, "be");
        ArrayList<BizMgrAction> result = new ArrayList<BizMgrAction>();
        for (BizOperation op : be.getBizMgrActions()) {
            BizMgrAction action = (BizMgrAction) op;
            if (action instanceof IInternalMgrAction) {
                continue;
            }
            result.add(action);
        }
        return result;
    }

    /**
     * 获取不包含内置动作的BE动作集合
     */
    public static Iterable<BizAction> getCustomBEActions(GspBizEntityObject obj) {
        DataValidator.checkForNullReference(obj, "obj");
        ArrayList<BizAction> result = new ArrayList<BizAction>();
        for (BizOperation op : obj.getBizActions()) {
            BizAction action = (BizAction) op;
            if (action instanceof IInternalBEAction) {
                continue;
            }
            result.add(action);
        }
        return result;
    }
}