package com.inspur.edp.bef.bizentity.json.operation;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.inspur.edp.bef.bizentity.operation.collection.TccSettingCollection;
import com.inspur.edp.bef.bizentity.operation.internalmgraction.TccSettingElement;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;

import static com.fasterxml.jackson.core.JsonToken.END_ARRAY;

public class TccSettingCollectionDeserializer extends JsonDeserializer<TccSettingCollection> {

    @Override
    public TccSettingCollection deserialize(JsonParser parser,
                                            DeserializationContext deserializationContext) {
        TccSettingCollection collection = createCollection();
        if (SerializerUtils.readNullObject(parser)) {
            return collection;
        }
        SerializerUtils.readStartArray(parser);
        JsonToken tokentype = parser.getCurrentToken();
        if (tokentype != END_ARRAY) {
            while (parser.getCurrentToken() == tokentype) {
                TccSettingElement ele = readTccSettingElement(parser);
                addTccSettingElement(collection, ele);
            }
        }
        SerializerUtils.readEndArray(parser);
        return collection;
    }

    protected TccSettingCollection createCollection() {
        return new TccSettingCollection();
    }

    protected TccSettingElement readTccSettingElement(JsonParser parser) {
        TccSettingElementDeserializer deserializer = createDeserializer();
        return deserializer.deserialize(parser, null);
    }

    protected void addTccSettingElement(TccSettingCollection collection, TccSettingElement element) {
        collection.add(element);
    }

    protected TccSettingElementDeserializer createDeserializer() {
        return new TccSettingElementDeserializer();
    }
}
