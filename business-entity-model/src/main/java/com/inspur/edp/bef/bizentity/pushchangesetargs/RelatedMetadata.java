package com.inspur.edp.bef.bizentity.pushchangesetargs;

public class RelatedMetadata {
    private String contentCode;
    private String nameSpace;
    private String contentName;
    private String contentType;

    private String displayName;

    public RelatedMetadata() {
    }

    public RelatedMetadata(String nameSpace, String contentCode, String contentName, String contentType) {
        this.nameSpace = nameSpace;
        this.contentCode = contentCode;
        this.contentName = contentName;
        this.contentType = contentType;
    }


    public String getNameSpace() {
        return nameSpace;
    }

    public String getContentCode() {
        return contentCode;
    }

    public String getContentName() {
        return contentName;
    }

    public String getContentType() {
        return contentType;
    }

    public String getDisplayName() {
        return displayName;
    }
}
