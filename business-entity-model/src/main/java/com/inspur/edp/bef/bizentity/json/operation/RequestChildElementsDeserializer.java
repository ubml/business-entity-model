package com.inspur.edp.bef.bizentity.json.operation;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.deser.std.StringDeserializer;
import com.inspur.edp.bef.bizentity.common.BizEntityJsonConst;
import com.inspur.edp.cef.designtime.api.collection.BaseList;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;

import java.util.HashMap;

import static com.fasterxml.jackson.core.JsonToken.END_ARRAY;
import static com.fasterxml.jackson.core.JsonToken.FIELD_NAME;

public abstract class RequestChildElementsDeserializer<T extends BaseList<String>> extends JsonDeserializer<HashMap<String, T>> {
    public String currentKey;

    @Override
    public HashMap<String, T> deserialize(JsonParser parser, DeserializationContext ctxt) {
        HashMap<String, T> hashMap = createHashMap();
        if (SerializerUtils.readNullObject(parser)) {
            return hashMap;
        }
        SerializerUtils.readStartArray(parser);
        JsonToken tokentype = parser.getCurrentToken();
        if (tokentype != END_ARRAY) {
            while (parser.getCurrentToken() == tokentype) {
                readHashMap(hashMap, parser);
            }
        }
        SerializerUtils.readEndArray(parser);

        return hashMap;
    }

    private void readHashMap(HashMap<String, T> hashMap, JsonParser parser) {
        SerializerUtils.readStartObject(parser);
        while (parser.getCurrentToken() == FIELD_NAME) {
            String propName = SerializerUtils.readPropertyName(parser);
            readPropertyValue(hashMap, propName, parser);
        }
        SerializerUtils.readEndObject(parser);
    }

    private void readPropertyValue(HashMap<String, T> hashMap, String propName, JsonParser jsonParser) {
        switch (propName) {
            case BizEntityJsonConst.RequestChildElementKey:
                currentKey = SerializerUtils.readPropertyValue_String(jsonParser);
                break;
            case BizEntityJsonConst.RequestChildElementValue:
                T collection = createValueType();
                SerializerUtils.readArray(jsonParser, new StringDeserializer(), collection, true);
                hashMap.put(currentKey, collection);
                break;
        }
    }

    abstract HashMap<String, T> createHashMap();

    abstract T createValueType();
}
