package com.inspur.edp.bef.bizentity.operation.collection;

import com.inspur.edp.bef.bizentity.GspBizEntityObject;
import com.inspur.edp.bef.bizentity.operation.internalmgraction.TccSettingElement;
import com.inspur.edp.cef.designtime.api.collection.BaseList;

public class TccSettingCollection extends BaseList<TccSettingElement> implements Cloneable {

    private static final long serialVersionUID = 1L;

    public TccSettingCollection() {
    }

    private GspBizEntityObject privateOwner;

    public final GspBizEntityObject getOwner() {
        return privateOwner;
    }

    public final void setOwner(GspBizEntityObject value) {
        privateOwner = value;
    }
}
