package com.inspur.edp.bef.bizentity.json.operation;

import com.fasterxml.jackson.core.JsonParser;
import com.inspur.edp.bef.bizentity.operation.BizAction;
import com.inspur.edp.bef.bizentity.operation.BizActionBase;
import com.inspur.edp.bef.bizentity.operation.BizOperation;
import com.inspur.edp.bef.bizentity.operation.beomponent.BizActionParamCollection;
import com.inspur.edp.bef.bizentity.operation.componentbase.BizParameterCollection;

public class BizActionDeserializer extends BizActionBaseDeserializer {
    @Override
    protected final boolean readExtendActionProperty(JsonParser jsonParser, BizOperation op, String propName) {
        return false;
    }

    @Override
    protected BizActionBase createBizActionBase() {
        return new BizAction();
    }

    @Override
    protected BizParaDeserializer createPrapConvertor() {
        return new BizActionParaDeserializer();
    }

    @Override
    protected BizParameterCollection createPrapCollection() {
        return new BizActionParamCollection();
    }
}
