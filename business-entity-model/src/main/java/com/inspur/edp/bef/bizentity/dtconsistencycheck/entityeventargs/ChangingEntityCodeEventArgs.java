package com.inspur.edp.bef.bizentity.dtconsistencycheck.entityeventargs;

public class ChangingEntityCodeEventArgs extends AbstractBeEntityArgs {

    protected String newValue;
    protected String originalValue;

    public ChangingEntityCodeEventArgs() {

    }


    public String getNewValue() {
        return newValue;
    }

    public String getOriginalValue() {
        return originalValue;
    }

    public void setNewValue(String newValue) {
        this.newValue = newValue;
    }

    public void setOriginalValue(String originalValue) {
        this.originalValue = originalValue;
    }
}
