package com.inspur.edp.bef.bizentity.controlrule.ruledefinition.entity;

public class BeEntityRuleDefNames {
    public static String BeEntityRuleObjectType = "GspBizObject";
    public static String AddUQConstraint = "AddUQConstraint";
    public static String ModifyUQConstraint = "ModifyUQConstraint";
    public static String AddDtermination = "AddDtermination";
    public static String ModifyDetermination = "ModifyDetermination";
    public static String AddValidation = "AddValidation";
    public static String ModifyValidation = "ModifyValidation";
}
