package com.inspur.edp.bef.bizentity.operation.componentinterface;

import com.inspur.edp.bef.bizentity.operation.componentbase.BizParActualValue;
import com.inspur.edp.bef.bizentity.operation.componentenum.BizCollectionParameterType;
import com.inspur.edp.bef.bizentity.operation.componentenum.BizParameterMode;
import com.inspur.edp.bef.bizentity.operation.componentenum.BizParameterType;

public interface IBizParameter {
    String getID();

    void setID(String value);

    String getParamCode();

    void setParamCode(String value);

    String getParamName();

    void setParamName(String value);

    BizParameterType getParameterType();

    void setParameterType(BizParameterType value);

    BizCollectionParameterType getCollectionParameterType();

    void setCollectionParameterType(BizCollectionParameterType value);

    String getAssembly();

    void setAssembly(String value);

    String getClassName();

    void setClassName(String value);

    BizParameterMode getMode();

    void setMode(BizParameterMode value);

    String getParamDescription();

    void setParamDescription(String value);

    /**
     * @return The Actual Value  Of The Parameter,It`s Not Required
     */
    BizParActualValue getActualValue();

    /**
     * @param value The Actual Value  Of The Parameter,It`s Not Required
     */
    void setActualValue(BizParActualValue value);
}