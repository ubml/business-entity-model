package com.inspur.edp.bef.bizentity.operation.internalmgraction;

import com.inspur.edp.bef.bizentity.operation.BizMgrAction;
import com.inspur.edp.das.commonmodel.util.MessageI18nUtils;

/**
 * 内置Retrieve操作
 */
public class RetrieveMgrAction extends BizMgrAction implements IInternalMgrAction {
    public static final String id = "fQbpcO7bIEGoRdTZZIyrag";
    public static final String code = "Retrieve";
    public static final String name = MessageI18nUtils.getMessage("Retrieve");

    public RetrieveMgrAction() {
        setID(id);
        setCode(code);
        setName(name);
    }
}