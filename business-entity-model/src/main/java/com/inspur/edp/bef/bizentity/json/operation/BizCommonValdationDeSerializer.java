package com.inspur.edp.bef.bizentity.json.operation;

import com.fasterxml.jackson.core.JsonParser;
import com.inspur.edp.bef.bizentity.common.BizEntityJsonConst;
import com.inspur.edp.bef.bizentity.operation.BizCommonValdation;
import com.inspur.edp.bef.bizentity.operation.componentbase.BizParameter;
import com.inspur.edp.bef.bizentity.operation.componentbase.BizParameterCollection;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.cef.designtime.api.json.operation.CommonValDeserializer;
import com.inspur.edp.cef.designtime.api.operation.CommonOperation;
import com.inspur.edp.cef.designtime.api.operation.CommonValidation;

public class BizCommonValdationDeSerializer extends CommonValDeserializer {

    protected CommonOperation CreateCommonOp() {
        return new BizCommonValdation();
    }

    @Override
    protected boolean readExtendValProp(CommonOperation op, String propName, JsonParser jsonParser) {
        boolean result = false;
        CommonValidation val = (CommonValidation) op;
        switch (propName) {
            case BizEntityJsonConst.Parameters:
                result = true;
                readParameters(jsonParser, (BizCommonValdation) val);
                break;
        }
        return result;
    }

    private void readParameters(JsonParser jsonParser, BizCommonValdation action) {
        BizParaDeserializer paraDeserializer = new BizActionParaDeserializer();
        BizParameterCollection<BizParameter> collection = new BizParameterCollection<>();
        SerializerUtils.readArray(jsonParser, paraDeserializer, collection);
        action.setParameterCollection(collection);
    }
}
