package com.inspur.edp.bef.bizentity.json.operation;

import com.inspur.edp.bef.bizentity.operation.collection.DeterminationCollection;

public class DtmCollectionDeserializer extends BizOperationCollectionDeserializer<DeterminationCollection> {
    @Override
    protected DeterminationCollection createCollection() {
        return new DeterminationCollection();
    }

    @Override
    protected BizOperationDeserializer createDeserializer() {
        return new BizDeterminationDeserializer();
    }
}
