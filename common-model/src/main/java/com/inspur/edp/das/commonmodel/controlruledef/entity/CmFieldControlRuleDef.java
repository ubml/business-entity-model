package com.inspur.edp.das.commonmodel.controlruledef.entity;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.entity.ControlRuleDefinition;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.cm.CommonFieldContrulRuleDef;
import com.inspur.edp.das.commonmodel.controlruledef.parser.CmFieldRuleDefParser;
import com.inspur.edp.das.commonmodel.controlruledef.serializer.CmFieldControlRuleDefSerializer;

@JsonSerialize(using = CmFieldControlRuleDefSerializer.class)
@JsonDeserialize(using = CmFieldRuleDefParser.class)
public class CmFieldControlRuleDef extends CommonFieldContrulRuleDef {
    public CmFieldControlRuleDef(ControlRuleDefinition parentRuleDefinition) {
        super(parentRuleDefinition, CmFieldRuleNames.CmFieldRuleObjectTypeName);
    }
}
