package com.inspur.edp.das.commonmodel.controlrule.parser;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.cm.parser.CommonFieldRuleParser;
import com.inspur.edp.das.commonmodel.controlrule.CmFieldControlRule;

public class CmFieldRuleParser<T extends CmFieldControlRule> extends CommonFieldRuleParser<T> {

    @Override
    protected final boolean readCommonFieldRuleExtendProperty(T controlRule, String propName, JsonParser jsonParser, DeserializationContext deserializationContext) {
        if (super.readCommonFieldRuleExtendProperty(controlRule, propName, jsonParser, deserializationContext))
            return true;
        return readCmFieldRuleExtendProperty(controlRule, propName, jsonParser, deserializationContext);
    }

    protected boolean readCmFieldRuleExtendProperty(T controlRule, String propName, JsonParser jsonParser, DeserializationContext deserializationContext) {
        return false;
    }

    @Override
    protected final CmFieldControlRule createCommonFieldRuleDefinition() {
        return createCmFieldRuleDefinition();
    }

    protected CmFieldControlRule createCmFieldRuleDefinition() {
        return new CmFieldControlRule();
    }
}
