package com.inspur.edp.das.commonmodel.entity.element;

/**
 * The Definition Of Element Code Generate Type
 *
 * @ClassName: GspBillCodeGenerateType
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public enum GspBillCodeGenerateType {
    /**
     * 未设置
     */
    none(0),
    /**
     * 系统生成
     */
    genersoft(1),
    /**
     * 手工生成
     */
    hand(2),
    /**
     * 自增
     */
    increment(3);

    private final int intValue;
    private static java.util.HashMap<Integer, GspBillCodeGenerateType> mappings;

    private synchronized static java.util.HashMap<Integer, GspBillCodeGenerateType> getMappings() {
        if (mappings == null) {
            mappings = new java.util.HashMap<Integer, GspBillCodeGenerateType>();
        }
        return mappings;
    }

    private GspBillCodeGenerateType(int value) {
        intValue = value;
        GspBillCodeGenerateType.getMappings().put(value, this);
    }

    public int getValue() {
        return intValue;
    }

    public static GspBillCodeGenerateType forValue(int value) {
        return getMappings().get(value);
    }
}