package com.inspur.edp.das.commonmodel.entity.element;

/**
 * The Definition Of Element Code Rule Generator Occasion
 *
 * @ClassName: GspBillCodeGenerateOccasion
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public enum GspBillCodeGenerateOccasion {
    /**
     * 系统定义
     */
    SystemProcess(0),

    /**
     * 创建时
     */
    CreatingTime(1),

    /**
     * 保存时
     */
    SavingTime(2),

    /**
     * 创建和保存时
     */
    BothCreatingAndSaving(3);

    private final int intValue;
    private static java.util.HashMap<Integer, GspBillCodeGenerateOccasion> mappings;

    private synchronized static java.util.HashMap<Integer, GspBillCodeGenerateOccasion> getMappings() {
        if (mappings == null) {
            mappings = new java.util.HashMap<Integer, GspBillCodeGenerateOccasion>();
        }
        return mappings;
    }

    private GspBillCodeGenerateOccasion(int value) {
        intValue = value;
        GspBillCodeGenerateOccasion.getMappings().put(value, this);
    }

    public int getValue() {
        return intValue;
    }

    public static GspBillCodeGenerateOccasion forValue(int value) {
        return getMappings().get(value);
    }
}