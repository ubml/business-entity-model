package com.inspur.edp.das.commonmodel.controlrule;

import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.basic.AbstractControlRule;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.basic.ControlRuleItem;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.basic.RangeControlRule;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.cm.CommonDataTypeRuleNames;
import com.inspur.edp.das.commonmodel.controlruledef.entity.CmRuleNames;

import java.util.HashMap;
import java.util.Map;

public class CmControlRule extends RangeControlRule {
    public CmControlRule() {
        createMainEntityControlRule();
    }

    public ControlRuleItem getNameControlRule() {
        return super.getControlRule(CmRuleNames.Name);
    }

    public void setNameControlRule(ControlRuleItem ruleItem) {
        super.setControlRule(CmRuleNames.Name, ruleItem);
    }

    public ControlRuleItem getCodeControlRule() {
        return super.getControlRule(CmRuleNames.Code);
    }

    public void setCodeControlRule(ControlRuleItem ruleItem) {
        super.setControlRule(CmRuleNames.Code, ruleItem);
    }

    protected CmEntityControlRule createMainEntityControlRule() {
        return new CmEntityControlRule();
    }

    public CmEntityControlRule getMainEntityControlRule() {
        if (!getChildRules().containsKey(CommonDataTypeRuleNames.MainObjectRule)) {
            return null;
        }
        Map<String, AbstractControlRule> rules = getChildRules().get(CommonDataTypeRuleNames.MainObjectRule);
        if (rules == null) {
            rules = new HashMap<>();
            getChildRules().put(CommonDataTypeRuleNames.MainObjectRule, rules);
        }
        return (CmEntityControlRule) rules.get(CommonDataTypeRuleNames.MainObjectRule);
    }

    public void setMainEntityControlRule(CmEntityControlRule mainObjRule) {
        if (!getChildRules().containsKey(CommonDataTypeRuleNames.MainObjectRule)) {
            getChildRules().put(CommonDataTypeRuleNames.MainObjectRule, new HashMap<>());
        }
        getChildRules().get(CommonDataTypeRuleNames.MainObjectRule).put(CommonDataTypeRuleNames.MainObjectRule, mainObjRule);
    }
}
