package com.inspur.edp.das.commonmodel.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.inspur.edp.cef.designtime.api.collection.GspAssociationCollection;
import com.inspur.edp.cef.designtime.api.element.GspAssociation;
import com.inspur.edp.cef.designtime.api.entity.GspCommonField;
import com.inspur.edp.das.commonmodel.IGspCommonElement;
import com.inspur.edp.das.commonmodel.IGspCommonObject;
import com.inspur.edp.das.commonmodel.entity.element.ElementCodeRuleConfig;
import com.inspur.edp.das.commonmodel.entity.element.GspCommonAssociation;

/**
 * The Definition Of Common Element
 *
 * @ClassName: GspCommonElement
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class GspCommonElement extends GspCommonField implements IGspCommonElement {
    public GspCommonElement() {
        billCodeConfig = new ElementCodeRuleConfig();
        setReadonly(false);
        setIsCustomItem(false);
    }

    // region 成员字段

    // 基础信息
    // private GspElementDataType mDataType = GspElementDataType.String;
    private transient ElementCodeRuleConfig billCodeConfig = new ElementCodeRuleConfig();
    @JsonIgnore
//	private transient GspAssociationCollection childAssociations = new GspAssociationCollection();
    // private GspEnumValueCollection enumValueCollection;
    private transient String columnid = "";
    // private GspCommonAssociation parentAssociation;
    // private IGspCommonObject belongObject;
    // private String refelementId = "";
    private transient String belongModelId = "";
    private transient boolean readOnly;

    // 维护信息
    // private String customExpression = "";
    private boolean isCustomItem;
    /**
     * 字段是否参与持久化
     */
    private boolean isPersistent = true;
    private String customizeRefObjectID = "";
    private String customizeRefElementID = "";

    private boolean IsEnableEncryption = false;

    // endregion 成员字段

    // region 公有属性

    /**
     * 编码规则配置
     */
    @Override
    public ElementCodeRuleConfig getBillCodeConfig() {
        return billCodeConfig;
    }

    public void setBillCodeConfig(ElementCodeRuleConfig value) {
        billCodeConfig = value;
    }

    /**
     * 对应的数据对象的列ID
     */
    public String getColumnID() {
        return columnid;
    }

    public void setColumnID(String value) {
        columnid = value;
    }

    /**
     * 是否自定义项
     */
    public boolean getIsCustomItem() {
        return isCustomItem;
    }

    public void setIsCustomItem(boolean value) {

    }

    /**
     * 当前属性所属结点
     */
    // [Newtonsoft.Json.jsonIgnore()]
    public IGspCommonObject getBelongObject() {
        return (IGspCommonObject) super.getBelongObject();
    }

    public void setBelongObject(IGspCommonObject value) {
        super.setBelongObject(value);
    }

    /**
     * 所属数据模型metadataId
     */
    public String getBelongModelID() {
        return belongModelId;
    }

    public void setBelongModelID(String value) {
        value = belongModelId;
    }

    public boolean getReadonly() {
        return readOnly;
    }

    public void setReadonly(boolean value) {
        readOnly = value;
    }

    // endregion

    // region 外部方法
    //
    // public bool hasNoneRefElementInAssociation()
    // {
    // if (HasAssociation == false)
    // return false;
    // foreach (var association in ChildAssociations)
    // {
    // foreach (var refElement in association.RefElementCollection)
    // {
    // if (refElement.IsRef == false)
    // return true;
    // }
    // }
    //
    // return false;
    // }
    //

    /**
     * 克隆
     *
     * @param absObj
     * @param association
     * @return
     */
    public final IGspCommonElement clone(IGspCommonObject absObj, GspCommonAssociation association) {
        Object tempVar = super.clone(absObj, association);
        GspCommonElement newObj = (GspCommonElement) ((tempVar instanceof GspCommonElement) ? tempVar : null);
        if (newObj == null) {
            return null;
        }
        newObj.setBelongObject(absObj);
        newObj.setParentAssociation(association);
        if (getChildAssociations() != null) {
            newObj.setChildAssociations(new GspAssociationCollection());
            for (GspAssociation item : getChildAssociations()) {
                newObj.getChildAssociations().add(item.clone(newObj));
            }
        }
        return newObj;
    }

    public final GspCommonAssociation getGspParentAssociation() {
        return (GspCommonAssociation) getParentAssociation();
    }

    public final void setGspParentAssociation(GspCommonAssociation value) {
        setParentAssociation(value);
    }

    @JsonIgnore
    public final String getAssociationTypeName() {
        if (getBelongObject().getParentObject() == null) {
            return getLabelID() + "Info";
        }
        return getBelongObject().getCode() + getLabelID() + "Info";
    }

    @Override
    @JsonIgnore
    public final String getEnumTypeName() {
        if (getBelongObject().getParentObject() == null) {
            return getLabelID() + "Enum";
        }
        return getBelongObject().getCode() + getLabelID() + "Enum";
    }
    // endregion

    /**
     * 字段是否参与持久化处理
     *
     * @return
     */
    public boolean getIsPersistent() {
        return isPersistent;
    }

    /**
     * 字段是否参与持久化处理
     *
     * @param value
     */
    public void setIsPersistent(boolean value) {
        isPersistent = value;
    }

    /**
     * 字段是否加密
     *
     * @return
     */
    public boolean getIsEnableEncryption() {
        return IsEnableEncryption;
    }

    public void setIsEnableEncryption(boolean value) {
        IsEnableEncryption = value;
    }

    @Override
    public String getCustomizeRefObjectID() {
        return this.customizeRefObjectID;
    }

    @Override
    public void setCustomizeRefObjectID(String value) {
        this.customizeRefObjectID = value;
    }

    @Override
    public String getCustomizeRefElementID() {
        return this.customizeRefElementID;
    }

    @Override
    public void setCustomizeRefElementID(String value) {
        this.customizeRefElementID = value;
    }
}
