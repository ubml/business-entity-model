package com.inspur.edp.das.commonmodel;

import com.inspur.edp.cef.designtime.api.IFieldCollection;

/**
 * The Interface  Of Element Collection
 *
 * @ClassName: IElementCollection
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public interface IElementCollection extends IFieldCollection {
    IGspCommonElement getItem(int index);
}