package com.inspur.edp.das.commonmodel.controlrule.parser;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.basic.parser.RangeRuleParser;
import com.inspur.edp.das.commonmodel.controlrule.CmControlRule;

public class CmControlRuleParser<T extends CmControlRule> extends RangeRuleParser<T> {
    @Override
    protected final boolean readRangeExtendControlRuleProperty(T controlRule, String propName, JsonParser jsonParser, DeserializationContext deserializationContext) {
        if (super.readRangeExtendControlRuleProperty(controlRule, propName, jsonParser, deserializationContext))
            return true;
        return readCmRuleExtendProperty(controlRule, propName, jsonParser, deserializationContext);
    }

    protected boolean readCmRuleExtendProperty(T ruleDefinition, String propName, JsonParser jsonParser, DeserializationContext deserializationContext) {
        return false;
    }

    protected CmControlRule createCmRule() {
        return new CmControlRule();
    }

    @Override
    protected T createControlRule() {
        return (T) createCmRule();
    }
}
