package com.inspur.edp.das.commonmodel;

import com.inspur.edp.cef.designtime.api.IGspCommonDataType;
import com.inspur.edp.cef.designtime.api.collection.GspAssociationKeyCollection;
import com.inspur.edp.cef.designtime.api.entity.MdRefInfo;
import com.inspur.edp.das.commonmodel.collection.GspUniqueConstraintCollection;
import com.inspur.edp.das.commonmodel.entity.object.GspColumnGenerate;
import com.inspur.edp.das.commonmodel.entity.object.GspCommonObjectType;

import java.util.ArrayList;
import java.util.List;

/**
 * The Interface  Of Common Object
 *
 * @ClassName: IGspCommonObject
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public interface IGspCommonObject extends IGspCommonDataType {


    /**
     * @return The Repository Components Of The Object
     */
    List<MdRefInfo> getRepositoryComps();

    /**
     * @param value The Repository Components Of The Object
     */
    void setRepositoryComps(List<MdRefInfo> value);

    /**
     * @return The Object Type Of The Object
     */
    GspCommonObjectType getObjectType();

    /**
     * @param value The Object Type Of The Object
     */
    void setObjectType(GspCommonObjectType value);

    /**
     * @return Reference Database Object Name Of The Object
     */
    String getRefObjectName();

    /**
     * @param value Reference Database Object Name Of The Object
     */
    void setRefObjectName(String value);

    /**
     * @return Id Column Generate Info Of The Object
     */
    GspColumnGenerate getColumnGenerateID();

    /**
     * @param value Id Column Generate Info Of The Object
     */
    void setColumnGenerateID(GspColumnGenerate value);

    /**
     * @return The Id Element Of The Object
     */
    IGspCommonElement getIDElement();

    /**
     * @return The Child Objects Of The Object
     */
    IObjectCollection getContainChildObjects();

    /**
     * @return The Unique Contraint Of The Object
     */
    GspUniqueConstraintCollection getContainConstraints();

    /**
     * @param value The Unique Contraint Of The Object
     */
    void setContainConstraints(GspUniqueConstraintCollection value);

    /**
     * @return The Parent Object Of The Object,It`s Useful When The Object Type Is ChildObject
     */
    IGspCommonObject getParentObject();

    /**
     * @param value The Parent Object Of The Object,It`s Useful When The Object Type Is ChildObject
     */
    void setParentObject(IGspCommonObject value);

    /**
     * @return The  Model Of The Object Belongs
     */
    IGspCommonModel getBelongModel();

    /**
     * @param value The  Model Of The Object Belongs
     */
    void setBelongModel(IGspCommonModel value);

    /**
     * @return The  Model Id Of The Object Belongs
     */
    String getBelongModelID();

    /**
     * @param value The  Model Id Of The Object Belongs
     */
    void setBelongModelID(String value);

    /**
     * @return The Order Condition Of The Object
     */
    String getOrderbyCondition();

    /**
     * @param value The Order Condition Of The Object
     */
    void setOrderbyCondition(String value);

    /**
     * @return The Filter Condition Of The Object
     */
    String getFilterCondition();

    /**
     * @param value The Filter Condition Of The Object
     */
    void setFilterCondition(String value);

    /**
     * @return Wether The Object Is Readonly ,It`s Useless Now
     */
    boolean getIsReadOnly();

    /**
     * @param value Wether The Object Is Readonly ,It`s Useless Now
     */
    void setIsReadOnly(boolean value);

    /**
     * @return Wether The Object Is Virtual
     */
    boolean getIsVirtual();

    /**
     * @param value Wether The Object Is Virtual
     */
    void setIsVirtual(boolean value);

    /**
     * @return The State Element Id Of The Object,It`s Useless Now
     */
    String getStateElementID();

    /**
     * @param value The State Element Id Of The Object,It`s Useless Now
     */
    void setStateElementID(String value);

    /**
     * @return The AssociationKeys Of The Object
     */
    GspAssociationKeyCollection getKeys();

    /**
     * @return The AssociationKeys Of The Object
     */
    IElementCollection getContainElements();

    /**
     * @return The Property Name Of The Object, It`s Used By The Parent Interface Property
     */
    String getPropertyName();

    /**
     * @param parentObject The Parent Object Of The New Object Cloned By This Object
     * @return
     */
    IGspCommonObject clone(IGspCommonObject parentObject);

    /**
     * @param containRef Wether Get Assocciation Ref Elements
     * @return All Elements In The Object
     */
    ArrayList<IGspCommonElement> getAllElementList(boolean containRef);

    /**
     * @param withChild Has None Ref Element
     * @return
     */
    boolean hasNoneRefElement(boolean withChild);

    /**
     * @param id The Element Id To Find
     * @return The Finded Element
     */
    IGspCommonElement findElement(String id);

    List<String> getExtendElementLabels();

}