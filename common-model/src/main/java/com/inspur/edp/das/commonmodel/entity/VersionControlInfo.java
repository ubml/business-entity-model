package com.inspur.edp.das.commonmodel.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The Definition Of Version Control  Info
 *
 * @ClassName: VersionControlInfo
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class VersionControlInfo {

    @JsonProperty("VersionControlElementId")
    public String getVersionControlElementId() {
        return privateVersionControlElementId;
    }

    public void setVersionControlElementId(String value) {
        this.privateVersionControlElementId = value;
    }

    private String privateVersionControlElementId;


}
