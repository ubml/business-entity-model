package com.inspur.edp.das.commonmodel.controlruledef.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.serializer.RangeRuleDefSerializer;
import com.inspur.edp.das.commonmodel.controlruledef.entity.CmControlRuleDef;

public class CmRuleDefSerializer<T extends CmControlRuleDef> extends RangeRuleDefSerializer<T> {
    @Override
    protected final void writeRangeExtendInfos(T controlRuleDefinition, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) {
        super.writeRangeExtendInfos(controlRuleDefinition, jsonGenerator, serializerProvider);
        writeCmRuleExtendInfos(controlRuleDefinition, jsonGenerator, serializerProvider);
    }

    protected void writeCmRuleExtendInfos(T controlRuleDefinition, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) {

    }
}
