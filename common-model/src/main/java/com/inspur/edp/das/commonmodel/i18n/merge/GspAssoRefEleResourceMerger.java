package com.inspur.edp.das.commonmodel.i18n.merge;

import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.designtime.api.i18n.context.ICefResourceMergeContext;
import com.inspur.edp.cef.designtime.api.i18n.merger.AssoRefFieldResourceMerger;

public abstract class GspAssoRefEleResourceMerger extends AssoRefFieldResourceMerger {
    protected GspAssoRefEleResourceMerger(IGspCommonField commonField, ICefResourceMergeContext context) {
        super(commonField, context);
    }

}
