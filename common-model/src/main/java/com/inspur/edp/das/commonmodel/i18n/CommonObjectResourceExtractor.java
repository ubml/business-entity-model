package com.inspur.edp.das.commonmodel.i18n;

import com.inspur.edp.cef.designtime.api.IGspCommonDataType;
import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.designtime.api.i18n.context.CefResourcePrefixInfo;
import com.inspur.edp.cef.designtime.api.i18n.context.ICefResourceExtractContext;
import com.inspur.edp.cef.designtime.api.i18n.extractor.CefFieldResourceExtractor;
import com.inspur.edp.cef.designtime.api.i18n.extractor.DataTypeResourceExtractor;
import com.inspur.edp.das.commonmodel.IGspCommonElement;
import com.inspur.edp.das.commonmodel.IGspCommonObject;
import com.inspur.edp.das.commonmodel.entity.object.GspUniqueConstraint;
import com.inspur.edp.das.commonmodel.i18n.names.CmResourceDescriptionNames;
import com.inspur.edp.das.commonmodel.i18n.names.CmResourceKeyNames;

public abstract class CommonObjectResourceExtractor extends DataTypeResourceExtractor {
    private final IGspCommonObject commonObj;

    /**
     * Co国际化项抽取器
     *
     * @param commonDataType
     * @param context
     * @param parentResourceInfo
     */
    protected CommonObjectResourceExtractor(IGspCommonObject commonDataType, ICefResourceExtractContext context,
                                            CefResourcePrefixInfo parentResourceInfo) {
        super(commonDataType, context, parentResourceInfo);
        this.commonObj = commonDataType;
    }

    // #region DataTypeResourceExtractor
    @Override
    protected final CefFieldResourceExtractor getCefFieldResourceExtractor(
            ICefResourceExtractContext context,
            CefResourcePrefixInfo objPrefixInfo,
            IGspCommonField field) {
        return getCommonEleResourceExtractor(context, objPrefixInfo,
                (IGspCommonElement) ((field instanceof IGspCommonElement) ? field : null));
    }

    @Override
    protected final void extractExtendProperties(IGspCommonDataType dataType) {
        // 子节点
        if (commonObj.getContainChildObjects() != null && !commonObj.getContainChildObjects().isEmpty()) {
            for (IGspCommonObject childObj : commonObj.getContainChildObjects()) {
                getObjectResourceExtractor(getContext(), getParentResourcePrefixInfo(), childObj).extract();
            }
        }
        // 唯一性约束
        extractUniqueConstraints(commonObj);
        // 扩展
        extractExtendObjProperties((IGspCommonObject) ((dataType instanceof IGspCommonObject) ? dataType : null));
    }
    // #endregion

    // #region 私有方法
    private void extractUniqueConstraints(IGspCommonObject obj) {
        if (obj.getContainConstraints() != null && !obj.getContainConstraints().isEmpty()) {
            for (GspUniqueConstraint con : obj.getContainConstraints()) {
                extractUniqueConstraint(con);
            }
        }
    }

    private void extractUniqueConstraint(GspUniqueConstraint item) {
        if (item.getConstraintMessage() == null || item.getConstraintMessage().isEmpty()) {
            return;
        }
        String keyPropName = item.getCode() + "." + CmResourceKeyNames.TipInfo;
        String value = item.getConstraintMessage();
        String descriptionPropName = "唯一性约束'" + item.getName() + "'的" + CmResourceDescriptionNames.TipInfo;
        addResourceInfo(keyPropName, value, descriptionPropName);

        String constraintKey = getCurrentResourcePrefixInfo().getResourceKeyPrefix() + "." + item.getCode();
        item.setI18nResourceInfoPrefix(constraintKey);
    }

    // #endregion

    protected abstract void extractExtendObjProperties(IGspCommonObject dataType);

    protected abstract CommonElementResourceExtractor getCommonEleResourceExtractor(
            ICefResourceExtractContext context,
            CefResourcePrefixInfo objPrefixInfo,
            IGspCommonElement field);

    protected abstract CommonObjectResourceExtractor getObjectResourceExtractor(
            ICefResourceExtractContext context,
            CefResourcePrefixInfo modelPrefixInfo,
            IGspCommonObject obj);

}