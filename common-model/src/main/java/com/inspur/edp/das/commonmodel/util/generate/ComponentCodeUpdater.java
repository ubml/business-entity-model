/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.das.commonmodel.util.generate;

import com.inspur.edp.das.commonmodel.util.MessageI18nUtils;
import com.inspur.edp.lcm.metadata.inner.api.IdeLogService;
import com.inspur.edp.lcm.metadata.inner.api.utils.IdeLogUtils;
import io.iec.edp.caf.boot.context.CAFContext;
import io.iec.edp.caf.commons.utils.CollectionUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.Assignment;
import org.eclipse.jdt.core.dom.CastExpression;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.ExpressionStatement;
import org.eclipse.jdt.core.dom.FieldAccess;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.SingleVariableDeclaration;
import org.eclipse.jdt.core.dom.SuperConstructorInvocation;
import org.eclipse.jdt.core.dom.ThisExpression;
import org.eclipse.jdt.core.dom.Type;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;
import org.eclipse.jdt.core.dom.rewrite.ASTRewrite;
import org.eclipse.jdt.core.dom.rewrite.ListRewrite;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.Document;
import org.eclipse.text.edits.TextEdit;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 通用构件代码更新方法类
 */
@Slf4j
public class ComponentCodeUpdater {
    /**
     * 获取单例
     */
    private static final ComponentCodeUpdater instance = new ComponentCodeUpdater();

    /**
     * 是否初始化, 主要用于标识ideLogService是否已尝试初始化
     */
    private static boolean isInit = false;

    /**
     * ide日志输出类
     */
    private static IdeLogService ideLogService;

    /**
     * 获取单例
     *
     * @return ComponentCodeUpdater单例
     */
    private static ComponentCodeUpdater getInstance() {
        return instance;
    }

    /**
     * 私有化构造方法
     */
    private ComponentCodeUpdater() {
    }

    private static void ideLog(String msgCode, Object... messageParams) {
        // 在未初始化时，尝试获取IdeLogService实现类
        if (!isInit) {
            try {
                ideLogService = IdeLogUtils.getIdeLogService();
            } catch (Throwable t) {
                log.warn("获取不到IdeLogService实现类", t);
            }
            isInit = true;
        }
        // 若未获取到ide log输出类,则不做处理
        if (ideLogService == null) {
            return;
        }
        // 尝试输出ide log, 若失败则记录日志
        try {
            String msg = MessageI18nUtils.getMessage(msgCode, messageParams);
            ideLogService.pushLog(msg, CAFContext.current.getUserId());
        } catch (Exception e) {
            log.warn("输出Ide Log失败", e);
        }
    }

    /**
     * 代码更新静态方法
     *
     * @param originalContent 原始代码
     * @param newContent      新构件代码
     * @param className       构件对应类名
     * @return 更新后的代码
     */
    public static String codeUpdate(String originalContent, String newContent, String className) {
        return codeUpdate(originalContent, newContent, className, null);
    }

    /**
     * 代码更新静态方法
     *
     * @param originalContent 原始代码
     * @param newContent      新构件代码
     * @param className       构件对应类名
     * @param prefix          输出idelog日志前缀
     * @return 更新后的代码
     */
    public static String codeUpdate(String originalContent, String newContent, String className, String prefix) {
        try {
            // 若给定前缀, 则将其作为idelog输出的标题
            if (StringUtils.isNotBlank(prefix)) {
                ideLog("--------------------------------------------------------------------------------------------");
                ideLog(prefix);
            }
            return getInstance().update(originalContent, newContent, className);
        } catch (Throwable e) {
            ideLog("GSP_COMPONENT_JAVA_UPDATE_0001", e.getMessage());
            log.error("ComponentCodeUpdater.codeUpdate error, prefix:[{}]", prefix, e);
            return null;
        }
    }

    /**
     * 代码更新
     *
     * @param originalContent 原始代码
     * @param newContent      新构件代码
     * @param className       构件对应类名
     * @return 更新后的代码
     */
    public String update(String originalContent, String newContent, String className) {
        // 构造AST读取类
        ASTParser parser = ASTParser.newParser(AST.JLS3);
        parser.setResolveBindings(true);
        parser.setStatementsRecovery(true);
        parser.setBindingsRecovery(true);
        parser.setKind(ASTParser.K_COMPILATION_UNIT);

        // 读取当前文件内容, 构造CompilationUnit
        parser.setSource(originalContent.toCharArray());
        CompilationUnit currentCompilationUnit;
        try {
            currentCompilationUnit = (CompilationUnit) parser.createAST(null);
        } catch (Exception e) {
            ideLog("GSP_COMPONENT_JAVA_UPDATE_0002", className);
            log.error("解析类[{}]代码文件失败", className, e);
            return null;
        }
        // 根据重新生成的代码模板,构造CompilationUnit
        parser.setSource(newContent.toCharArray());
        CompilationUnit targetCompilationUnit = (CompilationUnit) parser.createAST(null);

        // 获取当前文件的类定义
        TypeDeclaration currentTypeDeclaration = (TypeDeclaration) currentCompilationUnit.types().stream()
                .filter(type -> type instanceof TypeDeclaration && className.equals(((TypeDeclaration) type).getName().toString()))
                .findFirst().orElse(null);
        // 当前代码不存在类定义, 则不做修改
        if (currentTypeDeclaration == null) {
            ideLog("GSP_COMPONENT_JAVA_UPDATE_0003", className);
            return null;
        }

        TypeDeclaration targetTypeDeclaration = (TypeDeclaration) targetCompilationUnit.types().get(0);

        ASTRewrite astRewrite = ASTRewrite.create(currentCompilationUnit.getAST());

        // 对比类定义的父类参数(对应构件返回类型)和构造方法(对应构件入参), 并记录对比结果是否发生变更
        boolean isNeedUpdate = updateSuperClassType(astRewrite, currentTypeDeclaration, targetTypeDeclaration);
        isNeedUpdate = updateConstructorMethod(astRewrite, currentTypeDeclaration, targetTypeDeclaration) || isNeedUpdate;

        // 若对比结果未发生变更,则不进行代码更新
        if (!isNeedUpdate) {
            return null;
        }
        // 更新import
        appendImport(astRewrite, currentCompilationUnit, targetCompilationUnit);

        // 使用astrewrite进行代码更新
        Document document = new Document(originalContent);
        TextEdit textEdit = astRewrite.rewriteAST(document, null);
        try {
            textEdit.apply(document);
            String result = document.get();
            ideLog("GSP_COMPONENT_JAVA_UPDATE_0008", className);
            return result;
        } catch (BadLocationException e) {
            ideLog("GSP_COMPONENT_JAVA_UPDATE_0004", className, e.getMessage());
            log.error("更新类[{}]代码文件失败", className, e);
        }
        return null;
    }


    /**
     * 检查当前父类是否需要修改，若需要则将其修改为与目标类一致
     * <p>
     * 当前类的父类与目标类父类不一致时,将其修改为与目标类一致并返回True.
     * 若不需要修改则返回False.
     * </p>
     *
     * @param astRewrite             AST编辑器
     * @param currentTypeDeclaration 当前类
     * @param targetTypeDeclaration  目标类
     * @return 是否发生修改, 修改后返回true
     */
    private boolean updateSuperClassType(ASTRewrite astRewrite, TypeDeclaration currentTypeDeclaration, TypeDeclaration targetTypeDeclaration) {
        Type currentSuperclassType = currentTypeDeclaration.getSuperclassType();
        Type targetSuperclassType = targetTypeDeclaration.getSuperclassType();

        // 当前类型与目标类型不同, 则认为不一致, 将父类节点整体替换
        if (!StringUtils.equals(currentSuperclassType.toString(), targetSuperclassType.toString())) {
            astRewrite.replace(currentSuperclassType, targetSuperclassType, null);
            return true;
        }
        ideLog("GSP_COMPONENT_JAVA_UPDATE_0009", currentTypeDeclaration.getName().toString());
        return false;
    }

    /**
     * 检查当前构造方法是否需要修改,若需要修改则将当前构造方法及类私有属性修改为与目标类一致
     * <ul>
     *     需要修改的场景需满足以下场景
     *     <li>当前仅有一个构造方法</li>
     *     <li>当前构造方法入参与目标构造方法入参不一致(数量\类型\编号)</li>
     *     <li>当前构造方法内容未被自定义修改</li>
     * </ul>
     * <ul>
     *     修改方法
     *     <li>将构造方法修改为与目标构造方法一致</li>
     *     <li>将原构造方法入参关联的私有属性移除</li>
     *     <li>重新添加目标构造方法相关的私有属性</li>
     * </ul>
     *
     * @param astRewrite             AST编辑
     * @param currentTypeDeclaration 当前类
     * @param targetTypeDeclaration  目标类
     * @return 构造方法是否进行修改, 修改后返回true
     */
    private boolean updateConstructorMethod(ASTRewrite astRewrite, TypeDeclaration currentTypeDeclaration, TypeDeclaration targetTypeDeclaration) {
        // 获取目标类构造方法
        MethodDeclaration targetConstructorMethod = Arrays.stream(targetTypeDeclaration.getMethods())
                .filter(MethodDeclaration::isConstructor).findFirst().orElse(null);

        // 获取当前类构造方法(可能为多个)
        List<MethodDeclaration> currentConstructorMethodList = Arrays.stream(currentTypeDeclaration.getMethods())
                .filter(MethodDeclaration::isConstructor).collect(Collectors.toList());

        // 不符合 仅存在一个构造方法 条件时,不进行修改
        if (currentConstructorMethodList.size() != 1) {
            ideLog("GSP_COMPONENT_JAVA_UPDATE_0005", currentTypeDeclaration.getName().toString());
            return false;
        }

        MethodDeclaration currentConstructorMethod = currentConstructorMethodList.get(0);
        // 检查构造方法参数与目标构造方法参数是否一致
        boolean isParamEqual = compareMethodParameters(currentConstructorMethod, targetConstructorMethod);
        // 若参数一致, 则不进行修改
        if (isParamEqual) {
            ideLog("GSP_COMPONENT_JAVA_UPDATE_0007", currentTypeDeclaration.getName().toString());
            return false;
        }

        // 检查构造方法内容是否被自定义修改
        boolean constructorMethodBodyChanged = checkConstructorMethodBodyChanged(currentConstructorMethod);
        // 若构造方法被自定义修改, 则不进行修改
        if (constructorMethodBodyChanged) {
            ideLog("GSP_COMPONENT_JAVA_UPDATE_0006", currentTypeDeclaration.getName().toString());
            return false;
        }
        // 获取当前构造方法参数相关属性列表 和 目标比当前多出的属性列表
        Set<String> fieldCollection = getConstructorRelatedFields(currentConstructorMethod, targetConstructorMethod);

        // 记录待删除的节点集合
        Set<ASTNode> astNodesToDelete = new HashSet<>();
        // 获取当前类的所有私有属性列表
        for (FieldDeclaration field : currentTypeDeclaration.getFields()) {
            // 获取属性定义语句中可能存在的 多属性定义: private String field1,field2;
            List<VariableDeclarationFragment> fragments = field.fragments();
            Set<ASTNode> fragmentsToDelete = new HashSet<>();
            // 标识 当前语句中 多属性是否均应被删除
            boolean isAllFragmentToDelete = true;
            // 遍历当前属性定义语句中的所有 属性
            for (VariableDeclarationFragment fragment : fragments) {
                // 若属性在 当前构造方法相关属性中 或 目标构造方法属性中, 则将当前属性节点记录
                if (fieldCollection.remove(fragment.getName().getIdentifier())) {
                    fragmentsToDelete.add(fragment);
                } else {
                    // 若当前属性定义语句中存在至少一个属性不应被删除, 则标识 当前属性定义语句不应被删除
                    isAllFragmentToDelete = false;
                }
            }
            // 若当前语句应被删除, 则将当前属性定义语句加入待删除节点中
            if (isAllFragmentToDelete) {
                astNodesToDelete.add(field);
            } else {
                // 若当前语句中至少有一个属性不应被删除, 则将当前语句中需要删除的属性节点加入待删除节点中
                astNodesToDelete.addAll(fragmentsToDelete);
            }
        }
        // 将原构造方法替换为目标构造方法
        astRewrite.replace(currentConstructorMethod, targetConstructorMethod, null);

        // 删除所有相关的属性定义语句或定义语句中的属性
        for (ASTNode field : astNodesToDelete) {
            astRewrite.remove(field, null);
        }

        // 倒叙遍历, 正序插入类内起始位置
        for (int i = targetTypeDeclaration.getFields().length - 1; i >= 0; i--) {
            FieldDeclaration field = targetTypeDeclaration.getFields()[i];
            ListRewrite listRewrite = astRewrite.getListRewrite(currentTypeDeclaration, TypeDeclaration.BODY_DECLARATIONS_PROPERTY);
            listRewrite.insertFirst(field, null);
        }

        return true;
    }

    /**
     * <p>获取当前构造方法以及目标构造方法入参相关属性</p>
     * <ul>
     *     <li>当前构造方法参数名称</li>
     *     <li>目标构造方法参数比当前构造方法参数多的部分</li>
     *     *注:在未自定义修改场景下此名称与私有属性名称一致
     * </ul>
     *
     * @param currentConstructorMethod 当前构造方法
     * @param targetConstructorMethod  目标构造方法
     * @return 构造方法相关属性
     */
    private Set<String> getConstructorRelatedFields(MethodDeclaration currentConstructorMethod, MethodDeclaration targetConstructorMethod) {
        // 获取当前构造方法参数名称
        Set<String> currentParamNames = ((List<Object>) currentConstructorMethod.parameters())
                .stream()
                .map(t -> ((SingleVariableDeclaration) t).getName().getIdentifier())
                .collect(Collectors.toSet());
        // 获取目标构造方法比当前构造方法参数多的部分
        Set<String> targetNewParamNames = ((List<Object>) targetConstructorMethod.parameters())
                .stream()
                .map(t -> ((SingleVariableDeclaration) t).getName().getIdentifier())
                .filter(name -> !currentParamNames.contains(name))
                .collect(Collectors.toSet());
        currentParamNames.addAll(targetNewParamNames);
        return currentParamNames;

    }

    /**
     * 检查构造方法内容是否被用户修改
     * <ul>
     *     <li>方法内容仅存在super语句和赋值语句</li>
     *     <li>方法内容最多只有一个super方法,若存在super语句则语句应为第一句</li>
     *     <li>构造方法super语句参数均应为方法入参,或方法入参的类型转换形式</li>
     *     <li>赋值语句格式为this.xxx = xxx, 赋值属性与构造方法入参名称一致</li>
     *     <li>构造方法入参在方法内容中均被super或赋值语句使用,且仅能被使用一次</li>
     * </ul>
     *
     * @param currentConstructorMethod 当前构造方法
     * @return 是否被用户修改, 若被修改则返回True
     */
    private boolean checkConstructorMethodBodyChanged(MethodDeclaration currentConstructorMethod) {
        // 获取构造方法内容语句
        List statements = currentConstructorMethod.getBody().statements();
        // 获取构造方法入参名称集合
        Set<String> paramNames = ((List<Object>) currentConstructorMethod.parameters())
                .stream()
                .map(t -> ((SingleVariableDeclaration) t).getName().getIdentifier())
                .collect(Collectors.toSet());

        // 复制入参,标识其与赋值语句是否一一对应
        Set<String> checkIsExistsSet = new HashSet<>(paramNames);

        // 若构造方法体为空
        if (CollectionUtils.isEmpty(statements)) {
            // 若构造方法无入参则认为未被用户修改, 若构造方法有入参 则认为被用户修改.
            return !checkIsExistsSet.isEmpty();
        }

        // 初始为遍历构造方法体中的第一个语句
        int start = 0;

        // 检查构造方法体中的super构造方法, 若构造方法有super语句,则从第二个语句开始遍历
        if (statements.get(0) instanceof SuperConstructorInvocation) {
            start = 1;
            SuperConstructorInvocation superStatement = (SuperConstructorInvocation) statements.get(0);
            // 遍历super语句参数, 检查:1.参数名称是否均在构造方法入参中,2.参数是否均为变量或类型转换形式. 任一不满足即认为被用户修改
            for (Object argument : superStatement.arguments()) {
                if (argument instanceof SimpleName) {
                    if (!checkIsExistsSet.remove(((SimpleName) argument).getIdentifier())) {
                        return true;
                    }
                } else if (argument instanceof CastExpression && ((CastExpression) argument).getExpression() instanceof SimpleName) {
                    if (!checkIsExistsSet.remove(((SimpleName) ((CastExpression) argument).getExpression()).getIdentifier())) {
                        return true;
                    }
                } else {
                    return true;
                }
            }
        }

        // 遍历其他语句
        for (int i = start; i < statements.size(); i++) {
            Object statement = statements.get(i);

            // 语句应为 this.xxx = xxx 格式, 且xxx 与入参一一对应,否则认为已被用户修改
            if (!(statement instanceof ExpressionStatement)) {
                return true;
            }
            ExpressionStatement expressionStatement = (ExpressionStatement) statement;
            if (!(expressionStatement.getExpression() instanceof Assignment)) {
                return true;
            }
            Assignment assignment = (Assignment) expressionStatement.getExpression();
            // 语句左侧应为 this
            if (!(assignment.getLeftHandSide() instanceof FieldAccess)
                    || !(((FieldAccess) assignment.getLeftHandSide()).getExpression() instanceof ThisExpression)) {
                return true;
            }

            if (!(assignment.getRightHandSide() instanceof SimpleName)) {
                return true;
            }

            // 左右两参数名称应一致.
            String fieldName = ((FieldAccess) assignment.getLeftHandSide()).getName().getIdentifier();
            String paramName = ((SimpleName) assignment.getRightHandSide()).getIdentifier();

            if (!StringUtils.equals(fieldName, paramName)
                    || !checkIsExistsSet.remove(paramName)) {
                return true;
            }
        }

        // 判断入参是否均被使用
        return !checkIsExistsSet.isEmpty();
    }

    /**
     * 对比两个构造方法的入参列表是否一致
     *
     * @param targetConstructorMethod  目标构造方法
     * @param currentMethodDeclaration 当前构造方法
     * @return 参数对比结果, 参数一致时为true
     */
    private boolean compareMethodParameters(MethodDeclaration targetConstructorMethod, MethodDeclaration currentMethodDeclaration) {
        // 两构造方法参数数量不一致,则认为不一致
        if (targetConstructorMethod.parameters().size() != currentMethodDeclaration.parameters().size()) {
            return false;
        }
        // 依次遍历方法参数, 若参数不一致,则认为不一致
        List<SingleVariableDeclaration> targetParameters = targetConstructorMethod.parameters();
        List<SingleVariableDeclaration> currentParameters = currentMethodDeclaration.parameters();
        for (int i = 0; i < targetParameters.size(); i++) {
            SingleVariableDeclaration targetParameter = targetParameters.get(i);
            SingleVariableDeclaration currentParameter = currentParameters.get(i);
            if (!targetParameter.toString().equals(currentParameter.toString())) {
                return false;
            }
        }
        return true;
    }

    /**
     * 对比并增加当前代码的import
     *
     * @param astRewrite             ASTRewrite
     * @param currentCompilationUnit 当前文件结构
     * @param targetCompilationUnit  目标文件结构
     */
    private void appendImport(ASTRewrite astRewrite, CompilationUnit currentCompilationUnit, CompilationUnit targetCompilationUnit) {
        // 收集当前代码的import
        Set<String> collect = ((List<Object>) currentCompilationUnit.imports())
                .stream()
                .map(importDeclaration -> ((ASTNode) importDeclaration).toString())
                .collect(Collectors.toSet());

        // 构造AST修改类
        ListRewrite listRewrite = astRewrite.getListRewrite(currentCompilationUnit, CompilationUnit.IMPORTS_PROPERTY);

        // 遍历目标代码的import,若当前代码的import中不包含目标代码的import,则将其添加到当前代码的import中
        ((List<Object>) targetCompilationUnit.imports())
                .stream()
                .map(importDeclaration -> (ASTNode) importDeclaration)
                .filter(importDeclaration -> !collect.contains(importDeclaration.toString()))
                .forEach(importDeclaration -> listRewrite.insertLast(importDeclaration, null));

    }
}
