package com.inspur.edp.das.commonmodel.entity.object;


/**
 * The Definition Of Constraint Type
 *
 * @ClassName: ConstraintType
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public enum ConstraintType {
    /**
     * 唯一约束
     */
    Unique(0);

    private final int intValue;
    private static java.util.HashMap<Integer, ConstraintType> mappings;

    private synchronized static java.util.HashMap<Integer, ConstraintType> getMappings() {
        if (mappings == null) {
            mappings = new java.util.HashMap<Integer, ConstraintType>();
        }
        return mappings;
    }

    private ConstraintType(int value) {
        intValue = value;
        ConstraintType.getMappings().put(value, this);
    }

    public int getValue() {
        return intValue;
    }

    public static ConstraintType forValue(int value) {
        return getMappings().get(value);
    }
}
