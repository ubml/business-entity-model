package com.inspur.edp.das.commonmodel.controlruledef.parser;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.cm.CommonFieldContrulRuleDef;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.cm.parser.CommonFieldRuleDefParser;
import com.inspur.edp.das.commonmodel.controlruledef.entity.CmFieldControlRuleDef;

public class CmFieldRuleDefParser<T extends CmFieldControlRuleDef> extends CommonFieldRuleDefParser<T> {

    @Override
    protected final boolean readCommonFieldRuleExtendProperty(T ruleDefinition, String propName, JsonParser jsonParser, DeserializationContext deserializationContext) {
        if (super.readCommonFieldRuleExtendProperty(ruleDefinition, propName, jsonParser, deserializationContext))
            return true;
        return readCmFieldRuleExtendProperty(ruleDefinition, propName, jsonParser, deserializationContext);
    }

    protected boolean readCmFieldRuleExtendProperty(T ruleDefinition, String propName, JsonParser jsonParser, DeserializationContext deserializationContext) {
        return false;
    }

    @Override
    protected final CommonFieldContrulRuleDef createCommonFieldRuleDefinition() {
        return createCmFieldRuleDefinition();
    }

    protected CmFieldControlRuleDef createCmFieldRuleDefinition() {
        return new CmFieldControlRuleDef(null);
    }
}
