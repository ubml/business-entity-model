package com.inspur.edp.das.commonmodel.entity.object;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.inspur.edp.das.commonmodel.json.CommonModelNames;

import java.util.ArrayList;

/**
 * The Definition Of Column Generate
 *
 * @ClassName: GspColumnGenerate
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class GspColumnGenerate implements Cloneable {
    private String elementID = "";
    private String generateType = "";
    private ArrayList<String> cloumnParameters = new ArrayList<>();

    /**
     * 创建一个规则
     */
    public GspColumnGenerate() {
    }

    /**
     * 元素ID，此属性应该是ElementID，而不是ColumnID
     */
    @JsonProperty(CommonModelNames.ElementID)
    public final String getElementID() {
        return this.elementID;
    }

    public final void setElementID(String value) {
        this.elementID = value;
    }

    /**
     * 生成类型
     */
    @JsonProperty(CommonModelNames.GenerateType)
    public final String getGenerateType() {
        return this.generateType;
    }

    public final void setGenerateType(String value) {
        this.generateType = value;
    }

    @JsonProperty(CommonModelNames.ColumnParameters)
    public final ArrayList<String> getCloumnParameter() {
        return cloumnParameters;
    }

    public final void setCloumnParameter(ArrayList<String> cloumnParameter) {
        this.cloumnParameters = cloumnParameter;
    }

    // region ICloneable Members

    /**
     * 克隆
     *
     * @return
     */
    public final GspColumnGenerate clone() {
        try {
            Object tempVar = super.clone();
            GspColumnGenerate newObj = (GspColumnGenerate) ((tempVar instanceof GspColumnGenerate) ? tempVar : null);
            return newObj;
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

    // endregion
}