package com.inspur.edp.das.commonmodel.controlruledef.parser;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.cm.CommonDataTypeControlRuleDef;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.cm.parser.CommonDataTypeRuleDefParser;
import com.inspur.edp.das.commonmodel.controlruledef.entity.CmEntityControlRuleDef;

public class CmEntityControlRuleDefParser<T extends CmEntityControlRuleDef> extends CommonDataTypeRuleDefParser<T> {

    @Override
    protected final boolean readCommonDataTypeRuleExtendProperty(T ruleDefinition, String propName, JsonParser jsonParser, DeserializationContext deserializationContext) {
        if (super.readCommonDataTypeRuleExtendProperty(ruleDefinition, propName, jsonParser, deserializationContext))
            return true;
        return readCmEntityRuleExtendProperty(ruleDefinition, propName, jsonParser, deserializationContext);
    }

    protected boolean readCmEntityRuleExtendProperty(T ruleDefinition, String propName, JsonParser jsonParser, DeserializationContext deserializationContext) {
        return false;
    }

    @Override
    protected final CommonDataTypeControlRuleDef createCommonDataTypeRuleDefinition() {
        return createCmEntityRuleDef();
    }

    protected CmEntityControlRuleDef createCmEntityRuleDef() {
        return new CmEntityControlRuleDef(null);
    }
}
