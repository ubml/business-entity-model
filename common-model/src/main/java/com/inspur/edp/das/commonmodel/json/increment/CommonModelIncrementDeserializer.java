package com.inspur.edp.das.commonmodel.json.increment;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.inspur.edp.cef.designtime.api.entity.increment.CommonEntityIncrement;
import com.inspur.edp.cef.designtime.api.entity.increment.ModifyEntityIncrement;
import com.inspur.edp.cef.designtime.api.increment.property.PropertyIncrement;
import com.inspur.edp.cef.designtime.api.increment.property.json.PropertyIncrementDeserializer;
import com.inspur.edp.cef.designtime.api.json.CefNames;
import com.inspur.edp.das.commonmodel.entity.increment.CommonModelIncrement;
import com.inspur.edp.das.commonmodel.exception.CommonModelErrorCodeEnum;
import com.inspur.edp.das.commonmodel.exception.CommonModelException;
import com.inspur.edp.das.commonmodel.json.CommonModelNames;

public abstract class CommonModelIncrementDeserializer extends JsonDeserializer<CommonModelIncrement> {
    @Override
    public CommonModelIncrement deserialize(JsonParser jsonParser, DeserializationContext ctxt) {

        CommonModelIncrement modelIncrement = createCommonModelIncrement();
        ObjectMapper mapper = new ObjectMapper();
        JsonNode node;
        try {
            node = mapper.readTree(jsonParser);
        } catch (Throwable e) {
            throw CommonModelException.createException(CommonModelErrorCodeEnum.GSP_BEMODEL_JSON_0001, e, "CommonModelIncrement");
        }

        readBaseInfo(modelIncrement, node);
        readExtendInfo(modelIncrement, node);

        return modelIncrement;
    }

    protected CommonModelIncrement createCommonModelIncrement() {
        return new CommonModelIncrement();
    }

    private void readBaseInfo(CommonModelIncrement value, JsonNode node) {
        JsonNode objNode = node.get(CommonModelNames.MainObject);
        if (objNode != null)
            readMainObjIncrement(value, objNode);

        JsonNode propIncrements = node.get(CefNames.PropertyIncrements);
        if (propIncrements != null)
            readPropertyIncrementsInfo(value, propIncrements);
    }

    //region MainObject
    private void readMainObjIncrement(CommonModelIncrement value, JsonNode node) {
        CommonObjectIncrementDeserializer deserializer = getObjectIncrementDeserializer();
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(CommonEntityIncrement.class, deserializer);
        mapper.registerModule(module);
        try {
            value.setMainEntityIncrement((ModifyEntityIncrement) mapper.readValue(mapper.writeValueAsString(node), CommonEntityIncrement.class));
        } catch (Throwable e) {
            throw CommonModelException.createException(CommonModelErrorCodeEnum.GSP_BEMODEL_JSON_0001, e, "IGspCommonField");
        }
    }

    protected abstract CommonObjectIncrementDeserializer getObjectIncrementDeserializer();

    //endregion

    //region props

    private void readPropertyIncrementsInfo(CommonModelIncrement increment, JsonNode node) {
        ArrayNode array = trans2Array(node);
        for (JsonNode childNode : array) {
            PropertyIncrementDeserializer serializer = getPropertyIncrementSerializer();
            ObjectMapper mapper = new ObjectMapper();
            SimpleModule module = new SimpleModule();
            module.addDeserializer(PropertyIncrement.class, serializer);
            mapper.registerModule(module);
            try {
                String key = childNode.get(CefNames.Id).textValue();
                JsonNode valueNode = childNode.get(CefNames.Value);

                PropertyIncrement prop = mapper.readValue(mapper.writeValueAsString(valueNode), PropertyIncrement.class);
                increment.getChangeProperties().put(key, prop);
            } catch (Throwable e) {
                throw CommonModelException.createException(CommonModelErrorCodeEnum.GSP_BEMODEL_JSON_0001, e, "PropertyIncrement");
            }
        }
    }

    private PropertyIncrementDeserializer getPropertyIncrementSerializer() {
        return new PropertyIncrementDeserializer();
    }
    //endregion

    protected ArrayNode trans2Array(JsonNode node) {
        try {
            return new ObjectMapper().treeToValue(node, ArrayNode.class);
        } catch (JsonProcessingException e) {
            throw CommonModelException.createException(CommonModelErrorCodeEnum.GSP_BEMODEL_JSON_0003, e);
        }
    }

    protected void readExtendInfo(CommonModelIncrement value, JsonNode node) {

    }
}
