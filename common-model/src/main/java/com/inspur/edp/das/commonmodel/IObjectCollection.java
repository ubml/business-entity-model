package com.inspur.edp.das.commonmodel;

/**
 * The Collection  Of Object
 *
 * @ClassName: IObjectCollection
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public interface IObjectCollection extends java.util.List<IGspCommonObject> {

    /**
     * @return The Belong Object Of The Collection
     */
    IGspCommonObject getParentObject();
}