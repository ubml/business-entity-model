package com.inspur.edp.das.commonmodel.util.generate;

import com.inspur.edp.cef.designtime.api.entity.DataTypeAssemblyInfo;
import com.inspur.edp.das.commonmodel.IGspCommonModel;

public final class GeneratingUtils {
    public static final String InterfacePrefix = "I";

    public static DataTypeAssemblyInfo getGeneratingEntityAss(IGspCommonModel cm) {
        return cm.getEntityAssemblyInfo();
    }

    public static DataTypeAssemblyInfo getGeneratingApiAssInfo(IGspCommonModel cm) {
        return cm.getApiNamespace();
    }
}