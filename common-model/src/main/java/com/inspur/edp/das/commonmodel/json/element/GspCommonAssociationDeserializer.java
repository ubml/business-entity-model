package com.inspur.edp.das.commonmodel.json.element;

import com.inspur.edp.cef.designtime.api.element.GspAssociation;
import com.inspur.edp.cef.designtime.api.json.element.CefFieldDeserializer;
import com.inspur.edp.cef.designtime.api.json.element.GspAssociationDeserializer;
import com.inspur.edp.das.commonmodel.entity.element.GspCommonAssociation;

/**
 * The Json Deserializer Of Common  Association
 *
 * @ClassName: GspCommonAssociationDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class GspCommonAssociationDeserializer extends GspAssociationDeserializer {
    public GspCommonAssociationDeserializer(CefFieldDeserializer filedDeserializer) {
        super(filedDeserializer);
    }

    @Override
    protected GspAssociation CreateGspAssociation() {
        return new GspCommonAssociation();
    }
}
