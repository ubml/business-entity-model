package com.inspur.edp.das.commonmodel.i18n;

import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.designtime.api.element.GspAssociation;
import com.inspur.edp.cef.designtime.api.i18n.context.CefResourcePrefixInfo;
import com.inspur.edp.cef.designtime.api.i18n.context.ICefResourceExtractContext;
import com.inspur.edp.cef.designtime.api.i18n.extractor.AssoRefFieldResourceExtractor;
import com.inspur.edp.cef.designtime.api.i18n.extractor.AssoResourceExtractor;
import com.inspur.edp.das.commonmodel.IGspCommonElement;
import com.inspur.edp.das.commonmodel.entity.GspCommonElement;

public abstract class GspAssoResourceExtractor extends AssoResourceExtractor {
    protected GspAssoResourceExtractor(GspAssociation asso, ICefResourceExtractContext context, CefResourcePrefixInfo parentResourceInfo) {
        super(asso, context, parentResourceInfo);
    }

    @Override
    protected AssoRefFieldResourceExtractor getAssoRefFieldResourceExtractor(ICefResourceExtractContext context, CefResourcePrefixInfo assoPrefixInfo, IGspCommonField field) {
        return getAssoRefElementResourceExtractor(context, assoPrefixInfo, (GspCommonElement) ((field instanceof GspCommonElement) ? field : null));
    }

    protected abstract GspAssoRefEleResourceExtractor getAssoRefElementResourceExtractor(ICefResourceExtractContext context, CefResourcePrefixInfo assoPrefixInfo, IGspCommonElement field);
}