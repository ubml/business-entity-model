package com.inspur.edp.das.commonmodel.i18n;

import com.inspur.edp.cef.designtime.api.i18n.context.CefResourcePrefixInfo;
import com.inspur.edp.cef.designtime.api.i18n.context.ICefResourceExtractContext;
import com.inspur.edp.cef.designtime.api.i18n.extractor.AbstractResourceExtractor;
import com.inspur.edp.cef.designtime.api.i18n.names.CefResourceDescriptionNames;
import com.inspur.edp.cef.designtime.api.i18n.names.CefResourceKeyNames;
import com.inspur.edp.das.commonmodel.IGspCommonModel;
import com.inspur.edp.das.commonmodel.IGspCommonObject;
import com.inspur.edp.das.commonmodel.entity.GspCommonModel;

public abstract class CommonModelResourceExtractor extends AbstractResourceExtractor {
    private final IGspCommonModel model;

    protected CommonModelResourceExtractor(IGspCommonModel model, ICefResourceExtractContext context) {
        super(context);
        this.model = model;
    }

    @Override
    protected final void extractItems() {
        //Name
        addResourceInfo(CefResourceKeyNames.Name, model.getName(), CefResourceDescriptionNames.Name);
        extractMainObject(model);
    }

    private void extractMainObject(IGspCommonModel model) {
        IGspCommonObject mainObj = model.getMainObject();
        getObjectResourceExtractor(getContext(), getCurrentResourcePrefixInfo(), mainObj).extract();
    }

    @Override
    protected final CefResourcePrefixInfo buildCurrentPrefix() {
        String metaSuffix = getContext().getKeyPrefix();
        CefResourcePrefixInfo info = new CefResourcePrefixInfo();
        info.setDescriptionPrefix(getModelDescriptionName() + "'" + model.getName() + "'");
        info.setResourceKeyPrefix(metaSuffix + "." + model.getCode());
        return info;
    }

    /**
     * 赋值模型国际化前缀
     */
    @Override
    protected final void setPrefixInfo() {
        ((GspCommonModel) model).setI18nResourceInfoPrefix(getCurrentResourcePrefixInfo().getResourceKeyPrefix());
    }

    //[业务实体]、[视图对象]
    protected abstract String getModelDescriptionName();

    protected abstract CommonObjectResourceExtractor getObjectResourceExtractor(ICefResourceExtractContext context, CefResourcePrefixInfo modelPrefixInfo, IGspCommonObject obj);

    protected abstract void extractExtendProperties(IGspCommonModel model);

}