package com.inspur.edp.das.commonmodel.controlruledef.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.cm.serializer.CommonDataTypeRuleDefSerializer;
import com.inspur.edp.das.commonmodel.controlruledef.entity.CmEntityControlRuleDef;

public class CmEntityControlRuleDefSerializer<T extends CmEntityControlRuleDef> extends CommonDataTypeRuleDefSerializer<T> {
    @Override
    protected final void writeCommonDataTypeRuleExtendInfos(T controlRuleDefinition, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) {
        super.writeCommonDataTypeRuleExtendInfos(controlRuleDefinition, jsonGenerator, serializerProvider);
        writeCmEntityRuleExtendInfos(controlRuleDefinition, jsonGenerator, serializerProvider);
    }

    protected void writeCmEntityRuleExtendInfos(T controlRuleDefinition, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) {

    }
}
