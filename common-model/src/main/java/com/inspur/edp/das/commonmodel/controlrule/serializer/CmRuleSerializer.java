package com.inspur.edp.das.commonmodel.controlrule.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.basic.serializer.RangeRuleSerializer;
import com.inspur.edp.das.commonmodel.controlrule.CmControlRule;

public class CmRuleSerializer<T extends CmControlRule> extends RangeRuleSerializer<T> {
    @Override
    protected final void writeRangeExtendInfos(T controlRule, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) {
        super.writeRangeExtendInfos(controlRule, jsonGenerator, serializerProvider);
        writeCmRuleExtendInfos(controlRule, jsonGenerator, serializerProvider);
    }

    protected void writeCmRuleExtendInfos(T controlRule, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) {

    }
}