package com.inspur.edp.das.commonmodel.i18n;

import com.inspur.edp.cef.designtime.api.i18n.context.CefResourcePrefixInfo;
import com.inspur.edp.cef.designtime.api.i18n.context.ICefResourceExtractContext;
import com.inspur.edp.cef.designtime.api.i18n.extractor.AssoRefFieldResourceExtractor;
import com.inspur.edp.das.commonmodel.IGspCommonElement;

public abstract class GspAssoRefEleResourceExtractor extends AssoRefFieldResourceExtractor {
    protected GspAssoRefEleResourceExtractor(IGspCommonElement field, ICefResourceExtractContext context, CefResourcePrefixInfo parentResourceInfo) {
        super(field, context, parentResourceInfo);
    }
}