package com.inspur.edp.das.commonmodel.json.increment;

import com.inspur.edp.cef.designtime.api.element.increment.json.GspFieldIncrementSerializer;
import com.inspur.edp.cef.designtime.api.json.element.CefFieldSerializer;
import com.inspur.edp.das.commonmodel.json.element.CmElementSerializer;

public abstract class CommonElementIncrementSerializer extends GspFieldIncrementSerializer {
    @Override
    protected final CefFieldSerializer getFieldSerializer() {
        return getCmElementSerializer();
    }

    protected abstract CmElementSerializer getCmElementSerializer();
}
