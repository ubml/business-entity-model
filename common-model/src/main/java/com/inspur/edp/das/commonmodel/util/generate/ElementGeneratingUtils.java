package com.inspur.edp.das.commonmodel.util.generate;

import com.inspur.edp.cef.designtime.api.element.GspElementDataType;

public final class ElementGeneratingUtils {
    @SuppressWarnings("rawtypes")
    public static Class getNativeType(GspElementDataType elementType) {
        switch (elementType) {
            case String:
            case Text:
                return String.class;
            case Boolean:
                return Boolean.class;
            case Integer:
                return Integer.class;
            case Decimal:
                return java.math.BigDecimal.class;
            case Date:
            case DateTime:
                return java.util.Date.class;
            case Binary:
                return byte[].class;
            default:
                // throw new Exception(String.format("elementType'%1$s'invalid enum values"),
                // elementType.toString());
                return null;
        }
    }

}