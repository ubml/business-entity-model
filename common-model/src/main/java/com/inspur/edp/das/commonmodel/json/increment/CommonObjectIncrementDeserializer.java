package com.inspur.edp.das.commonmodel.json.increment;

import com.inspur.edp.cef.designtime.api.element.increment.json.GspFieldIncrementDeserializer;
import com.inspur.edp.cef.designtime.api.entity.increment.json.GspDataTypeIncrementDeserializer;
import com.inspur.edp.cef.designtime.api.json.object.GspCommonDataTypeDeserializer;
import com.inspur.edp.das.commonmodel.json.object.CmObjectDeserializer;

public abstract class CommonObjectIncrementDeserializer extends GspDataTypeIncrementDeserializer {

    @Override
    protected final GspCommonDataTypeDeserializer getDataTypeDeserializer() {
        return getCommonObjectDeserializer();
    }

    protected abstract CmObjectDeserializer getCommonObjectDeserializer();

    @Override
    protected final GspFieldIncrementDeserializer getFieldSerializer() {
        return getCmElementIncrementDeserizlizer();
    }

    protected abstract CommonElementIncrementDeserializer getCmElementIncrementDeserizlizer();


    @Override
    protected final GspDataTypeIncrementDeserializer getChildEntityDeserializer() {
        return getCmObjectIncrementDeserializer();
    }

    protected abstract CommonObjectIncrementDeserializer getCmObjectIncrementDeserializer();

}
