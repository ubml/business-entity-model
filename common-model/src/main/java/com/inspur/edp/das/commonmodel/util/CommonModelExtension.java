package com.inspur.edp.das.commonmodel.util;

import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.designtime.api.util.DataValidator;
import com.inspur.edp.das.commonmodel.IGspCommonElement;
import com.inspur.edp.das.commonmodel.IGspCommonModel;
import com.inspur.edp.das.commonmodel.IGspCommonObject;
import com.inspur.edp.das.commonmodel.entity.GspCommonElement;
import com.inspur.edp.das.commonmodel.entity.GspCommonModel;
import com.inspur.edp.das.commonmodel.entity.GspCommonObject;

import java.util.ArrayList;
import java.util.HashMap;

// import Inspur.Ecp.Caf.Common.*;

/**
 * CM扩展方法
 */
public final class CommonModelExtension {


    /**
     * 获取指定结点的属性字典，其中key 是LabelID
     * <p>
     * 包含非持久化属性和关联带出的属性
     *
     * @param node
     * @return
     */
    @SuppressWarnings("unchecked")
    static java.util.HashMap<String, IGspCommonElement> getElements(IGspCommonObject node) {
        final String elementDicKey = "NodeElementDic";

        if (node == null || node.getContainElements() == null) {
            return null;
        }
        GspCommonObject co = (GspCommonObject) ((node instanceof GspCommonObject) ? node : null);
        if (co != null) {
            if (co.getExtProperties().containsKey(elementDicKey)) {
                Object result = co.getExtProperties().get(elementDicKey);
                return (HashMap<String, IGspCommonElement>) result;
            }
        }

        HashMap<String, IGspCommonElement> rez = ((GspCommonObject) node).getAllElementDic();
        if (co != null) {
            co.getExtProperties().put(elementDicKey, rez);
        }
        return rez;
    }

    private static void getNodes(IGspCommonObject bizEntityObject, java.util.Map<String, IGspCommonObject> rez) {
        if (bizEntityObject.getContainChildObjects() == null) {
            return;
        }
        if (bizEntityObject.getContainChildObjects().isEmpty()) {
            return;
        }
        // Java:
        for (IGspCommonObject obj : bizEntityObject.getContainChildObjects()) {
            getNodes(obj, rez);
            rez.put(obj.getCode(), obj);
        }
    }

    /**
     * 递归取所有子Node
     *
     * @param model
     * @return 返回结果集为字典类型，不保证遍历顺序
     */
    public static HashMap<String, IGspCommonObject> getNodes(IGspCommonModel model) {
        final String nodeDicKey = "ModelNodesDic";

        if (model == null || model.getMainObject() == null) {
            return null;
        }
        HashMap<String, IGspCommonObject> result = getExtProperty(model, nodeDicKey);
        if (result != null) {
            return result;
        }

        java.util.HashMap<String, IGspCommonObject> rez = new java.util.HashMap<String, IGspCommonObject>();
        getNodes(model.getMainObject(), rez);
        rez.put(model.getMainObject().getCode(), model.getMainObject());
        result = new HashMap<String, IGspCommonObject>(rez);
        setExtProperty(model, nodeDicKey, result);
        return result;
    }

    static boolean HasCodeNumberColumn(IGspCommonModel model) {
        DataValidator.checkForNullReference(model, "model");

        GspCommonModel cm = (GspCommonModel) ((model instanceof GspCommonModel) ? model : null);
        final String key = "HasCodeNumberColumn";
        if (cm != null && cm.getExtProperties().containsKey(key)) {
            return (boolean) cm.getExtProperties().get(key);
        }
        boolean result = false;
        for (IGspCommonField item : ((GspCommonObject) model.getMainObject()).getContainElements()) {
            GspCommonElement element = (GspCommonElement) item;
            if (element.getBillCodeConfig() != null && element.getBillCodeConfig().getCanBillCode()
                    && !element.getBillCodeConfig().getBillCodeID().isEmpty()) {
                result = true;
                break;
            }
        }
        if (cm != null) {
            cm.getExtProperties().put(key, result);
        }
        return result;
    }

    /**
     * 按照层次遍历的BE结点列表
     *
     * @return 结果集使用先根后子方式排序
     */
    public static ArrayList<IGspCommonObject> getLevelOrderedNodes(IGspCommonModel model) {

        final String nodesListkey = "ModelLevelOrderedNodes";
        ArrayList<IGspCommonObject> nodes = getExtProperty(model, nodesListkey);
        if (nodes != null) {
            return nodes;
        }

        nodes = new java.util.ArrayList<IGspCommonObject>();
        java.util.LinkedList<IGspCommonObject> nodeQueue = new java.util.LinkedList<IGspCommonObject>();
        nodeQueue.offer(model.getMainObject());

        while (!nodeQueue.isEmpty()) {
            IGspCommonObject node = nodeQueue.poll();
            if (node == null) // TODO ERROR?
            {
                continue;
            }

            nodes.add(node);
            if (node.getContainChildObjects().size() <= 0) {
                continue;
            }

            for (IGspCommonObject child : node.getContainChildObjects()) {
                nodeQueue.offer(child);
            }
        }
        setExtProperty(model, nodesListkey, nodes);
        return nodes;
    }

    // region 私有方法
    // where T : class
    private static <T> T getExtProperty(IGspCommonModel model, String key) {
        GspCommonModel cm = (GspCommonModel) ((model instanceof GspCommonModel) ? model : null);
        if (cm == null) {
            return null;
        }
        return cm.getExtProperty(key);
    }

    // where T : class
    private static <T> void setExtProperty(IGspCommonModel model, String key, T value) {
        GspCommonModel cm = (GspCommonModel) ((model instanceof GspCommonModel) ? model : null);
        if (cm == null) {
            return;
        }

        cm.setExtProperty(key, value);
    }

    // endregion
    private static final String EXT_KEY_SU = "pfcommon_su";

    public static String getSUCode(IGspCommonModel model) {
        return getExtProperty(model, EXT_KEY_SU);
    }

    public static void setSUCode(IGspCommonModel model, String su) {
        setExtProperty(model, EXT_KEY_SU, su);
    }
}