package com.inspur.edp.das.commonmodel.controlrule.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlrule.cm.serializer.CommonFieldRuleSerializer;
import com.inspur.edp.das.commonmodel.controlrule.CmFieldControlRule;

public class CmFieldControlRuleSerializer<T extends CmFieldControlRule> extends CommonFieldRuleSerializer<T> {

    @Override
    protected final void writeCommonFieldRuleExtendInfos(T controlRule, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) {
        super.writeCommonFieldRuleExtendInfos(controlRule, jsonGenerator, serializerProvider);
        writeCmFieldRuleExtendInfos(controlRule, jsonGenerator, serializerProvider);
    }

    protected void writeCmFieldRuleExtendInfos(T controlRule, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) {


    }
}
