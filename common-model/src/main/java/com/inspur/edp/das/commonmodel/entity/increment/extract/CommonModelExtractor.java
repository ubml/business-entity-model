package com.inspur.edp.das.commonmodel.entity.increment.extract;

import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.cm.CommonDataTypeControlRuleDef;
import com.inspur.edp.cef.designtime.api.entity.increment.ModifyEntityIncrement;
import com.inspur.edp.cef.designtime.api.increment.extractor.AbstractIncrementExtractor;
import com.inspur.edp.cef.designtime.api.increment.extractor.ExtractUtils;
import com.inspur.edp.cef.designtime.api.json.CefNames;
import com.inspur.edp.das.commonmodel.controlrule.CmControlRule;
import com.inspur.edp.das.commonmodel.controlruledef.entity.CmControlRuleDef;
import com.inspur.edp.das.commonmodel.entity.GspCommonModel;
import com.inspur.edp.das.commonmodel.entity.increment.CommonModelIncrement;
import com.inspur.edp.das.commonmodel.entity.object.increment.extract.CommonObjectIncrementExtractor;
import com.inspur.edp.das.commonmodel.json.CommonModelNames;
import lombok.var;


public class CommonModelExtractor extends AbstractIncrementExtractor {

    protected boolean includeAll = false;

    public CommonModelExtractor() {

    }

    public CommonModelExtractor(boolean includeAll) {
        this.includeAll = includeAll;
    }

    public CommonModelIncrement extract(GspCommonModel oldModel, GspCommonModel newModel, CmControlRule rule, CmControlRuleDef def) {

        CommonModelIncrement increment = createCommonModelIncrement();
        //ChangeProperties()
        extractSelfInfo(increment, oldModel, newModel, rule, def);

        //MainObject
        extractMainObjectInfo(increment, oldModel, newModel, rule, def);

        //extendInfo
        extractExtendInfo(increment, oldModel, newModel, rule, def);

        return increment;
    }

    private void extractSelfInfo(
            CommonModelIncrement increment,
            GspCommonModel oldModel,
            GspCommonModel newModel,
            CmControlRule rule,
            CmControlRuleDef def) {

        if (includeAll) {
            extractSelfAllInfo(increment, oldModel, newModel);
            return;
        }

        ExtractUtils.extractValue(
                increment.getChangeProperties(),
                oldModel.getName(),
                newModel.getName(),
                CefNames.Name,
                rule.getNameControlRule(),
                def.getNameControlRule());
    }

    private void extractSelfAllInfo(CommonModelIncrement increment,
                                    GspCommonModel oldModel,
                                    GspCommonModel newModel) {
        ExtractUtils.extractValue(increment.getChangeProperties(), oldModel.getName(), newModel.getName(), CefNames.Name, null, null);
        ExtractUtils.extractValue(increment.getChangeProperties(), oldModel.getCode(), newModel.getCode(), CefNames.Code, null, null);
        ExtractUtils.extractValue(increment.getChangeProperties(), oldModel.getI18nResourceInfoPrefix(), newModel.getI18nResourceInfoPrefix(), CefNames.I18nResourceInfoPrefix, null, null);
        ExtractUtils.extractValue(increment.getChangeProperties(), oldModel.getIsVirtual(), newModel.getIsVirtual(), CefNames.IsVirtual, null, null);
        ExtractUtils.extractValue(increment.getChangeProperties(), oldModel.getEntityType(), newModel.getEntityType(), CommonModelNames.EntityType, null, null);
        ExtractUtils.extractValue(increment.getChangeProperties(), oldModel.getDotnetGeneratingAssembly(), newModel.getDotnetGeneratingAssembly(), CommonModelNames.GeneratingAssembly, null, null);
        ExtractUtils.extractValue(increment.getChangeProperties(), oldModel.getIsUseNamespaceConfig(), newModel.getIsUseNamespaceConfig(), CommonModelNames.IsUseNamespaceConfig, null, null);
        ExtractUtils.extractValue(increment.getChangeProperties(), oldModel.getVersionContronInfo().getVersionControlElementId(), newModel.getVersionContronInfo().getVersionControlElementId(), CommonModelNames.VersionControlElementId, null, null);
        ExtractUtils.extractValue(increment.getChangeProperties(), oldModel.getBeLabel(), newModel.getBeLabel(), CommonModelNames.BeLabel, null, null);

    }

    private void extractMainObjectInfo(
            CommonModelIncrement increment,
            GspCommonModel oldModel,
            GspCommonModel newModel,
            CmControlRule rule,
            CmControlRuleDef def) {
        var objExtractor = getCommonObjectIncrementExtractor();
        var objIncrement = objExtractor.extractorIncrement(oldModel.getMainObject(), newModel.getMainObject(), rule.getMainEntityControlRule(), (CommonDataTypeControlRuleDef) def.getChildControlRules().get(CommonModelNames.MainObject));
        increment.setMainEntityIncrement((ModifyEntityIncrement) objIncrement);
    }

    protected CommonModelIncrement createCommonModelIncrement() {
        return new CommonModelIncrement();
    }

    protected void extractExtendInfo(
            CommonModelIncrement increment,
            GspCommonModel oldModel,
            GspCommonModel newModel,
            CmControlRule rule,
            CmControlRuleDef def) {

    }

    protected CommonObjectIncrementExtractor getCommonObjectIncrementExtractor() {
        return new CommonObjectIncrementExtractor(includeAll);
    }
}
