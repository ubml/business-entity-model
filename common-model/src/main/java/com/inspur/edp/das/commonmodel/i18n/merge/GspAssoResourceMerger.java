package com.inspur.edp.das.commonmodel.i18n.merge;

import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.designtime.api.element.GspAssociation;
import com.inspur.edp.cef.designtime.api.i18n.context.ICefResourceMergeContext;
import com.inspur.edp.cef.designtime.api.i18n.merger.AssoRefFieldResourceMerger;
import com.inspur.edp.cef.designtime.api.i18n.merger.AssoResourceMerger;
import com.inspur.edp.das.commonmodel.IGspCommonElement;
import com.inspur.edp.das.commonmodel.entity.GspCommonElement;

public abstract class GspAssoResourceMerger extends AssoResourceMerger {
    protected GspAssoResourceMerger(GspAssociation asso, ICefResourceMergeContext context) {
        super(asso, context);
    }

    @Override
    protected AssoRefFieldResourceMerger getAssoRefFieldResourceMerger(ICefResourceMergeContext context, IGspCommonField field) {
        return getAssoRefElementResourcemerger(context, (GspCommonElement) ((field instanceof GspCommonElement) ? field : null));
    }

    protected abstract GspAssoRefEleResourceMerger getAssoRefElementResourcemerger(ICefResourceMergeContext context, IGspCommonElement field);

}
