package com.inspur.edp.das.commonmodel.controlruledef.entity;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.entity.ControlRuleDefItem;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.entity.ControlRuleDefinition;
import com.inspur.edp.cef.designtime.api.changecontrolrule.controlruledef.basic.entity.RangeRuleDefinition;
import com.inspur.edp.das.commonmodel.controlruledef.parser.CmRuleDefParser;
import com.inspur.edp.das.commonmodel.controlruledef.serializer.CmRuleDefSerializer;

@JsonSerialize(using = CmRuleDefSerializer.class)
@JsonDeserialize(using = CmRuleDefParser.class)
public class CmControlRuleDef extends RangeRuleDefinition {
    public CmControlRuleDef(ControlRuleDefinition parentRuleDefinition, String ruleObjectType) {
        super(parentRuleDefinition, ruleObjectType);
    }

    public ControlRuleDefItem getNameControlRule() {
        return super.getControlRuleItem(CmRuleNames.Name);
    }

    public void setNameControlRule(ControlRuleDefItem ruleItem) {
        super.setControlRuleItem(CmRuleNames.Name, ruleItem);
    }

    public ControlRuleDefItem getCodeControlRule() {
        return super.getControlRuleItem(CmRuleNames.Code);
    }

    public void setCodeControlRule(ControlRuleDefItem ruleItem) {
        super.setControlRuleItem(CmRuleNames.Code, ruleItem);
    }
}
