package com.inspur.edp.das.commonmodel.collection;

import com.inspur.edp.cef.designtime.api.collection.BaseList;
import com.inspur.edp.das.commonmodel.IGspCommonObject;
import com.inspur.edp.das.commonmodel.entity.object.GspUniqueConstraint;

/**
 * The Collection Of Unique Constraint
 *
 * @ClassName: GspUniqueConstraintCollection
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class GspUniqueConstraintCollection extends BaseList<GspUniqueConstraint> implements Cloneable {
    private static final long serialVersionUID = 1L;

    /**
     * 新增约束
     *
     * @param constraint
     */
    public final boolean add(GspUniqueConstraint constraint) {
        return super.add(constraint);
    }

    /**
     * 删除约束
     *
     * @param constraint
     */
    public final void remove(GspUniqueConstraint constraint) {
        if (contains(constraint)) {
            super.remove(constraint);
        }
    }

    /**
     * 删除约束
     *
     * @param index 约束的索引
     */
    @Override
    public void removeAt(int index) {
        remove(this.getItem(index));
    }

    /**
     * 插入一条新约束记录
     *
     * @param index      插入的索引
     * @param constraint 约束定义
     */
    public final void insert(int index, GspUniqueConstraint constraint) {
        super.insert(index, constraint);
    }

    /**
     按照索引序号访问集合中的约束

     @param index 索引
     @return
     */
//C# TO JAVA CONVERTER WARNING: There is no Java equivalent to C#'s shadowing via the 'new' keyword:
//ORIGINAL LINE: public new GspUniqueConstraint this[int index] => (GspUniqueConstraint)((_list[index] instanceof GspUniqueConstraint) ? _list[index] : null);
    // public GspUniqueConstraint this.setItem(int index, > (GspUniqueConstraint)((_list.get(index) instanceof GspUniqueConstraint) ? _list.get(index) : null));

    /**
     * 根据Id访问约束
     *
     * @param id
     * @return
     */
    public final GspUniqueConstraint getItem(String id) {
        for (GspUniqueConstraint constraint : this) {
            if (constraint.getId().equals(id)) {
                return constraint;
            }
        }
        return null;
    }

    /**
     * 克隆
     *
     * @return
     */
    public final GspUniqueConstraintCollection clone() {
        return clone(null);
    }

    /**
     * 克隆
     *
     * @param absObject 节点
     * @return
     */
    public final GspUniqueConstraintCollection clone(IGspCommonObject absObject) {
        GspUniqueConstraintCollection newObj = new GspUniqueConstraintCollection();
        for (GspUniqueConstraint item : this) {
            newObj.add(item.clone(absObject));
        }
        return newObj;
    }
}