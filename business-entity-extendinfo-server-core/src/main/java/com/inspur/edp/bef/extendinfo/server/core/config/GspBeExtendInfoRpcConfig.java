package com.inspur.edp.bef.extendinfo.server.core.config;

import com.inspur.edp.bef.bemanager.gspbusinessentity.cache.GspBeExtendInfoCacheManager;
import com.inspur.edp.bef.bemanager.gspbusinessentity.repository.GspBeExtendInfoRepository;
import com.inspur.edp.bef.extendinfo.server.api.IGspBeExtendInfoRpcService;
import com.inspur.edp.bef.extendinfo.server.core.GspBeExtendInfoRpcServiceImp;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GspBeExtendInfoRpcConfig {

    @Bean
    public IGspBeExtendInfoRpcService getBeExtendInfoRpcService(GspBeExtendInfoRepository beExtendInfoRepository,
                                                                GspBeExtendInfoCacheManager gspBeExtendInfoCacheManager) {
        return new GspBeExtendInfoRpcServiceImp(beExtendInfoRepository, gspBeExtendInfoCacheManager);
    }
}
