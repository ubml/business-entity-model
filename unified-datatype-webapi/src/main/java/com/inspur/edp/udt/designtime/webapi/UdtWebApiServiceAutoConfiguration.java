package com.inspur.edp.udt.designtime.webapi;

import io.iec.edp.caf.rest.RESTEndpoint;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UdtWebApiServiceAutoConfiguration {

    @Bean
    public UdtController getUdtWebApi() {
        return new UdtController();
    }

    @Bean
    public RESTEndpoint getUdtWebApiEndpoint(UdtController generateService) {
        return new RESTEndpoint(
                "/dev/main/v1.0/udt",
                generateService
        );
    }
}
