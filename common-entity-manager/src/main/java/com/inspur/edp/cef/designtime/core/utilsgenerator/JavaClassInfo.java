package com.inspur.edp.cef.designtime.core.utilsgenerator;

import com.inspur.edp.cef.designtime.api.exceptions.CommonEntityErrorCodeEnum;
import com.inspur.edp.cef.designtime.api.exceptions.CommonEntityException;
import com.inspur.edp.lcm.metadata.api.service.MetadataProjectService;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class JavaClassInfo {
    private static final Logger logger = LoggerFactory.getLogger(JavaClassInfo.class);
    private String packageName;
    private final ImportInfos importInfos = new ImportInfos();
    private String filePath;

    /**
     * 已存在文件的imports引用信息
     */
    private List<String> existVersionImports = new ArrayList<>();

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    private String className;

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    private final List<MethodInfo> methods = new ArrayList<>();


    public List<MethodInfo> getMethods() {
        return methods;
    }

    private final List<JavaAccessModifier> accessModifiers = new ArrayList<>();

    public List<JavaAccessModifier> getAccessModifiers() {
        return accessModifiers;
    }

    public ImportInfos getImportInfos() {
        return importInfos;
    }

    public void write2File() {
        MetadataProjectService service = SpringBeanUtils.getBean(MetadataProjectService.class);

        String compModulePath = service.getJavaCompProjectPath(filePath);
        String path = Paths.get(compModulePath).resolve(getPackageName().replace(".", "\\")).toString();
        String compositePath = path.replace("\\", File.separator);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("package ").append(this.packageName).append(";\n");
        importInfos.write(stringBuilder);
        stringBuilder.append("\n");
        writeClassHeader(stringBuilder);
        writeMethods(stringBuilder);
        writeClassEnd(stringBuilder);

        File folder = new File(compositePath);
        if (!folder.exists())
            folder.mkdirs();

        String fileName = compositePath + File.separator + this.className + ".java";
        File f = new File(fileName);

        try {
            if (!f.exists())
                f.createNewFile();

            // 使用try-with-resources确保OutputStreamWriter自动关闭
            try (OutputStreamWriter out = new OutputStreamWriter(
                    Files.newOutputStream(f.toPath()), // true to append
                    StandardCharsets.UTF_8
            )) {
                out.write(stringBuilder.toString());
                out.flush();
            }
        } catch (IOException e) {
            throw CommonEntityException.createException(CommonEntityErrorCodeEnum.GSP_BEMODEL_COMMON_ENTITY_0005, e);
        }
    }

    private void writeMethods(StringBuilder stringBuilder) {
        for (MethodInfo methodInfo : getMethods()) {
            stringBuilder.append("\n");
            methodInfo.write(stringBuilder);
        }
        stringBuilder.append("\n");
    }

    private void writeClassEnd(StringBuilder stringBuilder) {
        stringBuilder.append("}");
    }

    private void writeClassHeader(StringBuilder stringBuilder) {
        for (JavaAccessModifier accessModifier : getAccessModifiers())
            stringBuilder.append(accessModifier.toString()).append(" ");
        stringBuilder.append("class ");
        stringBuilder.append(getClassName()).append("{");
    }

    public void addMethodInfo(MethodInfo methodInfo) {
        getMethods().add(methodInfo);
        getImportInfos().addImportPackage(methodInfo.getReturnType());
        for (ParameterInfo parameterInfo : methodInfo.getParameters()){
            getImportInfos().addImportPackage(parameterInfo.getParamType());
        }

        //当方法需要注释的时候,import中的Udt类型也需要注释，注释是为了方便用户需要的时候，自己添加依赖使用
        //目前只有非内置UDT需要注释
        if(methodInfo.isNeedCommented()){
            getImportInfos().addCommentedImportPackage(methodInfo.getReturnType());
            for (ParameterInfo parameterInfo : methodInfo.getParameters()){
                //排除IEntityData参数
                if(!parameterInfo.getParamType().getTypeName().equals("IEntityData")){
                    getImportInfos().addCommentedImportPackage(parameterInfo.getParamType());
                }
            }
        }
    }

    /**
     * 获取当前类引用的import类列表
     * @return
     */
    public List<String> getImportClassInfo(){
        if(existVersionImports.size() > 0){
            return existVersionImports;
        }

        MetadataProjectService service = SpringBeanUtils.getBean(MetadataProjectService.class);

        String compModulePath = service.getJavaCompProjectPath(filePath);
        String path = Paths.get(compModulePath).resolve(getPackageName().replace(".", "\\")).toString();
        String compositePath = path.replace("\\", File.separator);

        File folder = new File(compositePath);
        if (!folder.exists()) {
            return existVersionImports;
        }

        String fileName = compositePath + File.separator + this.className + ".java";
        File f = new File(fileName);
        if (!f.exists()) {
            return existVersionImports;
        }

        try (BufferedReader bf = new BufferedReader(new InputStreamReader(Files.newInputStream(f.toPath()), StandardCharsets.UTF_8))) {
             String str;
             // 按行读取字符串
             while ((str = bf.readLine()) != null) {
                 str = str.trim();
                 if(str.startsWith("import")){
                     existVersionImports.add(str.substring(7, str.length()-1));
                     continue;
                 }
                 if(str.startsWith("public final class")){
                     break;
                 }
             }
        } catch (IOException e) {
            logger.error("ioexception", e);
        }

        return existVersionImports;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
}
