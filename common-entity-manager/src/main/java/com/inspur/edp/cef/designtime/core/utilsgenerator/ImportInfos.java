package com.inspur.edp.cef.designtime.core.utilsgenerator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ImportInfos {

    /**
     * 需要导入的包信息集合
     */
    private final List<String> importPackages = new ArrayList();

    /**
     * 需要注释的导入包信息集合
     */
    private final List<String> commentedImportPackages = new ArrayList();

    public List<String> getImportPackages() {
        return importPackages;
    }

    public void addImportPackage(String value) {
        if (importPackages.contains(value))
            return;
        importPackages.add(value);
    }

    public void addImportPackage(TypeRefInfo typeRefInfo) {
        if ("void".equals(typeRefInfo.getTypeName()))
            return;
        if ("java.lang".equals(typeRefInfo.getTypePackageName()))
            return;
        if (typeRefInfo.getTypePackageName() == null || "".equals(typeRefInfo.getTypePackageName()))
            return;
        if (importPackages.contains(typeRefInfo.getFullName()))
            return;
        importPackages.add(typeRefInfo.getFullName());
    }

    /**
     * 加入需要注释的导入包中
     * @param typeRefInfo
     */
    public void addCommentedImportPackage(TypeRefInfo typeRefInfo) {
        if ("void".equals(typeRefInfo.getTypeName()))
            return;
        if ("java.lang".equals(typeRefInfo.getTypePackageName()))
            return;
        if (typeRefInfo.getTypePackageName() == null || "".equals(typeRefInfo.getTypePackageName()))
            return;
        if (commentedImportPackages.contains(typeRefInfo.getFullName()))
            return;
        commentedImportPackages.add(typeRefInfo.getFullName());
    }

    public void write(StringBuilder stringBuilder) {
        Collections.sort(getImportPackages());
        for (String value : getImportPackages()) {
            String prefix = "";
            if(commentedImportPackages.contains(value)){
                prefix = "//";
            }
            stringBuilder.append(prefix).append("import ").append(value).append(";\n");
        }
    }
}
