package com.inspur.edp.cef.designtime.core.utilsgenerator;

import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.entity.entity.IEntityData;

class FieldGetMethodGenerator extends FieldMethodBaseGenerator {

    FieldGetMethodGenerator(IGspCommonField field, JavaClassInfo classInfo) {
        super(field, classInfo);
    }


    @Override
    protected String getMethodName() {
        return "get" + field.getLabelID();
    }

    @Override
    protected TypeRefInfo getReturnType() {
        return getTypeInfo(field);
    }

    @Override
    protected void generateMethodParameters() {
        methodInfo.getParameters().add(new ParameterInfo(varData, new TypeRefInfo(IEntityData.class)));
    }

    @Override
    protected void generateMethodBodies() {
        classInfo.getImportInfos().addImportPackage("com.inspur.edp.cef.entity.entity.EntityDataUtils");
        methodInfo.getMethodBodies().add("return (" + methodInfo.getReturnType().getTypeName() + ")EntityDataUtils.getValue(data,\"" + field.getLabelID() + "\");");
    }
}
