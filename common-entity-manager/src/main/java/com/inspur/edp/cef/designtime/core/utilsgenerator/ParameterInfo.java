package com.inspur.edp.cef.designtime.core.utilsgenerator;

public class ParameterInfo {
    private final String paramName;
    private final TypeRefInfo paramType;

    public ParameterInfo(String paramName, TypeRefInfo paramType) {
        this.paramName = paramName;
        this.paramType = paramType;
    }

    public TypeRefInfo getParamType() {
        return paramType;
    }

    public String getParamName() {
        return paramName;
    }

    public void write(StringBuilder stringBuilder) {
        paramType.write(stringBuilder);
        stringBuilder.append(" ");
        stringBuilder.append(getParamName());
    }
}
