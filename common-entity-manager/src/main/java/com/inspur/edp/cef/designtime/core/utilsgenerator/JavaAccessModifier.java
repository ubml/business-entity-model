package com.inspur.edp.cef.designtime.core.utilsgenerator;

import com.inspur.edp.cef.designtime.api.exceptions.CommonEntityErrorCodeEnum;
import com.inspur.edp.cef.designtime.api.exceptions.CommonEntityException;


public enum JavaAccessModifier {
    Public,
    Static,
    Final;

    public int getValue() {
        return this.ordinal();
    }

    public static JavaAccessModifier forValue(int value) {
        return values()[value];
    }

    @Override
    public String toString() {
        switch (this) {
            case Public:
                return "public";
            case Static:
                return "static";
            case Final:
                return "final";
            default:
                throw CommonEntityException.createException(CommonEntityErrorCodeEnum.GSP_BEMODEL_ENUM_NOTSUPPORT_0003, this.name());
        }
    }
}
