package com.inspur.edp.udt.designtime.api.utils;

import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.designtime.api.collection.CommonValCollection;
import com.inspur.edp.cef.designtime.api.element.GspElementDataType;
import com.inspur.edp.cef.designtime.api.operation.CommonOperation;
import com.inspur.edp.cef.designtime.api.operation.CommonValidation;
import com.inspur.edp.cef.designtime.api.operation.ExecutingDataStatus;
import com.inspur.edp.udt.designtime.api.entity.ComplexDataTypeDef;
import com.inspur.edp.udt.designtime.api.entity.SimpleDataTypeDef;
import com.inspur.edp.udt.designtime.api.entity.dbInfo.ColumnInfo;
import com.inspur.edp.udt.designtime.api.entity.validation.UdtTriggerTimePointType;
import com.inspur.edp.udt.designtime.api.entity.validation.ValidationCollection;
import com.inspur.edp.udt.designtime.api.entity.validation.ValidationInfo;
import com.inspur.edp.udt.designtime.api.exception.UdtModelErrorCodeEnum;
import com.inspur.edp.udt.designtime.api.exception.UdtModelException;

import java.io.File;
import java.nio.file.Paths;
import java.util.EnumSet;

/**
 * 业务字段工具类
 */
public class UdtUtils {

    public static String getCombinePath(String path1, String path2) {
        String path = Paths.get(path1).resolve(path2).toString();
        return handlePath(path);
    }

    public static String getCombinePath(String path1, String path2, String path3) {
        String path = Paths.get(path1).resolve(path2).resolve(path3).toString();
        return handlePath(path);
    }

    public static String handlePath(String path) {
        return path.replace("\\", File.separator);
    }

    public static String getSeparator() {
        return File.separator;
    }

    /**
     * 检查空对象，空字符串
     */
    public static boolean checkNull(Object propValue) {
        if (propValue == null) {
            return true;
        }
        if (propValue.getClass().isAssignableFrom(String.class)) {
            String stringValue = (String) propValue;
            if (stringValue == null || stringValue.isEmpty()) {
                return true;
            }
        }

        return false;
    }

    public static Class getType(GspElementDataType type) {
        switch (type) {
            case String:
            case Text:
                return String.class;
            case Integer:
                return Integer.class;
            case Decimal:
                return java.math.BigDecimal.class;
            case Boolean:
                return Boolean.class;
            case Date:
            case DateTime:
                return java.util.Date.class;
            case Binary:
                return byte[].class;
            default:
                throw UdtModelException.createException(UdtModelErrorCodeEnum.GSP_BEMODEL_ENUM_NOTSUPPORT_0002, type.toString());
        }
    }

    ///#endregion

    ///#region columnInfo,udtElement转换

    /**
     * 将多值udt字段转换为columnInfo
     */
    public static ColumnInfo convertUdtElementToColumnInfo(IGspCommonField ele) {
        ColumnInfo tempVar = new ColumnInfo();
        tempVar.setID(ele.getID());
        tempVar.setCode(ele.getCode());
        tempVar.setName(ele.getName());
        tempVar.setMDataType(ele.getMDataType());
        tempVar.setDefaultValue(ele.getDefaultValue());
        tempVar.setLength(ele.getLength());
        tempVar.setPrecision(ele.getPrecision());
        ColumnInfo colInfo = tempVar;
        return colInfo;
    }

    /**
     * 将单值udt转换为columnInfo
     */
    public static ColumnInfo convertSimpleDataTypeDefToColumnInfo(SimpleDataTypeDef sUdt) {
        ColumnInfo tempVar = new ColumnInfo();
        tempVar.setID(sUdt.getId());
        tempVar.setCode(sUdt.getCode());
        tempVar.setName(sUdt.getName());
        tempVar.setMDataType(sUdt.getMDataType());
        tempVar
                .setDefaultValue(sUdt.getDefaultValue() == null ? null : sUdt.getDefaultValue().toString());
        tempVar.setLength(sUdt.getLength());
        tempVar.setPrecision(sUdt.getPrecision());
        ColumnInfo colInfo = tempVar;
        return colInfo;
    }

    /**
     * 将多值udt转换为一个columnInfo
     */
    public static ColumnInfo convertComplexDataTypeDefToOneColumnInfo(ComplexDataTypeDef cUdt) {
        ColumnInfo tempVar = new ColumnInfo();
        tempVar.setID(cUdt.getId());
        tempVar.setCode(cUdt.getCode());
        tempVar.setName(cUdt.getName());
        ColumnInfo colInfo = tempVar;
        return colInfo;
    }

    /**
     * 转换校验规则列表
     */
    public static CommonValCollection convertToCommonValidations(ValidationCollection vallist,
                                                                 UdtTriggerTimePointType type) {
        CommonValCollection cValidtions = new CommonValCollection();
        if (vallist.size() > 0) {
            for (ValidationInfo item : vallist) {
                if (item.getTriggerTimePointType().contains(type)) {
                    cValidtions.add(convertToCommonValidation((ValidationInfo) item));
                }
            }
        }
        return cValidtions;
    }

    private static CommonValidation convertToCommonValidation(ValidationInfo validation) {
        CommonValidation cValidation = new CommonValidation();
        convertToCommonOperation(cValidation, validation);
        if (validation.getRequestElements().size() > 0) {
            for (String item : validation.getRequestElements()) {
                cValidation.getRequestElements().add(item);
            }
        }
        cValidation.setGetExecutingDataStatus(EnumSet.of(ExecutingDataStatus.None));
        return cValidation;
    }

    /**
     * commonOperation基类属性转换
     */
    private static void convertToCommonOperation(CommonOperation commonOp, ValidationInfo bizOp) {
        commonOp.setID(bizOp.getId());
        commonOp.setCode(bizOp.getCode());
        commonOp.setName(bizOp.getName());
        commonOp.setDescription(bizOp.getDescription());
        commonOp.setComponentId(bizOp.getCmpId());
        commonOp.setComponentName(bizOp.getCmpName());
        commonOp.setComponentPkgName(bizOp.getCmpPkgName());
        commonOp.setIsGenerateComponent(bizOp.getIsGenerateComponent());
    }


    public static String getDirectoryname(String fullPath) {

        return (new File(fullPath)).getParent();
    }

    public static String getFileNameWithoutExtension(String path) {
        File file = new File(path);
        return getFileNameWithoutExtension(file);
    }

    public static String getFileNameWithoutExtension(File path) {
        String fileName;
        if (path.isDirectory()) {
            fileName = path + " is  a directory";
            throw UdtModelException.createException(UdtModelErrorCodeEnum.GSP_BEMODEL_TEMPLATE_ERROR, fileName);
        } else {
            fileName = path.getName();
            int point = fileName.lastIndexOf(".");
            return point == -1 ? fileName : fileName.substring(0, point);
        }
    }
}