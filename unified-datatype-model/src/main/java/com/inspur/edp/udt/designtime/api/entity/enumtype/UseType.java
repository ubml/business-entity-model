package com.inspur.edp.udt.designtime.api.entity.enumtype;

/**
 * 属性使用方式
 */
public enum UseType {
    /**
     * 作为约束使用
     */
    AsConstraint(0),
    /**
     * 作为模板使用
     */
    AsTemplate(1);

    private final int intValue;
    private static java.util.HashMap<Integer, UseType> mappings;

    private synchronized static java.util.HashMap<Integer, UseType> getMappings() {
        if (mappings == null) {
            mappings = new java.util.HashMap<Integer, UseType>();
        }
        return mappings;
    }

    private UseType(int value) {
        intValue = value;
        UseType.getMappings().put(value, this);
    }

    public int getValue() {
        return intValue;
    }

    public static UseType forValue(int value) {
        return getMappings().get(value);
    }
}