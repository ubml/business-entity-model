package com.inspur.edp.udt.designtime.api.utils;

public class UdtThreadLocal {
    public static final ThreadLocal udtThreadLocal = new ThreadLocal();

    public static void set(Context user) {
        udtThreadLocal.set(user);
    }

    public static void unset() {
        udtThreadLocal.remove();
    }

    public static Context get() {
        return (Context) udtThreadLocal.get();
    }
}
