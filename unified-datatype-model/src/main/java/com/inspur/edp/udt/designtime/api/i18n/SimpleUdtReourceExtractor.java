package com.inspur.edp.udt.designtime.api.i18n;

import com.inspur.edp.cef.designtime.api.i18n.context.CefResourcePrefixInfo;
import com.inspur.edp.cef.designtime.api.i18n.context.ICefResourceExtractContext;
import com.inspur.edp.udt.designtime.api.entity.SimpleDataTypeDef;
import com.inspur.edp.udt.designtime.api.i18n.names.UdtResourceDescriptionNames;

public class SimpleUdtReourceExtractor extends BaseUdtResourceExtractor {

    static CefResourcePrefixInfo getParentResourcePrefixInfo(SimpleDataTypeDef udt) {
        CefResourcePrefixInfo info = new CefResourcePrefixInfo();
        info.setDescriptionPrefix(UdtResourceDescriptionNames.SimpleUnifiedDataType);
        info.setResourceKeyPrefix(udt.getDotnetAssemblyName());
        return info;
    }

    public SimpleUdtReourceExtractor(SimpleDataTypeDef sUdt, ICefResourceExtractContext context) {
        super(sUdt, context, getParentResourcePrefixInfo(sUdt));
    }

}
