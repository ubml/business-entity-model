package com.inspur.edp.udt.designtime.api.entity.validation;


public class ValidationCollection extends java.util.ArrayList<ValidationInfo> implements Cloneable {
    /**
     * 重写 Equals
     *
     * @param other
     * @return
     */
    public final boolean equals(ValidationCollection other) {
        if (other == null || size() != other.size()) {
            return false;
        }
        for (int i = 0; i < this.size(); i++) {
            if (this.get(i) != other.get(i)) {
                return false;
            }
        }
        return true;
    }

    ///// <summary>
    ///// 重写 GetHashCode
    ///// </summary>
    ///// <returns></returns>
    //public override int GetHashCode()
    //{
    //    return (Component != null ? Component.GetHashCode() : 0);
    //}

    /**
     * 克隆BE动作集合
     *
     * @param isGenerateId 是否生成Id
     * @return 返回动作集合
     */
    public final Object clone(boolean isGenerateId) {
        ValidationCollection col = new ValidationCollection();
        for (ValidationInfo item : this) {
            col.add((ValidationInfo) item.clone());
        }
        return col;
    }

    /**
     * 克隆BE动作集合
     *
     * @return 返回动作集合
     */
    public final Object clone() {
        return clone(false);
    }


    /**
     * 获取键值
     *
     * @param item
     * @return
     */
    protected final String getKeyForItem(ValidationInfo item) {
        return item.getCode();
    }


    public final ValidationInfo getItem(String id) {
        for (ValidationInfo op : this) {
            if (op.getId().equals(id)) {
                return op;
            }
        }
        return null;
    }
}