package com.inspur.edp.udt.designtime.api.exception;

import io.iec.edp.caf.commons.exception.CAFRuntimeException;
import io.iec.edp.caf.commons.exception.ExceptionLevel;

/**
 * UDT异常类
 *
 * @author sunhongfei
 * @className UdtModelException
 * @date 2023/9/14
 **/
public class UdtModelException extends CAFRuntimeException {
    private static final String SERVICE_UNIT_CODE = "pfcommon";
    private static final String RESOURCE_FILE = "business_entity_model_exception.properties";

    private UdtModelException(UdtModelErrorCodeEnum exceptionCode, String[] messageParams, Throwable innerException, ExceptionLevel level) {
        super(SERVICE_UNIT_CODE, RESOURCE_FILE, exceptionCode.name(), messageParams, innerException, level, exceptionCode.isBizException());
    }

    public static UdtModelException createException(UdtModelErrorCodeEnum exceptionCode) {
        return new UdtModelException(exceptionCode, null, null, ExceptionLevel.Error);
    }

    public static UdtModelException createException(UdtModelErrorCodeEnum exceptionCode, String... messageParams) {
        return new UdtModelException(exceptionCode, messageParams, null, ExceptionLevel.Error);
    }

    public static UdtModelException createException(UdtModelErrorCodeEnum exceptionCode, Throwable innerException) {
        return new UdtModelException(exceptionCode, null, innerException, ExceptionLevel.Error);
    }

    public static UdtModelException createException(UdtModelErrorCodeEnum exceptionCode, Throwable innerException, String... messageParams) {
        return new UdtModelException(exceptionCode, messageParams, innerException, ExceptionLevel.Error);
    }

    public static UdtModelException createException(UdtModelErrorCodeEnum exceptionCode, Throwable innerException, ExceptionLevel level, String... messageParams) {
        return new UdtModelException(exceptionCode, messageParams, innerException, level);
    }

}
