package com.inspur.edp.udt.designtime.api.entity.element;


import com.inspur.edp.cef.designtime.api.IFieldCollection;
import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.designtime.api.collection.GspFieldCollection;
import com.inspur.edp.cef.designtime.api.util.DataValidator;
import com.inspur.edp.udt.designtime.api.exception.UdtModelErrorCodeEnum;
import com.inspur.edp.udt.designtime.api.exception.UdtModelException;

/**
 * Udt字段集合类GspFieldCollection
 * //java.util.ArrayList<UdtElement>
 */
public class ElementCollection extends GspFieldCollection implements IFieldCollection, Cloneable {

    /**
     * 是否只读
     */
    private boolean privateIsReadOnly;

    public final boolean getIsReadOnly() {
        return privateIsReadOnly;
    }

    /**
     * 移动字段
     *
     * @param toIndex 目的索引为止
     * @param element 需要移动的字段
     */
    public final void move(int toIndex, UdtElement element) {
        if (contains(element)) {
            super.remove(element);
            super.add(toIndex, element);
        }
    }

    /**
     * 根据字段id获取字段
     */
    public final UdtElement getItem(String id) {
        for (IGspCommonField item : this) {

            if (item.getID().equals(id)) {
                return (UdtElement) item;

            }
        }
        return null;
        //return this.<UdtElement>FirstOrDefault(item => id.equals(item.ID));
    }

    /**
     * 添加一个新字段
     * <p>
     * // @param item 字段
     */
    @Override
    public final boolean add(IGspCommonField item) {
        UdtElement element = (UdtElement) ((item instanceof UdtElement) ? item : null);
        DataValidator.checkForNullReference(element, "element");
        return super.add(element);
    }


    /**
     * 是否包含某项
     *
     * @param item 待判断的项
     * @return 是否包含
     */
    public final boolean contains(IGspCommonField item) {
        UdtElement element = (UdtElement) ((item instanceof UdtElement) ? item : null);
        DataValidator.checkForNullReference(element, "element");
        return super.contains(element);
    }

    /**
     * 复制输入的字段列表到第arrayIndex个元素位置
     *
     * @param array      待复制的字段列表
     * @param arrayIndex 目标位置
     */
    public final void copyTo(IGspCommonField[] array, int arrayIndex) {
        for (int i = 0; i < array.length; i++) {
            super.add(arrayIndex + i, array[i]);
        }
    }

    /**
     * 删除某个元素
     *
     * @param item 待删除的元素
     * @return 是否删除成功
     */
    public final boolean remove(IGspCommonField item) {
        UdtElement element = (UdtElement) ((item instanceof UdtElement) ? item : null);
        DataValidator.checkForNullReference(element, "element");
        return super.remove(element);
    }

    /**
     * 克隆方法
     *
     * @return <see cref="object"/>
     */
    public final ElementCollection clone() {
        super.clone();
        ElementCollection newCollection = new ElementCollection();
        for (Object item : this) {
            Object tempVar = null;
            if (item instanceof UdtElement) {
                tempVar = ((UdtElement) (item)).clone();
            }
            newCollection.add((UdtElement) ((tempVar instanceof UdtElement) ? tempVar : null));
        }
        return newCollection;
    }

    /**
     * 迭代器
     *
     * @return 迭代器
     */
    public final java.util.Iterator<IGspCommonField> getEnumerator() {
        throw UdtModelException.createException(UdtModelErrorCodeEnum.GSP_BEMODEL_COMMON_0009, "ElementCollection", "getEnumerator");
//		UdtElement tempVar =this;
//		return ((java.util.ArrayList<UdtElement>)((tempVar instanceof java.util.ArrayList<UdtElement>) ? tempVar : null)).iterator();
    }

    /**
     * 获取某个元素的索引
     *
     * @param item 当前索引
     * @return 索引序号
     */
    public final int indexOf(IGspCommonField item) {
        UdtElement element = (UdtElement) ((item instanceof UdtElement) ? item : null);
        DataValidator.checkForNullReference(element, "element");
        return super.indexOf(element);
    }

    /**
     * 插入元素到index位置
     *
     * @param index 插入位置
     * @param item  待插入元素
     */
    public final void insert(int index, IGspCommonField item) {
        UdtElement element = (UdtElement) ((item instanceof UdtElement) ? item : null);
        DataValidator.checkForNullReference(element, "element");
        super.add(index, element);
    }

    /**
     * 访问索引为index的元素
     *
     * @param index 待访问元素索引
     * @return 索引为index的元素
     */
    public IGspCommonField getItem(int index) {
        return super.get(index);
    }

    public void setItem(int index, IGspCommonField value) {
        super.set(index, (UdtElement) value);
    }


}