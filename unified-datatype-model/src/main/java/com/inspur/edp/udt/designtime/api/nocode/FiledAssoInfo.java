package com.inspur.edp.udt.designtime.api.nocode;

import java.util.List;

public class FiledAssoInfo {
    public String getRefModelID() {
        return refModelID;
    }

    public void setRefModelID(String refModelID) {
        this.refModelID = refModelID;
    }

    public String getRefObjectID() {
        return refObjectID;
    }

    public void setRefObjectID(String refObjectID) {
        this.refObjectID = refObjectID;
    }

    public List<RefField> getElements() {
        return elements;
    }

    public void setElements(List<RefField> elements) {
        this.elements = elements;
    }

    private String refModelID;
    private String refObjectID;
    private List<RefField> elements;
}
