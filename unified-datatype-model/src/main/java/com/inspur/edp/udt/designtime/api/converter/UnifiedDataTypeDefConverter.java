package com.inspur.edp.udt.designtime.api.converter;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.inspur.edp.udt.designtime.api.entity.ComplexDataTypeDef;
import com.inspur.edp.udt.designtime.api.entity.SimpleDataTypeDef;
import com.inspur.edp.udt.designtime.api.entity.UnifiedDataTypeDef;
import com.inspur.edp.udt.designtime.api.exception.UdtModelErrorCodeEnum;
import com.inspur.edp.udt.designtime.api.exception.UdtModelException;
import com.inspur.edp.udt.designtime.api.utils.Context;
import com.inspur.edp.udt.designtime.api.utils.UdtThreadLocal;

import java.io.IOException;

public class UnifiedDataTypeDefConverter {
    String serializeResult = "{\n" +
            "    \"Type\": \"SimpleDataType\",\n" +
            "    \"Content\": {}}";
    String simpleUdtType = "SimpleDataType";
    String complexUdtType = "ComplexDataType";
    String para_Type = "Type";
    String para_Content = "Content";


    public String convertToDatabaseColumn(UnifiedDataTypeDef unifiedDataTypeDef) {
        Context context = new Context();
        context.setfull(false);
        UdtThreadLocal.set(context);
        ObjectMapper mapper = new ObjectMapper();
        try {
            JsonNode result = mapper.readTree(serializeResult);
            if (unifiedDataTypeDef.getClass().isAssignableFrom(SimpleDataTypeDef.class)) {
                String jsonResult = mapper.writeValueAsString((SimpleDataTypeDef) unifiedDataTypeDef);
                ((ObjectNode) result).put(para_Type, simpleUdtType);
                JsonNode sUdtJson = mapper.readTree(jsonResult);
                ((ObjectNode) result).set(this.para_Content, sUdtJson);
                return result.toString();
            } else if (unifiedDataTypeDef.getClass().isAssignableFrom(ComplexDataTypeDef.class)) {
                String jsonResult = mapper.writeValueAsString(unifiedDataTypeDef);
                ((ObjectNode) result).put(para_Type, complexUdtType);
                JsonNode cUdtJson = mapper.readTree(jsonResult);
                ((ObjectNode) result).set(this.para_Content, cUdtJson);
                return result.toString();
            } else {
                throw UdtModelException.createException(UdtModelErrorCodeEnum.GSP_BEMODEL_ENUM_NOTSUPPORT_0005, unifiedDataTypeDef.getClass().getTypeName());
            }
        } catch (IOException e) {
            throw UdtModelException.createException(UdtModelErrorCodeEnum.GSP_BEMODEL_JSON_0002, e, "UnifiedDataTypeDef", unifiedDataTypeDef.getName());
        }
    }

    public UnifiedDataTypeDef convertToEntityAttribute(String json) {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode jsonNode;
        try {
            jsonNode = mapper.readTree(json);
        } catch (IOException e) {
            throw UdtModelException.createException(UdtModelErrorCodeEnum.GSP_BEMODEL_JSON_0001, e, "UnifiedDataTypeDef");
        }
        String componentType = jsonNode.get(para_Type).textValue();
        String dataType = jsonNode.get(para_Content).toString();
        if (complexUdtType.equals(componentType)) {
            try {
                ComplexDataTypeDef complexDataTypeDef = mapper.readValue(dataType, ComplexDataTypeDef.class);
                complexDataTypeDef.updateColumnsInfo();
                return complexDataTypeDef;
            } catch (IOException e) {
                throw UdtModelException.createException(UdtModelErrorCodeEnum.GSP_BEMODEL_JSON_0001, e, "ComplexDataTypeDef");
            }
        }
        if (simpleUdtType.equals(componentType)) {
            try {
                SimpleDataTypeDef simpleDataTypeDef = mapper.readValue(dataType, SimpleDataTypeDef.class);
                simpleDataTypeDef.updateColumnsInfo();
                return simpleDataTypeDef;
            } catch (IOException e) {
                throw UdtModelException.createException(UdtModelErrorCodeEnum.GSP_BEMODEL_JSON_0001, e, "SimpleDataTypeDef");
            }
        }
        return null;
    }
}
