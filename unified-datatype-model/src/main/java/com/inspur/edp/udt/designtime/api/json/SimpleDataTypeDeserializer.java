package com.inspur.edp.udt.designtime.api.json;

import com.fasterxml.jackson.core.JsonParser;
import com.inspur.edp.cef.designtime.api.collection.GspAssociationCollection;
import com.inspur.edp.cef.designtime.api.collection.GspEnumValueCollection;
import com.inspur.edp.cef.designtime.api.element.EnumIndexType;
import com.inspur.edp.cef.designtime.api.element.GspElementDataType;
import com.inspur.edp.cef.designtime.api.element.GspElementObjectType;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.cef.designtime.api.json.element.GspEnumValueDeserializer;
import com.inspur.edp.udt.designtime.api.entity.SimpleDataTypeDef;
import com.inspur.edp.udt.designtime.api.entity.UnifiedDataTypeDef;
import com.inspur.edp.udt.designtime.api.exception.UdtModelErrorCodeEnum;
import com.inspur.edp.udt.designtime.api.exception.UdtModelException;


public class SimpleDataTypeDeserializer extends UdtDeserializer {

    @Override
    protected UnifiedDataTypeDef createUnifiedDataType() {
        return new SimpleDataTypeDef();
    }

    @Override
    protected void beforeUdtDeserializer(UnifiedDataTypeDef value) {
        SimpleDataTypeDef dataType = (SimpleDataTypeDef) value;
        dataType.setLength(0);
        dataType.setPrecision(0);
        dataType.setDefaultValue("");
        dataType.setIsUnique(false);
        dataType.setIsUnique(false);
        dataType.setObjectType(GspElementObjectType.None);
        dataType.setEnableRtrim(false);
        dataType.setChildAssociations(new GspAssociationCollection());
        dataType.setContainEnumValues(new GspEnumValueCollection());
    }

    protected void readBasicInfo(JsonParser jsonParser, UnifiedDataTypeDef value, String propertyName) {
        if (!(value instanceof SimpleDataTypeDef)) {
            throw UdtModelException.createException(UdtModelErrorCodeEnum.GSP_BEMODEL_ENUM_NOTSUPPORT_0005, value == null ? "Null" : value.getClass().getSimpleName());
        }
        SimpleDataTypeDef dataType = (SimpleDataTypeDef) value;
        switch (propertyName) {
            case UdtNames.DataType:
                dataType.setMDataType(SerializerUtils.readPropertyValue_Enum(jsonParser, GspElementDataType.class, GspElementDataType.values()));
                break;
            case UdtNames.Length:
                dataType.setLength(SerializerUtils.readPropertyValue_Integer(jsonParser));
                break;
            case UdtNames.Precision:
                dataType.setPrecision(SerializerUtils.readPropertyValue_Integer(jsonParser));
                break;
            case UdtNames.DefaultValue:
                dataType.setDefaultValue(SerializerUtils.readPropertyValue_String(jsonParser, null));
                break;
            case UdtNames.IsUnique:
                dataType.setIsUnique(SerializerUtils.readPropertyValue_boolean(jsonParser));
                break;
            case UdtNames.IsRequired:
                dataType.setIsRequired(SerializerUtils.readPropertyValue_boolean(jsonParser));
                break;
            case UdtNames.EnableRtrim:
                dataType.setEnableRtrim(SerializerUtils.readPropertyValue_boolean(jsonParser));
                break;
            case UdtNames.ObjectType:
                dataType.setObjectType(SerializerUtils.readPropertyValue_Enum(jsonParser, GspElementObjectType.class, GspElementObjectType.values()));
                break;
            case UdtNames.ContainEnumValues:
                readEnumValueList(jsonParser, dataType);
                break;
            case UdtNames.ChildAssociations:
                readAssociationCollection(jsonParser, dataType);
                break;
            case UdtNames.EnumIndexType:
                dataType.setEnumIndexType(SerializerUtils.readPropertyValue_Enum(jsonParser, EnumIndexType.class, EnumIndexType.values()));
                break;
            default:
                throw UdtModelException.createException(UdtModelErrorCodeEnum.GSP_BEMODEL_UDT_MODEL_0002, "SimpleDataTypeDef", propertyName);
        }

    }


    public static void readEnumValueList(JsonParser jsonParser, SimpleDataTypeDef dataType) {
        GspEnumValueCollection collection = new GspEnumValueCollection();
        GspEnumValueDeserializer der = new GspEnumValueDeserializer();
        SerializerUtils.readArray(jsonParser, der, collection);
        dataType.setContainEnumValues(collection);
    }

    private void readAssociationCollection(JsonParser jsonParser, SimpleDataTypeDef dataType) {
        UdtAssociationDeserializer converter = new UdtAssociationDeserializer(new UdtElementDeserializer());
        GspAssociationCollection collection = new GspAssociationCollection();
        SerializerUtils.readArray(jsonParser, converter, collection);
        dataType.setChildAssociations(collection);
    }
}
