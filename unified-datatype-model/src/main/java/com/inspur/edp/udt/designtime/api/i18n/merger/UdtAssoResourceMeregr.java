package com.inspur.edp.udt.designtime.api.i18n.merger;

import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.designtime.api.element.GspAssociation;
import com.inspur.edp.cef.designtime.api.i18n.context.ICefResourceMergeContext;
import com.inspur.edp.cef.designtime.api.i18n.merger.AssoRefFieldResourceMerger;
import com.inspur.edp.cef.designtime.api.i18n.merger.AssoResourceMerger;
import com.inspur.edp.udt.designtime.api.entity.element.UdtElement;

public class UdtAssoResourceMeregr extends AssoResourceMerger {

    public UdtAssoResourceMeregr(GspAssociation asso,
                                 ICefResourceMergeContext context) {
        super(asso, context);
    }

    @Override
    protected final AssoRefFieldResourceMerger getAssoRefFieldResourceMerger(
            ICefResourceMergeContext context, IGspCommonField field) {
        return new UdtAssoRefEleResourceMerger((UdtElement) field, context);
    }
}
