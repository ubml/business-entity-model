package com.inspur.edp.udt.designtime.api.i18n;

import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.designtime.api.i18n.context.CefResourcePrefixInfo;
import com.inspur.edp.cef.designtime.api.i18n.context.ICefResourceExtractContext;
import com.inspur.edp.cef.designtime.api.i18n.extractor.CefFieldResourceExtractor;
import com.inspur.edp.cef.designtime.api.i18n.extractor.DataTypeResourceExtractor;
import com.inspur.edp.udt.designtime.api.entity.UnifiedDataTypeDef;
import com.inspur.edp.udt.designtime.api.entity.element.UdtElement;

public class BaseUdtResourceExtractor extends DataTypeResourceExtractor {

    public BaseUdtResourceExtractor(
            UnifiedDataTypeDef commonDataType,
            ICefResourceExtractContext context,
            CefResourcePrefixInfo parentResourceInfo) {

        super(commonDataType, context, parentResourceInfo);
    }

    @Override
    protected final CefFieldResourceExtractor getCefFieldResourceExtractor(
            ICefResourceExtractContext context,
            CefResourcePrefixInfo objPrefixInfo,
            IGspCommonField field) {
        return new UdtEleResourceExtractor((UdtElement) field, context, objPrefixInfo);
    }
}
