package com.inspur.edp.udt.designtime.api.extension;

import com.inspur.edp.udt.designtime.api.exception.UdtModelErrorCodeEnum;
import com.inspur.edp.udt.designtime.api.exception.UdtModelException;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;

import java.util.ArrayList;
import java.util.List;

@PropertySource(value = {
        "application.yml"}, factory = io.iec.edp.caf.commons.event.config.CompositePropertySourceFactory.class)
@ConfigurationProperties(prefix = "udt-extension")
public class UdtExtensionConfigs {

    private static volatile UdtExtensionConfigs instance;

    public static UdtExtensionConfigs getInstance() {
        if (instance == null) {
            UdtExtensionConfigs rez = new UdtExtensionConfigs();
            UdtExtensionConfig udtExtensionConfig = new UdtExtensionConfig();
            udtExtensionConfig.setExtensiontype("Form");
            udtExtensionConfig.setDeserclass(
                    "com.inspur.edp.web.designschema.udtextensiondef.FormUdtExtensionDeserializer");
            udtExtensionConfig.setSerclass(
                    "com.inspur.edp.web.designschema.udtextensiondef.FormUdtExtensionSerializer");
            rez.getExtensionConfigs().add(udtExtensionConfig);
            instance = rez;
        }
        return instance;
    }

    private List<UdtExtensionConfig> extensionConfigs = new ArrayList<>();

    public List<UdtExtensionConfig> getExtensionConfigs() {
        return extensionConfigs;
    }

    public void setExtensionConfigs(List<UdtExtensionConfig> extensionConfigs) {
        this.extensionConfigs = extensionConfigs;
    }

    public UdtExtensionConfig getExtensionConfig(String type) {
        if (type == null || type.isEmpty()) {
            throw UdtModelException.createException(UdtModelErrorCodeEnum.GSP_BEMODEL_COMMON_0003, "type");
        }
        for (UdtExtensionConfig config : getExtensionConfigs()) {
            if (config.getExtensiontype().equals(type)) {
                return config;
            }
        }
        throw UdtModelException.createException(UdtModelErrorCodeEnum.GSP_BEMODEL_UDT_MODEL_0015, type, "UdtExtensionConfig");
    }
}
