package com.inspur.edp.udt.designtime.api.i18n.merger;

import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.designtime.api.i18n.context.ICefResourceMergeContext;
import com.inspur.edp.cef.designtime.api.i18n.merger.CefFieldResourceMerger;
import com.inspur.edp.cef.designtime.api.i18n.merger.DataTypeResourceMerger;
import com.inspur.edp.udt.designtime.api.entity.UnifiedDataTypeDef;
import com.inspur.edp.udt.designtime.api.entity.element.UdtElement;

public class BaseUdtResourceMerger extends DataTypeResourceMerger {

    public BaseUdtResourceMerger(
            UnifiedDataTypeDef commonDataType,
            ICefResourceMergeContext context) {
        super(commonDataType, context);
    }

    @Override
    protected CefFieldResourceMerger getCefFieldResourceMerger(
            ICefResourceMergeContext context, IGspCommonField field) {
        return new UdtEleResourceMerger((UdtElement) field, context);
    }
}
