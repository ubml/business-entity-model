package com.inspur.edp.udt.designtime.api.i18n.merger;

import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.designtime.api.element.GspAssociation;
import com.inspur.edp.cef.designtime.api.i18n.context.ICefResourceMergeContext;
import com.inspur.edp.cef.designtime.api.i18n.merger.AssoResourceMerger;
import com.inspur.edp.cef.designtime.api.i18n.merger.CefFieldResourceMerger;
import com.inspur.edp.udt.designtime.api.entity.element.UdtElement;

public class UdtEleResourceMerger extends CefFieldResourceMerger {

    public UdtEleResourceMerger(UdtElement element,
                                ICefResourceMergeContext context) {
        super(element, context);
    }

    protected void extractExtendProperties(IGspCommonField commonField) {
    }

    @Override
    protected AssoResourceMerger getAssoResourceMerger(
            ICefResourceMergeContext context, GspAssociation asso) {
        return new UdtAssoResourceMeregr(asso, context);
    }
}
