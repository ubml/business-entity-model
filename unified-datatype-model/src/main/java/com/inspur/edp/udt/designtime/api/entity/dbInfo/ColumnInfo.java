package com.inspur.edp.udt.designtime.api.entity.dbInfo;


import com.inspur.edp.cef.designtime.api.element.GspElementDataType;
import com.inspur.edp.cef.designtime.api.util.Guid;

public class ColumnInfo implements Cloneable {
    private String id = Guid.newGuid().toString();
    private String code = "";
    private String name = "";
    private GspElementDataType dataType = GspElementDataType.String;
    private String defaultValue = "";
    private int length;
    private int precision;

    public ColumnInfo() {
    }

    /**
     * 标识符
     */
    public String getID() {
        return id;
    }

    public void setID(String value) {
        id = value;
    }

    /**
     * 编号
     * <see cref="string"/>
     */
    public String getCode() {
        return code;
    }

    public void setCode(String value) {
        code = value;
    }

    /**
     * 名称
     * <see cref="string"/>
     */
    public String getName() {
        return name;
    }

    public void setName(String value) {
        name = value;
    }

    /**
     * 字段数据类型
     */
    public GspElementDataType getMDataType() {
        return dataType;
    }

    public void setMDataType(GspElementDataType value) {
        dataType = value;
    }

    /**
     * 默认值
     */
    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String value) {
        defaultValue = value;
    }

    /**
     * 长度
     */
    public int getLength() {
        return length;
    }

    public void setLength(int value) {
        length = value;
    }

    /**
     * 精度
     */
    public int getPrecision() {
        return precision;
    }

    public void setPrecision(int value) {
        precision = value;
    }

    /**
     * 克隆方法
     *
     * @return <see cref="object"/>
     */
    public final ColumnInfo clone() {
        ColumnInfo item = new ColumnInfo();
        ColumnInfo tempVar = item.clone();
        return (ColumnInfo) ((tempVar instanceof ColumnInfo) ? tempVar : null);
    }


}