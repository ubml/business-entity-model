package com.inspur.edp.udt.designtime.api.json;

import com.fasterxml.jackson.core.JsonParser;
import com.inspur.edp.cef.designtime.api.element.GspAssociation;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.cef.designtime.api.json.element.GspAssociationDeserializer;

public class UdtAssociationDeserializer extends GspAssociationDeserializer {

    public UdtAssociationDeserializer(UdtElementDeserializer udtElementDeserializer) {
        super(udtElementDeserializer);
    }

    @Override
    public boolean ReadExtendAssoProperty(GspAssociation asso, String propName, JsonParser jsonParser) {
        boolean result = true;

        GspAssociation element = asso;
        switch (propName) {
            case UdtNames.Id:
                element.setId(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case UdtNames.RefEntityId:
                element.setRefModelID(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case UdtNames.RefEntityPkgName:
                element.setRefModelPkgName(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case UdtNames.RefEntityCode:
                element.setRefModelCode(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case UdtNames.RefEntityName:
                element.setRefModelName(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            default:
                result = false;
        }
        return result;
    }
}