package com.inspur.edp.udt.designtime.api.nocode;

/**
 * @Author wangmaojian
 * @create 2023/4/14
 * 零代码实现修改helpid
 */
public interface IBusinessFieldCopyService {
    void updateBusinessField(BusinessField businessField);
}
