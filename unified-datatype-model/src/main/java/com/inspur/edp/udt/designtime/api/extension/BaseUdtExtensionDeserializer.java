package com.inspur.edp.udt.designtime.api.extension;

import com.fasterxml.jackson.databind.JsonDeserializer;

public abstract class BaseUdtExtensionDeserializer<T extends BaseUdtExtension> extends JsonDeserializer<T> {

}
