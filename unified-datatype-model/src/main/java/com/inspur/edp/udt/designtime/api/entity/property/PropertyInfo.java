package com.inspur.edp.udt.designtime.api.entity.property;

import com.inspur.edp.udt.designtime.api.entity.enumtype.UseType;
import com.inspur.edp.udt.designtime.api.exception.UdtModelErrorCodeEnum;
import com.inspur.edp.udt.designtime.api.exception.UdtModelException;

/**
 * 属性信息
 */
public class PropertyInfo implements Cloneable {
    /**
     * 属性名称
     * <see cref="string"/>
     */
    private String privatePropertyName;

    public final String getPropertyName() {
        return privatePropertyName;
    }

    public final void setPropertyName(String value) {
        privatePropertyName = value;
    }

    /**
     * 属性值
     * <see cref="object"/>
     */
    private Object privatePropertyValue;

    public final Object getPropertyValue() {
        return privatePropertyValue;
    }

    public final void setPropertyValue(Object value) {
        privatePropertyValue = value;
    }

    /**
     * 属性类型
     * <see cref="Type"/>
     */
    private Class privatePropertyType;

    public final Class getPropertyType() {
        return privatePropertyType;
    }

    public final void setPropertyType(Class value) {
        privatePropertyType = value;
    }

    /**
     * 属性使用方式
     * <see cref="UseType"/>
     */
    private UseType privatePropertyUseType = UseType.AsTemplate;

    public final UseType getPropertyUseType() {
        return privatePropertyUseType;
    }

    public final void setPropertyUseType(UseType value) {
        privatePropertyUseType = value;
    }

    /**
     * 是否可以编辑
     * <see cref="bool"/>
     */
    private boolean privateCanEdit = true;

    public final boolean getCanEdit() {
        return privateCanEdit;
    }

    public final void setCanEdit(boolean value) {
        privateCanEdit = value;
    }
    //= true;

    /**
     * 克隆
     *
     * @return
     */
    public final PropertyInfo clone() {
        Object tempVar = null;
        try {
            tempVar = super.clone();
        } catch (CloneNotSupportedException e) {
            throw UdtModelException.createException(UdtModelErrorCodeEnum.GSP_BEMODEL_CLONE_0001, e, "PropertyInfo");
        }
        return (PropertyInfo) ((tempVar instanceof PropertyInfo) ? tempVar : null);
    }
}