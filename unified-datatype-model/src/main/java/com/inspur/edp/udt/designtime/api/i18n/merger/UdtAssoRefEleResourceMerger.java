package com.inspur.edp.udt.designtime.api.i18n.merger;

import com.inspur.edp.cef.designtime.api.element.GspAssociation;
import com.inspur.edp.cef.designtime.api.i18n.context.ICefResourceMergeContext;
import com.inspur.edp.cef.designtime.api.i18n.merger.AssoRefFieldResourceMerger;
import com.inspur.edp.cef.designtime.api.i18n.merger.AssoResourceMerger;
import com.inspur.edp.udt.designtime.api.entity.element.UdtElement;

public class UdtAssoRefEleResourceMerger extends AssoRefFieldResourceMerger {

    public UdtAssoRefEleResourceMerger(UdtElement element,
                                       ICefResourceMergeContext context) {
        super(element, context);
    }

    @Override
    protected final AssoResourceMerger getAssoResourceMerger(
            ICefResourceMergeContext context, GspAssociation asso) {
        return new UdtAssoResourceMeregr(asso, context);
    }
}
