package com.inspur.edp.udt.designtime.api.entity.property;

import com.inspur.edp.cef.designtime.api.collection.BaseList;
import com.inspur.edp.udt.designtime.api.exception.UdtModelErrorCodeEnum;
import com.inspur.edp.udt.designtime.api.exception.UdtModelException;

/**
 * 属性集合
 */
public class PropertyCollection extends BaseList<PropertyInfo> implements Cloneable {
    public final boolean add(PropertyInfo info) {
        if (contains(info)) {
            throw UdtModelException.createException(UdtModelErrorCodeEnum.GSP_BEMODEL_UDT_MODEL_0012, info.getPropertyName());
        }
        return super.add(info);
    }

    /**
     * 获取关联外键
     */
    public PropertyInfo getIndex(int index) {
        return (PropertyInfo) ((this.get(index) instanceof PropertyInfo) ? this.get(index) : null);
    }

    public PropertyInfo getPropertyName(String propertyName) {
        return getPropertyInfo(propertyName);
    }

    private PropertyInfo getPropertyInfo(String name) {
        for (PropertyInfo propertyInfo : this) {
            if (propertyInfo == null) {
                continue;
            }
            if (propertyInfo.getPropertyName().equals(name)) {
                return propertyInfo;
            }

        }

        throw UdtModelException.createException(UdtModelErrorCodeEnum.GSP_BEMODEL_UDT_MODEL_0012, name);
    }

    public final boolean contains(PropertyInfo info) {
        for (PropertyInfo propertyInfo : this) {
            if (propertyInfo == null) {
                continue;
            }
            if (propertyInfo.getPropertyName().equals(info.getPropertyName())) {
                return true;
            }
        }
        return false;
    }

    public final boolean contains(String propertyName) {
        for (PropertyInfo item : this) {
            if (item.getPropertyName().equals(propertyName)) {
                return true;
            }
        }
        return false;
    }

    public final PropertyCollection clone() {
        PropertyCollection newCollection = new PropertyCollection();
        for (PropertyInfo propertyInfo : this) {
            newCollection.add(propertyInfo.clone());
        }

        return newCollection;
    }
}