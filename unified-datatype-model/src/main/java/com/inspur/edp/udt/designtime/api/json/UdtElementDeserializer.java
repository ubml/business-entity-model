package com.inspur.edp.udt.designtime.api.json;

import com.fasterxml.jackson.core.JsonParser;
import com.inspur.edp.cef.designtime.api.collection.GspAssociationCollection;
import com.inspur.edp.cef.designtime.api.collection.GspEnumValueCollection;
import com.inspur.edp.cef.designtime.api.element.EnumIndexType;
import com.inspur.edp.cef.designtime.api.element.GspElementDataType;
import com.inspur.edp.cef.designtime.api.entity.GspCommonField;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.cef.designtime.api.json.element.CefFieldDeserializer;
import com.inspur.edp.cef.designtime.api.json.element.GspEnumValueDeserializer;
import com.inspur.edp.udt.designtime.api.entity.ComplexDataTypeDef;
import com.inspur.edp.udt.designtime.api.entity.UnifiedDataTypeDef;
import com.inspur.edp.udt.designtime.api.entity.element.ElementCollection;
import com.inspur.edp.udt.designtime.api.entity.element.UdtElement;

public class UdtElementDeserializer extends CefFieldDeserializer {
    private UnifiedDataTypeDef udt;


    @Override
    protected void beforeCefElementDeserializer(GspCommonField field) {
        UdtElement element = (UdtElement) field;
        element.setRefElementId("");
    }

    @Override
    protected boolean readExtendFieldProperty(GspCommonField field, String propName, JsonParser jsonParser) {
        boolean result = true;

        UdtElement element = (UdtElement) field;

        //补充BelongObject信息
//        GetBelongObject(element);

        switch (propName) {
            case UdtNames.Id:
                element.setID(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case UdtNames.Code:
                element.setCode(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case UdtNames.Name:
                element.setName(SerializerUtils.readPropertyValue_String(jsonParser));
            case UdtNames.LabelId:
                element.setLabelID(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case UdtNames.RefElementId:
                element.setRefElementId(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case UdtNames.IsRef://boolean
                element.setIsRef(SerializerUtils.readPropertyValue_boolean(jsonParser));
                break;
            case UdtNames.IsRefElement://string
                element.setIsRefElement(SerializerUtils.readPropertyValue_boolean(jsonParser));
                break;
            case UdtNames.DataType:
                element.setMDataType(SerializerUtils.readPropertyValue_Enum(jsonParser, GspElementDataType.class, GspElementDataType.values()));
                break;
            case UdtNames.MDataType:
                SerializerUtils.readPropertyValue_Enum(jsonParser, GspElementDataType.class, GspElementDataType.values());
                break;
            case UdtNames.Length:
                element.setLength(SerializerUtils.readPropertyValue_Integer(jsonParser));
                break;
            case UdtNames.Precision:
                element.setPrecision(SerializerUtils.readPropertyValue_Integer(jsonParser));
                break;
            case UdtNames.DefaultValue:
                element.setDefaultValue(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case UdtNames.IsRequire:
                element.setIsRequire(SerializerUtils.readPropertyValue_boolean(jsonParser));
                break;
            case UdtNames.EnableRtrim:
                element.setEnableRtrim(SerializerUtils.readPropertyValue_boolean(jsonParser));
                break;
            case UdtNames.IsVirtual:
                element.setIsVirtual(SerializerUtils.readPropertyValue_boolean(jsonParser));
                break;
            case UdtNames.IsMultiLanguage:
                element.setIsMultiLanguage(SerializerUtils.readPropertyValue_boolean(jsonParser));
                break;
            case UdtNames.IsUdt:
                element.setIsUdt(SerializerUtils.readPropertyValue_boolean(jsonParser));
                break;
            case UdtNames.UdtID:
                element.setUdtID(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case UdtNames.UdtPkgName:
                element.setUdtPkgName(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case UdtNames.UdtName:
                element.setUdtName(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case UdtNames.ContainEnumValues:
                readEnumValueList(jsonParser, element);
                break;
            case UdtNames.ChildAssociations:
                readAssociationCollection(jsonParser, element);
                break;
            case UdtNames.EnumIndexType:
                element.setEnumIndexType(SerializerUtils.readPropertyValue_Enum(jsonParser, EnumIndexType.class, EnumIndexType.values()));
                break;
            default:
                result = false;
//                result = ReadExtendElementProperty(field, propName, jsonParser);
//                throw new RuntimeException(String.format("MappingInfoDeserializer未识别的属性名：%1$s", propName));
        }
        return result;
    }

    public static void readEnumValueList(JsonParser jsonParser, UdtElement element) {
        GspEnumValueCollection collection = new GspEnumValueCollection();
        GspEnumValueDeserializer der = new GspEnumValueDeserializer();
        SerializerUtils.readArray(jsonParser, der, collection);
        element.setContainEnumValues(collection);
    }

    private void readAssociationCollection(JsonParser jsonParser, UdtElement element) {
        UdtAssociationDeserializer converter = new UdtAssociationDeserializer(new UdtElementDeserializer());
        GspAssociationCollection collection = new GspAssociationCollection();
        SerializerUtils.readArray(jsonParser, converter, collection);
        element.setChildAssociations(collection);
    }
//    @Override
//    protected void readContainEnumValues(JsonParser jsonParser, GspCommonField field) {
//        UdtElement udtElement=(UdtElement)field;
//        GspEnumValueCollection collection = new GspEnumValueCollection();
//        GspEnumValueDeserializer deserializer = new GspEnumValueDeserializer();
//        SerializerUtils.readArray(jsonParser, deserializer, collection);
//        udtElement.setContainEnumValues(collection);
//    }

    private void getBelongObject(UdtElement ele) {
        if (ele.getBelongObject() == null) {
//            if (udt instanceof ComplexDataTypeDef cUdt)
//            if (udt instanceof ComplexDataTypeDef )
            if (udt instanceof ComplexDataTypeDef) {
                ele.setBelongObject(udt);
            }
        }
    }

    @Override
    protected UdtAssociationDeserializer CreateGspAssoDeserializer() {
//        GspAssociationDeserializer gad=new GspAssociationDeserializer(this);
        UdtAssociationDeserializer gad = new UdtAssociationDeserializer(new UdtElementDeserializer());
//        return super.CreateGspAssoDeserializer();
        return gad;
    }

    @Override
    protected UdtElement CreateField() {
        UdtElement ele = new UdtElement(new ComplexDataTypeDef().getPropertys());
        return ele;
//        return null;
    }

    @Override
    protected ElementCollection CreateFieldCollection() {
        ElementCollection elementCollection = new ElementCollection();

        return elementCollection;
    }

}
