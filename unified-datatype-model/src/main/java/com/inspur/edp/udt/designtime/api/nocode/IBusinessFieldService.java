package com.inspur.edp.udt.designtime.api.nocode;

import java.util.List;

public interface IBusinessFieldService {
    List<BusinessField> getBusinessFields();

    List<BusinessField> getBusinessFieldsByCategoryId(String categoryId);

    BusinessField getBusinessField(String id);

    void saveBusinessField(BusinessField businessField);
}
