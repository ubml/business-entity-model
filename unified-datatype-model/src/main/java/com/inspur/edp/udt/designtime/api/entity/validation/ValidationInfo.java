package com.inspur.edp.udt.designtime.api.entity.validation;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.inspur.edp.cef.designtime.api.util.Guid;
import com.inspur.edp.udt.designtime.api.exception.UdtModelErrorCodeEnum;
import com.inspur.edp.udt.designtime.api.exception.UdtModelException;
import com.inspur.edp.udt.designtime.api.json.TriggerTimePointTypeSerializer;
import com.inspur.edp.udt.designtime.api.json.UdtNames;

import java.util.EnumSet;

public class ValidationInfo implements Cloneable {
    public ValidationInfo() {
        setIsGenerateComponent(true);
    }

    /**
     * 物理主键
     */
    private String privateId;

    @JsonProperty(UdtNames.Id)
    public final String getId() {
        return privateId;
    }

    public final void setId(String value) {
        privateId = value;
    }

    /**
     * 业务主键
     */
    private String privateCode;

    @JsonProperty(UdtNames.Code)
    public final String getCode() {
        return privateCode;
    }

    public final void setCode(String value) {
        privateCode = value;
    }

    /**
     * 显示名称
     */
    private String privateName;

    @JsonProperty(UdtNames.Name)
    public final String getName() {
        return privateName;
    }

    public final void setName(String value) {
        privateName = value;
    }

    /**
     * 描述
     */
    private String privateDescription;

    @JsonProperty(UdtNames.Description)
    public final String getDescription() {
        return privateDescription;
    }

    public final void setDescription(String value) {
        privateDescription = value;
    }

    /**
     * 执行顺序
     */
    private int privateOrder;

    @JsonProperty(UdtNames.Order)
    public final int getOrder() {
        return privateOrder;
    }

    public final void setOrder(int value) {
        privateOrder = value;
    }

    /**
     * 构件ID
     */
    private String privateCmpId;

    @JsonProperty(UdtNames.CmpId)
    public final String getCmpId() {
        return privateCmpId;
    }

    public final void setCmpId(String value) {
        privateCmpId = value;
    }

    /**
     * 构件名称
     */
    private String privateCmpName;

    @JsonProperty(UdtNames.CmpName)
    public final String getCmpName() {
        return privateCmpName;
    }

    public final void setCmpName(String value) {
        privateCmpName = value;
    }

    /**
     * 触发时机
     */
    private EnumSet<UdtTriggerTimePointType> privateTriggerTimePointType = EnumSet.of(UdtTriggerTimePointType.None);

    @JsonProperty(UdtNames.TriggerTimePointType)
    @JsonSerialize(using = TriggerTimePointTypeSerializer.class)
    public final EnumSet<UdtTriggerTimePointType> getTriggerTimePointType() {
        return privateTriggerTimePointType;
    }

    public final void setTriggerTimePointType(EnumSet<UdtTriggerTimePointType> value) {
        privateTriggerTimePointType = value;
    }

    /**
     * 构件包名
     */
    private String privateCmpPkgName;

    @JsonProperty(UdtNames.CmpPkgName)
    public final String getCmpPkgName() {
        return privateCmpPkgName;
    }

    public final void setCmpPkgName(String value) {
        privateCmpPkgName = value;
    }

    private java.util.ArrayList<String> requestElements;

    /**
     * 触发字段
     */
    @JsonProperty(UdtNames.RequestElements)
    public final java.util.ArrayList<String> getRequestElements() {
        if (requestElements == null) {
            requestElements = new java.util.ArrayList<String>();
        }
        return requestElements;
    }

    //internal set => requestElements = value;
    private boolean privateIsGenerateComponent;

    @JsonProperty(UdtNames.IsGenerateComponent)
    public final boolean getIsGenerateComponent() {
        return privateIsGenerateComponent;
    }

    public final void setIsGenerateComponent(boolean value) {
        privateIsGenerateComponent = value;
    }

    /**
     * 克隆BE动作
     *
     * @return 返回当前动作的副本
     */
    public Object clone() {
        Object tempVar = null;
        try {
            tempVar = super.clone();

        } catch (CloneNotSupportedException e) {
            throw UdtModelException.createException(UdtModelErrorCodeEnum.GSP_BEMODEL_CLONE_0001, e, "ValidationInfo");
        }
        ValidationInfo result = null;
        if (tempVar instanceof ValidationInfo) {
            result = (ValidationInfo) tempVar;
            result.setId(Guid.newGuid().toString());
        }
        return result;
    }
}