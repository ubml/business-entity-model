package com.inspur.edp.udt.designtime.api.nocode;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.inspur.edp.udt.designtime.api.entity.UnifiedDataTypeDef;
import lombok.AllArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Date;
import java.util.Map;

/**
 * 零代码业务字段
 */
@Entity
@Table(name = "BusinessField")
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class BusinessField {
    public BusinessField() {
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getContent() {
        return Content;
    }

    public void setContent(String content) {
        Content = content;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public UnifiedDataTypeDef getUnifiedDataTypeDef() {
        return unifiedDataTypeDef;
    }

    public void setUnifiedDataTypeDef(UnifiedDataTypeDef unifiedDataTypeDef) {
        this.unifiedDataTypeDef = unifiedDataTypeDef;
    }

    @Id
    private String Id;
    private String Code;
    private String Name;
    private String categoryId;

    @Column(name = "Content")
    private String Content;
    private String Type;
//    private Date createOn;
//
//    public Date getCreateOn() {
//        return createOn;
//    }
//
//    public void setCreateOn(Date createOn) {
//        this.createOn = createOn;
//    }

    public Date getLastChangeOn() {
        return lastChangeOn;
    }

    public void setLastChangeOn(Date lastChangeOn) {
        this.lastChangeOn = lastChangeOn;
    }

    public String getLastChangeBy() {
        return lastChangeBy;
    }

    public void setLastChangeBy(String lastChangeBy) {
        this.lastChangeBy = lastChangeBy;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    private Date lastChangeOn;
    private String lastChangeBy;
    private String note;

    @Transient
    @JsonIgnore
    private UnifiedDataTypeDef unifiedDataTypeDef;

    public Map<String, String> getUdtExtensions() {
        return udtExtensions;
    }

    public void setUdtExtensions(Map<String, String> udtExtensions) {
        this.udtExtensions = udtExtensions;
    }

    //亚洲要传扩展信息
    @Transient
    private Map<String, String> udtExtensions;

    public FiledAssoInfo getAssoInfo() {
        return assoInfo;
    }

    public void setAssoInfo(FiledAssoInfo assoInfo) {
        this.assoInfo = assoInfo;
    }

    @Transient
    private FiledAssoInfo assoInfo;
}
