package com.inspur.edp.udt.designtime.manager;


import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.spi.MetadataContentManager;
import com.inspur.edp.udt.designtime.api.entity.SimpleDataTypeDef;
import io.iec.edp.caf.boot.context.CAFContext;

import java.util.Date;

public class UdtMetadataContentManager implements MetadataContentManager {

    public final void build(GspMetadata metadata) {
        String metadataID = metadata.getHeader().getId();
        String metadataName = metadata.getHeader().getName();
        String metadataCode = metadata.getHeader().getCode();
        String metadataAssembly = metadata.getHeader().getNameSpace();
        SimpleDataTypeDef sdt = initSimpleDataTypeDef(metadataID, metadataName, metadataCode,
                metadataAssembly);

        metadata.setContent(sdt);
    }

    //注意：Udt元数据初始化在Inspur.Gsp.Udt.WebApi.UdtController中
    private SimpleDataTypeDef initSimpleDataTypeDef(String metadataID, String metadataName,
                                                    String metadataCode, String metadataAssembly) {
        SimpleDataTypeDef sdt = new SimpleDataTypeDef();
        sdt.setId(metadataID);
        sdt.setCode(metadataCode);
        sdt.setName(metadataName);
        sdt.setCreatedDate(Date.from(CAFContext.current.getCurrentDateTime().toInstant()));
        sdt.setModifiedDate(Date.from(CAFContext.current.getCurrentDateTime().toInstant()));
        // 20190902-wj-恢复删除初始化时AssemblyName属性上的".udt." + udt.Code
        sdt.setDotnetAssemblyName(metadataAssembly + ".udt." + sdt.getCode());
        //sdt.AssemblyName = metadataAssembly;
        return sdt;
    }


}