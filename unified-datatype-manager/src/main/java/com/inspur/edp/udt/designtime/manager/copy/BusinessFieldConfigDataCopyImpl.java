package com.inspur.edp.udt.designtime.manager.copy;

import com.inspur.edp.metadata.rtcustomization.api.entity.ConfigData;
import com.inspur.edp.metadata.rtcustomization.api.entity.MimicParamKeyEnum;
import com.inspur.edp.metadata.rtcustomization.api.entity.MimicTypeEnum;
import com.inspur.edp.metadata.rtcustomization.context.MimicServiceContextHolder;
import com.inspur.edp.metadata.rtcustomization.spi.ConfigDataCopySpi;
import com.inspur.edp.udt.designtime.api.nocode.BusinessField;

import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * @Author wangmaojian
 * @create 2023/4/13
 */
public class BusinessFieldConfigDataCopyImpl implements ConfigDataCopySpi {
    @Override
    public void configDataCopy(Map<MimicParamKeyEnum, String> map, ConfigData configData) {
        if (!configData.getTableName().equals(BusinessFieldConstants.BusinessField_Table_Name)) {
            return;
        }
        if (configData.getEntityList() == null || configData.getEntityList().isEmpty()) {
            return;
        }
        // 2. 从paramMap中获取新轻应用的编号和名称
        String newBO_ID = map.get(MimicParamKeyEnum.BO_ID);
        String newAPP_Code = map.get(MimicParamKeyEnum.NEW_APP_CODE);
        String newAPP_Name = map.get(MimicParamKeyEnum.NEW_APP_NAME);

        // 3. 生成新的业务字段数据
        List<BusinessField> entityList = (List<BusinessField>) configData.getEntityList();
        for (BusinessField field : entityList) {
            String oldBusinessFieldId = field.getId();
            field.setId(UUID.randomUUID().toString());//新的业务字段ID
            field.setCode(newAPP_Code + field.getCode());
            field.setName(newAPP_Name + field.getName());
            //Udt和Udt扩展如何复制？？？？
            //保存ID Code Name的新旧值关系

            MimicServiceContextHolder.saveConfigData(BusinessFieldConstants.BusinessField_Table_Name + "_id_" + oldBusinessFieldId, field.getId());
        }
    }

    @Override
    public MimicTypeEnum getMimicTypeEnum() {
        return MimicTypeEnum.COMMON_APP_COPY;
    }
}
