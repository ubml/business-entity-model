package com.inspur.edp.udt.designtime.manager;


import com.inspur.edp.lcm.metadata.spi.event.MetadataEventArgs;
import com.inspur.edp.lcm.metadata.spi.event.MetadataEventListener;
import com.inspur.edp.udt.designtime.api.entity.UnifiedDataTypeDef;
import com.inspur.edp.udt.designtime.manager.i18nservice.UdtI18nService;
import com.inspur.edp.udt.designtime.manager.validate.UdtValidater;

public class UdtMetadataEventListener implements MetadataEventListener {

    public final void metadataSaving(MetadataEventArgs e) {
        // 类型判断
        if (!(e.getMetadata().getContent() instanceof UnifiedDataTypeDef)) {
            return;
        }
        UnifiedDataTypeDef udt = (UnifiedDataTypeDef) e.getMetadata().getContent();
        // 保存前校验
        new UdtValidater().validate(udt);

        //// 元数据依赖
        //      MetadataReferenceService.BuildMetadataReference(e.Metadata);
        //国际化抽取
        UdtI18nService service = new UdtI18nService();
        service.getResourceItem(e.getMetadata());
    }


    @Override
    public void fireMetadataSavingEvent(MetadataEventArgs metadataEventArgs) {
        metadataSaving(metadataEventArgs);
    }

    @Override
    public void fireMetadataSavedEvent(MetadataEventArgs metadataEventArgs) {

    }

    @Override
    public void fireMetadataDeletingEvent(MetadataEventArgs metadataEventArgs) {

    }

    @Override
    public void fireMetadataDeletedEvent(MetadataEventArgs metadataEventArgs) {

    }
}