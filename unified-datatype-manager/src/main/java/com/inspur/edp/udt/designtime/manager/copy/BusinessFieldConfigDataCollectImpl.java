package com.inspur.edp.udt.designtime.manager.copy;

import com.inspur.edp.metadata.rtcustomization.api.entity.ConfigData;
import com.inspur.edp.metadata.rtcustomization.api.entity.ConfigDataDependenciesEnum;
import com.inspur.edp.metadata.rtcustomization.api.entity.MimicParamKeyEnum;
import com.inspur.edp.metadata.rtcustomization.api.entity.MimicTypeEnum;
import com.inspur.edp.metadata.rtcustomization.context.MimicServiceContextHolder;
import com.inspur.edp.metadata.rtcustomization.spi.ConfigDataCollectSpi;
import com.inspur.edp.udt.designtime.api.nocode.BusinessField;
import com.inspur.edp.udt.designtime.api.nocode.IBusinessFieldService;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @Author wangmaojian
 * @create 2023/4/13
 */
public class BusinessFieldConfigDataCollectImpl implements ConfigDataCollectSpi {
    @Override
    public List<ConfigData> configDataCollect(Map<MimicParamKeyEnum, String> map) {
        List<String> businessFields = MimicServiceContextHolder.findConfigDataDependencies(ConfigDataDependenciesEnum.BUSINESS_FIELD);
        if (businessFields == null || businessFields.isEmpty()) {
            return new ArrayList<>();
        }
        IBusinessFieldService businessFieldService = SpringBeanUtils.getBean(IBusinessFieldService.class);
        //2. 获取业务字段
        List<BusinessField> businessFieldList = new ArrayList<>();
        for (String field : businessFields) {
            BusinessField businessField = businessFieldService.getBusinessField(field);
            businessFieldList.add(businessField);
        }

        //3. 构造ConfigData对象
        List<ConfigData> configDataList = new ArrayList<>();
        ConfigData configData = new ConfigData();
        configData.setTableName(BusinessFieldConstants.BusinessField_Table_Name);
        configData.setTargetClass(BusinessField.class.getName());
        configData.setEntityList(businessFieldList);
        configDataList.add(configData);
        return configDataList;
    }

    @Override
    public MimicTypeEnum getMimicTypeEnum() {
        return MimicTypeEnum.COMMON_APP_COPY;
    }
}
