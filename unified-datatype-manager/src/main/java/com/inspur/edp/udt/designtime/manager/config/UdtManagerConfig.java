package com.inspur.edp.udt.designtime.manager.config;

import com.inspur.edp.caf.cef.dt.spi.CommonStructureSchemaExtension;
import com.inspur.edp.udt.designtime.api.nocode.IBusinessFieldService;
import com.inspur.edp.udt.designtime.manager.commonstructure.UdtCommonStructureExtension;
import com.inspur.edp.udt.designtime.manager.repository.BusinessFieldRepository;
import com.inspur.edp.udt.designtime.manager.services.BusinessFieldService;
import io.iec.caf.data.jpa.repository.config.EnableCafJpaRepositories;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration("com.inspur.edp.udt.designtime.manager.config.UdtManagerConfig")
@EntityScan({"com.inspur.edp.udt.designtime.api.nocode"})
@EnableCafJpaRepositories({"com.inspur.edp.udt.designtime.manager.repository"})
public class UdtManagerConfig {
    @Bean("com.inspur.edp.udt.designtime.manager.config.UdtManagerConfig.UdtCommonStructureExtension")
    public CommonStructureSchemaExtension getBEComStructureSchemaExtension() {
        return new UdtCommonStructureExtension();
    }

    @Bean
    public IBusinessFieldService getBusinessFieldService(BusinessFieldRepository businessFieldRepository) {
        return new BusinessFieldService(businessFieldRepository);
    }
}
