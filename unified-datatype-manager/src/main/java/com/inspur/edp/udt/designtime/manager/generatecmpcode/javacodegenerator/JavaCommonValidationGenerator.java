package com.inspur.edp.udt.designtime.manager.generatecmpcode.javacodegenerator;


import com.inspur.edp.cef.designtime.api.operation.CommonOperation;
import com.inspur.edp.udt.designtime.api.entity.UnifiedDataTypeDef;
import com.inspur.edp.udt.designtime.manager.generatecomponent.JavaCompCodeNames;

public class JavaCommonValidationGenerator extends JavaBaseCommonCompCodeGenerator {

    @Override
    protected String getBaseClassName() {

        return JavaCompCodeNames.AbstractValidationClassName;
    }

    public JavaCommonValidationGenerator(UnifiedDataTypeDef udtDef, CommonOperation operation,
                                         String nameSpace, String path) {
        super(udtDef, operation, nameSpace, path);
    }

    @Override
    protected String getNameSpaceSuffix() {
        return JavaCompCodeNames.UDTValidationNameSpaceSuffix;
    }

    @Override
    protected void javaGenerateExtendUsing(StringBuilder result) {

        ///#region using
        result.append(getImportStr(JavaCompCodeNames.ValidationChangeset));
        result.append(getImportStr(JavaCompCodeNames.ValidationRuntimeSpiNameSpace));
        result.append(getImportStr(JavaCompCodeNames.ValidationApiNameSpace));
        result.append(getImportStr(JavaCompCodeNames.AbstractValidationNameSpace));
        result.append(getImportStr(JavaCompCodeNames.IValidationContextNameSpace));

        ///#endregion
    }

    @Override
    protected void javaGenerateConstructor(StringBuilder result) {
        result.append(getIndentationStr()).append(JavaCompCodeNames.KeywordPublic).append(" ")
                .append(getCompName()).append("(IValidationContext context, IChangeDetail change)")
                .append(" ").append("{").append(getNewline());

        result.append(getIndentationStr()).append(getIndentationStr()).append("super(context,change)")
                .append(";").append(getNewline());

        result.append(getIndentationStr()).append("}").append(getNewline());
    }

    @Override
    protected void javaGenerateExtendMethod(StringBuilder result) {
        result.append(getIndentationStr()).append(JavaCompCodeNames.KeywordPrivate).append(" ")
                .append(entityClassName).append(" ").append("getData()").append(" ").append("{")
                .append(getNewline());

        result.append(getIndentationStr()).append(getIndentationStr()).append("return").append(" ")
                .append("(").append(entityClassName).append(")").append("super.getContext().getData()")
                .append(";").append(getNewline());

        result.append(getIndentationStr()).append("}").append(getNewline());
    }

    /**
     * 获取构件名称
     */
    @Override
    protected String getInitializeCompName() {
        return String.format("%1$s%2$s", operation.getCode(), "Validation");
    }
}