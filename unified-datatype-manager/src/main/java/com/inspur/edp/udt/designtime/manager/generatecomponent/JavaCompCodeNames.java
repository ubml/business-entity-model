package com.inspur.edp.udt.designtime.manager.generatecomponent;

public final class JavaCompCodeNames {

    public static String UDTValidationNameSpaceSuffix = "UdtValidations";
    public static String UDTDeterminationNameSpaceSuffix = "UdtDeterminations";

    public static String ComponentExtensionName = ".cmp";
    public static String UDTVldControllerName = "UDTVldController";
    public static String UDTDtmControllerName = "UDTDtmController";

    public static String AbstractValidationClassName = "AbstractValidation";
    public static String AbstractDeterminationClassName = "AbstractDetermination";

    public static String KeywordPackage = "package";
    public static String KeywordExtends = "extends";
    public static String KeywordImport = "import";
    public static String KeywordPublic = "public";
    public static String KeywordProtected = "protected";
    public static String KeywordInternal = "internal";
    public static String KeywordPrivate = "private";
    public static String KeywordClass = "class";
    public static String KeywordOverride = "Override";
    public static String KeywordVoid = "void";

    public static String DeterminationSpiNameSpace = "com.inspur.edp.udt.spi.Determination.*";
    public static String DeterminationApiNameSpace = "com.inspur.edp.udt.api.Determination.*";
    public static String DeterminationChangeset = "com.inspur.edp.cef.entity.changeset.*";
    public static String AbstractDeterminationNameSpace = "com.inspur.edp.udt.spi.Determination.AbstractDetermination";
    public static String IDeterminationContextNameSpace = "com.inspur.edp.udt.api.Determination.IDeterminationContext";

    public static String ValidationChangeset = "com.inspur.edp.cef.entity.changeset.*";
    public static String ValidationRuntimeSpiNameSpace = "com.inspur.edp.udt.spi.Validation.*";
    public static String ValidationApiNameSpace = "com.inspur.edp.udt.api.Validation.*";
    public static String AbstractValidationNameSpace = "com.inspur.edp.udt.spi.Validation.AbstractValidation";
    public static String IValidationContextNameSpace = "com.inspur.edp.udt.api.Validation.IValidationContext";
    //public static string ValidationSystem = "System";
    //public static string ValidationGeneric = "System.Collections.Generic";
    //public static string ValidationText = "System.Text";


    public static String UDTValCmpExtendName = ".udtValCmp";
    public static String UDTDtmCmpExtendName = ".udtDtmCmp";

    public static final String UDTValidComponent = "UDTValidComponent";
    public static final String UDTDtmComponent = "UDTDtmComponent";
}