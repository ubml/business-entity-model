package com.inspur.edp.udt.designtime.manager.generatecmpcode;

import com.inspur.edp.cef.designtime.api.operation.CommonDetermination;
import com.inspur.edp.cef.designtime.api.operation.CommonOperation;
import com.inspur.edp.cef.designtime.api.operation.CommonValidation;
import com.inspur.edp.udt.designtime.api.entity.ComplexDataTypeDef;
import com.inspur.edp.udt.designtime.api.entity.UnifiedDataTypeDef;
import com.inspur.edp.udt.designtime.api.entity.validation.ValidationInfo;
import com.inspur.edp.udt.designtime.api.exception.UdtModelErrorCodeEnum;
import com.inspur.edp.udt.designtime.api.exception.UdtModelException;
import com.inspur.edp.udt.designtime.manager.generatecmpcode.javacodegenerator.JavaBaseCommonCompCodeGenerator;
import com.inspur.edp.udt.designtime.manager.generatecmpcode.javacodegenerator.JavaBaseCompCodeGenerator;
import com.inspur.edp.udt.designtime.manager.generatecmpcode.javacodegenerator.JavaCommonDeterminationGenerator;
import com.inspur.edp.udt.designtime.manager.generatecmpcode.javacodegenerator.JavaCommonValidationGenerator;
import com.inspur.edp.udt.designtime.manager.generatecmpcode.javacodegenerator.JavaValidationGenerator;

;

public final class JavaCmpCodeGeneratorFactory {

    public static JavaBaseCompCodeGenerator javaGetGenerator(UnifiedDataTypeDef udtDef,
                                                             ValidationInfo vldInfo, String nameSpace, String path) {
        return new JavaValidationGenerator(udtDef, vldInfo, nameSpace, path);
    }

    public static JavaBaseCommonCompCodeGenerator javaGetGenerator(UnifiedDataTypeDef udtDef,
                                                                   CommonOperation operation, String nameSpace, String path) {
        if (operation instanceof CommonDetermination) {
            return new JavaCommonDeterminationGenerator((ComplexDataTypeDef) udtDef, operation, nameSpace,
                    path);
        } else if (operation instanceof CommonValidation) {
            return new JavaCommonValidationGenerator(udtDef, operation, nameSpace, path);
        } else {
            throw UdtModelException.createException(UdtModelErrorCodeEnum.GSP_BEMODEL_UDT_MODEL_0007);
        }
    }
}