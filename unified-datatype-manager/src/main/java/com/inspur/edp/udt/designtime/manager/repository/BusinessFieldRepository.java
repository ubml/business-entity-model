package com.inspur.edp.udt.designtime.manager.repository;

import com.inspur.edp.udt.designtime.api.nocode.BusinessField;
import io.iec.caf.data.jpa.repository.CafJpaRepository;

import java.util.List;

public interface BusinessFieldRepository extends CafJpaRepository<BusinessField, String> {
    List<BusinessField> findByCategoryId(String categoryId);
}
