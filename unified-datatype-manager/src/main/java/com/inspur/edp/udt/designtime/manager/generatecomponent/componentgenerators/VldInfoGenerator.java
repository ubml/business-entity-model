package com.inspur.edp.udt.designtime.manager.generatecomponent.componentgenerators;


import com.inspur.edp.bef.component.base.GspComponent;
import com.inspur.edp.bef.component.base.VoidReturnType;
import com.inspur.edp.bef.component.detailcmpentity.udtvalidation.UDTValidComponent;
import com.inspur.edp.udt.designtime.api.entity.validation.ValidationInfo;
import com.inspur.edp.udt.designtime.api.utils.UdtUtils;
import com.inspur.edp.udt.designtime.manager.generatecomponent.ComponentClassNameGenerator;
import com.inspur.edp.udt.designtime.manager.generatecomponent.JavaCompCodeNames;

public class VldInfoGenerator extends BaseComponentGenerator {

    public static VldInfoGenerator getInstance() {
        return new VldInfoGenerator();
    }

    private VldInfoGenerator() {
    }

    @Override
    protected GspComponent buildComponent() {
        return new UDTValidComponent();
    }

    @Override
    protected void evaluateComponentInfo(GspComponent component, ValidationInfo vldInfo,
                                         GspComponent originalComponent) {
        this.originalComponent = originalComponent;
        // 基本信息（没有参数信息）
        evaluateComponentBasicInfo((UDTValidComponent) component, vldInfo);
        // 返回值信息
        if (component instanceof UDTValidComponent) {
            ((UDTValidComponent) component)
                    .getUdtValidMethod().setReturnValue(new VoidReturnType());
        }

    }

    private void evaluateComponentBasicInfo(UDTValidComponent component, ValidationInfo vldInfo) {
        if (!UdtUtils.checkNull(vldInfo.getCmpId())) {
            component.setComponentID(vldInfo.getCmpId());
        }
        component.setComponentCode(vldInfo.getCode());
        component.setComponentName(vldInfo.getCmpName());
        component.setComponentDescription(vldInfo.getDescription());
        component.getUdtValidMethod().setDotnetAssembly(this.assemblyName);

    String suffix = String.format("%1$s%2$s%3$s", this.udtDefCode, ".",
        JavaCompCodeNames.UDTValidationNameSpaceSuffix);
    String packageName = udtAssemblyName;
    packageName = javaModuleImportPackage(this.packagePrefix, packageName);
    packageName = String.format("%1$s%2$s", packageName, suffix.toLowerCase());
    if (this.originalComponent != null) {
      component.getUdtValidMethod()
          .setDotnetClassName(originalComponent.getMethod().getDotnetClassName());
      component.getMethod().setClassName(
          javaModuleClassName(originalComponent.getMethod().getDotnetClassName(), packageName));
    } else {
      String classNameSuffix = udtDefCode + vldInfo.getCode();
      component.getUdtValidMethod().setDotnetClassName(ComponentClassNameGenerator
          .generateUDTVldComponentClassName(this.nameSpace, classNameSuffix));
      component.getMethod().setClassName(javaModuleClassName(ComponentClassNameGenerator
          .generateUDTVldComponentClassName(this.nameSpace, classNameSuffix), packageName));
    }
  }
}