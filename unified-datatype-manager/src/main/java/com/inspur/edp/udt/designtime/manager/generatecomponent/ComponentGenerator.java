package com.inspur.edp.udt.designtime.manager.generatecomponent;


import com.inspur.edp.cef.designtime.api.collection.CommonDtmCollection;
import com.inspur.edp.cef.designtime.api.operation.CommonDetermination;
import com.inspur.edp.cef.designtime.api.util.DataValidator;
import com.inspur.edp.lcm.metadata.api.entity.MetadataProject;
import com.inspur.edp.lcm.metadata.api.service.MetadataProjectService;
import com.inspur.edp.udt.designtime.api.entity.ComplexDataTypeDef;
import com.inspur.edp.udt.designtime.api.entity.UnifiedDataTypeDef;
import com.inspur.edp.udt.designtime.api.entity.validation.ValidationInfo;
import com.inspur.edp.udt.designtime.api.utils.UdtUtils;
import com.inspur.edp.udt.designtime.manager.generatecomponent.componentgenerators.CommonDtmGenerator;
import com.inspur.edp.udt.designtime.manager.generatecomponent.componentgenerators.VldInfoGenerator;
import com.inspur.edp.udt.designtime.manager.generatecomponent.componentgenerators.CommonValGenerator;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

public class ComponentGenerator {

    public static ComponentGenerator getInstance() {
        return new ComponentGenerator();
    }

    private ComponentGenerator() {
    }

    /**
     * UnifiedDataTypeDef的编号
     */
    private String privateUnifiedDataTypeDefCode;

    private String getUnifiedDataTypeDefCode() {
        return privateUnifiedDataTypeDefCode;
    }

    private void setUnifiedDataTypeDefCode(String value) {
        privateUnifiedDataTypeDefCode = value;
    }

    /**
     * BE的程序集名称
     */
    private String privateAssemblyName;

    private String getAssemblyName() {
        return privateAssemblyName;
    }

    private void setAssemblyName(String value) {
        privateAssemblyName = value;
    }

    private String privateNameSpace;

    private String getNameSpace() {
        return privateNameSpace;
    }

    private void setNameSpace(String value) {
        privateNameSpace = value;
    }

    /**
     * 业务对象ID
     */
    private String privateBizObjectID;

    private String getBizObjectID() {
        return privateBizObjectID;
    }

    private void setBizObjectID(String value) {
        privateBizObjectID = value;
    }

    public final void generateComponent(UnifiedDataTypeDef udtDef, String path, String bizObjectID) {
        // 参数校验
        DataValidator.checkForNullReference(udtDef, "udtDef");
        DataValidator.checkForEmptyString(path, "path");

        MetadataProjectService projectService = SpringBeanUtils.getBean(MetadataProjectService.class);
        MetadataProject metadataProj = projectService.getMetadataProjInfo(path);
        setAssemblyName(metadataProj.getCsprojAssemblyName());
        setNameSpace(metadataProj.getNameSpace());
        DataValidator.checkForEmptyString(getAssemblyName(), "AssemblyName");

    setUnifiedDataTypeDefCode(udtDef.getCode());
    DataValidator.checkForEmptyString(getUnifiedDataTypeDefCode(), "BizEntityCode");
    setBizObjectID(bizObjectID);
    String cmpPath = ComponentGenUtil.prepareComponentDir(path);
    generateUDTVldComponents(udtDef, cmpPath);
    if (udtDef instanceof ComplexDataTypeDef) {
      generateUDTCommonDtmComponents((ComplexDataTypeDef) udtDef, cmpPath);
    }
  }

  private void generateUDTCommonDtmComponents(ComplexDataTypeDef cdtDef, String path) {
    generateUDTCommonDtms(cdtDef.getDtmBeforeSave(),path,cdtDef.getDotnetAssemblyName());
    generateUDTCommonDtms(cdtDef.getDtmAfterCreate(),path,cdtDef.getDotnetAssemblyName());
    generateUDTCommonDtms(cdtDef.getDtmAfterModify(),path,cdtDef.getDotnetAssemblyName());
  }

  /**
   * 生成联动计算构件
   * @param collection
   * @param path
   * @param dotNetAssemblyName
   */
  private void generateUDTCommonDtms(CommonDtmCollection collection, String path, String dotNetAssemblyName) {
    for (CommonDetermination dtmInfo : collection) {
      if (dtmInfo.getIsRef() || !dtmInfo.getIsGenerateComponent() && !UdtUtils
              .checkNull(dtmInfo.getComponentId())) {
        continue;
      }
      CommonDtmGenerator.getInstance()
              .generateComponent(dtmInfo, path, this.getUnifiedDataTypeDefCode(),
                      this.getAssemblyName(), getNameSpace(), this.getBizObjectID(),
                      dotNetAssemblyName);
    }
  }

    /**
     * 生成UDT的校验规则构件
     *
     * @param path 生成路径
     */
    private void generateUDTVldComponents(UnifiedDataTypeDef udtDef, String path) {
        for (ValidationInfo vldInfo : udtDef.getValidations()) {
            if (!vldInfo.getIsGenerateComponent() && !UdtUtils.checkNull(vldInfo.getCmpId())) {
                continue;
            }
            VldInfoGenerator.getInstance()
                    .GenerateComponent(vldInfo, path, this.getUnifiedDataTypeDefCode(),
                            this.getAssemblyName(), this.getNameSpace(), this.getBizObjectID(),
                            udtDef.getDotnetAssemblyName());
        }
    }
}